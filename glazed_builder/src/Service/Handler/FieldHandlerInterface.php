<?php

namespace Drupal\glazed_builder\Service\Handler;

use Drupal\Core\Asset\AttachedAssets;

interface FieldHandlerInterface {

  /**
   * Generate a node given it's node id
   *
   * @param array $node_id
   *   The nid of the node to load
   *
   * @param array $field_name
   *   The field machine name
   *
   * @return string
   *   The HTML of the retrieved node teaser
   */
  public function getField($node_id, $field_name);

}
