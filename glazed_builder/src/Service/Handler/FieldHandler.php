<?php

namespace Drupal\glazed_builder\Service\Handler;

use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;

class FieldHandler implements FieldHandlerInterface {

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer service
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;


  /**
   * Construct a BlockHandler entity
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager for nodes
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RendererInterface $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getField($node_id, $field_name) {
    $node = $this->entityTypeManager->getStorage('node')->load($node_id);

    if ($node && $node->access('view', $this->currentUser)) {
      if ($node->hasField($field_name) && !empty($node->get($field_name)->getValue())) {

        $field = $node->get($field_name)->view('default');
        $rendered = $this->renderer->renderRoot($field);

        if (is_string($rendered)) {
          return $rendered;
        }

        return $rendered->__toString();
      } elseif(!empty($field_name)) {
        return '<div class="alert alert-danger">' . t('Requested field @field_name not found, please contact site administrator.', array('@field_name' => $field_name)) . '</div>';
      }
      return;
    }
  }
}
