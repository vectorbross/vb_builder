<?php

namespace Drupal\glazed_builder\Service\Handler;

use Drupal\Core\Asset\AttachedAssets;

interface NodeHandlerInterface {

  /**
   * Generate a node given it's node id
   *
   * @param array $node_id
   *   The nid of the node to load
   *
   * @return string
   *   The HTML of the retrieved node teaser
   */
  public function getNode($node_id, AttachedAssets $assets);

  public function getNodeIds($search_term);

  public function getNodeLinks($search_term);
}
