<?php

namespace Drupal\glazed_builder\Service\Handler;

use Drupal\Core\Asset\AttachedAssets;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountProxyInterface;

class NodeHandler implements NodeHandlerInterface {

  /**
   * The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer service
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;


  /**
   * Construct a BlockHandler entity
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager for nodes
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, AccountProxyInterface $currentUser, RendererInterface $renderer) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public function getNode($node_id, AttachedAssets $assets) {
    $node = $this->entityTypeManager->getStorage('node')->load($node_id);
    if ($node && $node->access('view', $this->currentUser)) {

      $teaser = $this->entityTypeManager->getViewBuilder('node')->view($node, 'teaser');
      $rendered = $this->renderer->renderRoot($teaser);

      if (isset($teaser['#attached'], $teaser['#attached']['library']) && is_array($teaser['#attached']['library'])) {
        $assets->setLibraries($teaser['#attached']['library']);
      }

      if(isset($teaser['#attached'], $teaser['#attached']['drupalSettings'])) {
        $assets->setSettings($teaser['#attached']['drupalSettings']);
      }

      if (is_string($rendered)) {
        return $rendered;
      }

      return $rendered->__toString();
    }
  }


  /**
   * {@inheritdoc}
   */
  public function getNodeIds($search_term) {
    $nids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('title', '%' . $search_term . '%', 'like')
      ->execute();
    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    $response = array();

    foreach($nodes as $index => $node) {
      if($node->access('view', $this->currentUser)) {
        $response[$index]['value'] = $node->getTitle();
        $response[$index]['id'] = $node->id();
      }
    }
    return $response;
  }


  /**
   * {@inheritdoc}
   */
  public function getNodeLinks($search_term) {
    $nids = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('title', '%' . $search_term . '%', 'like')
      ->execute();
    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    $response = array();

    foreach($nodes as $index => $node) {
      if($node->access('view', $this->currentUser)) {
        $response[$index]['value'] = $node->getTitle();
        $response[$index]['id'] = $node->toUrl()->toString();
      }
    }
    return $response;
  }
}
