/*! glazed_builder 26-09-2019 */

! function(b) {
    if (b.ajaxPrefilter(function(e, t, a) {
            "script" != e.dataType && "script" != t.dataType || (e.cache = !0)
        }), !drupalSettings.glazedBuilder.glazedBaseUrl)
        if (0 < b('script[src*="glazed_builder.js"]').length) {
            var e = b('script[src*="glazed_builder.js"]').attr("src");
            drupalSettings.glazedBuilder.glazedBaseUrl = e.slice(0, e.indexOf("glazed_builder.js"))
        } else if (0 < b('script[src*="glazed_builder.min.js"]').length) {
        e = b('script[src*="glazed_builder.min.js"]').attr("src");
        drupalSettings.glazedBuilder.glazedBaseUrl = e.slice(0, e.indexOf("glazed_builder.min.js"))
    }

    function a(e) {
        return "Drupal" in window ? Drupal.t(e) : e
    }

    function r(e) {
        return "glazed_title" in window.glazedBuilder && e in window.glazedBuilder.glazed_title ? window.glazedBuilder.glazed_title[e] : a(e)
    }

    function i(e, t) {
        function a() {}
        a.prototype = t.prototype, e.prototype = new a, (e.prototype.constructor = e).baseclass = t
    }

    function f(e, t) {
        var a = {};
        for (var n in t) void 0 !== a[n] && a[n] == t[n] || (e[n] = t[n]);
        if (document.all && !document.isOpera) {
            var i = t.toString;
            "function" == typeof i && i != e.toString && i != a.toString && "\nfunction toString() {\n  [native code]\n}\n" != i && (e.toString = t.toString)
        }
        return e
    }

    function d(e, t) {
        var a = "col-" + t + "-",
            n = e ? e.split("/") : [1, 1],
            i = _.range(1, 13),
            o = !_.isUndefined(n[0]) && 0 <= _.indexOf(i, parseInt(n[0], 10)) && parseInt(n[0], 10),
            l = !_.isUndefined(n[1]) && 0 <= _.indexOf(i, parseInt(n[1], 10)) && parseInt(n[1], 10);
        return !1 !== o && !1 !== l ? a + 12 * o / l : a + "12"
    }
    "glazed_online" in window.glazedBuilder || (window.glazedBuilder.glazed_online = "http:" == window.location.protocol || "https:" == window.location.protocol), window.glazedBuilder.glazed_backend = !0, window.glazedBuilder.glazed_title = {
        "Drag and drop": Drupal.t("Drag and drop element."),
        Add: Drupal.t("Add new element into current element area."),
        Edit: Drupal.t("Open settings form to change element properties, set CSS styles and add CSS classes."),
        Paste: Drupal.t("Paste elements into current element area from clipboard copied into it before."),
        Copy: Drupal.t("Copy element or contained elements to clipboard."),
        Clone: Drupal.t("Clone current element."),
        Remove: Drupal.t("Delete current element"),
        "Save as template": Drupal.t("Save element or contained elements as template to template library."),
        "Save container": Drupal.t("Save to server all elements which placed in current container element.")
    }, drupalSettings.glazedBuilder.glazedEditor || (drupalSettings.glazedBuilder.glazedEditor = !1), window.glazedBuilder.glazed_frontend = !1, window.glazedBuilder.glazed_js_waiting_callbacks = {}, window.glazedBuilder.glazed_loaded_js = {}, window.glazedBuilder.glazedElements = {}, window.glazedBuilder.glazed_containers = [], window.glazedBuilder.glazed_containers_loaded = {}, window.glazedBuilder.glazed_edited = {}, window.glazedBuilder.glazed_animations = {
        "": Drupal.t("No animation"),
        bounce: Drupal.t("bounce"),
        float: Drupal.t("float"),
        floatSmall: Drupal.t("floatSmall"),
        floatLarge: Drupal.t("floatLarge"),
        pulse: Drupal.t("pulse"),
        shake: Drupal.t("shake"),
        wobble: Drupal.t("wobble"),
        jello: Drupal.t("jello"),
        fadeIn: Drupal.t("fadeIn"),
        fadeInDown: Drupal.t("fadeInDown"),
        fadeInDownBig: Drupal.t("fadeInDownBig"),
        fadeInLeft: Drupal.t("fadeInLeft"),
        fadeInLeftBig: Drupal.t("fadeInLeftBig"),
        fadeInRight: Drupal.t("fadeInRight"),
        fadeInRightBig: Drupal.t("fadeInRightBig"),
        fadeInUp: Drupal.t("fadeInUp"),
        fadeInUpBig: Drupal.t("fadeInUpBig"),
        fadeOut: Drupal.t("fadeOut"),
        fadeOutDown: Drupal.t("fadeOutDown"),
        fadeOutDownBig: Drupal.t("fadeOutDownBig"),
        fadeOutLeft: Drupal.t("fadeOutLeft"),
        fadeOutLeftBig: Drupal.t("fadeOutLeftBig"),
        fadeOutRight: Drupal.t("fadeOutRight"),
        fadeOutRightBig: Drupal.t("fadeOutRightBig"),
        fadeOutUp: Drupal.t("fadeOutUp"),
        fadeOutUpBig: Drupal.t("fadeOutUpBig"),
        flipInX: Drupal.t("flipInX"),
        flipInY: Drupal.t("flipInY"),
        zoomIn: Drupal.t("zoomIn"),
        zoomInDown: Drupal.t("zoomInDown"),
        zoomInLeft: Drupal.t("zoomInLeft"),
        zoomInRight: Drupal.t("zoomInRight"),
        zoomInUp: Drupal.t("zoomInUp"),
        zoomOut: Drupal.t("zoomOut"),
        zoomOutDown: Drupal.t("zoomOutDown"),
        zoomOutLeft: Drupal.t("zoomOutLeft"),
        zoomOutRight: Drupal.t("zoomOutRight"),
        zoomOutUp: Drupal.t("zoomOutUp"),
        slideInDown: Drupal.t("slideInDown"),
        slideInLeft: Drupal.t("slideInLeft"),
        slideInRight: Drupal.t("slideInRight"),
        slideInUp: Drupal.t("slideInUp"),
        slideOutDown: Drupal.t("slideOutDown"),
        slideOutLeft: Drupal.t("slideOutLeft"),
        slideOutRight: Drupal.t("slideOutRight"),
        slideOutUp: Drupal.t("slideOutUp")
    };
    _.memoize(function(e) {
        return new RegExp("(\\[(\\[?)[" + e + "]+(?![\\w-])[^\\]\\/]*[\\/(?!\\])[^\\]\\/]*]?(?:\\/]\\]|\\](?:[^\\[]*(?:\\[(?!\\/" + e + "\\])[^\\[]*)*\\[\\/" + e + "\\])?)\\]?)", "g")
    });
    var p = _.memoize(function(e) {
        return new RegExp("\\[(\\[?)(" + e + ")(?![\\w-])([^\\]\\/]*(?:\\/(?!\\])[^\\]\\/]*)*?)(?:(\\/)\\]|\\](?:([^\\[]*(?:\\[(?!\\/\\2\\])[^\\[]*)*)(\\[\\/\\2\\]))?)(\\]?)")
    });

    function o(e) {
        return _.isString(e) ? e.replace(/(\`{2})/g, '"') : e
    }

    function l(e) {
        return decodeURIComponent((e + "").replace(/%(?![\da-f]{2})/gi, function() {
            return "%25"
        }))
    }

    function c(e) {
        var t = function(e) {
            var t = parseInt(b(e).css("z-index"));
            return b(e).parent().find("*").each(function() {
                var e = parseInt(b(this).css("z-index"));
                t < e && (t = e)
            }), t
        }(e);
        b(e).css("z-index", t + 1)
    }

    function s(e, t) {
        var a = "<select>";
        for (var n in e) a = a + '<option value="' + n + '">"' + e[n] + '"</option>';
        a += "</select>", b(t).css("display", "none");
        var i = b(a).insertAfter(t);
        if (b(t).val().length) {
            b(i).append('<option value=""></option>');
            var o = b(t).val();
            b(i).find('option[value="' + o + '"]').length || b(i).append('<option value="' + o + '">"' + o + '"</option>'), b(i).find('option[value="' + o + '"]').attr("selected", "selected")
        } else b(i).append('<option value="" selected></option>');
        return b(i).chosen({
            search_contains: !0,
            allow_single_deselect: !0
        }), b(i).change(function() {
            b(this).find("option:selected").each(function() {
                b(t).val(b(this).val())
            })
        }), b(i).parent().find(".chosen-container").width("100%"), b('<div><a class="direct-input" href="#">' + Drupal.t("Edit as text") + "</a></div>").insertBefore(i).click(function() {
            b(t).css("display", "block"), b(i).parent().find(".chosen-container").remove(), b(i).remove(), b(this).remove()
        }), i
    }

    function h(t, a) {
        b.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            b.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_get_container_names",
                    container_type: t,
                    url: window.location.href
                },
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                a(e)
            })
        })
    }

    function n(i, o, t, l) {
        type = i.split("/")[0], name = i.split("/")[1], null == o && (o = i), b.ajax({
            type: "GET",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            b.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_save_container",
                    type: type,
                    name: name,
                    lang: l,
                    shortcode: function(e) {
                        var t = "";
                        for (y = 0; y < e.length; y++) {
                            var a = 7 ^ e.charCodeAt(y);
                            t += String.fromCharCode(a)
                        }
                        return t
                    }(encodeURIComponent(t))
                },
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                var t = window.drupalSettings.defaultLangCode,
                    a = window.drupalSettings.currentLanguage,
                    n = Drupal.t("Saved field") + " " + decodeURIComponent(escape(o));
                0 < l.length && l !== t && (n += " to " + a + " translation"), b.notify(n, {
                    type: "success",
                    z_index: "8000",
                    offset: {
                        x: 25,
                        y: 70
                    }
                }), i in window.glazedBuilder.glazed_edited && delete window.glazedBuilder.glazed_edited[i]
            }).fail(function(e) {
                var t = Drupal.t("Server error: Unable to save field") + " " + decodeURIComponent(escape(o));
                b.notify(t, {
                    type: "danger",
                    z_index: "8000",
                    offset: {
                        x: 25,
                        y: 70
                    }
                })
            })
        }).fail(function(e) {
            var t = Drupal.t("Server error: Unable to save field") + " " + decodeURIComponent(escape(o)) + ". Debug information: unable to obtain csrf token.";
            b.notify(t, {
                type: "danger",
                z_index: "8000",
                offset: {
                    x: 25,
                    y: 70
                }
            })
        })
    }

    function u(t, a, n, i, o) {
        b.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            e.originalPath = drupalSettings.glazedBuilder.currentPath, b.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_builder_load_cms_element",
                    name: t,
                    settings: a,
                    container: n,
                    data: i
                },
                dataType: "json",
                cache: !drupalSettings.glazedBuilder.glazedEditor
            }).done(function(e) {
                b(e.css).appendTo(b("head")), b(e.js).appendTo(b("head")), b.extend(!0, drupalSettings, e.settings), o(e.data)
            })
        })
    }

    function v(t, a) {
        b.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            b.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_load_template",
                    url: window.location.href,
                    name: t
                },
                cache: !1
            }).done(function(e) {
                a(e)
            }).fail(function() {
                a("")
            })
        })
    }

    function z(t) {
        b.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            b.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_delete_template",
                    url: window.location.href,
                    name: t
                },
                cache: !1,
                context: this
            }).done(function(e) {})
        })
    }

    function m(t, a) {
        b.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            b.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_load_page_template",
                    url: window.location.href,
                    uuid: t
                },
                cache: !1
            }).done(function(e) {
                a(e)
            }).fail(function() {
                a("")
            })
        })
    }
    b.fn.closest_descendents = function(e) {
        for (var t = b(), a = this; a.length;) t = b.merge(t, a.filter(e)), a = (a = a.not(e)).children();
        return t
    }, b.fn.deserialize = function(e) {
        var n = b(this),
            i = {};
        return b.each(e.split("&"), function() {
            var e = this.split("="),
                t = l(e[0]),
                a = 1 < e.length ? l(e[1]) : null;
            t in i || (i[t] = []), i[t].push(a)
        }), b.each(i, function(e, t) {
            n.find("[name='" + e + "']").val(t[0].replace(/\+/g, " ")), "on" == t[0].replace(/\+/g, " ") && n.find("[type='checkbox'][name='" + e + "']").attr("checked", "checked"), n.find("select[name='" + e + "'] > option").removeAttr("selected");
            for (var a = 0; a < t.length; a++) n.find("select[name='" + e + "'] > option[value='" + t[a] + "']").prop("selected", "selected")
        }), b("input:checkbox:checked,input:radio:checked", n).each(function() {
            b(this).attr("name") in i || (this.checked = !1)
        }), this
    }, window.glazedBuilder.btoaUTF16 = function(e) {
        if ("undefined" == typeof TextEncoder) return alert(a("Your browser does not support encoding special characters, please upgrade to a modern browser.")), btoa(e);
        var t = new("undefined" == typeof TextEncoder ? TextEncoderLite : TextEncoder)("utf-8").encode(e);
        return base64js.fromByteArray(t)
    }, window.glazedBuilder.atobUTF16 = function(e) {
        if ("undefined" == typeof TextEncoder) return alert(a("Your browser does not support encoding special characters, please upgrade to a modern browser.")), atob(e);
        var t = base64js.toByteArray(e);
        return new("undefined" == typeof TextDecoder ? TextDecoderLite : TextDecoder)("utf-8").decode(t)
    }, window.glazedBuilder.glazed_add_css = function(e, t) {
        var a = drupalSettings.glazedBuilder.glazedBaseUrl + e;
        if (b('link[href*="' + a + '"]').length || "glazed_exported" in window.glazedBuilder) t();
        else {
            var n = document.getElementsByTagName("head")[0],
                i = document.createElement("link");
            i.rel = "stylesheet", i.type = "text/css", i.href = a, i.onload = t, n.appendChild(i)
        }
    }, window.glazedBuilder.glazed_add_js_list = function(e) {
        if ("loaded" in e && e.loaded) e.callback();
        else
            for (var t = 0, a = 0; a < e.paths.length; a++) glazedBuilder.glazed_add_js({
                path: e.paths[a],
                callback: function() {
                    ++t == e.paths.length && e.callback()
                }
            })
    }, window.glazedBuilder.glazed_add_js = function(e) {
        "loaded" in e && e.loaded || "glazed_exported" in window.glazedBuilder ? e.callback() : glazedBuilder.glazed_add_external_js(drupalSettings.glazedBuilder.glazedBaseUrl + e.path, "callback" in e ? e.callback : function() {})
    }, window.glazedBuilder.glazed_add_external_js = function(a, e) {
        if (a in window.glazedBuilder.glazed_js_waiting_callbacks) window.glazedBuilder.glazed_js_waiting_callbacks[a].push(e);
        else if (a in window.glazedBuilder.glazed_loaded_js) e();
        else {
            window.glazedBuilder.glazed_js_waiting_callbacks[a] = [e];
            var t = document.getElementsByTagName("head")[0],
                n = document.createElement("script");
            n.type = "text/javascript", n.src = a, n.onload = function() {
                for (window.glazedBuilder.glazed_loaded_js[a] = !0; a in window.glazedBuilder.glazed_js_waiting_callbacks;) {
                    var e = window.glazedBuilder.glazed_js_waiting_callbacks[a];
                    window.glazedBuilder.glazed_js_waiting_callbacks[a] = void 0, delete window.glazedBuilder.glazed_js_waiting_callbacks[a];
                    for (var t = 0; t < e.length; t++) e[t]()
                }
            }, t.appendChild(n)
        }
    }, window.glazedBuilder.glazed_builder_set_ckeditor_config = function(e) {
        CKEDITOR.disableAutoInline = !0, CKEDITOR.config.fillEmptyBlocks = !1, CKEDITOR.config.allowedContent = !0, CKEDITOR.config.autoParagraph = !1, CKEDITOR.config.contentsCss = ["//cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css"], void 0 !== window.drupalSettings && void 0 !== window.drupalSettings.glazed && void 0 !== window.drupalSettings.glazed.glazedPath.length && CKEDITOR.config.contentsCss.push(drupalSettings.basePath + window.drupalSettings.glazed.glazedPath + "css/glazed.css"), void 0 !== drupalSettings.glazedBuilder.cke_stylesset ? CKEDITOR.config.stylesSet = drupalSettings.glazedBuilder.cke_stylesset : CKEDITOR.config.stylesSet = [{
            name: "Lead",
            element: "p",
            attributes: {
                class: "lead"
            }
        }, {
            name: "Muted",
            element: "p",
            attributes: {
                class: "text-muted"
            }
        }, {
            name: "Highlighted",
            element: "mark"
        }, {
            name: "Small",
            element: "small"
        }, {
            name: "Button Primary",
            element: "div",
            attributes: {
                class: "btn btn-primary"
            }
        }, {
            name: "Button Default",
            element: "div",
            attributes: {
                class: "btn btn-default"
            }
        }], void 0 !== drupalSettings.glazedBuilder.cke_fonts ? CKEDITOR.config.font_names = drupalSettings.glazedBuilder.cke_fonts : CKEDITOR.config.font_names = "Arial/Arial, Helvetica, sans-serif;Georgia/Georgia, serif;Times New Roman/Times New Roman, Times, serif;Verdana/Verdana, Geneva, sans-serif", CKEDITOR.config.fontSize_sizes = "8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;60/60px;72/72px;90/90px;117/117px;144/144px", CKEDITOR.config.protectedSource.push(/<link.*?>/gi);
        var t = [];
        for (var a in window.drupalSettings.glazedBuilder.palette) t.push(window.drupalSettings.glazedBuilder.palette[a].substring(1));
        var n = t.join(",") + ",";
        CKEDITOR.config.hasOwnProperty("colorButton_colors") && CKEDITOR.config.colorButton_colors.indexOf(n) < 0 && (CKEDITOR.config.colorButton_colors = n + CKEDITOR.config.colorButton_colors), "inline" == e ? "profile" in window.drupalSettings.glazedBuilder ? CKEDITOR.config.toolbar = window.drupalSettings.glazedBuilder.profile.ck_config.inline : CKEDITOR.config.toolbar = [{
            name: "basicstyles",
            items: ["Bold", "Italic", "RemoveFormat"]
        }, {
            name: "colors",
            items: ["TextColor"]
        }, {
            name: "styles",
            items: ["Format", "Styles", "FontSize"]
        }, {
            name: "paragraph",
            items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "BulletedList", "NumberedList"]
        }, {
            name: "links",
            items: ["Link", "Unlink"]
        }, {
            name: "insert",
            items: ["Image", "Table"]
        }, {
            name: "clipboard",
            items: ["Undo", "Redo"]
        }] : "profile" in drupalSettings.glazedBuilder ? CKEDITOR.config.toolbar = drupalSettings.glazedBuilder.profile.ck_config.modal : CKEDITOR.config.toolbar = [{
            name: "basicstyles",
            items: ["Bold", "Italic", "Underline", "Strike", "Superscript", "Subscript", "RemoveFormat"]
        }, {
            name: "paragraph",
            items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "BulletedList", "Outdent", "Indent", "Blockquote", "CreateDiv"]
        }, {
            name: "clipboard",
            items: ["Undo", "Redo", "PasteText", "PasteFromWord"]
        }, {
            name: "links",
            items: ["Link", "Unlink"]
        }, {
            name: "insert",
            items: ["Image", "HorizontalRule", "SpecialChar", "Table", "Templates"]
        }, {
            name: "colors",
            items: ["TextColor"]
        }, {
            name: "document",
            items: ["Source"]
        }, {
            name: "tools",
            items: ["ShowBlocks", "Maximize"]
        }, {
            name: "styles",
            items: ["Format", "Styles", "FontSize"]
        }, {
            name: "editing",
            items: ["Scayt"]
        }]
    };
    var g = {};

    function w() {
        this.dom_element = null, this.heading = "", this.description = "", this.param_name = "", this.required = !1, this.admin_label = "", this.holder = "", this.wrapper_class = "", this.value = null, this.can_be_empty = !1, this.hidden = !1, this.tab = "", this.dependency = {}, "create" in this && this.create()
    }

    function t(e, t) {
        i(t, w), t.prototype.type = e, w.prototype.param_types[e] = t
    }

    function D(e) {
        var t;
        return e.type in w.prototype.param_types ? f(t = new w.prototype.param_types[e.type], e) : f(t = new w, e), t
    }
    if (g.shortcode = {
            next: function(e, t, a) {
                var n, i, o = g.shortcode.regexp(e);
                if (o.lastIndex = a || 0, n = o.exec(t)) return "[" === n[1] && "]" === n[7] ? g.shortcode.next(e, t, o.lastIndex) : (i = {
                    index: n.index,
                    content: n[0],
                    shortcode: g.shortcode.fromMatch(n)
                }, n[1] && (i.match = i.match.slice(1), i.index++), n[7] && (i.match = i.match.slice(0, -1)), i)
            },
            replace: function(e, t, d) {
                return t.replace(g.shortcode.regexp(e), function(e, t, a, n, i, o, l, s) {
                    if ("[" === t && "]" === s) return e;
                    var r = d(g.shortcode.fromMatch(arguments));
                    return r ? t + r + s : e
                })
            },
            string: function(e) {
                return new g.shortcode(e).string()
            },
            regexp: _.memoize(function(e) {
                return new RegExp("\\[(\\[?)(" + e + ")(?![\\w-])([^\\]\\/]*(?:\\/(?!\\])[^\\]\\/]*)*?)(?:(\\/)\\]|\\](?:([^\\[]*(?:\\[(?!\\/\\2\\])[^\\[]*)*)(\\[\\/\\2\\]))?)(\\]?)", "g")
            }),
            attrs: _.memoize(function(e) {
                var t, a, n = {},
                    i = [];
                for (t = /(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/g, e = e.replace(/[\u00a0\u200b]/g, " "); a = t.exec(e);) a[1] ? n[a[1].toLowerCase()] = a[2] : a[3] ? n[a[3].toLowerCase()] = a[4] : a[5] ? n[a[5].toLowerCase()] = a[6] : a[7] ? i.push(a[7]) : a[8] && i.push(a[8]);
                return {
                    named: n,
                    numeric: i
                }
            }),
            fromMatch: function(e) {
                var t;
                return t = e[4] ? "self-closing" : e[6] ? "closed" : "single", new g.shortcode({
                    tag: e[2],
                    attrs: e[3],
                    type: t,
                    content: e[5]
                })
            }
        }, g.shortcode = _.extend(function(e) {
            _.extend(this, _.pick(e || {}, "tag", "attrs", "type", "content"));
            var t = this.attrs;
            this.attrs = {
                named: {},
                numeric: []
            }, t && (_.isString(t) ? this.attrs = g.shortcode.attrs(t) : _.isEqual(_.keys(t), ["named", "numeric"]) ? this.attrs = t : _.each(e.attrs, function(e, t) {
                this.set(t, e)
            }, this))
        }, g.shortcode), _.extend(g.shortcode.prototype, {
            get: function(e) {
                return this.attrs[_.isNumber(e) ? "numeric" : "named"][e]
            },
            set: function(e, t) {
                return this.attrs[_.isNumber(e) ? "numeric" : "named"][e] = t, this
            },
            string: function() {
                var a = "[" + this.tag;
                return _.each(this.attrs.numeric, function(e) {
                    /\s/.test(e) ? a += ' "' + e + '"' : a += " " + e
                }), _.each(this.attrs.named, function(e, t) {
                    a += " " + t + '="' + e + '"'
                }), "single" === this.type ? a + "]" : "self-closing" === this.type ? a + " /]" : (a += "]", this.content && (a += this.content), a + "[/" + this.tag + "]")
            }
        }), g.html = _.extend(g.html || {}, {
            attrs: function(e) {
                var t, a;
                return "/" === e[e.length - 1] && (e = e.slice(0, -1)), t = g.shortcode.attrs(e), a = t.named, _.each(t.numeric, function(e) {
                    /\s/.test(e) || (a[e] = "")
                }), a
            },
            string: function(e) {
                var a = "<" + e.tag,
                    t = e.content || "";
                return _.each(e.attrs, function(e, t) {
                    a += " " + t, "" !== e && (_.isBoolean(e) && (e = e ? "true" : "false"), a += '="' + e + '"')
                }), e.single ? a + " />" : (a += ">", (a += _.isObject(t) ? g.html.string(t) : t) + "</" + e.tag + ">")
            }
        }), w.prototype = {
            safe: !0,
            param_types: {},
            spawn_modal_skeleton: function(e) {
                b("#az-elements-modal").remove();
                var t = "";
                "base" in e && (t = '<a class="help-link close" href="https://www.sooperthemes.com/documentation/' + e.base.replace("az_", "drupal-") + '" target="_blank"><span role="button" class="glyphicon glyphicon-question-sign" title="Element Tutorial"> </span><a/>');
                b('<div id="az-editor-modal" class="modal glazed"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><span class="close" data-dismiss="modal" aria-hidden="true">&times;</span><h4 class="modal-title">' + e.name + " " + Drupal.t("settings") + t + '</h4></div><div class="modal-body"></div><div class="modal-footer"><span class="btn btn-default" data-dismiss="modal">' + Drupal.t("Close") + '</span><span class="save btn btn-primary">' + Drupal.t("Save changes") + "</span></div></div></div></div>").prependTo("body")
            },
            show_editor: function(a, n, i) {
                w.prototype.spawn_modal_skeleton(n);
                for (var e = b("#az-editor-modal"), t = {}, o = 0; o < a.length; o++) a[o].hidden || (a[o].element = n, a[o].tab in t ? t[a[o].tab].push(a[o]) : t[a[o].tab] = [a[o]]);
                var l = b('<div id="az-editor-tabs"></div>'),
                    s = (o = 0, '<ul class="nav nav-tabs">');
                for (var r in t) o++, "" === r && (r = Drupal.t("General")), s += '<li><a href="#az-editor-tab-' + o + '" data-toggle="tab">' + r + "</a></li>";
                s += "</ul>", b(l).append(s), o = 0;
                var d = b('<form role="form" class="tab-content"></form>');
                for (var r in t) {
                    for (var c = b('<div id="az-editor-tab-' + ++o + '" class="tab-pane"></div>'), p = 0; p < t[r].length; p++) t[r][p].render(n.attrs[t[r][p].param_name]), b(c).append(t[r][p].dom_element);
                    b(d).append(c)
                }
                b(l).append(d), b(e).find(".modal-body").append(l), b('#az-editor-tabs a[href="#az-editor-tab-1"]').tab("show"), b('#az-editor-modal input[name="el_class"]').each(function() {
                    ! function(e, t, a) {
                        var n = '<select multiple="multiple">',
                            i = "";
                        for (var o in e) 0 <= o.indexOf("optgroup") ? ("" == i && (n += "</optgroup>"), n = n + '<optgroup label="' + e[o] + '">', i = e[o]) : n = n + '<option value="' + o + '">"' + e[o] + '"</option>';
                        "" != i && (n += "</optgroup>"), n += "</select>", b(t).css("display", "none");
                        var l = b(n).insertAfter(t);
                        if (b(t).val().length)
                            for (var s = b(t).val().split(a), r = 0; r < s.length; r++) b(l).find('option[value="' + s[r] + '"]').length || b(l).append('<option value="' + s[r] + '">"' + s[r] + '"</option>'), b(l).find('option[value="' + s[r] + '"]').attr("selected", "selected");
                        b(l).chosen({
                            search_contains: !0
                        }), b(l).change(function() {
                            var e = [];
                            b(this).find("option:selected").each(function() {
                                e.push(b(this).val())
                            }), b(t).val(e.join(a))
                        }), b(l).parent().find(".chosen-container").width("100%"), b('<div><a class="direct-input" href="#">' + Drupal.t("Edit as text") + "</a></div>").insertBefore(l).click(function() {
                            b(t).css("display", "block"), b(l).parent().find(".chosen-container").remove(), b(l).remove(), b(this).remove()
                        })
                    }(T.prototype.el_classes, this, " ")
                });
                for (o = 0; o < a.length; o++)
                    if ("element" in a[o].dependency) {
                        var h = null;
                        for (p = 0; p < a.length; p++)
                            if (a[p].param_name === a[o].dependency.element) {
                                h = a[p];
                                break
                            }
                            "is_empty" in a[o].dependency && function(e, t) {
                            b(t.dom_element).find('[name="' + t.param_name + '"]').on("keyup change", function() {
                                "" === t.get_value() ? (a[e].display_none = !1, b(a[e].dom_element).css("display", "block"), "callback" in a[e].dependency && a[e].dependency.callback.call(a[e], t)) : (a[e].display_none = !0, b(a[e].dom_element).css("display", "none"))
                            }).trigger("change")
                        }(o, h), "not_empty" in a[o].dependency && function(e, t) {
                            b(t.dom_element).find('[name="' + t.param_name + '"]').on("keyup change", function() {
                                "" !== t.get_value() ? (a[e].display_none = !1, b(a[e].dom_element).css("display", "block"), "callback" in a[e].dependency && a[e].dependency.callback.call(a[e], t)) : (a[e].display_none = !0, b(a[e].dom_element).css("display", "none"))
                            }).trigger("change")
                        }(o, h), "value" in a[o].dependency && function(e, t) {
                            b(t.dom_element).find('[name="' + t.param_name + '"]').on("keyup change", function() {
                                0 <= _.indexOf(a[e].dependency.value, t.get_value()) ? (a[e].display_none = !1, b(a[e].dom_element).css("display", "block"), "callback" in a[e].dependency && a[e].dependency.callback.call(a[e], t)) : (a[e].display_none = !0, b(a[e].dom_element).css("display", "none"))
                            }).trigger("change")
                        }(o, h)
                    }
                b("#az-editor-modal").one("shown.bs.modal", function(e) {
                    b("body").addClass("modal-open");
                    for (var t = 0; t < a.length; t++) a[t].hidden || a[t].opened()
                }), b("#az-editor-modal").one("hidden.bs.modal", function(e) {
                    for (var t = 0; t < a.length; t++) a[t].closed();
                    b(window).scrollTop(u), b(window).off("scroll.az-editor-modal"), b("body").removeClass("modal-open")
                }), b("#az-editor-modal").find(".save").click(function() {
                    for (var e = {}, t = 0; t < a.length; t++)
                        if (!a[t].hidden && ("display_none" in a[t] && (!("display_none" in a[t]) || a[t].display_none) || (e[a[t].param_name] = a[t].get_value()), a[t].required && "" == e[a[t].param_name])) return b(a[t].dom_element).addClass("has-error"), !1;
                    return b("#az-editor-modal").modal("hide"), i.call(n, e), b(window).trigger("CKinlineAttach"), !1
                }), b("#az-editor-modal").find('[data-dismiss="modal"]').click(function() {
                    window.glazedBuilder.glazedElements.edit_stack = []
                });
                var u = b(window).scrollTop();
                b(window).on("scroll.az-editor-modal", function() {
                    b(window).scrollTop(u)
                }), b("#az-editor-modal").modal("show")
            },
            opened: function() {},
            closed: function() {},
            render: function(e) {}
        }, "glazed_param_types" in window.glazedBuilder)
        for (var y = 0; y < window.glazedBuilder.glazed_param_types.length; y++) {
            var x = window.glazedBuilder.glazed_param_types[y],
                B = function() {
                    B.baseclass.apply(this, arguments)
                };
            t(x.type, B), x.baseclass = B.baseclass, f(B.prototype, x)
        }

    function k() {
        k.baseclass.apply(this, arguments)
    }

    function S() {
        S.baseclass.apply(this, arguments)
    }

    function C() {}

    function T(e, t) {
        this.id = "gb" + Math.random().toString(36).substr(2, 8), null != e && (this.parent = e, "boolean" == typeof t ? t ? e.children.push(this) : e.children.unshift(this) : e.children.splice(t, 0, this)), this.children = [], this.dom_element = null, this.dom_content_element = null, this.attrs = {};
        for (var a = 0; a < this.params.length; a++) _.isString(this.params[a].value) ? this.attrs[this.params[a].param_name] = this.params[a].value : this.params[a].hidden || (this.attrs[this.params[a].param_name] = "");
        this.controls = null, window.glazedBuilder.glazedElements.add_element(this.id, this, t)
    }
    t("cms_settings", k), f(k.prototype, {
        get_value: function() {
            return b(this.dom_element).find("form").serialize()
        },
        render_form: function(e) {
            var t = this;
            ! function(t, a) {
                b.ajax({
                    type: "get",
                    url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    b.ajax({
                        type: "POST",
                        url: e,
                        data: {
                            action: "glazed_get_cms_element_settings",
                            name: t,
                            url: window.location.href
                        },
                        cache: !drupalSettings.glazedBuilder.glazedEditor
                    }).done(function(e) {
                        a(e)
                    })
                })
            }(e, function(e) {
                b(t.dom_element).empty(), b(e).appendTo(t.dom_element), b(t.dom_element).find('[type="submit"]').remove(), 0 < t.form_value.length && b(t.dom_element).deserialize(function(e) {
                    var t = document.createElement("div");
                    return t.innerHTML = e, 0 === t.childNodes.length ? "" : t.childNodes[0].nodeValue
                }(t.form_value))
            })
        },
        get_form: function(e) {
            var t = e.get_value();
            0 < t.length && this.render_form(t)
        },
        render: function(e) {
            this.form_value = e, this.dom_element = b('<div class="form-group"></div>')
        },
        opened: function() {
            "instance" in this && this.render_form(this.instance)
        }
    }), t("container", S), f(S.prototype, {
        get_value: function() {
            return b(this.dom_element).find('input[name="' + this.param_name + '_type"]').val() + "/" + b(this.dom_element).find('input[name="' + this.param_name + '_name"]').val()
        },
        render: function(e) {
            var t = e.split("/")[0],
                a = e.split("/")[1];
            this.dom_element = b('<div class="form-group"><label>' + this.heading + '</label><div class="wrap-type"><label>' + Drupal.t("Type") + '</label><input class="form-control" name="' + this.param_name + '_type" type="text" value="' + t + '"></div><div class="wrap-name"><label>' + Drupal.t("Name") + '</label><input class="form-control" name="' + this.param_name + '_name" type="text" value="' + a + '"></div><p class="help-block">' + this.description + "</p></div>")
        },
        opened: function() {
            var e = this.get_value(),
                t = null,
                a = null,
                n = this;
            ! function(t) {
                b.ajax({
                    type: "get",
                    url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    b.ajax({
                        type: "POST",
                        url: e,
                        data: {
                            action: "glazed_get_container_types",
                            url: window.location.href
                        },
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        t(e)
                    })
                })
            }(function(e) {
                t = s(e, b(n.dom_element).find('input[name="' + n.param_name + '_type"]')), b(t).chosen().change(function() {
                    h(b(this).val(), function(e) {
                        b(a).parent().find(".direct-input").click(), b(n.dom_element).find('input[name="' + n.param_name + '_name"]').val(""), a = s(e, b(n.dom_element).find('input[name="' + n.param_name + '_name"]'))
                    })
                })
            }), h(e.split("/")[0], function(e) {
                a = s(e, b(n.dom_element).find('input[name="' + n.param_name + '_name"]'))
            })
        }
    }), f(C.prototype, {
        elements_instances: {},
        elements_instances_by_an_name: {},
        template_elements_loaded: !1,
        cms_elements_loaded: !1,
        edit_stack: [],
        try_render_unknown_elements: function() {
            if (this.template_elements_loaded && this.cms_elements_loaded)
                for (var e in window.glazedBuilder.glazedElements.elements_instances) {
                    var t = window.glazedBuilder.glazedElements.elements_instances[e];
                    if (t instanceof $) {
                        var a = t.attrs.content;
                        /^\s*\<[\s\S]*\>\s*$/.exec(a) ? T.prototype.parse_html.call(t, a) : T.prototype.parse_shortcode.call(t, a);
                        for (var n = 0; n < t.children.length; n++) t.children[n].recursive_render();
                        b(t.dom_content_element).empty(), t.attach_children(), drupalSettings.glazedBuilder.glazedEditor && t.update_sortable(), t.recursive_showed()
                    }
                }
        },
        create_template_elements: function(l) {
            var s = {
                "link[href]": "href",
                "script[src]": "src",
                "img[src]": "src"
            };
            "glazed_urls_to_update" in window.glazedBuilder && (s = b.extend(s, window.glazedBuilder.glazed_urls_to_update));
            var e = [];
            "glazed_editable" in window.glazedBuilder && (e = window.glazedBuilder.glazed_editable);
            var t = [];
            "glazed_styleable" in window.glazedBuilder && (t = window.glazedBuilder.glazed_styleable);
            var a = [];
            "glazed_sortable" in window.glazedBuilder && (a = window.glazedBuilder.glazed_sortable);
            var n = [];
            "glazed_synchronizable" in window.glazedBuilder && (n = window.glazedBuilder.glazed_synchronizable);
            var i = [];
            "glazed_restoreable" in window.glazedBuilder && (i = window.glazedBuilder.glazed_restoreable);
            var o = [];
            "glazed_containable" in window.glazedBuilder && (o = window.glazedBuilder.glazed_containable);
            var z = w.prototype.param_types.icon.prototype.icons.map(function(e, t, a) {
                    return e.replace(/^/, ".").replace(/ /, ".")
                }),
                y = z.join(", ");
            for (var r in l) {
                var d = l[r].name,
                    c = l[r].html,
                    p = r.split("|");
                p.pop(), p = p.join("/");
                var h = drupalSettings.glazedBuilder.glazedBaseUrl + "../glazed_elements/" + p + "/";
                "baseurl" in l[r] && (h = l[r].baseurl);
                var u = "";
                "thumbnail" in l[r] && (u = l[r].thumbnail);

                function m(e, t) {
                    for (var i = this, a = 0; a < this.baseclass.prototype.params.length; a++)
                        if ("content" == this.baseclass.prototype.params[a].param_name && "" == this.baseclass.prototype.params[a].value) {
                            if (drupalSettings.glazedBuilder.glazedAjaxUrl) {
                                function n(e) {
                                    function a(e) {
                                        return 0 == e.indexOf("glazed_elements") ? drupalSettings.glazedBuilder.glazedBaseUrl + "../" + e : 0 != e.indexOf("/") && 0 != e.indexOf("http://") && 0 != e.indexOf("https://") ? i.baseurl + e : e
                                    }
                                    for (var t in s) {
                                        var n = s[t];
                                        b(e).find(t).each(function() {
                                            b(this).attr(n, a(b(this).attr(n)))
                                        })
                                    }
                                    b(e).find("[data-az-url]").each(function() {
                                        var e = b(this).attr("data-az-url");
                                        b(this).attr(e, a(b(this).attr(e)))
                                    }), b(e).find('[style*="background-image"]').each(function() {
                                        var e = b(this).attr("style").replace(/background-image[: ]*url\(([^\)]+)\) *;/, function(e, t) {
                                            return e.replace(t, encodeURI(a(decodeURI(t))))
                                        });
                                        b(this).attr("style", e)
                                    })
                                }
                                var o = b("<div>" + i.template + "</div>");
                                n(o), o = b(o).html(), this.baseclass.prototype.params[a].value = o
                            }
                            break
                        }
                    T.apply(this, arguments)
                }
                var g = 0 <= c.indexOf("az-rootable");
                I(d, !1, m), f(m.prototype, {
                    baseclass: m,
                    template: c,
                    baseurl: h,
                    path: r,
                    name: d,
                    icon: "fa fa-cube",
                    description: Drupal.t(""),
                    thumbnail: u,
                    params: [D({
                        type: "html",
                        heading: Drupal.t("Content"),
                        param_name: "content",
                        value: ""
                    })].concat(m.prototype.params),
                    show_settings_on_create: !1,
                    is_container: !0,
                    has_content: !0,
                    section: g,
                    category: Drupal.t("Template-elements"),
                    is_template_element: !0,
                    editable: [".az-editable"].concat(e),
                    styleable: [".az-styleable"].concat(t),
                    sortable: [".az-sortable"].concat(a),
                    synchronizable: [".az-synchronizable"].concat(n),
                    restoreable: [".az-restoreable"].concat(i),
                    containable: [".az-containable"].concat(o),
                    restore_nodes: {},
                    contained_elements: {},
                    show_controls: function() {
                        if (drupalSettings.glazedBuilder.glazedEditor) {
                            var g = this;
                            T.prototype.show_controls.apply(this, arguments);
                            var l = function() {
                                if (0 < window.glazedBuilder.glazedElements.edit_stack.length) {
                                    var t = window.glazedBuilder.glazedElements.edit_stack.shift();
                                    b(t.node).css("outline-width", "2px"), b(t.node).css("outline-style", "dashed");
                                    var e = setInterval(function() {
                                        "rgb(255, 0, 0)" != b(t.node).css("outline-color") ? b(t.node).css("outline-color", "rgb(255, 0, 0)") : b(t.node).css("outline-color", "rgb(255, 255, 255)")
                                    }, 100);
                                    setTimeout(function() {
                                        clearInterval(e), b(t.node).css("outline-color", ""), b(t.node).css("outline-width", ""), b(t.node).css("outline-style", ""),
                                            function(t, a, n, i) {
                                                var e = [],
                                                    o = "",
                                                    l = "",
                                                    s = "",
                                                    r = b.trim(b(t).text());
                                                r = "" != r ? b(t).html() : "&nbsp;&nbsp;&nbsp;";
                                                if (a)
                                                    if (b(t).is(y)) {
                                                        for (var d = 0; d < z.length; d++)
                                                            if (b(t).is(z[d])) {
                                                                (s = z[d].split(".")).shift(), s = s.join(" ");
                                                                break
                                                            }
                                                        e.push(D({
                                                            type: "icon",
                                                            heading: Drupal.t("Icon"),
                                                            param_name: "icon"
                                                        }))
                                                    } else "IMG" != b(t).prop("tagName") ? "" != r && e.push(D({
                                                        type: "textarea",
                                                        heading: Drupal.t("Content"),
                                                        param_name: "content"
                                                    })) : (o = b(t).attr("src"), e.push(D({
                                                        type: "image",
                                                        heading: Drupal.t("Image"),
                                                        param_name: "image",
                                                        description: Drupal.t("Select image from media library.")
                                                    }))), "A" == b(t).prop("tagName") && (l = b(t).attr("href"), e.push(D({
                                                        type: "link",
                                                        heading: Drupal.t("Link"),
                                                        param_name: "link",
                                                        description: Drupal.t("Content link (url).")
                                                    })));
                                                if (n) {
                                                    e.push(D({
                                                        type: "textfield",
                                                        heading: Drupal.t("Content classes"),
                                                        param_name: "el_class",
                                                        description: Drupal.t("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.")
                                                    }));
                                                    var c = D({
                                                        type: "style",
                                                        heading: Drupal.t("Content style"),
                                                        param_name: "style",
                                                        description: Drupal.t("Style options."),
                                                        tab: Drupal.t("Style")
                                                    });
                                                    a ? e.push(c) : e.unshift(c)
                                                }
                                                b(t).removeClass("editable-highlight"), b(t).removeClass("styleable-highlight"), b(t).removeClass(s);
                                                var p = b(t).attr("class");
                                                b(t).addClass(s), void 0 !== p && !1 !== p || (p = "");
                                                var h = "";
                                                for (var u in t.style) b.isNumeric(u) && (h = h + t.style[u] + ": " + t.style.getPropertyValue(t.style[u]) + "; ");
                                                h = (h = (h = (h = (h = (h = (h = (h = (_ = h, h = _.replace(/rgb\((\d+),\s*(\d+),\s*(\d+)\)/g, function(e, t, a, n) {
                                                    return "#" + m(t) + m(a) + m(n)
                                                })).replace(/\-value\: /g, ": ")).replace("border-top-color", "border-color")).replace("border-top-left-radius", "border-radius")).replace("border-top-style", "border-style")).replace("background-position-x: 50%; background-position-y: 50%;", "background-position: center;")).replace("background-position-x: 50%; background-position-y: 100%;", "background-position: center bottom;")).replace("background-repeat-x: no-repeat; background-repeat-y: no-repeat;", "background-repeat: no-repeat;")).replace("background-repeat-x: repeat;", "background-repeat: repeat-x;"), w.prototype.show_editor(e, {
                                                    name: Drupal.t("Content"),
                                                    attrs: {
                                                        content: r,
                                                        link: l,
                                                        image: o,
                                                        el_class: p,
                                                        style: h,
                                                        icon: s
                                                    }
                                                }, function(e) {
                                                    a && ("" != s && (b(t).removeClass(s), e.el_class = e.el_class + " " + e.icon), "A" == b(t).prop("tagName") && b(t).attr("href", e.link), "IMG" == b(t).prop("tagName") ? b(t).attr("src", e.image) : "" != r && "" != e.content ? b(t).html(e.content) : b(t).html("&nbsp;&nbsp;&nbsp;")), n && (b(t).attr("class", e.el_class), b(t).attr("style", e.style)), g.attrs.content = b(g.dom_content_element).html(), g.restore_content(), f(), v(), i()
                                                });

                                                function m(e) {
                                                    return ("0" + parseInt(e).toString(16)).slice(-2)
                                                }
                                                var _
                                            }(t.node, t.edit, t.style, function() {
                                                if (0 < window.glazedBuilder.glazedElements.edit_stack.length) {
                                                    var e = b(t.node).width() * b(t.node).height();
                                                    b(window.glazedBuilder.glazedElements.edit_stack[0].node).width() * b(window.glazedBuilder.glazedElements.edit_stack[0].node).height() / e < 2 ? l() : window.glazedBuilder.glazedElements.edit_stack = []
                                                }
                                            })
                                    }, 500)
                                }
                            };

                            function n(e) {
                                var t = b(e).clone();
                                b(t).find("*").each(function() {
                                    for (; 0 < this.attributes.length;) this.removeAttribute(this.attributes[0].name)
                                });
                                var a = b(t).html();
                                return a = a.replace(/\s*/g, "")
                            }

                            function f() {
                                s();
                                for (var e = 0; e < g.synchronizable.length; e++) b(g.dom_content_element).find(g.synchronizable[e]).each(function() {
                                    if (0 == b(this).closest("[data-az-restore]").length) {
                                        b(this).find(".editable-highlight").removeClass("editable-highlight"), b(this).find(".styleable-highlight").removeClass("styleable-highlight"), b(this).find(".sortable-highlight").removeClass("sortable-highlight"), b(this).find('[class=""]').removeAttr("class"), b(this).find('[style=""]').removeAttr("style");
                                        var e = b(this).data("synchronized");
                                        if (e)
                                            for (var t = 0; t < e.length; t++) b(e[t]).html(b(this).html());
                                        b(this).data("current-state") ? b(document).trigger("glazed_synchronize", {
                                            from_node: this,
                                            old_state: b(this).data("current-state"),
                                            new_state: b(this).html()
                                        }) : b(document).trigger("glazed_synchronize", {
                                            from_node: this,
                                            old_state: n(this),
                                            new_state: b(this).html()
                                        }), b(this).data("current-state", n(this)), g.attrs.content = b(g.dom_content_element).html(), g.restore_content()
                                    }
                                });
                                v()
                            }

                            function s() {
                                for (var e = 0; e < g.sortable.length; e++) b(g.dom_content_element).find(g.sortable[e]).each(function() {
                                    b(this).hasClass("ui-sortable") && b(this).data("sortable") && (b(this).data("sortable", !1), b(this).sortable("destroy"), b(this).find(".ui-sortable-handle").removeClass("ui-sortable-handle"))
                                })
                            }

                            function v() {
                                for (var e = 0; e < g.restoreable.length; e++) b(g.dom_element).find(g.restoreable[e]).off("mouseenter.az-restoreable").on("mouseenter.az-restoreable", function() {
                                    b(this).addClass("restoreable-highlight")
                                }), b(g.dom_element).find(g.restoreable[e]).off("mouseleave.az-restoreable").on("mouseleave.az-restoreable", function() {
                                    b(this).removeClass("restoreable-highlight")
                                }), b(g.dom_element).find(g.restoreable[e]).off("click.az-restoreable").on("click.az-restoreable", function(e) {
                                    if (b(this).is("[data-az-restore]")) {
                                        var t = [];
                                        t.push(D({
                                            type: "html",
                                            heading: Drupal.t("HTML"),
                                            param_name: "html"
                                        }));
                                        var a = b(this).attr("data-az-restore"),
                                            n = g.restore_nodes[a];
                                        return w.prototype.show_editor(t, {
                                            name: Drupal.t("Content"),
                                            attrs: {
                                                html: n
                                            }
                                        }, function(e) {
                                            g.restore_nodes[a] = e.html, g.restore_content(), g.update_dom(), f()
                                        }), !1
                                    }
                                });
                                for (e = 0; e < g.styleable.length; e++) b(g.dom_element).find(g.styleable[e]).off("mouseenter.az-styleable").on("mouseenter.az-styleable", function() {
                                    0 == b(this).closest("[data-az-restore]").length && b(this).addClass("styleable-highlight")
                                }), b(g.dom_element).find(g.styleable[e]).off("mouseleave.az-styleable").on("mouseleave.az-styleable", function() {
                                    0 == b(this).closest("[data-az-restore]").length && b(this).removeClass("styleable-highlight")
                                }), b(g.dom_element).find(g.styleable[e]).off("click.az-styleable").on("click.az-styleable", function(e) {
                                    if (0 == b(this).closest("[data-az-restore]").length) {
                                        if (0 == b(this).parent().closest(".styleable-highlight, .editable-highlight").length) return window.glazedBuilder.glazedElements.edit_stack.push({
                                            node: this,
                                            edit: !1,
                                            style: !0
                                        }), l(), !1;
                                        window.glazedBuilder.glazedElements.edit_stack.push({
                                            node: this,
                                            edit: !1,
                                            style: !0
                                        })
                                    }
                                });
                                for (e = 0; e < g.editable.length; e++) b(g.dom_element).find(g.editable[e]).off("mouseenter.az-editable").on("mouseenter.az-editable", function() {
                                    0 == b(this).closest("[data-az-restore]").length && b(this).addClass("editable-highlight")
                                }), b(g.dom_element).find(g.editable[e]).off("mouseleave.az-editable").on("mouseleave.az-editable", function() {
                                    0 == b(this).closest("[data-az-restore]").length && b(this).removeClass("editable-highlight")
                                }), b(g.dom_element).find(g.editable[e]).off("click.az-editable").on("click.az-editable", function(e) {
                                    if (0 == b(this).closest("[data-az-restore]").length) {
                                        if (0 == b(this).parent().closest(".styleable-highlight, .editable-highlight").length) return window.glazedBuilder.glazedElements.edit_stack.push({
                                            node: this,
                                            edit: !0,
                                            style: !0
                                        }), l(), !1;
                                        window.glazedBuilder.glazedElements.edit_stack.push({
                                            node: this,
                                            edit: !0,
                                            style: !0
                                        })
                                    }
                                });
                                var t, a = [],
                                    n = null,
                                    i = null;

                                function o(e) {
                                    if (b(e).hasClass("sortable-highlight")) {
                                        b(e).find(".az-sortable-controls").remove();
                                        var t = b('<div class="az-sortable-controls"></div>').appendTo(e),
                                            a = b('<div class="az-sortable-clone glyphicon glyphicon-duplicate" title="' + Drupal.t("Clone") + '"></div>').appendTo(t).click(function() {
                                                return s(), b(e).removeClass("sortable-highlight").find(".az-sortable-controls").remove(), b(e).clone().insertAfter(e), g.attrs.content = b(g.dom_content_element).html(), g.restore_content(), f(), v(), !1
                                            });
                                        b(a).css("line-height", b(a).height() + "px").css("font-size", b(a).height() / 2 + "px");
                                        var n = b('<div class="az-sortable-remove glyphicon glyphicon-trash" title="' + Drupal.t("Remove") + '"></div>').appendTo(t).click(function() {
                                            return s(), b(e).removeClass("sortable-highlight").find(".az-sortable-controls").remove(), b(e).remove(), g.attrs.content = b(g.dom_content_element).html(), g.restore_content(), f(), v(), !1
                                        });
                                        b(n).css("line-height", b(n).height() + "px").css("font-size", b(n).height() / 2 + "px")
                                    }
                                }
                                b(g.dom_element).off("mousemove.az-able").on("mousemove.az-able", function() {
                                    null != n && b(n).hasClass("sortable-highlight") && (clearTimeout(i), i = setTimeout(function() {
                                        o(n)
                                    }, 1e3))
                                });
                                for (e = 0; e < g.sortable.length; e++) t = e, b(g.dom_element).find(g.sortable[t]).find("> *").off("mouseenter.az-sortable").on("mouseenter.az-sortable", function() {
                                    if (0 == b(this).closest("[data-az-restore]").length) {
                                        var e = this;
                                        b(g.dom_element).find(".az-sortable-controls").remove(), b(g.dom_element).find(".sortable-highlight").removeClass("sortable-highlight"), null !== n && clearTimeout(i), b(e).addClass("sortable-highlight"), a.push(e), n = e, i = setTimeout(function() {
                                            o(e)
                                        }, 1e3)
                                    }
                                }), b(g.dom_element).find(g.sortable[t]).find("> *").off("mouseleave.az-sortable").on("mouseleave.az-sortable", function() {
                                    if (0 == b(this).closest("[data-az-restore]").length) {
                                        var e = this;
                                        b(g.dom_element).find(".az-sortable-controls").remove(), b(g.dom_element).find(".sortable-highlight").removeClass("sortable-highlight"), null !== n && clearTimeout(i), a.pop(), 0 < a.length ? (e = a[a.length - 1], b(e).addClass("sortable-highlight"), n = e, i = setTimeout(function() {
                                            o(e)
                                        }, 1e3)) : n = null
                                    }
                                });
                                ! function() {
                                    for (var e = 0; e < g.sortable.length; e++) b(g.dom_element).find(g.sortable[e]).each(function() {
                                        0 == b(this).closest("[data-az-restore]").length && (b(this).data("sortable", !0), b(this).sortable({
                                            items: "> *",
                                            placeholder: "az-sortable-placeholder",
                                            forcePlaceholderSize: !0,
                                            start: function(e, t) {
                                                b(t.item).removeClass("sortable-highlight").find(".az-sortable-controls").remove()
                                            },
                                            update: function(e, t) {
                                                g.attrs.content = b(g.dom_content_element).html(), g.restore_content(), f()
                                            },
                                            over: function(e, t) {
                                                t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                                            }
                                        }))
                                    })
                                }()
                            }
                            b(document).on("glazed_synchronize", function(e, t) {
                                s();
                                for (var a = 0; a < g.synchronizable.length; a++) b(g.dom_content_element).find(g.synchronizable[a]).each(function() {
                                    if (0 == b(this).closest("[data-az-restore]").length && (b(this).find(".editable-highlight").removeClass("editable-highlight"), b(this).find(".styleable-highlight").removeClass("styleable-highlight"), b(this).find(".sortable-highlight").removeClass("sortable-highlight"), b(this).find('[class=""]').removeAttr("class"), b(this).find('[style=""]').removeAttr("style"), this != t.from_node && n(this) == t.old_state)) {
                                        var e = b(t.from_node).data("synchronized");
                                        (e = e || []).push(this), e = b.unique(e), b(t.from_node).data("synchronized", e), (e = (e = b(this).data("synchronized")) || []).push(t.from_node), e = b.unique(e), b(this).data("synchronized", e), b(this).html(t.new_state), g.attrs.content = b(g.dom_content_element).html(), g.restore_content()
                                    }
                                });
                                v()
                            }), v(), f()
                        }
                    },
                    restore_content: function() {
                        var e = b("<div>" + this.attrs.content + "</div>");
                        for (var t in this.restore_nodes) b(e).find('[data-az-restore="' + t + '"]').html(this.restore_nodes[t]);
                        b(document).trigger("glazed_restore", {
                            dom: e
                        }), this.attrs.content = b(e).html()
                    },
                    get_content: function() {
                        return this.restore_content(), T.prototype.get_content.apply(this, arguments)
                    },
                    restore: function(e) {
                        for (var t in T.prototype.restore.apply(this, arguments), this.restore_nodes) b(e).find('[data-az-restore="' + t + '"]').html(this.restore_nodes[t]);
                        b(document).trigger("glazed_restore", {
                            dom: e
                        }), b(e).find("[data-az-restore]").removeAttr("data-az-restore")
                    },
                    showed: function(e) {
                        T.prototype.showed.apply(this, arguments);
                        var t = this;
                        if (t.section) {
                            var a = e(t.dom_element).parent().closest(".container, .container-fluid"),
                                n = e(t.dom_element).parentsUntil(".container, .container-fluid"),
                                i = e(t.dom_element).parent().closest(".az-popup-ctnr"),
                                o = e(t.dom_element).parentsUntil(".az-popup-ctnr");
                            (0 < a.length && 0 == i.length || 0 < a.length && 0 < i.length && n.length < o.length) && e(t.dom_content_element).find(".container, .container-fluid").each(function() {
                                e(this).removeClass("container"), e(this).removeClass("container-fluid"), t.attrs.content = e(t.dom_content_element).html(), t.restore_content(), t.section = !1
                            })
                        }
                    },
                    render: function(t) {
                        var a = this;
                        this.dom_element = t('<div class="az-element az-template ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = t("<div></div>").appendTo(this.dom_element);
                        var e = "<div>" + this.attrs.content + "</div>";
                        e = t(e), a.restore_nodes = {};
                        for (var n = 0; n < this.restoreable.length; n++) t(e).find(this.restoreable[n]).each(function() {
                            var e = _.uniqueId("r");
                            t(this).attr("data-az-restore", e), a.restore_nodes[e] = t(this).html()
                        });
                        this.attrs.content = t(e).html(), t(this.attrs.content).appendTo(this.dom_content_element), T.prototype.render.apply(this, arguments)
                    }
                })
            }
            this.template_elements_loaded = !0, J(), this.try_render_unknown_elements(), setTimeout(function() {
                b(function() {
                    if (showPanel = !0, "profile" in drupalSettings.glazedBuilder && (drupalSettings.glazedBuilder.profile.sidebar || (showPanel = !1)), showPanel && drupalSettings.glazedBuilder.glazedEditor && 0 < Object.keys(l).length && 0 < window.glazedBuilder.glazed_containers.length) {
                        var e = {
                            _: []
                        };
                        for (var t in l) {
                            var a = t.split("|");
                            a.pop();
                            for (var n = e, i = 0; i < a.length; i++) a[i] in n || (n[a[i]] = {
                                _: []
                            }), n = n[a[i]];
                            n._.push(l[t])
                        }
                        var o = b('<div id="az-template-elements" class="az-left-sidebar glazed"></div>').appendTo("body");
                        b('<div class="glazed-snippets-header clearfix"><img src="' + drupalSettings.glazedBuilder.glazedBaseUrl + 'images/glazed-logo-white.svg"><h3>' + Drupal.t("Glazed Snippets") + "</h3></div>").appendTo(o), b(o).append(function e(t) {
                            if (1 === Object.keys(t).length && "_" in t) return null;
                            var a = b('<ul class="nav az-nav-list"></ul>');
                            for (var n in t)
                                if ("_" != n) {
                                    var i = b("<li></li>").appendTo(a).on("mouseenter", function() {
                                        b(this).find("> .az-nav-list").css("display", "block")
                                    });
                                    ! function(e) {
                                        b('<a href="#">' + n + "</a>").appendTo(i).click(function() {
                                            var l = this;
                                            b(r).empty(), b(r).css("display", "block"), b(o).addClass("az-thumbnails"),
                                                function e(t) {
                                                    for (var a in t)
                                                        if ("_" == a)
                                                            for (var n = 0; n < t[a].length; n++) b('<img class="az-thumbnail" data-az-base="' + t[a][n].name + '" src="' + encodeURI(t[a][n].thumbnail) + '">').appendTo(r);
                                                        else e(t[a])
                                                }(e), b(o).off("mouseleave").on("mouseleave", function() {
                                                    a || (b(o).css("left", ""), b(o).removeClass("az-thumbnails"), b(r).css("overflow-y", "scroll"), b(r).css("display", "none"))
                                                });
                                            var a = !1,
                                                s = 0;
                                            return b(r).sortable({
                                                items: ".az-thumbnail",
                                                connectWith: ".az-ctnr",
                                                start: function(e, t) {
                                                    a = !0, b(o).css("left", "0px"), b(r).css("overflow-y", "visible"), s = b(window).scrollTop(), b(window).on("scroll.template-elements-sortable", function() {
                                                        b(window).scrollTop(s)
                                                    })
                                                },
                                                stop: function(e, t) {
                                                    a = !1, b(o).css("left", ""), b(o).removeClass("az-thumbnails"), b(r).css("overflow-y", "scroll"), b(r).css("display", "none"), b(window).off("scroll.template-elements-sortable")
                                                },
                                                update: function(e, t) {
                                                    for (var a = window.glazedBuilder.glazedElements.get_element(b(t.item).parent().closest("[data-az-id]").attr("data-az-id")), n = 0, i = b(t.item).parent().find("[data-az-id], .az-thumbnail"), o = 0; o < i.length; o++)
                                                        if (b(i[o]).hasClass("az-thumbnail")) {
                                                            n = o;
                                                            break
                                                        }
                                                    window.glazedBuilder.glazedElements.create_element(a, b(t.item).attr("data-az-base"), n, function() {}), b(t.item).detach(), b(l).click(), b(window).scrollTop(s)
                                                },
                                                placeholder: "az-sortable-placeholder",
                                                forcePlaceholderSize: !0,
                                                over: function(e, t) {
                                                    t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                                                }
                                            }), !1
                                        })
                                    }(t[n]), b(i).append(e(t[n]))
                                }
                            return a
                        }(e)), b(o).find("> .az-nav-list > li").on("mouseleave", function() {
                            b(this).find(".az-nav-list").css("display", "none")
                        });
                        var r = b('<div id="az-thumbnails"></div>').appendTo(o)
                    }
                })
            }, 500)
        },
        create_cms_elements: function(e) {
            for (var t in e) {
                var a = "az_" + t,
                    o = function(e, t) {
                        o.baseclass.apply(this, arguments)
                    };
                I(a, !1, o);
                var n = {
                    name: e[t],
                    icon: "fa fa-drupal",
                    description: Drupal.t(""),
                    category: "CMS",
                    instance: t,
                    params: [D({
                        type: "cms_settings",
                        heading: Drupal.t("Settings"),
                        param_name: "settings",
                        instance: t
                    })],
                    show_settings_on_create: !0,
                    is_container: !0,
                    has_content: !0,
                    is_cms_element: !0,
                    get_button: function() {
                        var e = this.name.replace(/^Block: /, "");
                        return '<div class="well text-center pull-left text-overflow glazed-cms" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i>' + e + "</div>"
                    },
                    get_button_with_tag: function() {
                        var e = "",
                            t = this.name.replace(/^View: /, "");
                        return b.inArray(drupalSettings.glazedBuilder.viewsTags, -1 < this.base) && (e = drupalSettings.glazedBuilder.viewsTags[this.base]), '<div class="well text-center pull-left text-overflow glazed-cms" data-az-element="' + this.base + '" data-az-tag="' + e + '"><i class="' + this.icon + '"></i>' + t + "</div>"
                    },
                    get_content: function() {
                        return ""
                    },
                    showed: function(n) {
                        if (o.baseclass.prototype.showed.apply(this, arguments), "content" in this.attrs && "" != this.attrs.content) n(this.dom_content_element).append(this.attrs.content), this.attrs.content = "";
                        else {
                            var i = this;
                            glazedBuilder.glazed_add_js({
                                path: "vendor/jquery.waypoints/lib/jquery.waypoints.min.js",
                                loaded: "waypoint" in n.fn,
                                callback: function() {
                                    n(i.dom_element).waypoint(function(e) {
                                        var t = i.parent.get_my_container(),
                                            a = {
                                                display_title: i.attrs.display_title,
                                                display_exposed_filters: i.attrs.display_exposed_filters,
                                                override_pager: i.attrs.override_pager,
                                                items: i.attrs.items,
                                                offset: i.attrs.offset,
                                                contextual_filter: i.attrs.contextual_filter,
                                                toggle_fields: i.attrs.toggle_fields
                                            };
                                        u(i.instance, i.attrs.settings, t.attrs.container, a, function(e) {
                                            n(i.dom_content_element).empty(), n(i.dom_content_element).append(e);
                                            var t = "az_" + i.instance;
                                            i.attrs.display_exposed_filters && !drupalSettings.glazedBuilder.cmsElementViewsSettings[t].ajax_enabled && 0 < document.getElementsByClassName("glazed-editor").length && n(i.dom_content_element).prepend('<div class="az-unknown glazed-builder-warning">' + Drupal.t("Exposed filters enabled but ajax turned off. Please enable Ajax on this display in order to show exposed filters here.") + "</div>"), Drupal.attachBehaviors(n(i.dom_content_element)[0])
                                        })
                                    }, {
                                        offset: "100%",
                                        handler: function(e) {
                                            this.destroy()
                                        }
                                    }), n(document).trigger("scroll")
                                }
                            })
                        }
                    },
                    render: function(e) {
                        this.dom_element = e('<div class="az-element az-cms-element ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = e("<div></div>").appendTo(this.dom_element), o.baseclass.prototype.render.apply(this, arguments)
                    }
                };
                if (t.match(/^block-/) && n.params.push(D({
                        type: "checkbox",
                        heading: Drupal.t("Show title"),
                        param_name: "display_title",
                        content: "yes",
                        value: {
                            yes: Drupal.t("Yes")
                        }
                    })), t.match("^view-")) {
                    var i = {
                        type: "textfield",
                        heading: Drupal.t("Items to display"),
                        param_name: "items",
                        description: Drupal.t("The number of items to display. Enter 0 for no limit."),
                        can_be_empty: !0,
                        dependency: {
                            element: "override_pager",
                            value: ["yes"]
                        }
                    };
                    if (!drupalSettings.glazedBuilder.cmsElementViewsSettings.hasOwnProperty(a)) continue;
                    drupalSettings.glazedBuilder.cmsElementViewsSettings[a].exposed_filter && n.params.push(D({
                        type: "checkbox",
                        heading: Drupal.t("Show exposed filters"),
                        param_name: "display_exposed_filters",
                        content: "yes",
                        value: {
                            yes: Drupal.t("Yes")
                        }
                    })), "block" == drupalSettings.glazedBuilder.cmsElementViewsSettings[a].view_display_type ? (i.heading = Drupal.t("Items to display:"), i.description = Drupal.t("The number of items to display. Enter 0 for no limit.")) : (i.heading = Drupal.t("Items per page:"), i.description = Drupal.t("The number to display per page. Enter 0 for no limit.")), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].title && n.params.push(D({
                        type: "checkbox",
                        heading: Drupal.t("Show title"),
                        param_name: "display_title",
                        content: "yes",
                        value: {
                            yes: Drupal.t("Yes")
                        }
                    })), n.params.push(D({
                        type: "dropdown",
                        heading: Drupal.t("Override pager"),
                        param_name: "override_pager",
                        value: {
                            no: Drupal.t("No"),
                            yes: Drupal.t("Yes")
                        }
                    })), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.items_per_page && (i.value = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.items_per_page), n.params.push(D(i));
                    var l = {
                        type: "textfield",
                        heading: Drupal.t("Pager Offset"),
                        param_name: "offset",
                        description: Drupal.t("The number of items to skip."),
                        can_be_empty: !0,
                        dependency: {
                            element: "override_pager",
                            value: ["yes"]
                        }
                    };
                    if (drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.offset && (l.value = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.offset), n.params.push(D(l)), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].contextual_filter && n.params.push(D({
                            type: "textfield",
                            heading: Drupal.t("Contextual filter:"),
                            param_name: "contextual_filter",
                            description: Drupal.t('Separate contextual filter values with a "/". For example, 40/12/10.'),
                            can_be_empty: !0
                        })), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].use_fields) {
                        var s = {
                            type: "checkboxes",
                            heading: Drupal.t("Field settings"),
                            param_name: "toggle_fields",
                            value: {},
                            tab: Drupal.t("Toggle Fields")
                        };
                        for (var r in drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_list) {
                            var d = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_list[r];
                            s.value[r] = d
                        }
                        "" != drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_values && (s.content = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_values), n.params.push(D(s))
                    }
                }
                n.params = n.params.concat(o.prototype.params), f(o.prototype, n)
            }
            this.cms_elements_loaded = !0, J(), this.try_render_unknown_elements()
        },
        create_element: function(e, t, a, n) {
            if (e.get_nested_depth(t) < T.prototype.max_nested_depth) {
                var i = T.prototype.elements[t];
                if (e instanceof W && null == e.parent && !i.prototype.section) {
                    var o = new L(e, a);
                    o.update_dom(), n(l = new i(o, !1)), l.update_dom(), e.update_empty(), o.update_empty()
                } else {
                    var l;
                    n(l = new i(e, a)), l.update_dom(), e.update_empty()
                }
                return l
            }
            return alert(Drupal.t("Element can not be added. Max nested depth reached.")), !1
        },
        make_elements_modal: function(o, a) {
            var e = o.get_all_disallowed_elements(),
                t = {};
            for (var n in T.prototype.elements)
                if (!T.prototype.elements[n].prototype.hidden && !("profile" in window.drupalSettings.glazedBuilder && T.prototype.elements[n].prototype.base in window.drupalSettings.glazedBuilder.profile.hide_els || "az_popup" != o.base && 0 <= e.indexOf(T.prototype.elements[n].prototype.base) || "Template-elements" == T.prototype.elements[n].prototype.category))
                    if ("CMS" == T.prototype.elements[n].prototype.category) {
                        var i = T.prototype.elements[n].prototype.name.match(/^Block/) ? "Blocks" : "Views";
                        T.prototype.elements[n].prototype.base.indexOf("block-views") < 0 && (i in t || (t[i] = []), t[i].push(T.prototype.elements[n]))
                    } else T.prototype.elements[n].prototype.category in t || (t[T.prototype.elements[n].prototype.category] = []), t[T.prototype.elements[n].prototype.category].push(T.prototype.elements[n]);
            var l = b('<div id="az-elements-tabs"></div>'),
                s = 0,
                r = '<ul class="nav nav-tabs">';
            for (var d in t) s++, "" === d && (d = Drupal.t("Content")), r += '<li><a href="#az-elements-tab-' + s + '" data-toggle="tab">' + d + "</a></li>";
            window.glazedBuilder.glazed_online && (r += '<li><a href="#az-elements-tab-templates" data-toggle="tab">' + Drupal.t("Saved Templates") + "</a></li>"), r += "</ul>", b(l).append(r), s = 0;
            var c = b('<div class="tab-content"></div>'),
                p = 0;
            for (var d in t) {
                var h = b('<div id="az-elements-tab-' + ++s + '" class="tab-pane clearfix"></div>');
                if ("Views" == d) {
                    p = s;
                    for (var u = 0; u < t[d].length; u++) b(h).append(t[d][u].prototype.get_button_with_tag())
                } else
                    for (u = 0; u < t[d].length; u++) b(h).append(t[d][u].prototype.get_button());
                b(c).append(h)
            }
            var m = [];
            for (var _ in drupalSettings.glazedBuilder.viewsTags) - 1 == m.indexOf(drupalSettings.glazedBuilder.viewsTags[_]) && m.push(drupalSettings.glazedBuilder.viewsTags[_]);
            var g = "<div class='filter-tags'><label>" + Drupal.t("Filter by tag") + "</label><select><option value='all_views'>" + Drupal.t("show all") + "</option>";
            for (var _ in m) g = g + '<option value="' + m[_] + '">' + m[_].replace(/_/g, " ") + "</option>";
            (g = b(g += "</select></div>")).find("select").bind("change", function() {
                var e = this.options[this.selectedIndex].value;
                c.find("#az-elements-tab-" + p + " .well").trigger("filtredData", e)
            }), c.find("#az-elements-tab-" + p + " .well").bind("filtredData", function(e, t) {
                var a = b(this);
                "all_views" == t ? a.show() : a.attr("data-az-tag") != t ? a.hide() : a.show()
            }), b(c).find("#az-elements-tab-" + p).prepend(g), window.glazedBuilder.glazed_online && (h = b('<div id="az-elements-tab-templates" class="tab-pane clearfix"></div>')), b(c).append(h), b(l).append(c), T.prototype.spawn_modal_skeleton();
            var f = b("#az-elements-modal");
            b(f).find(".modal-body").append(l), b(l).find("> ul a:first").tab("show"), b(f).find("[data-az-element]").click(function() {
                var e = b(this).attr("data-az-element"),
                    t = window.glazedBuilder.glazedElements.create_element(o, e, !1, a);
                t && (b("#az-elements-modal").modal("hide"), t.show_settings_on_create && t.edit())
            }), window.glazedBuilder.glazed_online && b(l).find('a[href="#az-elements-tab-templates"]').on("shown.bs.tab", function(e) {
                ! function(t) {
                    b.ajax({
                        type: "get",
                        url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        b.ajax({
                            type: "POST",
                            url: e,
                            data: {
                                action: "glazed_get_templates",
                                url: window.location.href
                            },
                            dataType: "json",
                            cache: !1,
                            context: this
                        }).done(function(e) {
                            t(e)
                        })
                    })
                }(function(e) {
                    var t = b(l).find("#az-elements-tab-templates");
                    b(t).empty();
                    for (var a = 0; a < e.length; a++) {
                        var n = e[a],
                            i = '<div class="well text-center pull-left text-overflow glazed-saved" data-az-template="' + n + '"><i class="glyphicon glyphicon-floppy-disk"></i><div>' + n + "</div></div>";
                        i = b(i).appendTo(t).click(function() {
                            v(b(this).attr("data-az-template"), function(e) {
                                var t = o.children.length;
                                T.prototype.parse_shortcode.call(o, e);
                                for (var a = t; a < o.children.length; a++) o.children[a].recursive_render();
                                for (a = t; a < o.children.length; a++) b(o.dom_content_element).append(o.children[a].dom_element);
                                drupalSettings.glazedBuilder.glazedEditor && (o.update_empty(), o.update_sortable()), o.recursive_showed(), b("#az-elements-modal").modal("hide")
                            })
                        }), b('<span class="fa fa-trash-o" data-az-template="' + n + '"></span>').appendTo(i).click(function() {
                            var e = b(this).attr("data-az-template");
                            z(e), b(t).find('[data-az-template="' + e + '"]').remove()
                        })
                    }
                })
            })
        },
        show: function(e, t) {
            b("#az-elements-modal").remove(), this.make_elements_modal(e, t), b("#az-elements-modal").modal("show"), b("#az-elements-modal #az-elements-tabs").find("> ul a:first").tab("show")
        },
        showTemplates: function(e, t) {
            b("#az-elements-modal").remove(), this.make_templates_modal(e, t), b("#az-elements-modal").modal("show"), b("#az-elements-modal #az-elements-tabs").find("> ul a:first").tab("show")
        },
        make_templates_modal: function(r, e) {
            var d = b('<div id="az-elements-tabs"></div>'),
                t = '<ul class="nav nav-tabs">';
            window.glazedBuilder.glazed_online && (t += '<li><a href="#az-elements-tab-templates" data-toggle="tab">' + Drupal.t("Layouts") + "</a></li>"), t += "</ul>", b(d).append(t);
            var a = b('<div class="tab-content"></div>');
            window.glazedBuilder.glazed_online && (tab = b('<div id="az-elements-tab-templates" class="tab-pane clearfix"></div>')), b(a).append(tab), b(d).append(a), T.prototype.spawn_modal_skeleton();
            var n = b("#az-elements-modal");
            b(n).find(".modal-body").append(d), window.glazedBuilder.glazed_online && function(t) {
                b.ajax({
                    type: "get",
                    url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    b.ajax({
                        type: "POST",
                        url: e,
                        data: {
                            action: "glazed_get_page_templates",
                            url: window.location.href
                        },
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        t(e)
                    })
                })
            }(function(e) {
                var t = b(d).find("#az-elements-tab-templates"),
                    a = [];
                a[0] = b('<div class="col-md-4"></div>'), a[1] = b('<div class="col-md-4"></div>'), a[2] = b('<div class="col-md-4"></div>');
                var n = 0;
                if (b(t).empty(), b.isArray(e))
                    for (var i = 0; i < e.length; i++) {
                        n = i % 3;
                        var o = e[i].title,
                            l = e[i].uuid,
                            s = b('<div class="page-template text-center pull-left text-overflow glazed-saved" data-az-template="' + l + '"><div class="lead">' + o + "</div></div>");
                        if ("" != e[i].image) b('<img class="template-image" src="' + e[i].image + '"></img>').appendTo(s);
                        else b('<i class="glyphicon glyphicon-floppy-disk"></i>').appendTo(s);
                        s.appendTo(a[n]).click(function() {
                            m(b(this).attr("data-az-template"), function(e) {
                                var t = r.children.length;
                                T.prototype.parse_shortcode.call(r, e);
                                for (var a = t; a < r.children.length; a++) r.children[a].recursive_render();
                                for (a = t; a < r.children.length; a++) b(r.dom_content_element).append(r.children[a].dom_element);
                                drupalSettings.glazedBuilder.glazedEditor && (r.update_empty(), r.update_sortable()), r.recursive_showed(), b("#az-elements-modal").modal("hide"), b(window).trigger("CKinlineAttach")
                            })
                        }), a[0].appendTo(t), a[1].appendTo(t), a[2].appendTo(t)
                    } else b(e).appendTo(t)
            })
        },
        get_element: function(e) {
            return this.elements_instances[e]
        },
        delete_element: function(e) {
            b(document).trigger("glazed_delete_element", e), delete this.elements_instances[e]
        },
        add_element: function(e, t, a) {
            this.elements_instances[e] = t, b(document).trigger("glazed_add_element", {
                id: e,
                position: a
            })
        }
    });
    var j, E = {};

    function I(e, t, a) {
        if (i(a, T), a.prototype.base = e, a.prototype.is_container = t, T.prototype.elements[e] = a, T.prototype.tags[e] = a, t)
            for (var n = 1; n < T.prototype.max_nested_depth; n++) T.prototype.tags[e + "_" + n] = a
    }

    function O(e, t) {
        return !(e.right < t.left || e.left > t.right || e.bottom < t.top || e.top > t.bottom)
    }

    function R(e, t) {
        R.baseclass.apply(this, arguments)
    }

    function U(e, t, a) {
        if (i(a, R), a.prototype.base = e, a.prototype.is_container = t, R.prototype.elements[e] = a, R.prototype.tags[e] = a, t)
            for (var n = 1; n < R.prototype.max_nested_depth; n++) R.prototype.tags[e + "_" + n] = a
    }

    function P(e) {
        if (0 < b(e).length) {
            var t = b(e).html();
            if (/^\s*\<[\s\S]*\>\s*$/.exec(t) || "" == t && drupalSettings.glazedBuilder.glazedAjaxUrl) {
                b(e).find("> script").detach().appendTo("head"), b(e).find("> link[href]").detach().appendTo("head"), (n = new W(null, !1)).attrs.container = b(e).attr("data-az-type") + "/" + b(e).attr("data-az-name"), n.attrs.langcode = b(e).attr("data-az-langcode"), b(e).attr("data-az-human-readable") && (n.attrs.human_readable = atob(b(e).attr("data-az-human-readable"))), n.dom_element = b(e), b(n.dom_element).attr("data-az-id", n.id), n.dom_content_element = b(e), b(n.dom_element).css("display", ""), b(n.dom_element).addClass("glazed"), b(n.dom_element).addClass("az-ctnr"), n.parse_html(n.dom_content_element), n.html_content = !0, n.loaded_container = n.attrs.container;
                for (var a = 0; a < n.children.length; a++) n.children[a].recursive_render();
                window.glazedBuilder.glazed_frontend || (n.dom_content_element.empty(), drupalSettings.glazedBuilder.glazedEditor && (n.show_controls(), n.update_sortable()), n.attach_children()), n.rendered = !0;
                for (a = 0; a < n.children.length; a++) n.children[a].recursive_showed()
            } else {
                var n;
                "" != t.replace(/^\s+|\s+$/g, "") && (window.glazedBuilder.glazed_containers_loaded[b(e).attr("data-az-type") + "/" + b(e).attr("data-az-name")] = t.replace(/^\s+|\s+$/g, "")), (n = new W(null, !1)).attrs.container = b(e).attr("data-az-type") + "/" + b(e).attr("data-az-name"), n.attrs.langcode = b(e).attr("data-az-langcode"), n.render(b);
                var i = b(n.dom_element).attr("class") + " " + b(e).attr("class");
                i = b.unique(i.split(" ")).join(" "), b(n.dom_element).attr("class", i), b(n.dom_element).attr("style", b(e).attr("style")), b(n.dom_element).css("display", ""), b(n.dom_element).addClass("glazed"), b(n.dom_element).addClass("az-ctnr");
                var o = b(e).attr("data-az-type"),
                    l = b(e).attr("data-az-name");
                b(e).replaceWith(n.dom_element), b(n.dom_element).attr("data-az-type", o), b(n.dom_element).attr("data-az-name", l), n.showed(b), drupalSettings.glazedBuilder.glazedEditor && n.show_controls()
            }
            return drupalSettings.glazedBuilder.glazedEditor && b(n.dom_element).addClass("glazed-editor"), n
        }
        return null
    }
    "drupalSettings" in window && "glazedBuilder" in window.drupalSettings && "glazedClasses" in window.drupalSettings.glazedBuilder && (E = drupalSettings.glazedBuilder.glazedClasses), T.prototype = {
            el_classes: b.extend({
                "optgroup-bootstrap": Drupal.t("Bootstrap classes"),
                "bg-default": Drupal.t("Background default style"),
                "bg-primary": Drupal.t("Background primary style"),
                "bg-success": Drupal.t("Background success style"),
                "center-block": Drupal.t("Block align center"),
                clearfix: Drupal.t("Clearfix"),
                "hidden-lg": Drupal.t("Hidden on large devices, desktops (≥1200px)"),
                "hidden-md": Drupal.t("Hidden on medium devices, desktops (≥992px)"),
                "hidden-sm": Drupal.t("Hidden on small devices, tablets (≥768px)"),
                "hidden-xs": Drupal.t("Hidden on extra small devices, phones (<768px)"),
                lead: Drupal.t("Text Lead style"),
                "pull-left": Drupal.t("Pull left"),
                "pull-right": Drupal.t("Pull right"),
                "text-center": Drupal.t("Text align center"),
                "text-default": Drupal.t("Text default style"),
                "text-justify": Drupal.t("Text align justify"),
                "text-left": Drupal.t("Text align left"),
                "text-muted": Drupal.t("Text muted style"),
                "text-primary": Drupal.t("Text primary style"),
                "text-right": Drupal.t("Text align right"),
                "text-success": Drupal.t("Text success style"),
                "visible-lg-block": Drupal.t("Visible on large devices, desktops (≥1200px)"),
                "visible-md-block": Drupal.t("Visible on medium devices, desktops (≥992px)"),
                "visible-sm-block": Drupal.t("Visible on small devices, tablets (≥768px)"),
                "visible-xs-block": Drupal.t("Visible on extra small devices, phones (<768px)"),
                well: Drupal.t("Well"),
                small: Drupal.t("Text small style"),
                "optgroup-glazed-shadows": Drupal.t("Drop Shadows"),
                "stpe-dropshadow stpe-dropshadow--curved-hz1 stpe-dropshadow--curved": Drupal.t("Curved Horiztonal Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--curved-hz2 stpe-dropshadow--curved": Drupal.t("Curved Horizontal Double Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--curved-vt2 stpe-dropshadow--curved": Drupal.t("Curved vertical double shadow"),
                "stpe-dropshadow stpe-dropshadow--lifted": Drupal.t("Lifted Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--perspective": Drupal.t("Perspective Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--raised": Drupal.t("Raised Drop Shadow")
            }, E),
            elements: {},
            tags: {},
            max_nested_depth: 3,
            name: "",
            category: "",
            description: "",
            params: [D({
                type: "textfield",
                heading: Drupal.t("Utility classes"),
                param_name: "el_class",
                description: Drupal.t("Add classes for Bootstrap effects or Glazed theme colors and utilities.")
            }), D({
                type: "style",
                heading: Drupal.t("Style"),
                param_name: "style",
                description: Drupal.t("Style options."),
                tab: Drupal.t("Style")
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Drop shadow"),
                param_name: "shadow",
                max: "5",
                value: "0",
                tab: Drupal.t("Style")
            }), D({
                type: "style",
                heading: Drupal.t("Hover style"),
                param_name: "hover_style",
                important: !0,
                description: Drupal.t("Hover style options."),
                tab: Drupal.t("Hover style")
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Drop shadow"),
                param_name: "hover_shadow",
                max: "5",
                value: "0",
                tab: Drupal.t("Hover style")
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Left"),
                param_name: "pos_left",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Right"),
                param_name: "pos_right",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Top"),
                param_name: "pos_top",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Bottom"),
                param_name: "pos_bottom",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Width"),
                param_name: "pos_width",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Height"),
                param_name: "pos_height",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Z-index"),
                param_name: "pos_zindex",
                tab: Drupal.t("Placement"),
                hidden: !0
            })],
            icon: "",
            thumbnail: "",
            is_container: !1,
            has_content: !1,
            frontend_render: !1,
            show_settings_on_create: !1,
            wrapper_class: "",
            weight: 0,
            hidden: !1,
            disallowed_elements: [],
            controls_base_position: "center",
            show_parent_controls: !1,
            highlighted: !0,
            style_selector: "",
            section: !1,
            controls_position: function(e) {
                if (this.dom_element) {
                    var t = this.dom_element[0].getBoundingClientRect();
                    if (e || 0 < t.bottom && t.top < document.documentElement.clientHeight) {
                        var a = this.controls[0].getBoundingClientRect();
                        if (0 < this.children.length && _.has(this.children[0].controls, "0") && !this.children[0].show_parent_controls) O(this.children[0].controls[0].getBoundingClientRect(), a) && this.children[0].dom_element.addClass("az-element--controls-spacer");
                        else if (_.has(this, "parent") && _.has(this.parent.controls, "0") && !this.show_parent_controls) {
                            if (O(this.parent.controls[0].getBoundingClientRect(), a) && b(this.dom_element).addClass("az-element--controls-spacer"), this.parent.show_parent_controls) O(this.parent.parent.controls[0].getBoundingClientRect(), a) && b(this.dom_element).addClass("az-element--controls-spacer")
                        }
                        if (t.top + b(window).scrollTop() < 300) {
                            var n;
                            if (0 < b("body.body--glazed-header-navbar_pull_down #navbar").length) O(n = b("#navbar .container-col")[0].getBoundingClientRect(), a) && b(this.dom_element).closest(".glazed-editor").css("margin-top", n.height / 2);
                            if (0 < b("body.body--glazed-header-overlay #navbar").length) O(n = b("#navbar")[0].getBoundingClientRect(), a) && this.controls.css("margin-top", n.height + 32);
                            if (0 < b("#navbar.glazed-header--fixed").length) O(n = b("#navbar")[0].getBoundingClientRect(), a) && this.controls.css("margin-top", n.height + 32)
                        }
                        if (!this.is_container || this.has_content) {
                            var i = b(this.dom_element).height(),
                                o = b(window).height();
                            if (o < i) {
                                var l = b(window).scrollTop(),
                                    s = (this.controls.offset().top, l - b(this.dom_element).offset().top + o / 2);
                                40 < s && s < i ? this.controls.css("top", s) : i < s ? this.controls.css("top", i - 40) : this.controls.css("top", 40)
                            }
                        }
                    }
                }
            },
            update_controls_zindex: function() {
                c(this.controls)
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    var e = this;
                    if (this.controls = b('<div class="controls btn-group btn-group-xs"></div>').prependTo(this.dom_element), b(this.dom_element).addClass("az-element--controls-" + this.controls_base_position), this.show_parent_controls && b(this.dom_element).addClass("az-element--controls-show-parent"), setTimeout(function() {
                            e.update_controls_zindex()
                        }, 1e3), b('<span title="' + r("Drag and drop") + '" class="control drag-and-drop btn btn-default glyphicon glyphicon-move"><span class="control-label">' + this.name + "</span></span>").appendTo(this.controls), this.is_container && !this.has_content && (b('<span title="' + r("Add") + '" class="control add btn btn-default glyphicon glyphicon-plus"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_add), b('<span title="' + r("Paste") + '" class="control paste btn btn-default glyphicon glyphicon-hand-down"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_paste)), b('<span title="' + r("Edit") + '" class="control edit btn btn-default glyphicon glyphicon-pencil"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_edit), b('<span title="' + r("Copy") + '" class="control copy btn btn-default glyphicon glyphicon-briefcase"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_copy), b('<span title="' + r("Clone") + '" class="control clone btn btn-default glyphicon glyphicon-duplicate"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_clone), b('<span title="' + r("Remove") + '" class="control remove btn btn-default glyphicon glyphicon-trash"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_remove), window.glazedBuilder.glazed_online && b('<span title="' + r("Save as template") + '" class="control save-template btn btn-default glyphicon glyphicon-floppy-save"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_save_template), this.update_empty(), 0 < e.children.length && e.children[0].show_parent_controls || b(e.dom_element).hover(function() {
                            e.controls.addClass("controls--show")
                        }, function() {
                            e.controls.removeClass("controls--show")
                        }), setTimeout(function() {
                            e.controls_position(!0)
                        }, 1500), b(window).scroll(_.debounce(function() {
                            30 < window.pageYOffset && e.controls_position(!1)
                        }, 500)), b(window).resize(_.debounce(function() {
                            e.controls_position(!0)
                        }, 500)), e.show_parent_controls) {
                        var a = e.parent;
                        _.isString(e.show_parent_controls) && (a = window.glazedBuilder.glazedElements.get_element(b(e.dom_element).closest(e.show_parent_controls).attr("data-az-id"))), b(e.dom_element).off("mouseenter").on("mouseenter", function() {
                            b(e.dom_element).data("hover", !0), 0 < b(e.dom_element).parents(".glazed-editor").length && (b(a.controls).addClass("controls--show"), e.controls.addClass("controls--show"), function(e) {
                                e.controls_position(!0), b(a.controls).attr("data-az-cid", b(e.dom_element).attr("data-az-id"));
                                var t = b(e.dom_element).offset();
                                t.top = t.top - parseInt(b(e.dom_element).css("margin-top")) + parseInt(b(a.controls).css("margin-top")), b(a.controls).offset(t), t.left = t.left + b(a.controls).width() - 1, e.controls.offset(t)
                            }(e))
                        }), b(e.dom_element).off("mouseleave").on("mouseleave", function() {
                            b(e.dom_element).data("hover", !1), 0 < b(e.dom_element).parents(".glazed-editor").length && (b(a.controls).removeClass("controls--show"), e.controls.removeClass("controls--show"))
                        }), setTimeout(function() {
                            b(a.controls).off("mouseenter").on("mouseenter", function() {
                                b(a.controls).data("hover", !0);
                                var e = window.glazedBuilder.glazedElements.get_element(b(this).closest("[data-az-cid]").attr("data-az-cid"));
                                _.isUndefined(e) || b(e.controls).addClass("controls--show")
                            }), b(a.controls).off("mouseleave").on("mouseleave", function() {
                                b(a.controls).data("hover", !1);
                                var e = window.glazedBuilder.glazedElements.get_element(b(this).closest("[data-az-cid]").attr("data-az-cid"));
                                _.isUndefined(e) || b(e.controls).removeClass("controls--show")
                            })
                        }, 100)
                    }
                }
            },
            get_empty: function() {
                return '<div class="az-empty"></div>'
            },
            update_empty: function() {
                if (drupalSettings.glazedBuilder.glazedEditor)
                    if (0 == this.children.length && this.is_container && !this.has_content || this.has_content && "" == this.attrs.content) {
                        b(this.dom_content_element).find("> .az-empty").remove();
                        var e = b(this.get_empty()).appendTo(this.dom_content_element);
                        0 == b(e).find(".bottom").length && "bottom", 0 == b(e).find(".top").length && "top", b(e).click(function(e) {
                            if (1 == e.which) {
                                var t = b(this).closest("[data-az-id]").attr("data-az-id");
                                window.glazedBuilder.glazedElements.show(window.glazedBuilder.glazedElements.get_element(t), function(e) {})
                            }
                        })
                    } else b(this.dom_content_element).find("> .az-empty").remove()
            },
            get_button: function() {
                return "" == this.thumbnail ? '<div class="well text-center pull-left text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>" : '<div class="well pull-left" data-az-element="' + this.base + '" style="background-image: url(' + encodeURI(this.thumbnail) + '); background-position: center center; background-size: cover;"></div>'
            },
            spawn_modal_skeleton: function() {
                b("#az-elements-modal").remove();
                b('<div id="az-elements-modal" class="modal glazed"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><span class="close" data-dismiss="modal">&times;</span><h4 class="modal-title"><img src="' + drupalSettings.glazedBuilder.glazedBaseUrl + 'images/glazed-logo-white.svg"></h4></div><div class="modal-body"></div></div></div></div>').prependTo("body")
            },
            click_add: function(e) {
                return e.data.object.add(), !1
            },
            add: function() {
                window.glazedBuilder.glazedElements.show(this, function(e) {})
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && (this.is_container && !this.has_content || this instanceof $) && b(this.dom_content_element).sortable({
                    items: "> .az-element",
                    connectWith: ".az-ctnr",
                    handle: "> .controls > .drag-and-drop",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    scrollSpeed: 100,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            replace_render: function() {
                var e = this.dom_element,
                    t = this.dom_content_element;
                null != e && (this.render(b), b(e).replaceWith(this.dom_element), null != t && b(this.dom_content_element).replaceWith(t)), drupalSettings.glazedBuilder.glazedEditor && this.show_controls()
            },
            update_dom: function() {
                this.detach_children(), b(this.dom_element).remove(), this.parent.detach_children(), this.render(b), this.attach_children(), drupalSettings.glazedBuilder.glazedEditor && this.show_controls(), this.parent.attach_children(), drupalSettings.glazedBuilder.glazedEditor && (this.update_sortable(), this.update_empty()), this.showed(b)
            },
            get_el_classes: function() {
                return this.attrs.el_class
            },
            get_content_classes: function() {
                var e = "";
                return 0 < this.attrs.shadow && (e = "glazed-shadow-" + this.attrs.shadow), 0 < this.attrs.hover_shadow && (e = e + " glazed-shadow-hover-" + this.attrs.hover_shadow), e
            },
            get_hover_style: function() {
                return "hover_style" in this.attrs ? "<style>\x3c!-- .hover-style-" + this.id + ":hover " + this.style_selector + " { " + this.attrs.hover_style + "} --\x3e</style>" : ""
            },
            restore: function(e) {},
            recursive_restore: function(e) {
                for (var t = 0; t < this.children.length; t++) this.children[t].recursive_restore(e);
                this.restore(e)
            },
            showed: function(e) {
                "pos_left" in this.attrs && "" != this.attrs.pos_left && e(this.dom_element).css("left", this.attrs.pos_left), "pos_right" in this.attrs && "" != this.attrs.pos_right && e(this.dom_element).css("right", this.attrs.pos_right), "pos_top" in this.attrs && "" != this.attrs.pos_top && e(this.dom_element).css("top", this.attrs.pos_top), "pos_bottom" in this.attrs && "" != this.attrs.pos_bottom && e(this.dom_element).css("bottom", this.attrs.pos_bottom), "pos_width" in this.attrs && "" != this.attrs.pos_width && e(this.dom_element).css("width", this.attrs.pos_width), "pos_height" in this.attrs && "" != this.attrs.pos_height && e(this.dom_element).css("height", this.attrs.pos_height), "pos_zindex" in this.attrs && "" != this.attrs.pos_zindex && e(this.dom_element).css("z-index", this.attrs.pos_zindex), "hover_style" in this.attrs && "" != this.attrs.hover_style && (e("head").find("#hover-style-" + this.id).remove(), e("head").append(this.get_hover_style()), e(this.dom_element).addClass("hover-style-" + this.id))
            },
            render: function(e) {
                e(this.dom_element).attr("data-az-id", this.id)
            },
            trigger_start_in_animation: function() {
                for (var e = 0; e < this.children.length; e++) "trigger_start_in_animation" in this.children[e] && this.children[e].trigger_start_in_animation()
            },
            trigger_start_out_animation: function() {
                for (var e = 0; e < this.children.length; e++) "trigger_start_out_animation" in this.children[e] && this.children[e].trigger_start_out_animation()
            },
            update_data: function() {
                b(this.dom_element).attr("data-azb", this.base);
                for (var e = 0; e < this.params.length; e++) {
                    var t = this.params[e];
                    if (t.param_name in this.attrs) {
                        var a = this.attrs[t.param_name];
                        ("" == a && t.can_be_empty || "" != a) && "content" !== t.param_name && a !== t.value && (t.safe || (a = encodeURIComponent(a)), b(this.dom_element).attr("data-azat-" + t.param_name, a))
                    }
                }
                null != this.dom_content_element && b(this.dom_content_element).attr("data-azcnt", "true")
            },
            recursive_update_data: function() {
                this.update_data();
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_update_data()
            },
            recursive_clear_animation: function() {
                "clear_animation" in this && this.clear_animation();
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_clear_animation()
            },
            recursive_showed: function() {
                this.showed(b);
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_showed()
            },
            update_sorting_children: function() {
                var e = b(this.dom_content_element).sortable("option"),
                    t = [];
                b(this.dom_content_element).find(e.items).each(function() {
                    t.push(window.glazedBuilder.glazedElements.get_element(b(this).attr("data-az-id")))
                }), this.children = t.filter(Boolean);
                for (var a = 0; a < this.children.length; a++) this.children[a].parent = this;
                this.update_empty()
            },
            update_sorting: function(e, t) {
                var a = window.glazedBuilder.glazedElements.get_element(b(t.item).closest("[data-az-id]").attr("data-az-id"));
                a && (t.source = window.glazedBuilder.glazedElements.get_element(b(this).closest("[data-az-id]").attr("data-az-id")), t.from_pos = a.get_child_position(), t.source.update_sorting_children(), t.target = window.glazedBuilder.glazedElements.get_element(b(t.item).parent().closest("[data-az-id]").attr("data-az-id")), t.source.id != t.target.id && t.target.update_sorting_children(), t.to_pos = a.get_child_position(), b(document).trigger("glazed_update_sorting", t))
            },
            click_edit: function(e) {
                return X(e), e.data.object.edit(), !1
            },
            edit: function() {
                w.prototype.show_editor(this.params, this, this.edited)
            },
            edited: function(e) {
                for (var t in e) this.attrs[t] = o(e[t]);
                this.update_dom(), b(document).trigger("glazed_edited_element", this.id)
            },
            attrs2string: function() {
                for (var e = "", t = 0; t < this.params.length; t++) {
                    var a = this.params[t];
                    if (a.param_name in this.attrs) {
                        var n = this.attrs[a.param_name];
                        ("" == n && a.can_be_empty || "" != n) && "content" !== a.param_name && n !== a.value && (a.safe || (n = encodeURIComponent(n)), e += a.param_name + '="' + n + '" ')
                    }
                }
                return e
            },
            get_content: function() {
                for (var e = 0; e < this.params.length; e++) {
                    var t = this.params[e];
                    if ("content" === t.param_name && "html" == t.type) return encodeURIComponent(this.attrs.content)
                }
                return this.attrs.content
            },
            set_content: function(e) {
                for (var t = o(e), a = 0; a < this.params.length; a++) {
                    var n = this.params[a];
                    if ("content" === n.param_name && "html" == n.type) {
                        try {
                            t = decodeURIComponent(atob(t.replace(/^#E\-8_/, "")))
                        } catch (e) {
                            t = decodeURIComponent(t.replace(/^#E\-8_/, ""))
                        }
                        return void(this.attrs.content = t)
                    }
                }
                this.attrs.content = t
            },
            parse_attrs: function(e) {
                for (var t = 0; t < this.params.length; t++) {
                    var a = this.params[t];
                    if (a.param_name in e)
                        if (a.safe) this.attrs[a.param_name] = o(e[a.param_name]);
                        else {
                            var n = o(e[a.param_name]);
                            try {
                                this.attrs[a.param_name] = decodeURIComponent(atob(n.replace(/^#E\-8_/, "")))
                            } catch (e) {
                                this.attrs[a.param_name] = decodeURIComponent(n.replace(/^#E\-8_/, ""))
                            }
                        } else "value" in a && _.isString(a.value) && (this.attrs[a.param_name] = a.value)
                }
                for (var i in e) i in this.attrs || (this.attrs[i] = e[i]);
                b(document).trigger("glazed_edited_element", this.id)
            },
            get_nested_depth: function(e) {
                var t = 0;
                return null != this.parent && (t += this.parent.get_nested_depth(e)), this.base == e && t++, t
            },
            get_my_shortcode: function() {
                var e = _.keys(T.prototype.elements),
                    t = _.object(e, Array.apply(null, new Array(e.length)).map(Number.prototype.valueOf, 0));
                return this.get_shortcode(t)
            },
            get_children_shortcode: function() {
                for (var e = _.keys(T.prototype.elements), t = _.object(e, Array.apply(null, new Array(e.length)).map(Number.prototype.valueOf, 0)), a = "", n = 0; n < this.children.length; n++) a += this.children[n].get_shortcode(t);
                return a
            },
            get_shortcode: function(e) {
                e[this.base]++;
                for (var t = "", a = 0; a < this.children.length; a++) t += this.children[a].get_shortcode(e);
                if ("az_unknown" == this.base) o = t;
                else {
                    var n = "";
                    if (1 == e[this.base]) n = this.base;
                    else {
                        var i = e[this.base] - 1;
                        n = this.base + "_" + i
                    }
                    var o = "[" + n + " " + this.attrs2string() + "]";
                    this.is_container && (this.has_content ? o += this.get_content() + "[/" + n + "]" : o += t + "[/" + n + "]")
                }
                return e[this.base]--, o
            },
            parse_shortcode: function(d) {
                var c = _.keys(T.prototype.tags).join("|"),
                    e = g.shortcode.regexp(c),
                    t = b.trim(d).match(e);
                if (_.isNull(t)) {
                    if (0 == d.length) return;
                    "[" == d.substring(0, 1) && "]" == d.slice(-1) ? this.parse_shortcode("[az_unknown]" + d + "[/az_unknown]") : this.parse_shortcode('[az_row][az_column width="1/1"][az_text]' + d + "[/az_text][/az_column][/az_row]")
                }
                _.each(t, function(e) {
                    var t = e.match(p(c)),
                        a = t[5],
                        n = new RegExp("^[\\s]*\\[\\[?(" + _.keys(T.prototype.tags).join("|") + ")(?![\\w-])"),
                        i = g.shortcode.attrs(t[3]),
                        o = t[2];
                    if (!(this.get_nested_depth(o) > T.prototype.max_nested_depth)) {
                        var l = $;
                        if (o in T.prototype.tags && (l = T.prototype.tags[o]), this instanceof W && null == this.parent && !l.prototype.section) this.parse_shortcode("[az_section]" + d + "[/az_section]");
                        else {
                            var s = new l(this, !0);
                            s.parse_attrs(i.named);
                            var r = T.prototype.tags[o].prototype;
                            _.isString(a) && a.match(n) && !0 === r.is_container ? s.parse_shortcode(a) : _.isString(a) && a.length && "az_row" === o ? s.parse_shortcode('[az_column width="1/1"][az_text]' + a + "[/az_text][/az_column]") : _.isString(a) && a.length && "az_column" === o && ("[" != a.substring(0, 1) || "]" != a.slice(-1)) ? s.parse_shortcode("[az_text]" + a + "[/az_text]") : _.isString(a) && (!0 === r.has_content ? s.set_content(a) : "" != a && s.parse_shortcode("[az_unknown]" + a + "[/az_unknown]"))
                        }
                    }
                }, this)
            },
            parse_html: function(e) {
                var l = this;
                if (0 == b(e).children().closest_descendents("[data-azb]").length && 0 < b.trim(b(e).html()).length) {
                    var t = new N(l, !1);
                    t.children = [];
                    var a = new q(t, !1),
                        n = new T.prototype.elements.az_text(a, !1);
                    n.attrs.content = b(e).html(), n.update_dom(), "update_empty" in l && l.update_empty(), "update_empty" in a && a.update_empty(), "update_empty" in t && t.update_empty()
                } else b(e).children().closest_descendents("[data-azb]").each(function() {
                    var e = b(this).attr("data-azb"),
                        t = $;
                    e in T.prototype.tags && (t = T.prototype.tags[e]);
                    var a = new t(l, !0);
                    window.glazedBuilder.glazed_frontend && (window.glazedBuilder.glazedElements.elements_instances[a.id] = null, delete window.glazedBuilder.glazedElements.elements_instances[a.id], a.id = b(this).attr("data-az-id"), window.glazedBuilder.glazedElements.elements_instances[a.id] = a), a.dom_element = b(this);
                    var n = {};
                    if (b(b(this)[0].attributes).each(function() {
                            0 <= this.nodeName.indexOf("data-azat") && (n[this.nodeName.replace("data-azat-", "")] = this.value)
                        }), a.parse_attrs(n), a.is_container) {
                        var i = b(this).closest_descendents("[data-azcnt]");
                        if (0 < i.length)
                            if (a.dom_content_element = b(i), a.has_content)
                                if (a instanceof $) a.attrs.content = b(i).wrap("<div></div>").parent().html(), b(i).unwrap();
                                else {
                                    var o = a.dom_element[0].getAttribute("data-az-persist");
                                    "az_html" == a.base && null != o && 0 < o.length ? a.attrs.content = window.glazedBuilder.atobUTF16(o) : a.attrs.content = b(i).html()
                                } else a.parse_html(i)
                    }
                })
            },
            recursive_render: function() {
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_render();
                window.glazedBuilder.glazed_frontend ? this.frontend_render && (this.detach_children(), this.parent.detach_children(), this.render(b), this.attach_children(), this.parent.attach_children()) : (this.render(b), this.attach_children()), drupalSettings.glazedBuilder.glazedEditor && (this.show_controls(), this.update_sortable())
            },
            detach_children: function() {
                for (var e = 0; e < this.children.length; e++) b(this.children[e].dom_element).detach()
            },
            attach_children: function() {
                for (var e = 0; e < this.children.length; e++) b(this.dom_content_element).append(this.children[e].dom_element)
            },
            click_copy: function(e) {
                return e.data.object.copy(), !1
            },
            copy: function() {
                var e = this.get_my_shortcode();
                b("#glazed-clipboard").html(encodeURIComponent(e))
            },
            click_paste: function(e) {
                return e.data.object.paste(0), !1
            },
            paste: function(e) {
                var t = decodeURIComponent(b("#glazed-clipboard").html());
                if ("" != t) {
                    var a = this.children.length;
                    T.prototype.parse_shortcode.call(this, t);
                    for (var n = [], i = a; i < this.children.length; i++) this.children[i].recursive_render(), n.push(this.children[i]);
                    this.children = this.children.slice(0, a), this.children.splice.apply(this.children, [e, 0].concat(n)), this.detach_children(), this.attach_children(), this.update_empty(), this.update_sortable(), this.recursive_showed()
                }
            },
            click_save_template: function(e) {
                return e.data.object.save_template(), !1
            },
            save_template: function() {
                var e = this.get_my_shortcode(),
                    t = window.prompt(Drupal.t("Enter template name"), "");
                "" != t && null != t && function(t, a) {
                    b.ajax({
                        type: "get",
                        url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        b.ajax({
                            type: "POST",
                            url: e,
                            data: {
                                action: "glazed_save_template",
                                url: window.location.href,
                                name: t,
                                template: a
                            },
                            cache: !1,
                            context: this
                        }).done(function(e) {})
                    })
                }(t, e)
            },
            click_clone: function(e) {
                return X(e), e.data.object.clone(), !1
            },
            clone: function() {
                this.copy();
                for (var e = 0; e < this.parent.children.length; e++)
                    if (this.parent.children[e].id == this.id) {
                        this.parent.paste(e);
                        break
                    }
                b(window).trigger("CKinlineAttach")
            },
            click_remove: function(e) {
                return e.data.object.remove(), !1
            },
            remove: function() {
                window.glazedBuilder.glazedElements.delete_element(this.id);
                for (var e = 0; e < this.children.length; e++) this.children[e].remove();
                b(this.dom_element).remove();
                for (e = 0; e < this.parent.children.length; e++)
                    if (this.parent.children[e].id == this.id) {
                        this.parent.children.splice(e, 1);
                        break
                    }
                this.parent.update_empty()
            },
            get_child_position: function() {
                for (var e = 0; e < this.parent.children.length; e++)
                    if (this.parent.children[e].id == this.id) return e;
                return -1
            },
            add_css: function(e, t, a) {
                this.get_my_container().css[drupalSettings.glazedBuilder.glazedBaseUrl + e] = !0, t || window.glazedBuilder.glazed_add_css(e, a)
            },
            add_js_list: function(e) {
                for (var t = this.get_my_container(), a = 0; a < e.paths.length; a++) t.js[drupalSettings.glazedBuilder.glazedBaseUrl + e.paths[a]] = !0;
                window.glazedBuilder.glazed_add_js_list(e)
            },
            add_js: function(e) {
                this.get_my_container().js[drupalSettings.glazedBuilder.glazedBaseUrl + e.path] = !0, window.glazedBuilder.glazed_add_js(e)
            },
            add_external_js: function(e, t) {
                this.get_my_container().js[e] = !0, window.glazedBuilder.glazed_add_external_js(e, t)
            },
            get_my_container: function() {
                return this instanceof W ? this : this.parent.get_my_container()
            },
            get_all_disallowed_elements: function() {
                return "parent" in this ? _.uniq(this.parent.get_all_disallowed_elements().concat(this.disallowed_elements)) : this.disallowed_elements
            }
        }, i(R, T), f(R.prototype, {
            params: [D({
                type: "dropdown",
                heading: Drupal.t("Animation start"),
                param_name: "an_start",
                tab: Drupal.t("Animation"),
                value: {
                    "": Drupal.t("No animation"),
                    appear: Drupal.t("On appear"),
                    hover: Drupal.t("On hover"),
                    click: Drupal.t("On click"),
                    trigger: Drupal.t("On trigger")
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Animation in"),
                param_name: "an_in",
                tab: Drupal.t("Animation"),
                value: window.glazedBuilder.glazed_animations,
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Animation out"),
                param_name: "an_out",
                tab: Drupal.t("Animation"),
                value: window.glazedBuilder.glazed_animations,
                dependency: {
                    element: "an_start",
                    value: ["hover", "trigger"]
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Hidden"),
                param_name: "an_hidden",
                tab: Drupal.t("Animation"),
                value: {
                    before_in: Drupal.t("Before in-animation"),
                    after_in: Drupal.t("After in-animation")
                },
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Infinite"),
                param_name: "an_infinite",
                tab: Drupal.t("Animation"),
                value: {
                    yes: Drupal.t("Yes")
                },
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Appear Boundary"),
                param_name: "an_offset",
                tab: Drupal.t("Animation"),
                max: "100",
                description: Drupal.t("In percent. (50% is center, 100% is bottom of screen)"),
                value: "100",
                dependency: {
                    element: "an_start",
                    value: ["appear"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Duration"),
                param_name: "an_duration",
                tab: Drupal.t("Animation"),
                max: "3000",
                description: Drupal.t("In milliseconds."),
                value: "1000",
                step: "50",
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("In-delay"),
                param_name: "an_in_delay",
                tab: Drupal.t("Animation"),
                max: "10000",
                description: Drupal.t("In milliseconds."),
                value: "0",
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Out-delay"),
                param_name: "an_out_delay",
                tab: Drupal.t("Animation"),
                max: "10000",
                description: Drupal.t("In milliseconds."),
                value: "0",
                dependency: {
                    element: "an_start",
                    value: ["hover", "trigger"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Parent number"),
                param_name: "an_parent",
                tab: Drupal.t("Animation"),
                max: "10",
                min: "0",
                description: Drupal.t("Define the number of Parent Containers the animation should attempt to break away from."),
                value: "1",
                dependency: {
                    element: "an_start",
                    value: ["hover", "click"]
                }
            }), D({
                type: "textfield",
                heading: Drupal.t("Name for animations"),
                param_name: "an_name",
                hidden: !0
            })].concat(R.prototype.params),
            set_in_timeout: function() {
                var e = this;
                e.in_timeout = setTimeout(function() {
                    e.clear_animation(), b(e.dom_element).css("opacity", ""), b(e.dom_element).removeClass("animated"), b(e.dom_element).removeClass(e.attrs.an_in), b(e.dom_element).removeClass(e.attrs.an_out), e.animation_in = !1, e.animation_out = !1, b(e.dom_element).css("animation-duration", e.attrs.an_duration + "ms"), b(e.dom_element).css("-webkit-animation-duration", e.attrs.an_duration + "ms"), b(e.dom_element).addClass("animated"), e.animated = !0, "yes" == e.attrs.an_infinite && b(e.dom_element).addClass("infinite"), b(e.dom_element).addClass(e.attrs.an_in), e.animation_in = !0
                }, Math.round(e.attrs.an_in_delay))
            },
            start_in_animation: function() {
                var e = this;
                0 == b(e.dom_element).parents(".glazed-animations-disabled").length && "" != e.attrs.an_in && (e.animated ? e.animation_out ? e.set_in_timeout() : 0 < e.out_timeout && (clearTimeout(e.out_timeout), e.hidden_after_in || e.set_in_timeout()) : e.set_in_timeout())
            },
            set_out_timeout: function() {
                var e = this;
                e.out_timeout = setTimeout(function() {
                    e.clear_animation(), b(e.dom_element).css("opacity", ""), b(e.dom_element).removeClass("animated"), b(e.dom_element).removeClass(e.attrs.an_in), b(e.dom_element).removeClass(e.attrs.an_out), e.animation_in = !1, e.animation_out = !1, b(e.dom_element).css("animation-duration", e.attrs.an_duration + "ms"), b(e.dom_element).css("-webkit-animation-duration", e.attrs.an_duration + "ms"), b(e.dom_element).addClass("animated"), e.animated = !0, "yes" == e.attrs.an_infinite && b(e.dom_element).addClass("infinite"), b(e.dom_element).addClass(e.attrs.an_out), e.animation_out = !0
                }, Math.round(e.attrs.an_out_delay))
            },
            start_out_animation: function() {
                var e = this;
                0 == b(e.dom_element).parents(".glazed-animations-disabled").length && "" != e.attrs.an_out && (e.animated ? e.animation_in ? e.set_out_timeout() : 0 < e.in_timeout && (clearTimeout(e.in_timeout), e.hidden_before_in || e.set_out_timeout()) : e.set_out_timeout())
            },
            clear_animation: function() {
                this.animation_in && (this.hidden_before_in && b(this.dom_element).css("opacity", "1"), this.hidden_after_in && b(this.dom_element).css("opacity", "0")), this.animation_out && (this.hidden_before_in && b(this.dom_element).css("opacity", "0"), this.hidden_after_in && b(this.dom_element).css("opacity", "1")), b(this.dom_element).hasClass("animated") && (b(this.dom_element).css("animation-duration", ""), b(this.dom_element).css("-webkit-animation-duration", ""), b(this.dom_element).removeClass("animated"), this.animated = !1, b(this.dom_element).removeClass("infinite"), b(this.dom_element).removeClass(this.attrs.an_in), b(this.dom_element).removeClass(this.attrs.an_out), this.animation_in = !1, this.animation_out = !1)
            },
            end_animation: function() {
                this.in_timeout = 0, this.out_timeout = 0, this.animation_in && (this.clear_animation(), "hover" != this.attrs.an_start || this.hover || this.attrs.an_in != this.attrs.an_out && this.start_out_animation()), this.animation_out && (this.clear_animation(), "hover" == this.attrs.an_start && this.hover && this.attrs.an_in != this.attrs.an_out && this.start_in_animation())
            },
            trigger_start_in_animation: function() {
                "trigger" == this.attrs.an_start ? this.start_in_animation() : R.baseclass.prototype.trigger_start_in_animation.apply(this, arguments)
            },
            trigger_start_out_animation: function() {
                "trigger" == this.attrs.an_start ? this.start_out_animation() : R.baseclass.prototype.trigger_start_out_animation.apply(this, arguments)
            },
            animation: function() {
                var t = this;
                t.hidden_before_in = 0 <= _.indexOf(t.attrs.an_hidden.split(","), "before_in"), t.hidden_after_in = 0 <= _.indexOf(t.attrs.an_hidden.split(","), "after_in"), t.hidden_before_in && b(t.dom_element).css("opacity", "0"), t.hidden_after_in && b(t.dom_element).css("opacity", "1");
                var e = t.attrs.an_parent;
                "" == e && (e = 1), e = Math.round(e);
                for (var a = 0, n = b(t.dom_element); a < e;) n = b(n).parent().closest("[data-az-id]"), a++;
                if ("" != t.attrs.an_start) {
                    t.in_timeout = 0, t.out_timeout = 0, t.animated = !1, t.animation_in = !1, t.animation_out = !1;
                    t.add_css("vendor/animate.css/animate.min.css", !1, function() {
                        ! function() {
                            switch (b(n).off("click.az_animation" + t.id), b(n).off("mouseenter.az_animation" + t.id), b(n).off("mouseleave.az_animation" + t.id), t.attrs.an_start) {
                                case "click":
                                    b(n).on("click.az_animation" + t.id, function() {
                                        t.animated || t.start_in_animation()
                                    });
                                    break;
                                case "appear":
                                    t.add_js({
                                        path: "vendor/jquery.waypoints/lib/jquery.waypoints.min.js",
                                        loaded: "waypoint" in b.fn,
                                        callback: function() {
                                            b(t.dom_element).waypoint(function(e) {
                                                t.animated || t.start_in_animation()
                                            }, {
                                                offset: t.attrs.an_offset + "%",
                                                handler: function(e) {
                                                    this.destroy()
                                                }
                                            }), b(document).trigger("scroll")
                                        }
                                    });
                                    break;
                                case "hover":
                                    b(n).on("mouseenter.az_animation" + t.id, function() {
                                        t.hover = !0, t.start_in_animation()
                                    }), b(n).on("mouseleave.az_animation" + t.id, function() {
                                        t.hover = !1, t.start_out_animation()
                                    })
                            }
                        }()
                    })
                }
            },
            update_scroll_animation: function() {
                return !1
            },
            showed: function(e) {
                R.baseclass.prototype.showed.apply(this, arguments), this.an_name = "", "an_name" in this.attrs && "" != this.attrs.an_name && (this.an_name = this.attrs.an_name, window.glazedBuilder.glazedElements.elements_instances_by_an_name[this.an_name] = this), "an_start" in this.attrs && "" != this.attrs.an_start && "no" != this.attrs.an_start && this.animation()
            },
            render: function(e) {
                "an_name" in this.attrs && "" != this.attrs.an_name && e(this.dom_element).attr("data-an-name", this.attrs.an_name), R.baseclass.prototype.render.apply(this, arguments)
            }
        }), window.glazedBuilder.glazedElements = new C, drupalSettings.glazedBuilder.glazedAjaxUrl || drupalSettings.glazedBuilder.glazedEditor && !window.glazedBuilder.glazed_online || delete drupalSettings.glazedBuilder.glazedEditor,
        function(t) {
            drupalSettings.glazedBuilder.glazedEditor ? t(drupalSettings.glazedBuilder.glazedEditor) : b.ajax({
                type: "get",
                url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                b.ajax({
                    type: "POST",
                    url: e,
                    data: {
                        action: "glazed_login",
                        url: window.location.href
                    },
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    t(e)
                })
            })
        }(function(e) {
            drupalSettings.glazedBuilder.glazedEditor = e, b(function() {
                ! function() {
                    if (window.drupalSettings.glazedBuilder.glazedEditor) {
                        for (var e in window.glazedBuilder.glazed_add_css("vendor/font-awesome/css/font-awesome.min.css", function() {}), 0 == b("#glazed-clipboard").length && b("body").prepend('<div id="glazed-clipboard" style="display:none"></div>'), window.glazedBuilder.glazed_add_js({
                                path: "vendor/chosen/chosen.jquery.min.js"
                            }), window.glazedBuilder.glazed_add_css("vendor/chosen/chosen.min.css", function() {}), window.glazedBuilder.glazedElements.elements_instances)(t = window.glazedBuilder.glazedElements.elements_instances[e]) instanceof W && b(t.dom_element).addClass("glazed-editor"), null == t.controls && t.show_controls(), t.update_sortable();
                        b("#az-exporter").show()
                    } else {
                        for (var e in window.glazedBuilder.glazedElements.elements_instances) {
                            var t;
                            (t = window.glazedBuilder.glazedElements.elements_instances[e]) instanceof W && b(t.dom_element).removeClass("glazed-editor"), null != t.controls && b(t.controls).remove(), t.update_empty()
                        }
                        b("#az-exporter").hide()
                    }
                }()
            })
        }), j = function() {
            ! function() {
                if (A) return;
                A = !0, drupalSettings.glazedBuilder.disallowContainers;
                var a = drupalSettings.glazedBuilder.disallowContainers;
                b(".az-container").each(function() {
                    var e = b(this).attr("data-az-name");
                    if (-1 == b.inArray(e, a)) {
                        var t = P(this);
                        t && window.glazedBuilder.glazed_containers.push(t)
                    }
                }), !drupalSettings.glazedBuilder.glazedEditor || 0 == b("#glazed-clipboard").length && b("body").prepend('<div id="glazed-clipboard" class="glazed-backend" style="display:none"></div>')
            }(), b.holdReady(!1)
        }, b.holdReady(!0), "complete" === document.readyState ? setTimeout(j) : document.addEventListener ? (document.addEventListener("DOMContentLoaded", j, !1), window.addEventListener("load", j, !1)) : (document.attachEvent("onreadystatechange", j), window.attachEvent("onload", j));
    var A = !1;

    function L(e, t) {
        L.baseclass.apply(this, arguments)
    }

    function N(e, t) {
        N.baseclass.apply(this, arguments), this.columns = "", t && "boolean" == typeof t || this.set_columns("1/2 + 1/2"), this.attrs.device = "sm"
    }

    function q(e, t) {
        q.baseclass.call(this, e, t)
    }

    function K(e, t) {
        K.baseclass.apply(this, arguments), t && "boolean" == typeof t || this.add_tab()
    }

    function M(e, t) {
        M.baseclass.apply(this, arguments)
    }

    function V(e, t) {
        V.baseclass.apply(this, arguments), t && "boolean" == typeof t || this.add_toggle()
    }

    function Y(e, t) {
        Y.baseclass.apply(this, arguments)
    }

    function F(e, t) {
        F.baseclass.apply(this, arguments), t && "boolean" == typeof t || this.add_slide()
    }

    function H(e, t) {
        H.baseclass.apply(this, arguments)
    }

    function G(e, t) {
        G.baseclass.apply(this, arguments)
    }

    function W(e, t) {
        W.baseclass.apply(this, arguments), this.rendered = !1, this.loaded_container = null, this.js = {}, this.css = {}
    }

    function $(e, t) {
        $.baseclass.apply(this, arguments)
    }

    function J() {
        if ("glazed_extend" in window.glazedBuilder)
            for (var e in window.glazedBuilder.glazed_extend) {
                var t = window.glazedBuilder.glazed_extend[e],
                    a = [];
                "params" in t && (a = t.params), delete t.params;
                var n = T.prototype.elements[e];
                if (!("extended" in n)) {
                    n.extended = !0, f(n.prototype, t);
                    for (var i = 0; i < a.length; i++) {
                        var o = D(a[i]);
                        n.prototype.params.push(o)
                    }
                }
            }
    }

    function X(e) {
        var a = [];

        function n(e) {
            for (var t = 0; t < e.children.length; t++) n(e.children[t]);
            e.id in a && (e.attrs.content = a[e.id])
        }
        b(document).find(".az-element.az-text, .az-element.az-blockquote").each(function() {
            var e = b(this);
            0 < e.children(".ckeditor-inline").length && (a[e.attr("data-az-id")] = e.children(".ckeditor-inline").html())
        }), e.data.object.id in a && (e.data.object.attrs.content = a[e.data.object.id]);
        for (var t = 0; t < e.data.object.children.length; t++) n(e.data.object.children[t])
    }
    b.fn.glazed_builder = function(e) {
            var t = {
                init: function(e) {
                    b.extend({
                        test: "test"
                    }, e);
                    return this.each(function() {
                        var n = this;
                        if (!(i = b(this).data("glazed_builder"))) {
                            var e = b("<div>" + b(n).val() + "</div>")[0];
                            b(e).find("> script").remove(), b(e).find("> link[href]").remove(), b(e).find("> .az-container > script").remove(), b(e).find("> .az-container > link[href]").remove(), b(n).css("display", "none");
                            var i, t = null;
                            if (0 < b(e).find("> .az-container[data-az-type][data-az-name]").length) t = b(e).children().insertAfter(n);
                            else {
                                var a = "textarea",
                                    o = "gb" + Math.random().toString(36).substr(2);
                                window.glazedBuilder.glazed_online && (a = window.glazedBuilder.glazed_type, o = window.glazedBuilder.glazed_name), (t = b('<div class="az-element az-container" data-az-type="' + a + '" data-az-name="' + o + '"></div>').insertAfter(n)).append(b(e).html())
                            }
                            window.glazedBuilder.glazed_title["Save container"] = Drupal.t("Generate HTML and JS for all elements which placed in current container element."), (i = P(t)) && (window.glazedBuilder.glazed_containers.push(i), b(n).data("glazed_builder", i), i.save_container = function() {
                                glazedBuilder.glazed_add_js({
                                    path: "jsON-js/json2.min.js",
                                    loaded: "JSON" in window,
                                    callback: function() {
                                        _.defer(function() {
                                            if (i.id in window.glazedBuilder.glazedElements.elements_instances) {
                                                var e = i.get_container_html();
                                                if (window.glazedBuilder.glazed_online) b(n).val(e);
                                                else {
                                                    var t = i.attrs.container.split("/")[0],
                                                        a = i.attrs.container.split("/")[1];
                                                    b(n).val('<div class="az-element az-container" data-az-type="' + t + '" data-az-name="' + a + '">' + e + "</div>")
                                                }
                                            }
                                        })
                                    }
                                })
                            }, b(document).on("glazed_add_element", i.save_container), b(document).on("glazed_edited_element", i.save_container), b(document).on("glazed_update_element", i.save_container), b(document).on("glazed_delete_element", i.save_container), b(document).on("glazed_update_sorting", i.save_container))
                        }
                    })
                },
                show: function() {
                    this.each(function() {})
                },
                hide: function() {
                    this.each(function() {
                        var e = b(this).data("glazed_builder");
                        if (e) {
                            window.glazedBuilder.glazedElements.delete_element(e.id);
                            for (var t = 0; t < e.children.length; t++) e.children[t].remove();
                            b(e.dom_element).remove(), b(this).removeData("glazed_builder"), b(this).css("display", "")
                        }
                    })
                }
            };
            return t[e] ? t[e].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof e && e ? void b.error(e) : t.init.apply(this, arguments)
        }, U("az_section", !0, L), f(L.prototype, {
            name: Drupal.t("Section"),
            icon: "et et-icon-focus",
            category: Drupal.t("Layout"),
            params: [D({
                type: "checkbox",
                heading: Drupal.t("Full Width"),
                param_name: "fluid",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("100% Height"),
                param_name: "fullheight",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Vertical Centering"),
                param_name: "vertical_centering",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Background Effect"),
                param_name: "effect",
                tab: Drupal.t("Background Effects"),
                value: {
                    "": Drupal.t("Simple Image"),
                    fixed: Drupal.t("Fixed Image"),
                    parallax: Drupal.t("Parallax Image"),
                    gradient: Drupal.t("Gradient"),
                    youtube: Drupal.t("YouTube Video")
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Parallax speed"),
                param_name: "parallax_speed",
                tab: Drupal.t("Background Effects"),
                value: 20,
                dependency: {
                    element: "effect",
                    value: ["parallax"]
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Parallax Mode"),
                param_name: "parallax_mode",
                tab: Drupal.t("Background Effects"),
                value: {
                    fixed: Drupal.t("Fixed"),
                    scroll: Drupal.t("Local")
                },
                dependency: {
                    element: "effect",
                    value: ["parallax"]
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Disable on Mobile"),
                param_name: "parallax_mobile_disable",
                tab: Drupal.t("Background Effects"),
                value: {
                    yes: Drupal.t("On")
                },
                dependency: {
                    element: "effect",
                    value: ["parallax"]
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Start Color"),
                param_name: "gradient_start_color",
                tab: Drupal.t("Background Effects"),
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("End Color"),
                param_name: "gradient_end_color",
                tab: Drupal.t("Background Effects"),
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Gradient Direction"),
                param_name: "gradient_direction",
                tab: Drupal.t("Background Effects"),
                value: "180",
                min: "1",
                max: "360",
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Start Position"),
                param_name: "gradient_start",
                tab: Drupal.t("Background Effects"),
                value: "0",
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("End Position"),
                param_name: "gradient_end",
                tab: Drupal.t("Background Effects"),
                value: "100",
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Video Play Options"),
                param_name: "video_options",
                tab: Drupal.t("Background Effects"),
                value: {
                    disMobile: Drupal.t("Disable on mobile devices"),
                    loop: Drupal.t("Loop"),
                    mute: Drupal.t("Muted"),
                    noCrop: Drupal.t("Disable cropping out player"),
                    resizeSection: Drupal.t("Resize section to match video"),
                    showControls: Drupal.t("Show controls")
                },
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            }), D({
                type: "textfield",
                heading: Drupal.t("YouTube Video URL"),
                param_name: "video_youtube",
                tab: Drupal.t("Background Effects"),
                description: Drupal.t("Enter the YouTube video URL."),
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            }), D({
                type: "textfield",
                heading: Drupal.t("Start Time in seconds"),
                param_name: "video_start",
                tab: Drupal.t("Background Effects"),
                description: Drupal.t("Enter time in seconds from where video start to play."),
                value: "0",
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            }), D({
                type: "textfield",
                heading: Drupal.t("Stop Time in seconds"),
                param_name: "video_stop",
                tab: Drupal.t("Background Effects"),
                description: Drupal.t("Enter time in seconds where video ends."),
                value: "0",
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            })].concat(L.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            section: !0,
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor && (L.baseclass.prototype.show_controls.apply(this, arguments), this.parent instanceof W)) {
                    var e = this;
                    b('<div title="' + r("Add Section Below") + '" class="control add-section btn btn-default glyphicon glyphicon-plus-sign" > </div>').appendTo(this.dom_element).click(function() {
                        new T.prototype.elements.az_section(e.parent, e.dom_element.index()).update_dom(), e.parent.update_empty()
                    })
                }
            },
            showed: function(n) {
                L.baseclass.prototype.showed.apply(this, arguments);
                var i = this;
                switch (this.attrs.effect) {
                    case "parallax":
                        if (window.innerWidth < 481 && this.attrs.parallax_mobile_disable) {
                            n(i.dom_element).css("background-attachment", "scroll"), n(i.dom_element).css("background-position", "");
                            break
                        }
                        this.add_js_list({
                            paths: ["vendor/jquery.parallax/jquery.parallax.js", "vendor/jquery.waypoints/lib/jquery.waypoints.min.js"],
                            loaded: "waypoint" in n.fn && "parallax" in n.fn,
                            callback: function() {
                                n(i.dom_element).waypoint(function(e) {
                                    var t = n(i.dom_element).css("background-position").match(/([\w%]*) [\w%]/);
                                    if (null == t) var a = "50%";
                                    else a = t[1];
                                    n(i.dom_element).css("background-attachment", i.attrs.parallax_mode), n(i.dom_element).css("background-position", a + " 0"), n(i.dom_element).parallax(a, i.attrs.parallax_speed / 100)
                                }, {
                                    offset: "100%",
                                    handler: function(e) {
                                        this.destroy()
                                    }
                                }), n(document).trigger("scroll")
                            }
                        });
                        break;
                    case "fixed":
                        n(i.dom_element).css("background-attachment", "fixed");
                        break;
                    case "youtube":
                        ;
                        var t = 0 <= _.indexOf(i.attrs.video_options.split(","), "loop"),
                            a = 0 <= _.indexOf(i.attrs.video_options.split(","), "mute"),
                            o = -1 == _.indexOf(i.attrs.video_options.split(","), "noCrop"),
                            l = 0 <= _.indexOf(i.attrs.video_options.split(","), "resizeSection"),
                            s = -1 == _.indexOf(i.attrs.video_options.split(","), "disMobile"),
                            r = 0 <= _.indexOf(i.attrs.video_options.split(","), "showControls");
                        this.add_css("vendor/jquery.mb.YTPlayer/dist/css/jquery.mb.YTPlayer.min.css", "mb_YTPlayer" in n.fn, function() {}), this.add_js_list({
                            paths: ["vendor/jquery.mb.YTPlayer/dist/jquery.mb.YTPlayer.min.js", "vendor/jquery.waypoints/lib/jquery.waypoints.min.js"],
                            loaded: "waypoint" in n.fn && "mb_YTPlayer" in n.fn,
                            callback: function() {
                                if (l) {
                                    var e = 9 * n(i.dom_element)[0].offsetWidth / 16;
                                    n(i.dom_element).css("height", e), n(i.dom_element).css("overflow", "hidden")
                                }
                                n(i.dom_element).waypoint(function(e) {
                                    n(i.dom_element).attr("data-property", "{videoURL:'" + function(e) {
                                        var t = e.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/);
                                        return !(!t || 11 != t[7].length) && t[7]
                                    }(i.attrs.video_youtube) + "',containment:'[data-az-id=" + i.id + "]', showControls:" + r.toString() + ", autoPlay:true, stopMovieOnBlur:false, loop:" + t.toString() + ", mute:" + a.toString() + ", optimizeDisplay:" + o.toString() + ", startAt:" + i.attrs.video_start + ", stopAt:" + i.attrs.video_stop + ", useOnMobile:" + s.toString() + ", showYTLogo:false, isStandAlonePlayer: false}"), n(i.dom_element).mb_YTPlayer()
                                }, {
                                    offset: "300%",
                                    handler: function(e) {
                                        this.destroy()
                                    }
                                }), n(document).trigger("scroll")
                            }
                        })
                }
            },
            render: function(e) {
                switch (this.dom_element = e('<div id="' + this.id + '" class="az-element az-section ' + this.get_el_classes() + " " + this.get_content_classes() + ' " style="' + this.attrs.style + '"></div>'), "yes" == this.attrs.fullheight && e(this.dom_element).css("height", "100vh"), "yes" == this.attrs.fluid ? this.dom_content_element = e('<div class="az-ctnr container-fluid"></div>').appendTo(this.dom_element) : this.dom_content_element = e('<div class="az-ctnr container"></div>').appendTo(this.dom_element), this.attrs.effect) {
                    case "parallax":
                        e(this.dom_element).attr("data-glazed-builder-libraries", "waypoints");
                        break;
                    case "youtube":
                        e(this.dom_element).attr("data-glazed-builder-libraries", "ytplayer")
                }
                if ("gradient" == this.attrs.effect) {
                    var t = "linear-gradient(" + this.attrs.gradient_direction + "deg, " + this.attrs.gradient_start_color + " " + this.attrs.gradient_start + "%, " + this.attrs.gradient_end_color + " " + this.attrs.gradient_end + "%)";
                    e(this.dom_element).css("background-image") ? e(this.dom_element).css("background-image", t + "," + e(this.dom_element).css("background-image")) : e(this.dom_element).css("background-image", t)
                }
                this.attrs.vertical_centering && "yes" === this.attrs.vertical_centering && e(this.dom_element).addClass("az-util-vertical-centering"), L.baseclass.prototype.render.apply(this, arguments)
            }
        }), U("az_row", !0, N), f(N.prototype, {
            name: Drupal.t("Row"),
            icon: "et et-icon-grid",
            category: Drupal.t("Layout"),
            params: [D({
                type: "dropdown",
                heading: Drupal.t("Device breakpoint"),
                param_name: "device",
                value: {
                    xs: Drupal.t("Extra small devices Phones (<768px)"),
                    sm: Drupal.t("Small devices Tablets (≥768px)"),
                    md: Drupal.t("Medium devices Desktops (≥992px)"),
                    lg: Drupal.t("Large devices Desktops (≥1200px)")
                },
                description: Drupal.t("Bootstrap responsive grid breakpoints")
            }), D({
                type: "checkbox",
                heading: Drupal.t("Equal Height Columns"),
                param_name: "equal",
                value: {
                    yes: Drupal.t("Yes")
                }
            })].concat(N.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    N.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove();
                    for (var e = this, t = this.controls, a = '<div class="row-layouts clearfix">', n = ["1/1", "1/2 + 1/2", "1/3 + 1/3 + 1/3", "1/4 + 1/4 + 1/4 + 1/4", "1/6 + 1/6 + 1/6 + 1/6 + 1/6 + 1/6", "1/4 + 1/2 + 1/4", "1/6 + 4/6 + 1/6", "1/4 + 3/4", "3/4 + 1/4", "2/3 + 1/3", "1/3 + 2/3", "5/12 + 7/12"], i = "", o = 0; o < n.length; o++) {
                        a += '<div title="' + r("Set " + n[o] + " colums") + '" " role="' + n[o] + '" class="az-mini-container control set-columns-layout"><div class="row">', i = n[o].replace(" ", "").split("+");
                        for (var l = 0; l < i.length; l++) a += '<div class="' + d(i[l], "xs") + '"><div class="az-col-solid"></div></div>';
                        a += "</div>", a += "</div>"
                    }
                    a += '<small class="az-row-custom set-columns-layout"><a href="#" class="custom-row-control glazed-util-text-muted text-small">Custom layout</a></small>', a += "</div>";
                    var s = b('<span title="' + r("Set row layout") + '" class="control set-columns btn btn-default glyphicon glyphicon-th"> </span>').insertAfter(this.controls.find(".drag-and-drop")).popover({
                        container: !1,
                        selector: !1,
                        placement: "right",
                        html: "true",
                        trigger: "manual",
                        content: a
                    }).click(function() {
                        b(s).popover("toggle"), c(b(t)), c(b(t).find(".popover")), b(t).find(".popover .set-columns-layout").each(function() {
                            b(this).click({
                                object: e
                            }, e.click_set_columns)
                        }), e.controls.find(".popover").mouseleave(function() {
                            b(s).popover("hide"), b(s).css("display", "")
                        })
                    })
                }
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && b(this.dom_element).sortable({
                    axis: "x",
                    items: "> .az-column",
                    handle: "> .controls > .drag-and-drop",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    tolerance: "pointer",
                    distance: 1,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            update_sorting: function(e, t) {
                N.baseclass.prototype.update_sorting.apply(this, arguments);
                var a = window.glazedBuilder.glazedElements.get_element(b(this).closest("[data-az-id]").attr("data-az-id"));
                if (a)
                    for (var n = 0; n < a.children.length; n++) a.children[n].update_empty()
            },
            update_dom: function() {
                N.baseclass.prototype.update_dom.apply(this, arguments);
                for (var e = 0; e < this.children.length; e++) this.children[e].update_dom()
            },
            click_set_columns: function(e) {
                var t = b(this).attr("role");
                if ("" == t || null == t) {
                    if ("" == e.data.object.columns) {
                        t = [];
                        for (var a = 0; a < e.data.object.children.length; a++) t.push(e.data.object.children[a].attrs.width);
                        e.data.object.columns = t.join(" + ")
                    }
                    t = window.prompt(Drupal.t("Enter bootstrap grid layout. For example 1/2 + 1/2."), e.data.object.columns)
                }
                return "" != t && null != t && e.data.object.set_columns(t), !1
            },
            set_columns: function(e) {
                var t = (this.columns = e).replace(" ", "").split("+");
                if (0 == this.children.length)
                    for (var a = 0; a < t.length; a++) {
                        (s = new q(this, !0)).update_dom(), s.update_width(t[a])
                    } else if (this.children.length == t.length)
                        for (a = 0; a < t.length; a++) this.children[a].update_width(t[a]);
                    else if (this.children.length > t.length) {
                    var n = this.children[t.length - 1];
                    for (a = 0; a < this.children.length; a++)
                        if (a < t.length) this.children[a].update_width(t[a]);
                        else {
                            for (var i = this.children[a], o = 0; o < i.children.length; o++)(i.children[o].parent = n).children.push(i.children[o]);
                            i.children = []
                        }
                    n.update_dom();
                    var l = this.children.slice(t.length, this.children.length);
                    for (a = 0; a < l.length; a++) l[a].remove()
                } else
                    for (a = 0; a < t.length; a++) {
                        var s;
                        if (a < this.children.length) this.children[a].update_width(t[a]);
                        else(s = new q(this, !0)).update_dom(), s.update_width(t[a])
                    }
                this.update_sortable()
            },
            showed: function(e) {
                N.baseclass.prototype.showed.apply(this, arguments)
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-row row ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, e(this.dom_element).addClass("az-row--" + this.attrs.device), this.attrs.equal && "yes" === this.attrs.equal && e(this.dom_element).addClass("az-row--equal-height"), N.baseclass.prototype.render.apply(this, arguments)
            }
        }), I("az_column", !0, q), f(q.prototype, {
            name: Drupal.t("Column"),
            params: [D({
                type: "textfield",
                heading: Drupal.t("Column with"),
                param_name: "width",
                hidden: !0
            }), D({
                type: "checkbox",
                heading: Drupal.t("Vertical Centering"),
                param_name: "vertical_centering",
                value: {
                    yes: Drupal.t("Yes")
                }
            })].concat(q.prototype.params),
            hidden: !0,
            is_container: !0,
            controls_base_position: "top-left",
            show_parent_controls: !0,
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (q.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".clone").remove(), this.controls.find(".copy").remove(), this.controls.find(".remove").remove())
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            update_width: function(e) {
                b(this.dom_element).removeClass(d(this.attrs.width, this.parent.attrs.device)), this.attrs.width = e, b(this.dom_element).addClass(d(this.attrs.width, this.parent.attrs.device)), b(document).trigger("glazed_update_element", this.id)
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-ctnr az-column ' + this.get_el_classes() + " " + this.get_content_classes() + " " + d(this.attrs.width, this.parent.attrs.device) + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, this.attrs.vertical_centering && "yes" === this.attrs.vertical_centering && e(this.dom_element).addClass("az-util-vertical-centering"), q.baseclass.prototype.render.apply(this, arguments)
            }
        }), U("az_tabs", !0, K), f(K.prototype, {
            name: Drupal.t("Tabs"),
            icon: "pe pe-7s-folder",
            category: Drupal.t("Layout"),
            params: [D({
                type: "dropdown",
                heading: Drupal.t("Tab direction"),
                param_name: "az_dirrection",
                value: {
                    "": Drupal.t("Default"),
                    "tabs-left": Drupal.t("Left"),
                    "tabs-right": Drupal.t("Right")
                }
            })].concat(K.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (K.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove(), b('<span title="' + r("Add tab") + '" class="control add-tab btn btn-default glyphicon glyphicon-plus-sign" > </span>').appendTo(this.controls).click({
                    object: this
                }, this.click_add_tab))
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && b(this.dom_element).sortable({
                    items: "> ul > li",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            update_sorting: function(e, t) {
                var a = window.glazedBuilder.glazedElements.get_element(b(this).attr("data-az-id"));
                if (a) {
                    var n = b(this).sortable("option"),
                        i = [];
                    b(this).find(n.items).each(function() {
                        var e = b(this).find('a[data-toggle="tab"]').attr("element-id");
                        i.push(window.glazedBuilder.glazedElements.get_element(e))
                    }), a.children = i;
                    for (var o = 0; o < a.children.length; o++) a.children[o].parent = a;
                    a.update_dom(), b(document).trigger("glazed_update_sorting", t)
                }
            },
            click_add_tab: function(e) {
                return e.data.object.add_tab(), !1
            },
            add_tab: function() {
                var e = new M(this, !1);
                e.update_dom(), this.update_dom(), b(this.dom_element).find('a[href="#' + e.id + '"]').tab("show")
            },
            showed: function(e) {
                K.baseclass.prototype.showed.apply(this, arguments), e(this.dom_element).find("ul.nav-tabs li:first a").tab("show");
                var t = window.location.hash;
                t && e('ul.nav a[href="' + t + '"]').tab("show");
                var a = 0 < e("#toolbar-bar").length ? e("#toolbar-bar").height() : 0,
                    n = 0 < e(".toolbar-tray.toolbar-tray-horizontal.is-active").length ? e("#toolbar-item-administration-tray").height() : 0,
                    i = 0 < e(".glazed-header--sticky, .glazed-header--fixed").length ? e("#navbar").height() : 0;
                if (t && e('ul.nav a[href="' + t + '"]').attr("element-id")) var o = e('ul.nav a[href="' + t + '"]').closest(".az-tabs").parent();
                o && window.innerWidth < 481 ? e("html, body").animate({
                    scrollTop: o.offset().top - 100
                }) : o && e("html, body").animate({
                    scrollTop: o.offset().top - (a + i + n + 30)
                }, 300)
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-tabs tabbable ' + this.get_el_classes() + " " + this.get_content_classes() + this.attrs.az_dirrection + '" style="' + this.attrs.style + '"></div>');
                for (var t = '<ul class="nav nav-tabs" role="tablist">', a = 0; a < this.children.length; a++) {
                    t += '<li><a href="#' + (this.children[a].attrs.hash ? this.children[a].attrs.hash : this.children[a].id) + '" role="tab" data-toggle="tab" element-id="' + this.children[a].id + '">' + this.children[a].attrs.title + "</a></li>"
                }
                t += "</ul>", e(this.dom_element).append(t);
                var n = '<div id="' + this.id + '" class="tab-content"></div>';
                this.dom_content_element = e(n).appendTo(this.dom_element), K.baseclass.prototype.render.apply(this, arguments)
            }
        }), I("az_tab", !0, M), f(M.prototype, {
            name: Drupal.t("Tab"),
            params: [D({
                type: "textfield",
                heading: Drupal.t("Tab title"),
                param_name: "title",
                value: Drupal.t("Title")
            }), D({
                type: "textfield",
                heading: Drupal.t("Tab URL hash"),
                param_name: "hash",
                value: ""
            })].concat(M.prototype.params),
            hidden: !0,
            is_container: !0,
            controls_base_position: "top-left",
            get_empty: function() {
                return '<div class="az-empty"><div class="top-left well"><h1>↖</h1><span class="glyphicon glyphicon-plus-sign"></span>' + Drupal.t(" add a new tab.") + " " + Drupal.t("Drag tab headers to change order.") + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (M.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".drag-and-drop").remove(), b('<span class="control btn btn-default glyphicon">' + this.name + "</span>").prependTo(this.controls))
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            edited: function(e) {
                M.baseclass.prototype.edited.apply(this, arguments), this.parent.update_dom(), this.attrs.hash ? elementId = this.attrs.hash : elementId = this.id, b('a[href="#' + elementId + '"]').tab("show")
            },
            clone: function() {
                var e = M.baseclass.prototype.get_my_shortcode.apply(this, arguments);
                b("#glazed-clipboard").html(encodeURIComponent(e)), this.parent.paste(this.parent.children.length), this.parent.update_dom()
            },
            remove: function() {
                M.baseclass.prototype.remove.apply(this, arguments), this.parent.update_dom()
            },
            render: function(e) {
                var t = "";
                t = this.attrs.hash ? this.attrs.hash : this.id, this.dom_element = e('<div id="' + t + '" class="az-element az-ctnr az-tab tab-pane ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, M.baseclass.prototype.render.apply(this, arguments)
            }
        }), U("az_accordion", !0, V), f(V.prototype, {
            name: Drupal.t("Collapsibles"),
            icon: "pe pe-7s-menu",
            category: Drupal.t("Layout"),
            params: [D({
                type: "checkbox",
                heading: Drupal.t("Collapsed?"),
                param_name: "collapsed",
                value: {
                    yes: Drupal.t("Yes")
                }
            })].concat(V.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (V.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove(), b('<span title="' + r("Add toggle") + '" class="control add-toggle btn btn-default glyphicon glyphicon-plus-sign" > </span>').appendTo(this.controls).click({
                    object: this
                }, this.click_add_toggle))
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && b(this.dom_element).sortable({
                    axis: "y",
                    items: "> .az-toggle",
                    handle: "> .controls > .drag-and-drop",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            click_add_toggle: function(e) {
                return e.data.object.add_toggle(), !1
            },
            add_toggle: function() {
                new Y(this, !1).update_dom(), this.update_dom()
            },
            update_dom: function() {
                for (var e = 0; e < this.children.length; e++) this.children[e].update_dom();
                V.baseclass.prototype.update_dom.apply(this, arguments)
            },
            showed: function(e) {
                V.baseclass.prototype.showed.apply(this, arguments), e(this.dom_element).find("> .az-toggle > .in").removeClass("in"), e(this.dom_element).find("> .az-toggle > .collapse:not(:first)").collapse({
                    toggle: !1,
                    parent: "#" + this.id
                }), e(this.dom_element).find("> .az-toggle > .collapse:first").collapse({
                    toggle: "yes" != this.attrs.collapsed,
                    parent: "#" + this.id
                })
            },
            render: function(e) {
                this.dom_element = e('<div id="' + this.id + '" class="az-element az-accordion panel-group ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, V.baseclass.prototype.render.apply(this, arguments)
            }
        }), I("az_toggle", !0, Y), f(Y.prototype, {
            name: Drupal.t("Toggle"),
            params: [D({
                type: "textfield",
                heading: Drupal.t("Toggle title"),
                param_name: "title",
                value: Drupal.t("Title")
            })].concat(Y.prototype.params),
            hidden: !0,
            is_container: !0,
            controls_base_position: "top-left",
            get_empty: function() {
                return '<div class="az-empty"><div class="top-left well"><h1>↖</h1><span class="glyphicon glyphicon-plus-sign"></span>' + Drupal.t(" add a new toggle.") + "</div></div>"
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            clone: function() {
                var e = Y.baseclass.prototype.get_my_shortcode.apply(this, arguments);
                b("#glazed-clipboard").html(encodeURIComponent(e)), this.parent.paste(this.parent.children.length)
            },
            render: function(e) {
                var t = "panel-default";
                "" != this.parent.attrs.type && (t = this.parent.attrs.type), this.dom_element = e('<div class="az-element az-toggle panel ' + t + " " + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"><div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#' + this.parent.id + '" href="#' + this.id + '">' + this.attrs.title + '</a></h4></div><div id="' + this.id + '" class="panel-collapse collapse"><div class="panel-body az-ctnr"></div></div></div>'), this.dom_content_element = e(this.dom_element).find(".panel-body"), Y.baseclass.prototype.render.apply(this, arguments)
            }
        }), U("az_carousel", !0, F), f(F.prototype, {
            name: Drupal.t("Carousel"),
            icon: "pe pe-7s-more",
            category: Drupal.t("Layout"),
            params: [D({
                type: "bootstrap_slider",
                heading: Drupal.t("Frames per slide"),
                param_name: "items",
                min: "1",
                max: "10",
                value: "1"
            }), D({
                type: "checkbox",
                heading: Drupal.t("Disable Auto Play"),
                param_name: "autoplay",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Dots Navigation"),
                param_name: "pagination",
                tab: "Dots",
                value: {
                    on: Drupal.t("On")
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Dots Orientation"),
                param_name: "pagination_orientation",
                tab: "Dots",
                value: {
                    outside: Drupal.t("Outside Slider"),
                    inside: Drupal.t("Inside Slider")
                },
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Shape"),
                param_name: "pagination_shape",
                tab: "Dots",
                value: {
                    circle: Drupal.t("Circle"),
                    square: Drupal.t("Square"),
                    triangle: Drupal.t("Triangle"),
                    bar: Drupal.t("Bar")
                },
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Transform Active"),
                param_name: "pagination_transform",
                tab: "Dots",
                value: {
                    "": Drupal.t("None"),
                    growTaller: Drupal.t("Grow Taller"),
                    growWider: Drupal.t("Grow Wider"),
                    scaleUp: Drupal.t("Scale up")
                },
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Dots Color"),
                param_name: "pagination_color",
                tab: "Dots",
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Active Dot Color"),
                param_name: "pagination_active_color",
                tab: "Dots",
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Next/Previous"),
                param_name: "navigation",
                tab: "Next/Previous",
                value: {
                    on: Drupal.t("On")
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Orientation"),
                param_name: "navigation_orientation",
                tab: "Next/Previous",
                value: {
                    outside: Drupal.t("Outside Slider"),
                    inside: Drupal.t("Inside Slider")
                },
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Shape"),
                param_name: "navigation_shape",
                tab: "Next/Previous",
                value: {
                    "": Drupal.t("No Background"),
                    circle: Drupal.t("Circle"),
                    square: Drupal.t("Square"),
                    bar: Drupal.t("Bar")
                },
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Icon Color"),
                param_name: "navigation_icon_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Icon Hover Color"),
                param_name: "navigation_icon_hover_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Background Color"),
                param_name: "navigation_background_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "colorpicker",
                heading: Drupal.t("Background Hover"),
                param_name: "navigation_background_hover_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Icon Thickness"),
                param_name: "navigation_thickness",
                tab: "Next/Previous",
                min: "1",
                max: "10",
                value: "2",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "dropdown",
                heading: Drupal.t("Outside Navigation Position"),
                param_name: "navigation_position",
                tab: "Next/Previous",
                value: {
                    adjacent: Drupal.t("Adjacent"),
                    bottomCenter: Drupal.t("Bottom Center"),
                    topLeft: Drupal.t("Top Left"),
                    topRight: Drupal.t("Top Right"),
                    bottomCenter: Drupal.t("Bottom Center"),
                    bottomLeft: Drupal.t("Bottom Left"),
                    bottomRight: Drupal.t("Bottom Right")
                },
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Auto Play Interval"),
                param_name: "interval",
                min: "1000",
                max: "20000",
                value: "5000",
                step: "100"
            }), D({
                type: "dropdown",
                heading: Drupal.t("Transition style"),
                param_name: "transition",
                value: {
                    "": Drupal.t("Default"),
                    fade: Drupal.t("fade"),
                    backSlide: Drupal.t("backSlide"),
                    goDown: Drupal.t("goDown"),
                    fadeUp: Drupal.t("fadeUp")
                }
            }), D({
                type: "checkbox",
                heading: Drupal.t("Stop on Hover"),
                param_name: "stoponhover",
                value: {
                    on: Drupal.t("On")
                }
            }), D({
                type: "checkbox",
                hidden: !0,
                heading: Drupal.t("Options"),
                param_name: "options",
                value: {
                    navigation: Drupal.t("Navigation"),
                    auto_play: Drupal.t("Auto play"),
                    mouse: Drupal.t("Mouse drag"),
                    touch: Drupal.t("Touch drag")
                }
            })].concat(F.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            show_settings_on_create: !0,
            frontend_render: !0,
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    F.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove();
                    b('<span title="' + r("Add slide") + '" class="control add-toggle btn btn-default glyphicon glyphicon-plus-sign" > </span>').appendTo(this.controls).click({
                        object: this
                    }, this.click_add_slide)
                }
            },
            update_sortable: function() {},
            click_add_slide: function(e) {
                return e.data.object.add_slide(), !1
            },
            add_slide: function() {
                new H(this, !0).update_dom(), this.update_dom()
            },
            showed: function(s) {
                F.baseclass.prototype.showed.apply(this, arguments);
                this.add_css("vendor/owl.carousel/owl-carousel/owl.carousel.css", "owlCarousel" in s.fn, function() {}), this.add_css("css/st-owl-carousel.css", "owlCarousel" in s.fn, function() {}), this.add_css("vendor/owl.carousel/owl-carousel/owl.transitions.css", "owlCarousel" in s.fn, function() {});
                var n = this;
                this.add_js({
                    path: "vendor/owl.carousel/owl-carousel/owl.carousel.js",
                    loaded: "owlCarousel" in s.fn,
                    callback: function() {
                        function e(e) {
                            var t = null;
                            t = "userItems" in e ? e.userItems : e.$userItems;
                            var a = null;
                            a = "visibleItems" in e ? e.visibleItems : e.$visibleItems;
                            for (var n = 0; n < t.length; n++)
                                if (_.indexOf(a, n) < 0) {
                                    var i = t[n],
                                        o = s(i).attr("data-az-id"),
                                        l = window.glazedBuilder.glazedElements.get_element(o);
                                    _.isUndefined(l) || "trigger_start_out_animation" in l && l.trigger_start_out_animation()
                                }
                            for (n = 0; n < a.length; n++)
                                if (a[n] < t.length) {
                                    i = t[a[n]], o = s(i).attr("data-az-id"), l = window.glazedBuilder.glazedElements.get_element(o);
                                    _.isUndefined(l) || "trigger_start_in_animation" in l && l.trigger_start_in_animation()
                                }
                        }
                        var t = "st-owl-theme";
                        n.attrs.pagination && (n.attrs.pagination_orientation && (t += " st-owl-pager-" + n.attrs.pagination_orientation), n.attrs.pagination_shape && (t += " st-owl-pager-" + n.attrs.pagination_shape), n.attrs.pagination_transform && (t += " st-owl-pager-" + n.attrs.pagination_transform)), n.attrs.navigation && (n.attrs.navigation_orientation && (t += " st-owl-navigation-" + n.attrs.navigation_orientation), n.attrs.navigation_shape && (t += " st-owl-navigation-" + n.attrs.navigation_shape), n.attrs.navigation_position && (t += " st-owl-navigation-" + n.attrs.navigation_position));
                        var a = !1;
                        !Boolean(n.attrs.autoplay) && 0 < n.attrs.interval && (a = n.attrs.interval), s(n.dom_content_element).owlCarousel({
                            addClassActive: !0,
                            afterAction: function() {
                                e(this.owl)
                            },
                            afterMove: function() {},
                            autoPlay: a,
                            beforeMove: function() {},
                            items: n.attrs.items,
                            mouseDrag: !0,
                            navigation: Boolean(n.attrs.navigation),
                            navigationText: !1,
                            pagination: Boolean(n.attrs.pagination),
                            singleItem: "1" == n.attrs.items,
                            startDragging: function() {},
                            stopOnHover: Boolean(n.attrs.stoponhover),
                            theme: t,
                            touchDrag: !0,
                            transitionStyle: "" != n.attrs.transition && n.attrs.transition
                        }), e(n.dom_content_element.data("owlCarousel")), "outside" != n.attrs.navigation_orientation || "topLeft" != n.attrs.navigation_position && "topRight" != n.attrs.navigation_position && "topCenter" != n.attrs.navigation_position || s(n.dom_content_element).find(".owl-buttons").prependTo(s(n.dom_content_element)), s("head").find("#carousel-style-" + n.id).remove(), s("head").append(function(e) {
                            var t = "[data-az-id=" + e.id + "] .st-owl-theme";
                            return output = '<style id="carousel-style-' + e.id + '">\x3c!-- ', "triangle" == e.attrs.pagination_shape ? (output += t + " .owl-page {background:transparent !important; border-bottom-color: " + e.attrs.pagination_color + " !important}", output += t + " .owl-page.active {background:transparent !important; border-bottom-color: " + e.attrs.pagination_active_color + " !important}") : (output += t + " .owl-page {background: " + e.attrs.pagination_color + " !important}", output += t + " .owl-page.active {background: " + e.attrs.pagination_active_color + " !important}"), output += t + " .owl-buttons .owl-prev::after, " + t + " .owl-buttons .owl-next::after," + t + " .owl-buttons .owl-prev::before, " + t + " .owl-buttons .owl-next::before {background: " + e.attrs.navigation_icon_color + ";width: " + e.attrs.navigation_thickness + "px;}", output += t + " .owl-buttons .owl-prev:hover::after, " + t + " .owl-buttons .owl-next:hover::after," + t + " .owl-buttons .owl-prev:hover::before, " + t + " .owl-buttons .owl-next:hover::before {background: " + e.attrs.navigation_icon_hover_color + "}", output += t + " .owl-buttons .owl-prev, " + t + " .owl-buttons .owl-next {background: " + e.attrs.navigation_background_color + ";border-color: " + e.attrs.navigation_background_color + " }", output += t + " .owl-buttons .owl-prev:hover, " + t + " .owl-buttons .owl-next:hover {background: " + e.attrs.navigation_background_hover_color + ";border-color: " + e.attrs.navigation_background_hover_color + " }", output += " --\x3e</style>", output
                        }(n))
                    }
                })
            },
            render: function(e) {
                this.dom_element = e('<div id="' + this.id + '" class="az-element az-carousel ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '" data-glazed-builder-libraries="owlcarousel"></div>'), this.dom_content_element = e("<div></div>").appendTo(this.dom_element), F.baseclass.prototype.render.apply(this, arguments)
            }
        }), I("az_slide", !0, H), f(H.prototype, {
            name: Drupal.t("Slide"),
            params: [].concat(H.prototype.params),
            hidden: !0,
            is_container: !0,
            show_parent_controls: !0,
            controls_base_position: "top-left",
            get_empty: function() {
                return '<div class="az-empty"><div class="top-left well"><h1>↖</h1><span class="glyphicon glyphicon-plus-sign"></span>' + Drupal.t(" add a new slide.") + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (H.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".clone").remove(), this.controls.find(".drag-and-drop").remove(), b('<span class="control btn btn-default glyphicon">' + this.name + "</span>").prependTo(this.controls))
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            edited: function() {
                H.baseclass.prototype.edited.apply(this, arguments), this.parent.update_dom()
            },
            render: function(e) {
                "" != this.parent.attrs.type && this.parent.attrs.type, this.dom_element = e('<div class="az-element az-slide az-ctnr ' + this.get_el_classes() + " " + this.get_content_classes() + ' clearfix" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, H.baseclass.prototype.render.apply(this, arguments)
            }
        }), U("az_layers", !0, G), f(G.prototype, {
            name: Drupal.t("Positioned Layers"),
            icon: "et et-icon-layers",
            description: Drupal.t("Free Positioning"),
            category: Drupal.t("Layout"),
            params: [D({
                type: "textfield",
                heading: Drupal.t("Width"),
                param_name: "width",
                description: Drupal.t("For example 100px, or 50%."),
                value: "100%"
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Height"),
                param_name: "height",
                max: "10000",
                value: "500"
            }), D({
                type: "checkbox",
                heading: Drupal.t("Responsive?"),
                param_name: "responsive",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), D({
                type: "bootstrap_slider",
                heading: Drupal.t("Original width"),
                param_name: "o_width",
                hidden: !0
            })].concat(G.prototype.params),
            show_settings_on_create: !0,
            is_container: !0,
            controls_base_position: "top-left",
            disallowed_elements: ["az_layers"],
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            zindex_normalize: function() {
                for (var e = [], t = 0; t < this.children.length; t++) isNaN(parseInt(this.children[t].attrs.pos_zindex)) && (this.children[t].attrs.pos_zindex = 0), e.push(parseInt(this.children[t].attrs.pos_zindex));
                e = _.sortBy(e, function(e) {
                    return e
                }), e = _.uniq(e);
                for (t = 0; t < this.children.length; t++) {
                    var a = _.sortedIndex(e, parseInt(this.children[t].attrs.pos_zindex));
                    b(this.children[t].dom_element).css("z-index", a), this.children[t].attrs.pos_zindex = a
                }
            },
            update_sortable: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    var n = this;

                    function a(e) {
                        var t = b(e).closest("[data-az-id]").attr("data-az-id"),
                            a = window.glazedBuilder.glazedElements.get_element(t);
                        a.attrs.pos_left = parseInt(b(e).css("left")) / (b(n.dom_content_element).width() / 100) + "%", a.attrs.pos_top = parseInt(b(e).css("top")) / (b(n.dom_content_element).height() / 100) + "%", a.attrs.pos_width = parseInt(b(e).css("width")) / (b(n.dom_content_element).width() / 100) + "%", a.attrs.pos_height = parseInt(b(e).css("height")) / (b(n.dom_content_element).height() / 100) + "%", i(e), n.attrs.o_width = b(n.dom_element).width(), b(document).trigger("glazed_update_element", t)
                    }

                    function i(e) {
                        b(e).css("left", parseInt(b(e).css("left")) / (b(n.dom_content_element).width() / 100) + "%"), b(e).css("top", parseInt(b(e).css("top")) / (b(n.dom_content_element).height() / 100) + "%"), b(e).css("width", parseInt(b(e).css("width")) / (b(n.dom_content_element).width() / 100) + "%"), b(e).css("height", parseInt(b(e).css("height")) / (b(n.dom_content_element).height() / 100) + "%")
                    }
                    n.zindex_normalize(), b(this.dom_content_element).resizable({
                        start: function(e, t) {
                            for (var a = 0; a < n.children.length; a++) {
                                i(n.children[a].dom_element)
                            }
                        },
                        stop: function(e, t) {
                            n.attrs.width = parseInt(b(n.dom_content_element).css("width")) / (b(n.dom_element).width() / 100) + "%", b(n.dom_content_element).width(n.attrs.width), n.attrs.height = b(n.dom_content_element).height(), b(document).trigger("glazed_update_element", n.id)
                        }
                    });
                    for (var e = 0; e < this.children.length; e++) b.isNumeric(b(this.children[e].dom_element).css("z-index")) || b(this.children[e].dom_element).css("z-index", 0), null == this.children[e].controls && this.children[e].show_controls(), null == this.children[e].attrs.pos_top && (this.children[e].attrs.pos_top = "50%"), null == this.children[e].attrs.pos_left && (this.children[e].attrs.pos_left = "50%"), null == this.children[e].attrs.pos_width && (this.children[e].attrs.pos_width = "50%"), null == this.children[e].attrs.pos_height && (this.children[e].attrs.pos_height = "50%"), 0 == this.children[e].controls.find(".width100").length && b('<span title="' + r("100% width") + '" class="control width100 btn btn-default glyphicon glyphicon-resize-horizontal" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return e.data.object.attrs.pos_left = "0%", b(e.data.object.dom_element).css("left", "0%"), e.data.object.attrs.pos_width = "100%", b(e.data.object.dom_element).css("width", "100%"), !1
                    }), 0 == this.children[e].controls.find(".heigth100").length && b('<span title="' + r("100% heigth") + '" class="control heigth100 btn btn-default glyphicon glyphicon-resize-vertical" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return e.data.object.attrs.pos_top = "0%", b(e.data.object.dom_element).css("top", "0%"), e.data.object.attrs.pos_height = "100%", b(e.data.object.dom_element).css("height", "100%"), !1
                    }), 0 == this.children[e].controls.find(".forward").length && b('<span title="' + r("Bring forward") + '" class="control forward btn btn-default glyphicon glyphicon-arrow-up" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return b.isNumeric(b(e.data.object.dom_element).css("z-index")) ? (b(e.data.object.dom_element).css("z-index", Math.round(b(e.data.object.dom_element).css("z-index")) + 1), e.data.object.attrs.pos_zindex = b(e.data.object.dom_element).css("z-index")) : (b(e.data.object.dom_element).css("z-index", 0), e.data.object.attrs.pos_zindex = 0), n.zindex_normalize(), !1
                    }), 0 == this.children[e].controls.find(".backward").length && b('<span title="' + r("Send backward") + '" class="control backward btn btn-default glyphicon glyphicon-arrow-down" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return b.isNumeric(b(e.data.object.dom_element).css("z-index")) ? 0 < Math.round(b(e.data.object.dom_element).css("z-index")) && (b(e.data.object.dom_element).css("z-index", Math.round(b(e.data.object.dom_element).css("z-index")) - 1), e.data.object.attrs.pos_zindex = b(e.data.object.dom_element).css("z-index")) : (b(e.data.object.dom_element).css("z-index", 0), e.data.object.attrs.pos_zindex = 0), n.zindex_normalize(), !1
                    }), b(this.children[e].dom_element).draggable({
                        handle: "> .controls > .drag-and-drop",
                        containment: "#" + this.id,
                        scroll: !1,
                        snap: "#" + this.id + ", .az-element",
                        stop: function(e, t) {
                            a(this)
                        }
                    }), b(this.children[e].dom_element).resizable({
                        containment: "#" + this.id,
                        stop: function(e, t) {
                            a(this)
                        }
                    })
                }
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    G.baseclass.prototype.show_controls.apply(this, arguments), this.update_sortable();
                    var e = this;
                    b(this.dom_content_element).dblclick(function(t) {
                        1 == t.which && window.glazedBuilder.glazedElements.show(e, function(e) {
                            e.attrs.pos_top = t.offsetY.toString() + "px", e.attrs.pos_left = t.offsetX.toString() + "px"
                        })
                    })
                }
            },
            attach_children: function() {
                G.baseclass.prototype.attach_children.apply(this, arguments), drupalSettings.glazedBuilder.glazedEditor && this.update_sortable()
            },
            showed: function(o) {
                G.baseclass.prototype.showed.apply(this, arguments);
                var l = this;
                if (o(window).off("resize.az_layers" + l.id), "yes" == this.attrs.responsive) {
                    o(window).on("resize.az_layers" + l.id, function() {
                        var e = o(l.dom_element).width();
                        "o_width" in l.attrs && "" != l.attrs.o_width || (l.attrs.o_width = e);
                        var t = e / l.attrs.o_width;
                        o(l.dom_element).css("font-size", 100 * t + "%"), o(l.dom_content_element).css("height", l.attrs.height * t + "px"),
                            function e(t, a) {
                                var n = function(e, t) {
                                    var a = "",
                                        n = e.attrs[t].match(/font-size[: ]*([\-\d\.]*)(px|%|em) *;/);
                                    return null != n && (a = n[1]), a
                                }(t, "style");
                                "" != n && (n *= a, o(t.dom_element).css("font-size", n + "px"));
                                for (var i = 0; i < t.children.length; i++) e(l.children[i], a)
                            }(l, t)
                    }), o(window).trigger("resize")
                }
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-layers ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"><div id="' + this.id + '" class="az-ctnr"></div></div>'), this.dom_content_element = e(this.dom_element).find(".az-ctnr"), e(this.dom_content_element).css("width", this.attrs.width), e(this.dom_content_element).css("height", this.attrs.height), G.baseclass.prototype.render.apply(this, arguments)
            }
        }), U("az_container", !0, W), f(W.prototype, {
            name: Drupal.t("Glazed Container"),
            icon: "et et-icon-download",
            description: Drupal.t("AJAX Load Fields"),
            category: Drupal.t("Layout"),
            params: [D({
                type: "container",
                heading: Drupal.t("Glazed Container"),
                param_name: "container",
                description: Drupal.t("Type and name used as identificator to save container on server."),
                value: "/"
            })].concat(W.prototype.params),
            show_settings_on_create: !0,
            is_container: !0,
            hidden: !window.glazedBuilder.glazed_online,
            controls_base_position: "top-left",
            saveable: !0,
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            get_empty: function() {
                var e = '<div class="az-empty"><div class="top well"><strong>  <h3 class="glazed-choose-layout">Choose a template</h3></strong><p class="lead">' + Drupal.t("Or add elements from the + button or sidebar.</p></div>") + '<div class="top-right well"><h1>↗</h1> <span class="glyphicon glyphicon-save"></span>' + Drupal.t(" Don't forget to save your work") + "</div></div>",
                    t = b(e);
                return t.find(".glazed-choose-layout").bind("click", function(e) {
                    if (e.stopPropagation(), 1 == e.which) {
                        var t = b(this).closest("[data-az-id]").attr("data-az-id");
                        window.glazedBuilder.glazedElements.showTemplates(window.glazedBuilder.glazedElements.get_element(t), function(e) {})
                    }
                }), t
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    var a = this;
                    '<li><a href="https://www.youtube.com/watch?v=lODF-8byKRA" target="_blank">Basic Controls Video</a></li>', '<li><a href="http://www.sooperthemes.com/documentation" target="_blank">Documentation</a></li>', '<li><a href="http://www.sooperthemes.com/dashboard/tickets" target="_blank">Support Forum</a></li>', "</ul>", W.baseclass.prototype.show_controls.apply(this, arguments), null == this.parent && (b(document).ready(function() {
                        if (!b(a.dom_element).find(".az-empty").parent().hasClass("az-container") && "profile" in drupalSettings.glazedBuilder && !drupalSettings.glazedBuilder.profile.glazed_editor) {
                            b(a.dom_element).removeClass("glazed-editor");
                            var e = b(a.dom_element).find(".glazed-editor");
                            0 < e.length && e.each(function() {
                                b(this).removeClass("glazed-editor")
                            }), b(a.dom_element).find(".toggle-editor").addClass("editor-disable").html('<i class="glyphicon glyphicon-ban-circle" style="color:Tomato"></i>')
                        }
                    }), b('<span title="' + r("Toggle editor") + '" class="control toggle-editor btn btn-default glyphicon glyphicon-eye-open" > </span>').appendTo(this.controls).click(function() {
                        b(a.dom_element).toggleClass("glazed-editor");
                        var e = b(a.dom_element).find(".glazed-editor");
                        0 < e.length && e.each(function() {
                            b(this).removeClass("glazed-editor")
                        });
                        var t = b(a.dom_element).find(".toggle-editor");
                        return t.toggleClass("editor-disable"), t.hasClass("editor-disable") ? b(a.dom_element).find(".editor-disable").html('<i class="glyphicon glyphicon-ban-circle" style="color:Tomato"></i>') : t.children().remove(), !1
                    }), b('<span role="button" tabindex="0" title="' + r("Documentation and Support") + '" class="control glazed-help btn btn-default glyphicon glyphicon-question-sign glazed-builder-popover"> </span>').appendTo(this.controls).popover({
                        html: !0,
                        placement: "left",
                        content: function() {
                            return '<ul class="nav"><li><a href="https://www.youtube.com/watch?v=lODF-8byKRA" target="_blank">Basic Controls Video</a></li><li><a href="http://www.sooperthemes.com/documentation" target="_blank">Documentation</a></li><li><a href="http://www.sooperthemes.com/dashboard/tickets" target="_blank">Support Forum</a></li></ul>'
                        },
                        title: function() {
                            return "Support and Documentation"
                        }
                    }), this.controls.removeClass("btn-group-xs"), this.controls.find(".edit").remove(), this.controls.find(".copy").remove(), this.controls.find(".clone").remove(), this.controls.find(".remove").remove(), this.controls.find(".js-animation").remove(), this.controls.find(".drag-and-drop").attr("title", ""), this.controls.find(".drag-and-drop").removeClass("glyphicon-move"), this.controls.find(".drag-and-drop").removeClass("drag-and-drop")), this.saveable && this.loaded_container && (b(window).bind("beforeunload", function() {
                        if (void 0 !== window.glazedBuilder.glazed_edited && a.loaded_container in window.glazedBuilder.glazed_edited) {
                            var e = a.loaded_container;
                            "human_readable" in a.attrs && (e = a.attrs.human_readable);
                            var t = Drupal.t("Field") + " " + decodeURIComponent(escape(e)) + " " + Drupal.t("might have unsaved changes");
                            return b.notify(t, {
                                type: "warning",
                                z_index: "8000",
                                offset: {
                                    x: 25,
                                    y: 70
                                }
                            }), " "
                        }
                    }), null !== this.dom_element && this.dom_element.on("mousedown", null, a, function(e, t) {
                        var a = (t = e.data).attrs.container;
                        "human_readable" in t.attrs && (a = t.attrs.human_readable), window.glazedBuilder.glazed_edited[t.loaded_container] = a
                    }), b('<span title="' + r("Save container") + '" class="control save-container btn btn-success glyphicon glyphicon-save" > </span>').appendTo(this.controls).click({
                        object: this
                    }, this.click_save_container))
                }
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            get_hover_styles: function(e) {
                var t = "";
                "" != e.attrs.hover_style && (t = e.get_hover_style());
                for (var a = 0; a < e.children.length; a++) t += this.get_hover_styles(e.children[a]);
                return t
            },
            get_js: function(e) {
                var t = "";
                for (var a in e.js) t += '<script src="' + a + '"><\/script>\n';
                return t
            },
            get_css: function(e) {
                var t = "";
                for (var a in e.css) t += '<link rel="stylesheet" type="text/css" href="' + a + '">\n';
                return t
            },
            get_loader: function() {
                (function e(t) {
                    var a = {};
                    a[t.base] = !0;
                    for (var n = 0; n < t.children.length; n++) {
                        var i = e(t.children[n]);
                        b.extend(a, i)
                    }
                    return a
                })(this),
                function e(t) {
                    var a = {};
                    "an_start" in t.attrs && "" != t.attrs.an_start && (a.an_start = !0);
                    for (var n = 0; n < t.children.length; n++) b.extend(a, e(t.children[n]));
                    return a
                }(this);
                var e = "",
                    t = "";
                return t = drupalSettings.glazedBuilder.glazedDevelopment ? drupalSettings.glazedBuilder.glazedBaseUrl + "glazed_frontend.js" : drupalSettings.glazedBuilder.glazedBaseUrl + "glazed_frontend.min.js",
                    function e(t) {
                        if ("an_start" in t.attrs && "" != t.attrs.an_start) return animation = !0;
                        if (t.constructor.prototype.hasOwnProperty("showed")) {
                            var a = !0;
                            switch (("is_cms_element" in t || "is_template_element" in t) && (a = !1), t.base) {
                                case "az_container":
                                    null == t.parent && (a = !1);
                                    break;
                                case "az_section":
                                    "" == t.attrs.effect && (a = !1);
                                    break;
                                case "az_row":
                                    "yes" != t.attrs.equal && (a = !1)
                            }
                            if (a) return !0
                        }
                        for (var n = 0; n < t.children.length; n++)
                            if (e(t.children[n])) return !0;
                        return !1
                    }(this) && (e += '<script src="' + t + '"><\/script>\n'), e
            },
            get_html: function() {
                this.recursive_update_data(), this.recursive_clear_animation();
                var e = b("<div>" + b(this.dom_content_element).html() + "</div>");
                return this.recursive_restore(e), b(e).find(".az-element > .controls").remove(), b(e).find(".az-section > .add-section").remove(), b(e).find("> .controls").remove(), b(e).find(".az-sortable-controls").remove(), b(e).find(".az-empty").remove(), b(e).find(".ui-resizable-e").remove(), b(e).find(".ui-resizable-s").remove(), b(e).find(".ui-resizable-se").remove(), b(e).find(".ckeditor-inline").each(function() {
                    var e = b(this),
                        t = e.contents();
                    e.replaceWith(t)
                }), b(e).find(".az-text").removeClass("cke_editable cke_editable_inline cke_contents_ltr cke_show_borders"), b(e).find(".init-colorbox-processed").removeClass("init-colorbox-processed"), b(e).find(".az-element--controls-center").removeClass("az-element--controls-center"), b(e).find(".az-element--controls-top-left").removeClass("az-element--controls-top-left"), b(e).find(".az-element--controls-show-parent").removeClass("az-element--controls-show-parent"), b(e).find(".az-element--controls-spacer").removeClass("az-element--controls-spacer"), b(e).find(".az-element--hide-controls").removeClass("az-element--hide-controls"), b(e).find(".editable-highlight").removeClass("editable-highlight"), b(e).find(".styleable-highlight").removeClass("styleable-highlight"), b(e).find(".sortable-highlight").removeClass("sortable-highlight"), b(e).find(".ui-draggable").removeClass("ui-draggable"), b(e).find(".ui-resizable").removeClass("ui-resizable"), b(e).find(".ui-sortable").removeClass("ui-sortable"), b(e).find(".az-element.az-container > .az-ctnr").empty(), b(e).find(".az-element.az-cms-element").empty(), b(e).find(".mbYTP_wrapper").remove(), b(e).find("grammarly-extension").remove(), b(e).find(".az-html").each(function() {
                    var e = b(this)[0].getAttribute("data-az-persist");
                    con = window.glazedBuilder.atobUTF16(e), b(this).html(con)
                }), b(e).html()
            },
            get_container_html: function() {
                return this.get_html() + this.get_css(this) + this.get_hover_styles(this) + this.get_js(this) + this.get_loader()
            },
            click_save_container: function(e) {
                return e.data.object.save_container(), !1
            },
            save_container: function() {
                var t = this;
                glazedBuilder.glazed_add_js({
                    path: "jsON-js/json2.min.js",
                    loaded: "JSON" in window,
                    callback: function() {
                        var e = t.get_container_html();
                        n(t.attrs.container, t.attrs.human_readable, e, t.attrs.langcode)
                    }
                })
            },
            load_container: function() {
                var a = this;
                window.glazedBuilder.loadedContainers = window.glazedBuilder.loadedContainers || {}, window.glazedBuilder.loadedContainers[a.id] || (window.glazedBuilder.loadedContainers[a.id] = !0, "" != this.attrs.container && function(t, a, n) {
                    window.glazedBuilder.glazed_containers_loaded.hasOwnProperty(t + "/" + a) ? n(window.glazedBuilder.glazed_containers_loaded[t + "/" + a]) : window.glazedBuilder.glazed_online && b.ajax({
                        type: "get",
                        url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        b.ajax({
                            type: "POST",
                            url: e,
                            data: {
                                action: "glazed_load_container",
                                type: t,
                                name: a
                            },
                            cache: !drupalSettings.glazedBuilder.glazedEditor
                        }).done(function(e) {
                            window.glazedBuilder.glazed_containers_loaded[t + "/" + a] = e, n(e)
                        }).fail(function() {
                            n("")
                        })
                    })
                }(this.attrs.container.split("/")[0], this.attrs.container.split("/")[1], function(e) {
                    if (/^\s*\<[\s\S]*\>\s*$/.exec(e)) {
                        a.loaded_container = a.attrs.container, b(e).appendTo(a.dom_content_element), b(a.dom_content_element).find("> script").detach().appendTo("head"), b(a.dom_content_element).find("> link[href]").detach().appendTo("head"), b(a.dom_element).css("display", ""), b(a.dom_element).addClass("glazed"), a.parse_html(a.dom_content_element), b(a.dom_element).attr("data-az-id", a.id), a.html_content = !0;
                        for (var t = 0; t < a.children.length; t++) a.children[t].recursive_render();
                        a.dom_content_element.empty(), drupalSettings.glazedBuilder.glazedEditor && (a.show_controls(), a.update_sortable()), a.parent.attach_children(), a.attach_children();
                        for (t = 0; t < a.children.length; t++) a.children[t].recursive_showed();
                        b(document).trigger("scroll")
                    } else if (!window.glazedBuilder.glazed_frontend) {
                        a.loaded_container = a.attrs.container, a.parse_shortcode(e), b(a.dom_element).attr("data-az-id", a.id), drupalSettings.glazedBuilder.glazedEditor && (a.show_controls(), a.update_sortable());
                        for (t = 0; t < a.children.length; t++) a.children[t].recursive_render();
                        a.attach_children(), null != a.parent && a.parent.update_dom();
                        for (t = 0; t < a.children.length; t++) a.children[t].recursive_showed();
                        b(document).trigger("scroll")
                    }
                    window.glazedBuilder.glazedElements.try_render_unknown_elements()
                }))
            },
            clone: function() {
                W.baseclass.prototype.clone.apply(this, arguments), this.rendered = !0
            },
            recursive_render: function() {
                window.glazedBuilder.glazed_frontend ? (this.render(b), this.children = []) : W.baseclass.prototype.recursive_render.apply(this, arguments), drupalSettings.glazedBuilder.glazedEditor && (this.show_controls(), this.update_sortable())
            },
            update_dom: function() {
                this.loaded_container != this.attrs.container && (this.children = [], b(this.dom_content_element).empty(), this.rendered = !1, null != this.parent && W.baseclass.prototype.update_dom.apply(this, arguments))
            },
            showed: function(e) {
                W.baseclass.prototype.showed.apply(this, arguments);
                var t = this;
                null == this.parent ? t.rendered || (t.rendered = !0, t.load_container()) : this.add_js({
                    path: "vendor/jquery.waypoints/lib/jquery.waypoints.min.js",
                    loaded: "waypoint" in e.fn,
                    callback: function() {
                        e(t.dom_element).waypoint(function(e) {
                            t.rendered || (t.rendered = !0, t.load_container())
                        }, {
                            offset: "100%",
                            handler: function(e) {
                                this.destroy()
                            }
                        }), e(document).trigger("scroll")
                    }
                })
            },
            render: function(e) {
                "/" != this.attrs.container && (this.dom_element = e('<div class="az-element az-container"><div class="az-ctnr"></div></div>'), this.dom_content_element = e(this.dom_element).find(".az-ctnr"), W.baseclass.prototype.render.apply(this, arguments))
            }
        }), I("az_unknown", !0, $), f($.prototype, {
            has_content: !0,
            hidden: !0,
            render: function(e) {
                if (this.dom_element = e('<div class = "az-element az-unknown">' + Drupal.t("Element Not Found.") + "</div>"), this.dom_content_element = this.dom_element, "content" in this.attrs) {
                    var t = /\[[^\]]*\]([^\[]*)\[\/[^\]]*\]/.exec(this.attrs.content);
                    t && e(this.dom_element).append(t[1])
                }
                $.baseclass.prototype.render.apply(this, arguments)
            }
        }),
        function() {
            if ("glazed_elements" in window.glazedBuilder)
                for (var e = 0; e < window.glazedBuilder.glazed_elements.length; e++) {
                    var t = window.glazedBuilder.glazed_elements[e],
                        a = function(e, t) {
                            a.baseclass.apply(this, arguments)
                        };
                    U(t.base, t.is_container, a), t.baseclass = a.baseclass, t.params = t.params.concat(a.prototype.params), f(a.prototype, t);
                    for (var n = 0; n < a.prototype.params.length; n++) {
                        var i = D(a.prototype.params[n]);
                        a.prototype.params[n] = i
                    }
                }
        }(), drupalSettings.glazedBuilder.glazedEditor && !window.glazedBuilder.glazed_online || function(e) {
            if (drupalSettings.glazedBuilder && drupalSettings.glazedBuilder.glazedTemplateElements) {
                for (var t in drupalSettings.glazedBuilder.glazedTemplateElements) drupalSettings.glazedBuilder.glazedTemplateElements[t].html = decodeURIComponent(drupalSettings.glazedBuilder.glazedTemplateElements[t].html);
                e(drupalSettings.glazedBuilder.glazedTemplateElements)
            }
        }(function(e) {
            _.isObject(e) && window.glazedBuilder.glazedElements.create_template_elements(e)
        }),
        function(t) {
            drupalSettings.glazedBuilder.cmsElementNames ? t(drupalSettings.glazedBuilder.cmsElementNames) : b.ajax({
                type: "get",
                url: drupaLsettings.glazeDbuilder.glazeDcsrFurl,
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                b.ajax({
                    type: "POST",
                    url: e,
                    data: {
                        action: "glazed_builder_get_cms_element_names",
                        url: window.location.href
                    },
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    t(e)
                }).fail(function() {
                    t(!1)
                })
            })
        }(function(e) {
            _.isObject(e) ? window.glazedBuilder.glazedElements.create_cms_elements(e) : window.glazedBuilder.glazedElements.cms_elements_loaded = !0
        }), J(), Drupal.behaviors.CKinlineAttach = {
            attach: function() {
                b(window).bind("CKinlineAttach", function() {
                    function e() {
                        function e() {
                            b("body").find(".glazed").each(function() {
                                b(this).hasClass("glazed-editor") && b(this).find(".az-element.az-text, .az-element.az-blockquote").each(function() {
                                    var e = b(this);
                                    if (!e.find(".ckeditor-inline").length) {
                                        $controls = e.find(".controls").appendTo("body"), e.wrapInner("<div class='ckeditor-inline' contenteditable='true' />"), e.prepend($controls);
                                        var t = e.find(".ckeditor-inline")[0];
                                        void 0 !== t && 0 == b(t).hasClass("cke_focus") && CKEDITOR.inline(t)
                                    }
                                })
                            })
                        }
                        CKEDITOR.on("instanceCreated", function(e) {
                            var t = e.editor;
                            t.on("focus", function() {
                                b(t.element.$).parents(".az-element").addClass("az-element--hide-controls")
                            }), t.on("blur", function() {
                                b(t.element.$).parents(".az-element").removeClass("az-element--hide-controls")
                            })
                        }), window.glazedBuilder.glazed_builder_set_ckeditor_config("inline"), b(".glazed-editor").length ? e() : b(".controls .control.toggle-editor").bind("click", function() {
                            e()
                        })
                    }
                    "CKEDITOR" in window ? e() : glazedBuilder.glazed_add_js({
                        path: "vendor/ckeditor/ckeditor.js",
                        callback: function() {
                            _.isObject(CKEDITOR) && e()
                        }
                    })
                }).trigger("CKinlineAttach"), b("button.control.save-container").on("click", function() {
                    b(window).trigger("CKinlineAttach")
                }), b(document).ready(function() {
                    b(".controls .control.toggle-editor").bind("click", function() {
                        b(".wrap-containers .glazed").each(function() {
                            var e = b(this);
                            if (e.hasClass("glazed-editor")) e.find(".az-element.az-text .ckeditor-inline, .az-element.az-blockquote .ckeditor-inline").each(function() {
                                var e = b(this);
                                e.attr("contenteditable", !0);
                                var t = e[0];
                                void 0 !== t && CKEDITOR.inline(t)
                            });
                            else
                                for (var t in b(this).find(".az-element.az-text .ckeditor-inline, .az-element.az-blockquote .ckeditor-inline").each(function() {
                                        var e = b(this);
                                        e.attr("contenteditable", !1), e.off("click")
                                    }), CKEDITOR.instances) CKEDITOR.instances[t].destroy()
                        })
                    })
                }), b(document).on("click", function(e) {
                    b(e.target).is(".glazed-builder-popover") || b(".glazed-builder-popover").popover("hide")
                })
            }
        }
}(window.jQuery);