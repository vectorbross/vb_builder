/*! glazed_builder 23-07-2019 */

! function(y) {
    if (y.ajaxPrefilter(function(e, t, a) {
            "script" != e.dataType && "script" != t.dataType || (e.cache = !0)
        }), !drupalSettings.glazedBuilder.glazedBaseUrl)
        if (0 < y('script[src*="glazed_builder.js"]').length) {
            var e = y('script[src*="glazed_builder.js"]').attr("src");
            drupalSettings.glazedBuilder.glazedBaseUrl = e.slice(0, e.indexOf("glazed_builder.js"))
        } else if (0 < y('script[src*="glazed_builder.min.js"]').length) {
        e = y('script[src*="glazed_builder.min.js"]').attr("src");
        drupalSettings.glazedBuilder.glazedBaseUrl = e.slice(0, e.indexOf("glazed_builder.min.js"))
    }
    "glazed_online" in window || (window.glazed_online = "http:" == window.location.protocol || "https:" == window.location.protocol), window.glazed_backend = !0, window.glazed_title = {
        "Drag and drop": Drupal.t("Drag and drop element."),
        Add: Drupal.t("Add new element into current element area."),
        Edit: Drupal.t("Open settings form to change element properties, set CSS styles and add CSS classes."),
        Paste: Drupal.t("Paste elements into current element area from clipboard copied into it before."),
        Copy: Drupal.t("Copy element or contained elements to clipboard."),
        Clone: Drupal.t("Clone current element."),
        Remove: Drupal.t("Delete current element"),
        "Save as template": Drupal.t("Save element or contained elements as template to template library."),
        "Save container": Drupal.t("Save to server all elements which placed in current container element.")
    }, drupalSettings.glazedBuilder.glazedEditor || (drupalSettings.glazedBuilder.glazedEditor = !1);
    var l = !1,
        i = {},
        o = {},
        b = {},
        z = [],
        r = {};
    window.glazed_edited = {};
    var t = {
        "": Drupal.t("No animation"),
        bounce: Drupal.t("bounce"),
        float: Drupal.t("float"),
        floatSmall: Drupal.t("floatSmall"),
        floatLarge: Drupal.t("floatLarge"),
        pulse: Drupal.t("pulse"),
        shake: Drupal.t("shake"),
        wobble: Drupal.t("wobble"),
        jello: Drupal.t("jello"),
        fadeIn: Drupal.t("fadeIn"),
        fadeInDown: Drupal.t("fadeInDown"),
        fadeInDownBig: Drupal.t("fadeInDownBig"),
        fadeInLeft: Drupal.t("fadeInLeft"),
        fadeInLeftBig: Drupal.t("fadeInLeftBig"),
        fadeInRight: Drupal.t("fadeInRight"),
        fadeInRightBig: Drupal.t("fadeInRightBig"),
        fadeInUp: Drupal.t("fadeInUp"),
        fadeInUpBig: Drupal.t("fadeInUpBig"),
        fadeOut: Drupal.t("fadeOut"),
        fadeOutDown: Drupal.t("fadeOutDown"),
        fadeOutDownBig: Drupal.t("fadeOutDownBig"),
        fadeOutLeft: Drupal.t("fadeOutLeft"),
        fadeOutLeftBig: Drupal.t("fadeOutLeftBig"),
        fadeOutRight: Drupal.t("fadeOutRight"),
        fadeOutRightBig: Drupal.t("fadeOutRightBig"),
        fadeOutUp: Drupal.t("fadeOutUp"),
        fadeOutUpBig: Drupal.t("fadeOutUpBig"),
        flipInX: Drupal.t("flipInX"),
        flipInY: Drupal.t("flipInY"),
        zoomIn: Drupal.t("zoomIn"),
        zoomInDown: Drupal.t("zoomInDown"),
        zoomInLeft: Drupal.t("zoomInLeft"),
        zoomInRight: Drupal.t("zoomInRight"),
        zoomInUp: Drupal.t("zoomInUp"),
        zoomOut: Drupal.t("zoomOut"),
        zoomOutDown: Drupal.t("zoomOutDown"),
        zoomOutLeft: Drupal.t("zoomOutLeft"),
        zoomOutRight: Drupal.t("zoomOutRight"),
        zoomOutUp: Drupal.t("zoomOutUp"),
        slideInDown: Drupal.t("slideInDown"),
        slideInLeft: Drupal.t("slideInLeft"),
        slideInRight: Drupal.t("slideInRight"),
        slideInUp: Drupal.t("slideInUp"),
        slideOutDown: Drupal.t("slideOutDown"),
        slideOutLeft: Drupal.t("slideOutLeft"),
        slideOutRight: Drupal.t("slideOutRight"),
        slideOutUp: Drupal.t("slideOutUp")
    };

    function d(e) {
        return "glazed_title" in window && e in window.glazed_title ? window.glazed_title[e] : (t = e, "Drupal" in window ? Drupal.t(t) : t);
        var t
    }

    function s(e, t) {
        var a = function() {};
        a.prototype = t.prototype, e.prototype = new a, (e.prototype.constructor = e).baseclass = t
    }

    function w(e, t) {
        var a = {};
        for (var n in t) void 0 !== a[n] && a[n] == t[n] || (e[n] = t[n]);
        if (document.all && !document.isOpera) {
            var i = t.toString;
            "function" == typeof i && i != e.toString && i != a.toString && "\nfunction toString() {\n  [native code]\n}\n" != i && (e.toString = t.toString)
        }
        return e
    }

    function D(e) {
        function i(e) {
            return ("0" + parseInt(e).toString(16)).slice(-2)
        }
        return e.replace(/rgb\((\d+),\s*(\d+),\s*(\d+)\)/g, function(e, t, a, n) {
            return "#" + i(t) + i(a) + i(n)
        })
    }

    function c(e, t) {
        var a = "col-" + t + "-",
            n = e ? e.split("/") : [1, 1],
            i = _.range(1, 13),
            o = !_.isUndefined(n[0]) && 0 <= _.indexOf(i, parseInt(n[0], 10)) && parseInt(n[0], 10),
            s = !_.isUndefined(n[1]) && 0 <= _.indexOf(i, parseInt(n[1], 10)) && parseInt(n[1], 10);
        return !1 !== o && !1 !== s ? a + 12 * o / s : a + "12"
    }
    _.memoize(function(e) {
        return new RegExp("(\\[(\\[?)[" + e + "]+(?![\\w-])[^\\]\\/]*[\\/(?!\\])[^\\]\\/]*]?(?:\\/]\\]|\\](?:[^\\[]*(?:\\[(?!\\/" + e + "\\])[^\\[]*)*\\[\\/" + e + "\\])?)\\]?)", "g")
    });
    var p = _.memoize(function(e) {
        return new RegExp("\\[(\\[?)(" + e + ")(?![\\w-])([^\\]\\/]*(?:\\/(?!\\])[^\\]\\/]*)*?)(?:(\\/)\\]|\\](?:([^\\[]*(?:\\[(?!\\/\\2\\])[^\\[]*)*)(\\[\\/\\2\\]))?)(\\]?)")
    });

    function h(e) {
        return _.isString(e) ? e.replace(/(\`{2})/g, '"') : e
    }

    function m(e) {
        return decodeURIComponent((e + "").replace(/%(?![\da-f]{2})/gi, function() {
            return "%25"
        }))
    }

    function u(e) {
        var t, a, n = (t = e, a = parseInt(y(t).css("z-index")), y(t).parent().find("*").each(function() {
            var e = parseInt(y(this).css("z-index"));
            a < e && (a = e)
        }), a);
        y(e).css("z-index", n + 1)
    }

    function g(e, t) {
        var a = "<select>";
        for (var n in e) a = a + '<option value="' + n + '">"' + e[n] + '"</option>';
        a += "</select>", y(t).css("display", "none");
        var i = y(a).insertAfter(t);
        if (y(t).val().length) {
            y(i).append('<option value=""></option>');
            var o = y(t).val();
            y(i).find('option[value="' + o + '"]').length || y(i).append('<option value="' + o + '">"' + o + '"</option>'), y(i).find('option[value="' + o + '"]').attr("selected", "selected")
        } else y(i).append('<option value="" selected></option>');
        return y(i).chosen({
            search_contains: !0,
            allow_single_deselect: !0
        }), y(i).change(function() {
            y(this).find("option:selected").each(function() {
                y(t).val(y(this).val())
            })
        }), y(i).parent().find(".chosen-container").width("100%"), y('<div><a class="direct-input" href="#">' + Drupal.t("Edit as text") + "</a></div>").insertBefore(i).click(function() {
            y(t).css("display", "block"), y(i).parent().find(".chosen-container").remove(), y(i).remove(), y(this).remove()
        }), i
    }

    function f(t, a) {
        y.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            y.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_get_container_names",
                    container_type: t,
                    url: window.location.href
                },
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                a(e)
            })
        })
    }

    function a(a, n, t, i) {
        type = a.split("/")[0], name = a.split("/")[1], null == n && (n = a), y.ajax({
            type: "GET",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            y.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_save_container",
                    type: type,
                    name: name,
                    lang: i,
                    shortcode: function(e) {
                        var t = "";
                        for (j = 0; j < e.length; j++) {
                            var a = 7 ^ e.charCodeAt(j);
                            t += String.fromCharCode(a)
                        }
                        return t
                    }(encodeURIComponent(t))
                },
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                var t = Drupal.t("Saved field") + " " + n;
                y.notify(t, {
                    type: "success",
                    z_index: "8000",
                    offset: {
                        x: 25,
                        y: 70
                    }
                }), a in window.glazed_edited && delete window.glazed_edited[a]
            }).fail(function(e) {
                var t = Drupal.t("Server error: Unable to save field") + " " + n;
                y.notify(t, {
                    type: "danger",
                    z_index: "8000",
                    offset: {
                        x: 25,
                        y: 70
                    }
                })
            })
        }).fail(function(e) {
            var t = Drupal.t("Server error: Unable to save field") + " " + n + ". Debug information: unable to obtain csrf token.";
            y.notify(t, {
                type: "danger",
                z_index: "8000",
                offset: {
                    x: 25,
                    y: 70
                }
            })
        })
    }

    function v(t, a, n, i, o) {
        y.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            e.originalPath = drupalSettings.glazedBuilder.currentPath, y.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_builder_load_cms_element",
                    name: t,
                    settings: a,
                    container: n,
                    data: i
                },
                dataType: "json",
                cache: !drupalSettings.glazedBuilder.glazedEditor
            }).done(function(e) {
                y(e.css).appendTo(y("head")), y(e.js).appendTo(y("head")), y.extend(!0, drupalSettings, e.settings), o(e.data)
            })
        })
    }

    function x(t, a) {
        y.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            y.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_load_template",
                    url: window.location.href,
                    name: t
                },
                cache: !1
            }).done(function(e) {
                a(e)
            }).fail(function() {
                a("")
            })
        })
    }

    function k(t) {
        y.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            y.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_delete_template",
                    url: window.location.href,
                    name: t
                },
                cache: !1,
                context: this
            }).done(function(e) {})
        })
    }

    function S(t, a) {
        y.ajax({
            type: "get",
            url: drupalSettings.glazedBuilder.glazedCsrfUrl,
            dataType: "json",
            cache: !1,
            context: this
        }).done(function(e) {
            y.ajax({
                type: "POST",
                url: e,
                data: {
                    action: "glazed_load_page_template",
                    url: window.location.href,
                    uuid: t
                },
                cache: !1
            }).done(function(e) {
                a(e)
            }).fail(function() {
                a("")
            })
        })
    }
    y.fn.closest_descendents = function(e) {
        for (var t = y(), a = this; a.length;) t = y.merge(t, a.filter(e)), a = (a = a.not(e)).children();
        return t
    }, y.fn.deserialize = function(e) {
        var n = y(this),
            i = {};
        return y.each(e.split("&"), function() {
            var e = this.split("="),
                t = m(e[0]),
                a = 1 < e.length ? m(e[1]) : null;
            t in i || (i[t] = []), i[t].push(a)
        }), y.each(i, function(e, t) {
            n.find("[name='" + e + "']").val(t[0].replace(/\+/g, " ")), "on" == t[0].replace(/\+/g, " ") && n.find("[type='checkbox'][name='" + e + "']").attr("checked", "checked"), n.find("select[name='" + e + "'] > option").removeAttr("selected");
            for (var a = 0; a < t.length; a++) n.find("select[name='" + e + "'] > option[value='" + t[a] + "']").prop("selected", "selected")
        }), y("input:checkbox:checked,input:radio:checked", n).each(function() {
            y(this).attr("name") in i || (this.checked = !1)
        }), this
    }, window.glazed_add_css = function(e, t) {
        var a = drupalSettings.glazedBuilder.glazedBaseUrl + e;
        if (y('link[href*="' + a + '"]').length || "glazed_exported" in window) t();
        else {
            var n = document.getElementsByTagName("head")[0],
                i = document.createElement("link");
            i.rel = "stylesheet", i.type = "text/css", i.href = a, i.onload = t, n.appendChild(i)
        }
    }, window.glazed_add_js_list = function(e) {
        if ("loaded" in e && e.loaded) e.callback();
        else
            for (var t = 0, a = 0; a < e.paths.length; a++) glazed_add_js({
                path: e.paths[a],
                callback: function() {
                    ++t == e.paths.length && e.callback()
                }
            })
    }, window.glazed_add_js = function(e) {
        "loaded" in e && e.loaded || "glazed_exported" in window ? e.callback() : glazed_add_external_js(drupalSettings.glazedBuilder.glazedBaseUrl + e.path, "callback" in e ? e.callback : function() {})
    }, window.glazed_add_external_js = function(a, e) {
        if (a in i) i[a].push(e);
        else if (a in o) e();
        else {
            i[a] = [e];
            var t = document.getElementsByTagName("head")[0],
                n = document.createElement("script");
            n.type = "text/javascript", n.src = a, n.onload = function() {
                for (o[a] = !0; a in i;) {
                    var e = i[a];
                    i[a] = void 0, delete i[a];
                    for (var t = 0; t < e.length; t++) e[t]()
                }
            }, t.appendChild(n)
        }
    }, window.glazed_builder_set_ckeditor_config = function(e) {
        CKEDITOR.disableAutoInline = !0, CKEDITOR.config.fillEmptyBlocks = !1, CKEDITOR.config.allowedContent = !0, CKEDITOR.config.autoParagraph = !1, CKEDITOR.config.contentsCss = ["//cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css"], void 0 !== window.drupalSettings && void 0 !== window.drupalSettings.glazed && void 0 !== window.drupalSettings.glazed.glazedPath.length && CKEDITOR.config.contentsCss.push(drupalSettings.basePath + window.drupalSettings.glazed.glazedPath + "css/glazed.css"), void 0 !== drupalSettings.glazedBuilder.cke_stylesset ? CKEDITOR.config.stylesSet = drupalSettings.glazedBuilder.cke_stylesset : CKEDITOR.config.stylesSet = [{
            name: "Lead",
            element: "p",
            attributes: {
                class: "lead"
            }
        }, {
            name: "Muted",
            element: "p",
            attributes: {
                class: "text-muted"
            }
        }, {
            name: "Highlighted",
            element: "mark"
        }, {
            name: "Small",
            element: "small"
        }, {
            name: "Button Primary",
            element: "div",
            attributes: {
                class: "btn btn-primary"
            }
        }, {
            name: "Button Default",
            element: "div",
            attributes: {
                class: "btn btn-default"
            }
        }], void 0 !== drupalSettings.glazedBuilder.cke_fonts ? CKEDITOR.config.font_names = drupalSettings.glazedBuilder.cke_fonts : CKEDITOR.config.font_names = "Arial/Arial, Helvetica, sans-serif;Georgia/Georgia, serif;Times New Roman/Times New Roman, Times, serif;Verdana/Verdana, Geneva, sans-serif", CKEDITOR.config.fontSize_sizes = "8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;60/60px;72/72px;90/90px;117/117px;144/144px", CKEDITOR.config.protectedSource.push(/<link.*?>/gi);
        var t = [];
        for (var a in window.sooperthemes_theme_palette) t.push(window.sooperthemes_theme_palette[a].substring(1));
        var n = t.join(",") + ",";
        CKEDITOR.config.hasOwnProperty("colorButton_colors") && CKEDITOR.config.colorButton_colors.indexOf(n) < 0 && (CKEDITOR.config.colorButton_colors = n + CKEDITOR.config.colorButton_colors), "inline" == e ? "profile" in window.drupalSettings.glazedBuilder ? CKEDITOR.config.toolbar = window.drupalSettings.glazedBuilder.profile.ck_config.inline : CKEDITOR.config.toolbar = [{
            name: "basicstyles",
            items: ["Bold", "Italic", "RemoveFormat"]
        }, {
            name: "colors",
            items: ["TextColor"]
        }, {
            name: "styles",
            items: ["Format", "Styles", "FontSize"]
        }, {
            name: "paragraph",
            items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "BulletedList", "NumberedList"]
        }, {
            name: "links",
            items: ["Link", "Unlink"]
        }, {
            name: "insert",
            items: ["Image", "Table"]
        }, {
            name: "clipboard",
            items: ["Undo", "Redo"]
        }] : "profile" in drupalSettings.glazedBuilder ? CKEDITOR.config.toolbar = drupalSettings.glazedBuilder.profile.ck_config.modal : CKEDITOR.config.toolbar = [{
            name: "basicstyles",
            items: ["Bold", "Italic", "Underline", "Strike", "Superscript", "Subscript", "RemoveFormat"]
        }, {
            name: "paragraph",
            items: ["JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock", "BulletedList", "Outdent", "Indent", "Blockquote", "CreateDiv"]
        }, {
            name: "clipboard",
            items: ["Undo", "Redo", "PasteText", "PasteFromWord"]
        }, {
            name: "links",
            items: ["Link", "Unlink"]
        }, {
            name: "insert",
            items: ["Image", "HorizontalRule", "SpecialChar", "Table", "Templates"]
        }, {
            name: "colors",
            items: ["TextColor"]
        }, {
            name: "document",
            items: ["Source"]
        }, {
            name: "tools",
            items: ["ShowBlocks", "Maximize"]
        }, {
            name: "styles",
            items: ["Format", "Styles", "FontSize"]
        }, {
            name: "editing",
            items: ["Scayt"]
        }]
    };
    var C = {};

    function T() {
        this.dom_element = null, this.heading = "", this.description = "", this.param_name = "", this.required = !1, this.admin_label = "", this.holder = "", this.wrapper_class = "", this.value = null, this.can_be_empty = !1, this.hidden = !1, this.tab = "", this.dependency = {}, "create" in this && this.create()
    }

    function n(e, t) {
        s(t, T), t.prototype.type = e, T.prototype.param_types[e] = t
    }

    function B(e) {
        var t;
        return e.type in T.prototype.param_types ? w(t = new T.prototype.param_types[e.type], e) : w(t = new T, e), t
    }
    if (C.shortcode = {
            next: function(e, t, a) {
                var n, i, o = C.shortcode.regexp(e);
                if (o.lastIndex = a || 0, n = o.exec(t)) return "[" === n[1] && "]" === n[7] ? C.shortcode.next(e, t, o.lastIndex) : (i = {
                    index: n.index,
                    content: n[0],
                    shortcode: C.shortcode.fromMatch(n)
                }, n[1] && (i.match = i.match.slice(1), i.index++), n[7] && (i.match = i.match.slice(0, -1)), i)
            },
            replace: function(e, t, d) {
                return t.replace(C.shortcode.regexp(e), function(e, t, a, n, i, o, s, l) {
                    if ("[" === t && "]" === l) return e;
                    var r = d(C.shortcode.fromMatch(arguments));
                    return r ? t + r + l : e
                })
            },
            string: function(e) {
                return new C.shortcode(e).string()
            },
            regexp: _.memoize(function(e) {
                return new RegExp("\\[(\\[?)(" + e + ")(?![\\w-])([^\\]\\/]*(?:\\/(?!\\])[^\\]\\/]*)*?)(?:(\\/)\\]|\\](?:([^\\[]*(?:\\[(?!\\/\\2\\])[^\\[]*)*)(\\[\\/\\2\\]))?)(\\]?)", "g")
            }),
            attrs: _.memoize(function(e) {
                var t, a, n = {},
                    i = [];
                for (t = /(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/g, e = e.replace(/[\u00a0\u200b]/g, " "); a = t.exec(e);) a[1] ? n[a[1].toLowerCase()] = a[2] : a[3] ? n[a[3].toLowerCase()] = a[4] : a[5] ? n[a[5].toLowerCase()] = a[6] : a[7] ? i.push(a[7]) : a[8] && i.push(a[8]);
                return {
                    named: n,
                    numeric: i
                }
            }),
            fromMatch: function(e) {
                var t;
                return t = e[4] ? "self-closing" : e[6] ? "closed" : "single", new C.shortcode({
                    tag: e[2],
                    attrs: e[3],
                    type: t,
                    content: e[5]
                })
            }
        }, C.shortcode = _.extend(function(e) {
            _.extend(this, _.pick(e || {}, "tag", "attrs", "type", "content"));
            var t = this.attrs;
            this.attrs = {
                named: {},
                numeric: []
            }, t && (_.isString(t) ? this.attrs = C.shortcode.attrs(t) : _.isEqual(_.keys(t), ["named", "numeric"]) ? this.attrs = t : _.each(e.attrs, function(e, t) {
                this.set(t, e)
            }, this))
        }, C.shortcode), _.extend(C.shortcode.prototype, {
            get: function(e) {
                return this.attrs[_.isNumber(e) ? "numeric" : "named"][e]
            },
            set: function(e, t) {
                return this.attrs[_.isNumber(e) ? "numeric" : "named"][e] = t, this
            },
            string: function() {
                var a = "[" + this.tag;
                return _.each(this.attrs.numeric, function(e) {
                    /\s/.test(e) ? a += ' "' + e + '"' : a += " " + e
                }), _.each(this.attrs.named, function(e, t) {
                    a += " " + t + '="' + e + '"'
                }), "single" === this.type ? a + "]" : "self-closing" === this.type ? a + " /]" : (a += "]", this.content && (a += this.content), a + "[/" + this.tag + "]")
            }
        }), C.html = _.extend(C.html || {}, {
            attrs: function(e) {
                var t, a;
                return "/" === e[e.length - 1] && (e = e.slice(0, -1)), t = C.shortcode.attrs(e), a = t.named, _.each(t.numeric, function(e) {
                    /\s/.test(e) || (a[e] = "")
                }), a
            },
            string: function(e) {
                var a = "<" + e.tag,
                    t = e.content || "";
                return _.each(e.attrs, function(e, t) {
                    a += " " + t, "" !== e && (_.isBoolean(e) && (e = e ? "true" : "false"), a += '="' + e + '"')
                }), e.single ? a + " />" : (a += ">", (a += _.isObject(t) ? C.html.string(t) : t) + "</" + e.tag + ">")
            }
        }), T.prototype = {
            safe: !0,
            param_types: {},
            spawn_modal_skeleton: function(e) {
                y("#az-elements-modal").remove();
                var t = "";
                "base" in e && (t = '<a class="help-link close" href="https://www.sooperthemes.com/documentation/' + e.base.replace("az_", "drupal-") + '" target="_blank"><span role="button" class="glyphicon glyphicon-question-sign" title="Element Tutorial"> </span><a/>');
                y('<div id="az-editor-modal" class="modal glazed"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><span class="close" data-dismiss="modal" aria-hidden="true">&times;</span><h4 class="modal-title">' + e.name + " " + Drupal.t("settings") + t + '</h4></div><div class="modal-body"></div><div class="modal-footer"><span class="btn btn-default" data-dismiss="modal">' + Drupal.t("Close") + '</span><span class="save btn btn-primary">' + Drupal.t("Save changes") + "</span></div></div></div></div>").prependTo("body")
            },
            show_editor: function(a, n, i) {
                T.prototype.spawn_modal_skeleton(n);
                for (var e = y("#az-editor-modal"), t = {}, o = 0; o < a.length; o++) a[o].hidden || (a[o].element = n, a[o].tab in t ? t[a[o].tab].push(a[o]) : t[a[o].tab] = [a[o]]);
                var s = y('<div id="az-editor-tabs"></div>'),
                    l = (o = 0, '<ul class="nav nav-tabs">');
                for (var r in t) o++, "" === r && (r = Drupal.t("General")), l += '<li><a href="#az-editor-tab-' + o + '" data-toggle="tab">' + r + "</a></li>";
                l += "</ul>", y(s).append(l), o = 0;
                var d = y('<form role="form" class="tab-content"></form>');
                for (var r in t) {
                    for (var c = y('<div id="az-editor-tab-' + ++o + '" class="tab-pane"></div>'), p = 0; p < t[r].length; p++) t[r][p].render(n.attrs[t[r][p].param_name]), y(c).append(t[r][p].dom_element);
                    y(d).append(c)
                }
                y(s).append(d), y(e).find(".modal-body").append(s), y('#az-editor-tabs a[href="#az-editor-tab-1"]').tab("show"), y('#az-editor-modal input[name="el_class"]').each(function() {
                    ! function(e, t, a) {
                        var n = '<select multiple="multiple">',
                            i = "";
                        for (var o in e) 0 <= o.indexOf("optgroup") ? ("" == i && (n += "</optgroup>"), n = n + '<optgroup label="' + e[o] + '">', i = e[o]) : n = n + '<option value="' + o + '">"' + e[o] + '"</option>';
                        "" != i && (n += "</optgroup>"), n += "</select>", y(t).css("display", "none");
                        var s = y(n).insertAfter(t);
                        if (y(t).val().length)
                            for (var l = y(t).val().split(a), r = 0; r < l.length; r++) y(s).find('option[value="' + l[r] + '"]').length || y(s).append('<option value="' + l[r] + '">"' + l[r] + '"</option>'), y(s).find('option[value="' + l[r] + '"]').attr("selected", "selected");
                        y(s).chosen({
                            search_contains: !0
                        }), y(s).change(function() {
                            var e = [];
                            y(this).find("option:selected").each(function() {
                                e.push(y(this).val())
                            }), y(t).val(e.join(a))
                        }), y(s).parent().find(".chosen-container").width("100%"), y('<div><a class="direct-input" href="#">' + Drupal.t("Edit as text") + "</a></div>").insertBefore(s).click(function() {
                            y(t).css("display", "block"), y(s).parent().find(".chosen-container").remove(), y(s).remove(), y(this).remove()
                        })
                    }(U.prototype.el_classes, this, " ")
                });
                for (o = 0; o < a.length; o++)
                    if ("element" in a[o].dependency) {
                        var h = null;
                        for (p = 0; p < a.length; p++)
                            if (a[p].param_name === a[o].dependency.element) {
                                h = a[p];
                                break
                            }
                            "is_empty" in a[o].dependency && function(e, t) {
                            y(t.dom_element).find('[name="' + t.param_name + '"]').on("keyup change", function() {
                                "" === t.get_value() ? (a[e].display_none = !1, y(a[e].dom_element).css("display", "block"), "callback" in a[e].dependency && a[e].dependency.callback.call(a[e], t)) : (a[e].display_none = !0, y(a[e].dom_element).css("display", "none"))
                            }).trigger("change")
                        }(o, h), "not_empty" in a[o].dependency && function(e, t) {
                            y(t.dom_element).find('[name="' + t.param_name + '"]').on("keyup change", function() {
                                "" !== t.get_value() ? (a[e].display_none = !1, y(a[e].dom_element).css("display", "block"), "callback" in a[e].dependency && a[e].dependency.callback.call(a[e], t)) : (a[e].display_none = !0, y(a[e].dom_element).css("display", "none"))
                            }).trigger("change")
                        }(o, h), "value" in a[o].dependency && function(e, t) {
                            y(t.dom_element).find('[name="' + t.param_name + '"]').on("keyup change", function() {
                                0 <= _.indexOf(a[e].dependency.value, t.get_value()) ? (a[e].display_none = !1, y(a[e].dom_element).css("display", "block"), "callback" in a[e].dependency && a[e].dependency.callback.call(a[e], t)) : (a[e].display_none = !0, y(a[e].dom_element).css("display", "none"))
                            }).trigger("change")
                        }(o, h)
                    }
                y("#az-editor-modal").one("shown.bs.modal", function(e) {
                    y("body").addClass("modal-open");
                    for (var t = 0; t < a.length; t++) a[t].hidden || a[t].opened()
                }), y("#az-editor-modal").one("hidden.bs.modal", function(e) {
                    for (var t = 0; t < a.length; t++) a[t].closed();
                    y(window).scrollTop(m), y(window).off("scroll.az-editor-modal"), y("body").removeClass("modal-open")
                }), y("#az-editor-modal").find(".save").click(function() {
                    for (var e = {}, t = 0; t < a.length; t++)
                        if (!a[t].hidden && ((!("display_none" in a[t]) || "display_none" in a[t] && !a[t].display_none) && (e[a[t].param_name] = a[t].get_value()), a[t].required && "" == e[a[t].param_name])) return y(a[t].dom_element).addClass("has-error"), !1;
                    return y("#az-editor-modal").modal("hide"), i.call(n, e), y(window).trigger("CKinlineAttach"), !1
                }), y("#az-editor-modal").find('[data-dismiss="modal"]').click(function() {
                    b.edit_stack = []
                });
                var m = y(window).scrollTop();
                y(window).on("scroll.az-editor-modal", function() {
                    y(window).scrollTop(m)
                }), y("#az-editor-modal").modal("show")
            },
            opened: function() {},
            closed: function() {},
            render: function(e) {}
        }, "glazed_param_types" in window)
        for (var j = 0; j < window.glazed_param_types.length; j++) {
            var I = window.glazed_param_types[j],
                E = function() {
                    E.baseclass.apply(this, arguments)
                };
            n(I.type, E), I.baseclass = E.baseclass, w(E.prototype, I)
        }

    function O() {
        O.baseclass.apply(this, arguments)
    }

    function R() {
        R.baseclass.apply(this, arguments)
    }

    function P() {}

    function U(e, t) {
        this.id = "gb" + Math.random().toString(36).substr(2, 8), null != e && (this.parent = e, "boolean" == typeof t ? t ? e.children.push(this) : e.children.unshift(this) : e.children.splice(t, 0, this)), this.children = [], this.dom_element = null, this.dom_content_element = null, this.attrs = {};
        for (var a = 0; a < this.params.length; a++) _.isString(this.params[a].value) ? this.attrs[this.params[a].param_name] = this.params[a].value : this.params[a].hidden || (this.attrs[this.params[a].param_name] = "");
        this.controls = null, b.add_element(this.id, this, t)
    }
    n("cms_settings", O), w(O.prototype, {
        get_value: function() {
            return y(this.dom_element).find("form").serialize()
        },
        render_form: function(e) {
            var n = this;
            ! function(t, a) {
                y.ajax({
                    type: "get",
                    url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    y.ajax({
                        type: "POST",
                        url: e,
                        data: {
                            action: "glazed_get_cms_element_settings",
                            name: t,
                            url: window.location.href
                        },
                        cache: !drupalSettings.glazedBuilder.glazedEditor
                    }).done(function(e) {
                        a(e)
                    })
                })
            }(e, function(e) {
                var t, a;
                y(n.dom_element).empty(), y(e).appendTo(n.dom_element), y(n.dom_element).find('[type="submit"]').remove(), 0 < n.form_value.length && y(n.dom_element).deserialize((t = n.form_value, (a = document.createElement("div")).innerHTML = t, 0 === a.childNodes.length ? "" : a.childNodes[0].nodeValue))
            })
        },
        get_form: function(e) {
            var t = e.get_value();
            0 < t.length && this.render_form(t)
        },
        render: function(e) {
            this.form_value = e, this.dom_element = y('<div class="form-group"></div>')
        },
        opened: function() {
            "instance" in this && this.render_form(this.instance)
        }
    }), n("container", R), w(R.prototype, {
        get_value: function() {
            return y(this.dom_element).find('input[name="' + this.param_name + '_type"]').val() + "/" + y(this.dom_element).find('input[name="' + this.param_name + '_name"]').val()
        },
        render: function(e) {
            var t = e.split("/")[0],
                a = e.split("/")[1];
            this.dom_element = y('<div class="form-group"><label>' + this.heading + '</label><div class="wrap-type"><label>' + Drupal.t("Type") + '</label><input class="form-control" name="' + this.param_name + '_type" type="text" value="' + t + '"></div><div class="wrap-name"><label>' + Drupal.t("Name") + '</label><input class="form-control" name="' + this.param_name + '_name" type="text" value="' + a + '"></div><p class="help-block">' + this.description + "</p></div>")
        },
        opened: function() {
            var e = this.get_value(),
                t = null,
                a = null,
                n = this;
            ! function(t) {
                y.ajax({
                    type: "get",
                    url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    y.ajax({
                        type: "POST",
                        url: e,
                        data: {
                            action: "glazed_get_container_types",
                            url: window.location.href
                        },
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        t(e)
                    })
                })
            }(function(e) {
                t = g(e, y(n.dom_element).find('input[name="' + n.param_name + '_type"]')), y(t).chosen().change(function() {
                    f(y(this).val(), function(e) {
                        y(a).parent().find(".direct-input").click(), y(n.dom_element).find('input[name="' + n.param_name + '_name"]').val(""), a = g(e, y(n.dom_element).find('input[name="' + n.param_name + '_name"]'))
                    })
                })
            }), f(e.split("/")[0], function(e) {
                a = g(e, y(n.dom_element).find('input[name="' + n.param_name + '_name"]'))
            })
        }
    }), w(P.prototype, {
        elements_instances: {},
        elements_instances_by_an_name: {},
        template_elements_loaded: !1,
        cms_elements_loaded: !1,
        edit_stack: [],
        try_render_unknown_elements: function() {
            if (this.template_elements_loaded && this.cms_elements_loaded)
                for (var e in b.elements_instances) {
                    var t = b.elements_instances[e];
                    if (t instanceof ae) {
                        var a = t.attrs.content;
                        /^\s*\<[\s\S]*\>\s*$/.exec(a) ? U.prototype.parse_html.call(t, a) : U.prototype.parse_shortcode.call(t, a);
                        for (var n = 0; n < t.children.length; n++) t.children[n].recursive_render();
                        y(t.dom_content_element).empty(), t.attach_children(), drupalSettings.glazedBuilder.glazedEditor && t.update_sortable(), t.recursive_showed()
                    }
                }
        },
        create_template_elements: function(s) {
            var l = {
                "link[href]": "href",
                "script[src]": "src",
                "img[src]": "src"
            };
            "glazed_urls_to_update" in window && (l = y.extend(l, window.glazed_urls_to_update));
            var e = [];
            "glazed_editable" in window && (e = window.glazed_editable);
            var t = [];
            "glazed_styleable" in window && (t = window.glazed_styleable);
            var a = [];
            "glazed_sortable" in window && (a = window.glazed_sortable);
            var n = [];
            "glazed_synchronizable" in window && (n = window.glazed_synchronizable);
            var i = [];
            "glazed_restoreable" in window && (i = window.glazed_restoreable);
            var o = [];
            "glazed_containable" in window && (o = window.glazed_containable);
            var f = T.prototype.param_types.icon.prototype.icons.map(function(e, t, a) {
                    return e.replace(/^/, ".").replace(/ /, ".")
                }),
                v = f.join(", ");
            for (var r in s) {
                var d = s[r].name,
                    c = s[r].html,
                    p = r.split("|");
                p.pop(), p = p.join("/");
                var h = drupalSettings.glazedBuilder.glazedBaseUrl + "../glazed_elements/" + p + "/";
                "baseurl" in s[r] && (h = s[r].baseurl);
                var m = "";
                "thumbnail" in s[r] && (m = s[r].thumbnail);
                var u = 0 <= c.indexOf("az-rootable"),
                    g = function(e, t) {
                        for (var i = this, a = 0; a < this.baseclass.prototype.params.length; a++)
                            if ("content" == this.baseclass.prototype.params[a].param_name && "" == this.baseclass.prototype.params[a].value) {
                                if (drupalSettings.glazedBuilder.glazedAjaxUrl) {
                                    function n(e) {
                                        function a(e) {
                                            return 0 == e.indexOf("glazed_elements") ? drupalSettings.glazedBuilder.glazedBaseUrl + "../" + e : 0 != e.indexOf("/") && 0 != e.indexOf("http://") && 0 != e.indexOf("https://") ? i.baseurl + e : e
                                        }
                                        for (var t in l) {
                                            var n = l[t];
                                            y(e).find(t).each(function() {
                                                y(this).attr(n, a(y(this).attr(n)))
                                            })
                                        }
                                        y(e).find("[data-az-url]").each(function() {
                                            var e = y(this).attr("data-az-url");
                                            y(this).attr(e, a(y(this).attr(e)))
                                        }), y(e).find('[style*="background-image"]').each(function() {
                                            var e = y(this).attr("style").replace(/background-image[: ]*url\(([^\)]+)\) *;/, function(e, t) {
                                                return e.replace(t, encodeURI(a(decodeURI(t))))
                                            });
                                            y(this).attr("style", e)
                                        })
                                    }
                                    var o = y("<div>" + i.template + "</div>");
                                    n(o), o = y(o).html(), this.baseclass.prototype.params[a].value = o
                                }
                                break
                            }
                        U.apply(this, arguments)
                    };
                N(d, !1, g), w(g.prototype, {
                    baseclass: g,
                    template: c,
                    baseurl: h,
                    path: r,
                    name: d,
                    icon: "fa fa-cube",
                    description: Drupal.t(""),
                    thumbnail: m,
                    params: [B({
                        type: "html",
                        heading: Drupal.t("Content"),
                        param_name: "content",
                        value: ""
                    })].concat(g.prototype.params),
                    show_settings_on_create: !1,
                    is_container: !0,
                    has_content: !0,
                    section: u,
                    category: Drupal.t("Template-elements"),
                    is_template_element: !0,
                    editable: [".az-editable"].concat(e),
                    styleable: [".az-styleable"].concat(t),
                    sortable: [".az-sortable"].concat(a),
                    synchronizable: [".az-synchronizable"].concat(n),
                    restoreable: [".az-restoreable"].concat(i),
                    containable: [".az-containable"].concat(o),
                    restore_nodes: {},
                    contained_elements: {},
                    show_controls: function() {
                        if (drupalSettings.glazedBuilder.glazedEditor) {
                            var u = this;
                            U.prototype.show_controls.apply(this, arguments);
                            var s = function() {
                                if (0 < b.edit_stack.length) {
                                    var t = b.edit_stack.shift();
                                    y(t.node).css("outline-width", "2px"), y(t.node).css("outline-style", "dashed");
                                    var e = setInterval(function() {
                                        "rgb(255, 0, 0)" != y(t.node).css("outline-color") ? y(t.node).css("outline-color", "rgb(255, 0, 0)") : y(t.node).css("outline-color", "rgb(255, 255, 255)")
                                    }, 100);
                                    setTimeout(function() {
                                        clearInterval(e), y(t.node).css("outline-color", ""), y(t.node).css("outline-width", ""), y(t.node).css("outline-style", ""),
                                            function(t, a, n, i) {
                                                var e = [],
                                                    o = "",
                                                    s = "",
                                                    l = "",
                                                    r = y.trim(y(t).text());
                                                r = "" != r ? y(t).html() : "&nbsp;&nbsp;&nbsp;";
                                                if (a)
                                                    if (y(t).is(v)) {
                                                        for (var d = 0; d < f.length; d++)
                                                            if (y(t).is(f[d])) {
                                                                (l = f[d].split(".")).shift(), l = l.join(" ");
                                                                break
                                                            }
                                                        e.push(B({
                                                            type: "icon",
                                                            heading: Drupal.t("Icon"),
                                                            param_name: "icon"
                                                        }))
                                                    } else "IMG" != y(t).prop("tagName") ? "" != r && e.push(B({
                                                        type: "textarea",
                                                        heading: Drupal.t("Content"),
                                                        param_name: "content"
                                                    })) : (o = y(t).attr("src"), e.push(B({
                                                        type: "image",
                                                        heading: Drupal.t("Image"),
                                                        param_name: "image",
                                                        description: Drupal.t("Select image from media library.")
                                                    }))), "A" == y(t).prop("tagName") && (s = y(t).attr("href"), e.push(B({
                                                        type: "link",
                                                        heading: Drupal.t("Link"),
                                                        param_name: "link",
                                                        description: Drupal.t("Content link (url).")
                                                    })));
                                                if (n) {
                                                    e.push(B({
                                                        type: "textfield",
                                                        heading: Drupal.t("Content classes"),
                                                        param_name: "el_class",
                                                        description: Drupal.t("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.")
                                                    }));
                                                    var c = B({
                                                        type: "style",
                                                        heading: Drupal.t("Content style"),
                                                        param_name: "style",
                                                        description: Drupal.t("Style options."),
                                                        tab: Drupal.t("Style")
                                                    });
                                                    a ? e.push(c) : e.unshift(c)
                                                }
                                                y(t).removeClass("editable-highlight"), y(t).removeClass("styleable-highlight"), y(t).removeClass(l);
                                                var p = y(t).attr("class");
                                                y(t).addClass(l), (void 0 === p || !1 === p) && (p = "");
                                                var h = "";
                                                for (var m in t.style) y.isNumeric(m) && (h = h + t.style[m] + ": " + t.style.getPropertyValue(t.style[m]) + "; ");
                                                h = (h = (h = (h = (h = (h = (h = (h = (h = D(h)).replace(/\-value\: /g, ": ")).replace("border-top-color", "border-color")).replace("border-top-left-radius", "border-radius")).replace("border-top-style", "border-style")).replace("background-position-x: 50%; background-position-y: 50%;", "background-position: center;")).replace("background-position-x: 50%; background-position-y: 100%;", "background-position: center bottom;")).replace("background-repeat-x: no-repeat; background-repeat-y: no-repeat;", "background-repeat: no-repeat;")).replace("background-repeat-x: repeat;", "background-repeat: repeat-x;"), T.prototype.show_editor(e, {
                                                    name: Drupal.t("Content"),
                                                    attrs: {
                                                        content: r,
                                                        link: s,
                                                        image: o,
                                                        el_class: p,
                                                        style: h,
                                                        icon: l
                                                    }
                                                }, function(e) {
                                                    a && ("" != l && (y(t).removeClass(l), e.el_class = e.el_class + " " + e.icon), "A" == y(t).prop("tagName") && y(t).attr("href", e.link), "IMG" == y(t).prop("tagName") ? y(t).attr("src", e.image) : "" != r && "" != e.content ? y(t).html(e.content) : y(t).html("&nbsp;&nbsp;&nbsp;")), n && (y(t).attr("class", e.el_class), y(t).attr("style", e.style)), u.attrs.content = y(u.dom_content_element).html(), u.restore_content(), _(), g(), i()
                                                })
                                            }(t.node, t.edit, t.style, function() {
                                                if (0 < b.edit_stack.length) {
                                                    var e = y(t.node).width() * y(t.node).height();
                                                    y(b.edit_stack[0].node).width() * y(b.edit_stack[0].node).height() / e < 2 ? s() : b.edit_stack = []
                                                }
                                            })
                                    }, 500)
                                }
                            };

                            function n(e) {
                                var t = y(e).clone();
                                y(t).find("*").each(function() {
                                    for (; 0 < this.attributes.length;) this.removeAttribute(this.attributes[0].name)
                                });
                                var a = y(t).html();
                                return a = a.replace(/\s*/g, "")
                            }

                            function _() {
                                l();
                                for (var e = 0; e < u.synchronizable.length; e++) y(u.dom_content_element).find(u.synchronizable[e]).each(function() {
                                    if (0 == y(this).closest("[data-az-restore]").length) {
                                        y(this).find(".editable-highlight").removeClass("editable-highlight"), y(this).find(".styleable-highlight").removeClass("styleable-highlight"), y(this).find(".sortable-highlight").removeClass("sortable-highlight"), y(this).find('[class=""]').removeAttr("class"), y(this).find('[style=""]').removeAttr("style");
                                        var e = y(this).data("synchronized");
                                        if (e)
                                            for (var t = 0; t < e.length; t++) y(e[t]).html(y(this).html());
                                        y(this).data("current-state") ? y(document).trigger("glazed_synchronize", {
                                            from_node: this,
                                            old_state: y(this).data("current-state"),
                                            new_state: y(this).html()
                                        }) : y(document).trigger("glazed_synchronize", {
                                            from_node: this,
                                            old_state: n(this),
                                            new_state: y(this).html()
                                        }), y(this).data("current-state", n(this)), u.attrs.content = y(u.dom_content_element).html(), u.restore_content()
                                    }
                                });
                                g()
                            }

                            function l() {
                                for (var e = 0; e < u.sortable.length; e++) y(u.dom_content_element).find(u.sortable[e]).each(function() {
                                    y(this).hasClass("ui-sortable") && y(this).data("sortable") && (y(this).data("sortable", !1), y(this).sortable("destroy"), y(this).find(".ui-sortable-handle").removeClass("ui-sortable-handle"))
                                })
                            }

                            function g() {
                                for (var e = 0; e < u.restoreable.length; e++) y(u.dom_element).find(u.restoreable[e]).off("mouseenter.az-restoreable").on("mouseenter.az-restoreable", function() {
                                    y(this).addClass("restoreable-highlight")
                                }), y(u.dom_element).find(u.restoreable[e]).off("mouseleave.az-restoreable").on("mouseleave.az-restoreable", function() {
                                    y(this).removeClass("restoreable-highlight")
                                }), y(u.dom_element).find(u.restoreable[e]).off("click.az-restoreable").on("click.az-restoreable", function(e) {
                                    if (y(this).is("[data-az-restore]")) {
                                        var t = [];
                                        t.push(B({
                                            type: "html",
                                            heading: Drupal.t("HTML"),
                                            param_name: "html"
                                        }));
                                        var a = y(this).attr("data-az-restore"),
                                            n = u.restore_nodes[a];
                                        return T.prototype.show_editor(t, {
                                            name: Drupal.t("Content"),
                                            attrs: {
                                                html: n
                                            }
                                        }, function(e) {
                                            u.restore_nodes[a] = e.html, u.restore_content(), u.update_dom(), _()
                                        }), !1
                                    }
                                });
                                for (e = 0; e < u.styleable.length; e++) y(u.dom_element).find(u.styleable[e]).off("mouseenter.az-styleable").on("mouseenter.az-styleable", function() {
                                    0 == y(this).closest("[data-az-restore]").length && y(this).addClass("styleable-highlight")
                                }), y(u.dom_element).find(u.styleable[e]).off("mouseleave.az-styleable").on("mouseleave.az-styleable", function() {
                                    0 == y(this).closest("[data-az-restore]").length && y(this).removeClass("styleable-highlight")
                                }), y(u.dom_element).find(u.styleable[e]).off("click.az-styleable").on("click.az-styleable", function(e) {
                                    if (0 == y(this).closest("[data-az-restore]").length) {
                                        if (0 == y(this).parent().closest(".styleable-highlight, .editable-highlight").length) return b.edit_stack.push({
                                            node: this,
                                            edit: !1,
                                            style: !0
                                        }), s(), !1;
                                        b.edit_stack.push({
                                            node: this,
                                            edit: !1,
                                            style: !0
                                        })
                                    }
                                });
                                for (e = 0; e < u.editable.length; e++) y(u.dom_element).find(u.editable[e]).off("mouseenter.az-editable").on("mouseenter.az-editable", function() {
                                    0 == y(this).closest("[data-az-restore]").length && y(this).addClass("editable-highlight")
                                }), y(u.dom_element).find(u.editable[e]).off("mouseleave.az-editable").on("mouseleave.az-editable", function() {
                                    0 == y(this).closest("[data-az-restore]").length && y(this).removeClass("editable-highlight")
                                }), y(u.dom_element).find(u.editable[e]).off("click.az-editable").on("click.az-editable", function(e) {
                                    if (0 == y(this).closest("[data-az-restore]").length) {
                                        if (0 == y(this).parent().closest(".styleable-highlight, .editable-highlight").length) return b.edit_stack.push({
                                            node: this,
                                            edit: !0,
                                            style: !0
                                        }), s(), !1;
                                        b.edit_stack.push({
                                            node: this,
                                            edit: !0,
                                            style: !0
                                        })
                                    }
                                });
                                var t, a = [],
                                    n = null,
                                    i = null;

                                function o(e) {
                                    if (y(e).hasClass("sortable-highlight")) {
                                        y(e).find(".az-sortable-controls").remove();
                                        var t = y('<div class="az-sortable-controls"></div>').appendTo(e),
                                            a = y('<div class="az-sortable-clone glyphicon glyphicon-duplicate" title="' + Drupal.t("Clone") + '"></div>').appendTo(t).click(function() {
                                                return l(), y(e).removeClass("sortable-highlight").find(".az-sortable-controls").remove(), y(e).clone().insertAfter(e), u.attrs.content = y(u.dom_content_element).html(), u.restore_content(), _(), g(), !1
                                            });
                                        y(a).css("line-height", y(a).height() + "px").css("font-size", y(a).height() / 2 + "px");
                                        var n = y('<div class="az-sortable-remove glyphicon glyphicon-trash" title="' + Drupal.t("Remove") + '"></div>').appendTo(t).click(function() {
                                            return l(), y(e).removeClass("sortable-highlight").find(".az-sortable-controls").remove(), y(e).remove(), u.attrs.content = y(u.dom_content_element).html(), u.restore_content(), _(), g(), !1
                                        });
                                        y(n).css("line-height", y(n).height() + "px").css("font-size", y(n).height() / 2 + "px")
                                    }
                                }
                                y(u.dom_element).off("mousemove.az-able").on("mousemove.az-able", function() {
                                    null != n && y(n).hasClass("sortable-highlight") && (clearTimeout(i), i = setTimeout(function() {
                                        o(n)
                                    }, 1e3))
                                });
                                for (e = 0; e < u.sortable.length; e++) t = e, y(u.dom_element).find(u.sortable[t]).find("> *").off("mouseenter.az-sortable").on("mouseenter.az-sortable", function() {
                                    if (0 == y(this).closest("[data-az-restore]").length) {
                                        var e = this;
                                        y(u.dom_element).find(".az-sortable-controls").remove(), y(u.dom_element).find(".sortable-highlight").removeClass("sortable-highlight"), null !== n && clearTimeout(i), y(e).addClass("sortable-highlight"), a.push(e), n = e, i = setTimeout(function() {
                                            o(e)
                                        }, 1e3)
                                    }
                                }), y(u.dom_element).find(u.sortable[t]).find("> *").off("mouseleave.az-sortable").on("mouseleave.az-sortable", function() {
                                    if (0 == y(this).closest("[data-az-restore]").length) {
                                        var e = this;
                                        y(u.dom_element).find(".az-sortable-controls").remove(), y(u.dom_element).find(".sortable-highlight").removeClass("sortable-highlight"), null !== n && clearTimeout(i), a.pop(), 0 < a.length ? (e = a[a.length - 1], y(e).addClass("sortable-highlight"), n = e, i = setTimeout(function() {
                                            o(e)
                                        }, 1e3)) : n = null
                                    }
                                });
                                ! function() {
                                    for (var e = 0; e < u.sortable.length; e++) y(u.dom_element).find(u.sortable[e]).each(function() {
                                        0 == y(this).closest("[data-az-restore]").length && (y(this).data("sortable", !0), y(this).sortable({
                                            items: "> *",
                                            placeholder: "az-sortable-placeholder",
                                            forcePlaceholderSize: !0,
                                            start: function(e, t) {
                                                y(t.item).removeClass("sortable-highlight").find(".az-sortable-controls").remove()
                                            },
                                            update: function(e, t) {
                                                u.attrs.content = y(u.dom_content_element).html(), u.restore_content(), _()
                                            },
                                            over: function(e, t) {
                                                t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                                            }
                                        }))
                                    })
                                }()
                            }
                            y(document).on("glazed_synchronize", function(e, t) {
                                l();
                                for (var a = 0; a < u.synchronizable.length; a++) y(u.dom_content_element).find(u.synchronizable[a]).each(function() {
                                    if (0 == y(this).closest("[data-az-restore]").length && (y(this).find(".editable-highlight").removeClass("editable-highlight"), y(this).find(".styleable-highlight").removeClass("styleable-highlight"), y(this).find(".sortable-highlight").removeClass("sortable-highlight"), y(this).find('[class=""]').removeAttr("class"), y(this).find('[style=""]').removeAttr("style"), this != t.from_node && n(this) == t.old_state)) {
                                        var e = y(t.from_node).data("synchronized");
                                        e || (e = []), e.push(this), e = y.unique(e), y(t.from_node).data("synchronized", e), (e = y(this).data("synchronized")) || (e = []), e.push(t.from_node), e = y.unique(e), y(this).data("synchronized", e), y(this).html(t.new_state), u.attrs.content = y(u.dom_content_element).html(), u.restore_content()
                                    }
                                });
                                g()
                            }), g(), _()
                        }
                    },
                    restore_content: function() {
                        var e = y("<div>" + this.attrs.content + "</div>");
                        for (var t in this.restore_nodes) y(e).find('[data-az-restore="' + t + '"]').html(this.restore_nodes[t]);
                        y(document).trigger("glazed_restore", {
                            dom: e
                        }), this.attrs.content = y(e).html()
                    },
                    get_content: function() {
                        return this.restore_content(), U.prototype.get_content.apply(this, arguments)
                    },
                    restore: function(e) {
                        for (var t in U.prototype.restore.apply(this, arguments), this.restore_nodes) y(e).find('[data-az-restore="' + t + '"]').html(this.restore_nodes[t]);
                        y(document).trigger("glazed_restore", {
                            dom: e
                        }), y(e).find("[data-az-restore]").removeAttr("data-az-restore")
                    },
                    showed: function(e) {
                        U.prototype.showed.apply(this, arguments);
                        var t = this;
                        if (t.section) {
                            var a = e(t.dom_element).parent().closest(".container, .container-fluid"),
                                n = e(t.dom_element).parentsUntil(".container, .container-fluid"),
                                i = e(t.dom_element).parent().closest(".az-popup-ctnr"),
                                o = e(t.dom_element).parentsUntil(".az-popup-ctnr");
                            (0 < a.length && 0 == i.length || 0 < a.length && 0 < i.length && n.length < o.length) && e(t.dom_content_element).find(".container, .container-fluid").each(function() {
                                e(this).removeClass("container"), e(this).removeClass("container-fluid"), t.attrs.content = e(t.dom_content_element).html(), t.restore_content(), t.section = !1
                            })
                        }
                    },
                    render: function(t) {
                        var a = this;
                        this.dom_element = t('<div class="az-element az-template ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = t("<div></div>").appendTo(this.dom_element);
                        var e = "<div>" + this.attrs.content + "</div>";
                        e = t(e), a.restore_nodes = {};
                        for (var n = 0; n < this.restoreable.length; n++) t(e).find(this.restoreable[n]).each(function() {
                            var e = _.uniqueId("r");
                            t(this).attr("data-az-restore", e), a.restore_nodes[e] = t(this).html()
                        });
                        this.attrs.content = t(e).html(), t(this.attrs.content).appendTo(this.dom_content_element), U.prototype.render.apply(this, arguments)
                    }
                })
            }
            this.template_elements_loaded = !0, ne(), this.try_render_unknown_elements(), setTimeout(function() {
                y(function() {
                    if (showPanel = !0, "profile" in drupalSettings.glazedBuilder && (drupalSettings.glazedBuilder.profile.sidebar || (showPanel = !1)), showPanel && drupalSettings.glazedBuilder.glazedEditor && 0 < Object.keys(s).length && 0 < z.length) {
                        var e = {
                            _: []
                        };
                        for (var t in s) {
                            var a = t.split("|");
                            a.pop();
                            for (var n = e, i = 0; i < a.length; i++) a[i] in n || (n[a[i]] = {
                                _: []
                            }), n = n[a[i]];
                            n._.push(s[t])
                        }
                        var o = y('<div id="az-template-elements" class="az-left-sidebar glazed"></div>').appendTo("body");
                        y('<div class="glazed-snippets-header clearfix"><img src="' + drupalSettings.glazedBuilder.glazedBaseUrl + 'images/glazed-logo-white.svg"><h3>' + Drupal.t("Glazed Snippets") + "</h3></div>").appendTo(o), y(o).append(function e(t) {
                            if (1 === Object.keys(t).length && "_" in t) return null;
                            var a = y('<ul class="nav az-nav-list"></ul>');
                            for (var n in t)
                                if ("_" != n) {
                                    var i = y("<li></li>").appendTo(a).on("mouseenter", function() {
                                        y(this).find("> .az-nav-list").css("display", "block")
                                    });
                                    ! function(e) {
                                        y('<a href="#">' + n + "</a>").appendTo(i).click(function() {
                                            var s = this;
                                            y(r).empty(), y(r).css("display", "block"), y(o).addClass("az-thumbnails"),
                                                function e(t) {
                                                    for (var a in t)
                                                        if ("_" == a)
                                                            for (var n = 0; n < t[a].length; n++) y('<img class="az-thumbnail" data-az-base="' + t[a][n].name + '" src="' + encodeURI(t[a][n].thumbnail) + '">').appendTo(r);
                                                        else e(t[a])
                                                }(e), y(o).off("mouseleave").on("mouseleave", function() {
                                                    a || (y(o).css("left", ""), y(o).removeClass("az-thumbnails"), y(r).css("overflow-y", "scroll"), y(r).css("display", "none"))
                                                });
                                            var a = !1,
                                                l = 0;
                                            return y(r).sortable({
                                                items: ".az-thumbnail",
                                                connectWith: ".az-ctnr",
                                                start: function(e, t) {
                                                    a = !0, y(o).css("left", "0px"), y(r).css("overflow-y", "visible"), l = y(window).scrollTop(), y(window).on("scroll.template-elements-sortable", function() {
                                                        y(window).scrollTop(l)
                                                    })
                                                },
                                                stop: function(e, t) {
                                                    a = !1, y(o).css("left", ""), y(o).removeClass("az-thumbnails"), y(r).css("overflow-y", "scroll"), y(r).css("display", "none"), y(window).off("scroll.template-elements-sortable")
                                                },
                                                update: function(e, t) {
                                                    for (var a = b.get_element(y(t.item).parent().closest("[data-az-id]").attr("data-az-id")), n = 0, i = y(t.item).parent().find("[data-az-id], .az-thumbnail"), o = 0; o < i.length; o++)
                                                        if (y(i[o]).hasClass("az-thumbnail")) {
                                                            n = o;
                                                            break
                                                        }
                                                    b.create_element(a, y(t.item).attr("data-az-base"), n, function() {}), y(t.item).detach(), y(s).click(), y(window).scrollTop(l)
                                                },
                                                placeholder: "az-sortable-placeholder",
                                                forcePlaceholderSize: !0,
                                                over: function(e, t) {
                                                    t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                                                }
                                            }), !1
                                        })
                                    }(t[n]), y(i).append(e(t[n]))
                                }
                            return a
                        }(e)), y(o).find("> .az-nav-list > li").on("mouseleave", function() {
                            y(this).find(".az-nav-list").css("display", "none")
                        });
                        var r = y('<div id="az-thumbnails"></div>').appendTo(o)
                    }
                })
            }, 500)
        },
        create_cms_elements: function(e) {
            for (var t in e) {
                var a = "az_" + t,
                    o = function(e, t) {
                        o.baseclass.apply(this, arguments)
                    };
                N(a, !1, o);
                var n = {
                    name: e[t],
                    icon: "fa fa-drupal",
                    description: Drupal.t(""),
                    category: "CMS",
                    instance: t,
                    params: [B({
                        type: "cms_settings",
                        heading: Drupal.t("Settings"),
                        param_name: "settings",
                        instance: t
                    })],
                    show_settings_on_create: !0,
                    is_container: !0,
                    has_content: !0,
                    is_cms_element: !0,
                    get_button: function() {
                        var e = this.name.replace(/^Block: /, "");
                        return '<div class="well text-center pull-left text-overflow glazed-cms" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i>' + e + "</div>"
                    },
                    get_button_with_tag: function() {
                        var e = "",
                            t = this.name.replace(/^View: /, "");
                        return y.inArray(drupalSettings.glazedBuilder.viewsTags, -1 < this.base) && (e = drupalSettings.glazedBuilder.viewsTags[this.base]), '<div class="well text-center pull-left text-overflow glazed-cms" data-az-element="' + this.base + '" data-az-tag="' + e + '"><i class="' + this.icon + '"></i>' + t + "</div>"
                    },
                    get_content: function() {
                        return ""
                    },
                    showed: function(n) {
                        if (o.baseclass.prototype.showed.apply(this, arguments), "content" in this.attrs && "" != this.attrs.content) n(this.dom_content_element).append(this.attrs.content), this.attrs.content = "";
                        else {
                            var i = this;
                            glazed_add_js({
                                path: "vendor/jquery.waypoints/lib/jquery.waypoints.min.js",
                                loaded: "waypoint" in n.fn,
                                callback: function() {
                                    n(i.dom_element).waypoint(function(e) {
                                        var t = i.parent.get_my_container(),
                                            a = {
                                                display_title: i.attrs.display_title,
                                                display_exposed_filters: i.attrs.display_exposed_filters,
                                                override_pager: i.attrs.override_pager,
                                                items: i.attrs.items,
                                                offset: i.attrs.offset,
                                                contextual_filter: i.attrs.contextual_filter,
                                                toggle_fields: i.attrs.toggle_fields
                                            };
                                        v(i.instance, i.attrs.settings, t.attrs.container, a, function(e) {
                                            n(i.dom_content_element).empty(), n(i.dom_content_element).append(e);
                                            var t = "az_" + i.instance;
                                            i.attrs.display_exposed_filters && !drupalSettings.glazedBuilder.cmsElementViewsSettings[t].ajax_enabled && 0 < document.getElementsByClassName("glazed-editor").length && n(i.dom_content_element).prepend('<div class="az-unknown glazed-builder-warning">' + Drupal.t("Exposed filters enabled but ajax turned off. Please enable Ajax on this display in order to show exposed filters here.") + "</div>"), Drupal.attachBehaviors(n(i.dom_content_element)[0])
                                        })
                                    }, {
                                        offset: "100%",
                                        handler: function(e) {
                                            this.destroy()
                                        }
                                    }), n(document).trigger("scroll")
                                }
                            })
                        }
                    },
                    render: function(e) {
                        this.dom_element = e('<div class="az-element az-cms-element ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = e("<div></div>").appendTo(this.dom_element), o.baseclass.prototype.render.apply(this, arguments)
                    }
                };
                if (t.match(/^block-/) && n.params.push(B({
                        type: "checkbox",
                        heading: Drupal.t("Show title"),
                        param_name: "display_title",
                        content: "yes",
                        value: {
                            yes: Drupal.t("Yes")
                        }
                    })), t.match("^view-")) {
                    var i = {
                        type: "textfield",
                        heading: Drupal.t("Items to display"),
                        param_name: "items",
                        description: Drupal.t("The number of items to display. Enter 0 for no limit."),
                        can_be_empty: !0,
                        dependency: {
                            element: "override_pager",
                            value: ["yes"]
                        }
                    };
                    if (!drupalSettings.glazedBuilder.cmsElementViewsSettings.hasOwnProperty(a)) continue;
                    drupalSettings.glazedBuilder.cmsElementViewsSettings[a].exposed_filter && n.params.push(B({
                        type: "checkbox",
                        heading: Drupal.t("Show exposed filters"),
                        param_name: "display_exposed_filters",
                        content: "yes",
                        value: {
                            yes: Drupal.t("Yes")
                        }
                    })), "block" == drupalSettings.glazedBuilder.cmsElementViewsSettings[a].view_display_type ? (i.heading = Drupal.t("Items to display:"), i.description = Drupal.t("The number of items to display. Enter 0 for no limit.")) : (i.heading = Drupal.t("Items per page:"), i.description = Drupal.t("The number to display per page. Enter 0 for no limit.")), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].title && n.params.push(B({
                        type: "checkbox",
                        heading: Drupal.t("Show title"),
                        param_name: "display_title",
                        content: "yes",
                        value: {
                            yes: Drupal.t("Yes")
                        }
                    })), n.params.push(B({
                        type: "dropdown",
                        heading: Drupal.t("Override pager"),
                        param_name: "override_pager",
                        value: {
                            no: Drupal.t("No"),
                            yes: Drupal.t("Yes")
                        }
                    })), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.items_per_page && (i.value = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.items_per_page), n.params.push(B(i));
                    var s = {
                        type: "textfield",
                        heading: Drupal.t("Pager Offset"),
                        param_name: "offset",
                        description: Drupal.t("The number of items to skip."),
                        can_be_empty: !0,
                        dependency: {
                            element: "override_pager",
                            value: ["yes"]
                        }
                    };
                    if (drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.offset && (s.value = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].pager.offset), n.params.push(B(s)), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].contextual_filter && n.params.push(B({
                            type: "textfield",
                            heading: Drupal.t("Contextual filter:"),
                            param_name: "contextual_filter",
                            description: Drupal.t('Separate contextual filter values with a "/". For example, 40/12/10.'),
                            can_be_empty: !0
                        })), drupalSettings.glazedBuilder.cmsElementViewsSettings[a].use_fields) {
                        var l = {
                            type: "checkboxes",
                            heading: Drupal.t("Field settings"),
                            param_name: "toggle_fields",
                            value: {},
                            tab: Drupal.t("Toggle Fields")
                        };
                        for (var r in drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_list) {
                            var d = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_list[r];
                            l.value[r] = d
                        }
                        "" != drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_values && (l.content = drupalSettings.glazedBuilder.cmsElementViewsSettings[a].field_values), n.params.push(B(l))
                    }
                }
                n.params = n.params.concat(o.prototype.params), w(o.prototype, n)
            }
            this.cms_elements_loaded = !0, ne(), this.try_render_unknown_elements()
        },
        create_element: function(e, t, a, n) {
            if (e.get_nested_depth(t) < U.prototype.max_nested_depth) {
                var i = U.prototype.elements[t];
                if (e instanceof te && null == e.parent && !i.prototype.section) {
                    var o = new F(e, a);
                    o.update_dom(), n(s = new i(o, !1)), s.update_dom(), e.update_empty(), o.update_empty()
                } else {
                    var s;
                    n(s = new i(e, a)), s.update_dom(), e.update_empty()
                }
                return s
            }
            return alert(Drupal.t("Element can not be added. Max nested depth reached.")), !1
        },
        make_elements_modal: function(o, a) {
            var e = o.get_all_disallowed_elements(),
                t = {};
            for (var n in U.prototype.elements)
                if (!U.prototype.elements[n].prototype.hidden && !("profile" in window.drupalSettings.glazedBuilder && U.prototype.elements[n].prototype.base in window.drupalSettings.glazedBuilder.profile.hide_els || "az_popup" != o.base && 0 <= e.indexOf(U.prototype.elements[n].prototype.base) || "Template-elements" == U.prototype.elements[n].prototype.category))
                    if ("CMS" == U.prototype.elements[n].prototype.category) {
                        var i = U.prototype.elements[n].prototype.name.match(/^Block/) ? "Blocks" : "Views";
                        U.prototype.elements[n].prototype.base.indexOf("block-views") < 0 && (i in t || (t[i] = []), t[i].push(U.prototype.elements[n]))
                    } else U.prototype.elements[n].prototype.category in t || (t[U.prototype.elements[n].prototype.category] = []), t[U.prototype.elements[n].prototype.category].push(U.prototype.elements[n]);
            var s = y('<div id="az-elements-tabs"></div>'),
                l = 0,
                r = '<ul class="nav nav-tabs">';
            for (var d in t) l++, "" === d && (d = Drupal.t("Content")), r += '<li><a href="#az-elements-tab-' + l + '" data-toggle="tab">' + d + "</a></li>";
            window.glazed_online && (r += '<li><a href="#az-elements-tab-templates" data-toggle="tab">' + Drupal.t("Saved Templates") + "</a></li>"), r += "</ul>", y(s).append(r), l = 0;
            var c = y('<div class="tab-content"></div>'),
                p = 0;
            for (var d in t) {
                var h = y('<div id="az-elements-tab-' + ++l + '" class="tab-pane clearfix"></div>');
                if ("Views" == d) {
                    p = l;
                    for (var m = 0; m < t[d].length; m++) y(h).append(t[d][m].prototype.get_button_with_tag())
                } else
                    for (m = 0; m < t[d].length; m++) y(h).append(t[d][m].prototype.get_button());
                y(c).append(h)
            }
            var u = [];
            for (var _ in drupalSettings.glazedBuilder.viewsTags) - 1 == u.indexOf(drupalSettings.glazedBuilder.viewsTags[_]) && u.push(drupalSettings.glazedBuilder.viewsTags[_]);
            var g = "<div class='filter-tags'><label>" + Drupal.t("Filter by tag") + "</label><select><option value='all_views'>" + Drupal.t("show all") + "</option>";
            for (var _ in u) g = g + '<option value="' + u[_] + '">' + u[_].replace(/_/g, " ") + "</option>";
            (g = y(g += "</select></div>")).find("select").bind("change", function() {
                var e = this.options[this.selectedIndex].value;
                c.find("#az-elements-tab-" + p + " .well").trigger("filtredData", e)
            }), c.find("#az-elements-tab-" + p + " .well").bind("filtredData", function(e, t) {
                var a = y(this);
                "all_views" == t ? a.show() : a.attr("data-az-tag") != t ? a.hide() : a.show()
            }), y(c).find("#az-elements-tab-" + p).prepend(g), window.glazed_online && (h = y('<div id="az-elements-tab-templates" class="tab-pane clearfix"></div>')), y(c).append(h), y(s).append(c), U.prototype.spawn_modal_skeleton();
            var f = y("#az-elements-modal");
            y(f).find(".modal-body").append(s), y(s).find("> ul a:first").tab("show"), y(f).find("[data-az-element]").click(function() {
                var e = y(this).attr("data-az-element"),
                    t = b.create_element(o, e, !1, a);
                t && (y("#az-elements-modal").modal("hide"), t.show_settings_on_create && t.edit())
            }), window.glazed_online && y(s).find('a[href="#az-elements-tab-templates"]').on("shown.bs.tab", function(e) {
                ! function(t) {
                    y.ajax({
                        type: "get",
                        url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        y.ajax({
                            type: "POST",
                            url: e,
                            data: {
                                action: "glazed_get_templates",
                                url: window.location.href
                            },
                            dataType: "json",
                            cache: !1,
                            context: this
                        }).done(function(e) {
                            t(e)
                        })
                    })
                }(function(e) {
                    var t = y(s).find("#az-elements-tab-templates");
                    y(t).empty();
                    for (var a = 0; a < e.length; a++) {
                        var n = e[a],
                            i = '<div class="well text-center pull-left text-overflow glazed-saved" data-az-template="' + n + '"><i class="glyphicon glyphicon-floppy-disk"></i><div>' + n + "</div></div>";
                        i = y(i).appendTo(t).click(function() {
                            x(y(this).attr("data-az-template"), function(e) {
                                var t = o.children.length;
                                U.prototype.parse_shortcode.call(o, e);
                                for (var a = t; a < o.children.length; a++) o.children[a].recursive_render();
                                for (a = t; a < o.children.length; a++) y(o.dom_content_element).append(o.children[a].dom_element);
                                drupalSettings.glazedBuilder.glazedEditor && (o.update_empty(), o.update_sortable()), o.recursive_showed(), y("#az-elements-modal").modal("hide")
                            })
                        }), y('<span class="fa fa-trash-o" data-az-template="' + n + '"></span>').appendTo(i).click(function() {
                            var e = y(this).attr("data-az-template");
                            k(e), y(t).find('[data-az-template="' + e + '"]').remove()
                        })
                    }
                })
            })
        },
        show: function(e, t) {
            y("#az-elements-modal").remove(), this.make_elements_modal(e, t), y("#az-elements-modal").modal("show"), y("#az-elements-modal #az-elements-tabs").find("> ul a:first").tab("show")
        },
        showTemplates: function(e, t) {
            y("#az-elements-modal").remove(), this.make_templates_modal(e, t), y("#az-elements-modal").modal("show"), y("#az-elements-modal #az-elements-tabs").find("> ul a:first").tab("show")
        },
        make_templates_modal: function(r, e) {
            var d = y('<div id="az-elements-tabs"></div>'),
                t = '<ul class="nav nav-tabs">';
            window.glazed_online && (t += '<li><a href="#az-elements-tab-templates" data-toggle="tab">' + Drupal.t("Layouts") + "</a></li>"), t += "</ul>", y(d).append(t);
            var a = y('<div class="tab-content"></div>');
            window.glazed_online && (tab = y('<div id="az-elements-tab-templates" class="tab-pane clearfix"></div>')), y(a).append(tab), y(d).append(a), U.prototype.spawn_modal_skeleton();
            var n = y("#az-elements-modal");
            y(n).find(".modal-body").append(d), window.glazed_online && function(t) {
                y.ajax({
                    type: "get",
                    url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    y.ajax({
                        type: "POST",
                        url: e,
                        data: {
                            action: "glazed_get_page_templates",
                            url: window.location.href
                        },
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        t(e)
                    })
                })
            }(function(e) {
                var t = y(d).find("#az-elements-tab-templates"),
                    a = [];
                a[0] = y('<div class="col-md-4"></div>'), a[1] = y('<div class="col-md-4"></div>'), a[2] = y('<div class="col-md-4"></div>');
                var n = 0;
                if (y(t).empty(), y.isArray(e))
                    for (var i = 0; i < e.length; i++) {
                        n = i % 3;
                        var o = e[i].title,
                            s = e[i].uuid,
                            l = y('<div class="page-template text-center pull-left text-overflow glazed-saved" data-az-template="' + s + '"><div class="lead">' + o + "</div></div>");
                        if ("" != e[i].image) y('<img class="template-image" src="' + e[i].image + '"></img>').appendTo(l);
                        else y('<i class="glyphicon glyphicon-floppy-disk"></i>').appendTo(l);
                        l.appendTo(a[n]).click(function() {
                            S(y(this).attr("data-az-template"), function(e) {
                                var t = r.children.length;
                                U.prototype.parse_shortcode.call(r, e);
                                for (var a = t; a < r.children.length; a++) r.children[a].recursive_render();
                                for (a = t; a < r.children.length; a++) y(r.dom_content_element).append(r.children[a].dom_element);
                                drupalSettings.glazedBuilder.glazedEditor && (r.update_empty(), r.update_sortable()), r.recursive_showed(), y("#az-elements-modal").modal("hide"), y(window).trigger("CKinlineAttach")
                            })
                        }), a[0].appendTo(t), a[1].appendTo(t), a[2].appendTo(t)
                    } else y(e).appendTo(t)
            })
        },
        get_element: function(e) {
            return this.elements_instances[e]
        },
        delete_element: function(e) {
            y(document).trigger("glazed_delete_element", e), delete this.elements_instances[e]
        },
        add_element: function(e, t, a) {
            this.elements_instances[e] = t, y(document).trigger("glazed_add_element", {
                id: e,
                position: a
            })
        }
    });
    var A, L = {};

    function N(e, t, a) {
        if (s(a, U), a.prototype.base = e, a.prototype.is_container = t, U.prototype.elements[e] = a, U.prototype.tags[e] = a, t)
            for (var n = 1; n < U.prototype.max_nested_depth; n++) U.prototype.tags[e + "_" + n] = a
    }

    function q(e, t) {
        return !(e.right < t.left || e.left > t.right || e.bottom < t.top || e.top > t.bottom)
    }

    function K(e, t) {
        K.baseclass.apply(this, arguments)
    }

    function M(e, t, a) {
        if (s(a, K), a.prototype.base = e, a.prototype.is_container = t, K.prototype.elements[e] = a, K.prototype.tags[e] = a, t)
            for (var n = 1; n < K.prototype.max_nested_depth; n++) K.prototype.tags[e + "_" + n] = a
    }

    function V(e) {
        if (0 < y(e).length) {
            var t = y(e).html();
            if (/^\s*\<[\s\S]*\>\s*$/.exec(t) || "" == t && drupalSettings.glazedBuilder.glazedAjaxUrl) {
                y(e).find("> script").detach().appendTo("head"), y(e).find("> link[href]").detach().appendTo("head"), (n = new te(null, !1)).attrs.container = y(e).attr("data-az-type") + "/" + y(e).attr("data-az-name"), n.attrs.langcode = y(e).attr("data-az-langcode"), y(e).attr("data-az-human-readable") && (n.attrs.human_readable = atob(y(e).attr("data-az-human-readable"))), n.dom_element = y(e), y(n.dom_element).attr("data-az-id", n.id), n.dom_content_element = y(e), y(n.dom_element).css("display", ""), y(n.dom_element).addClass("glazed"), y(n.dom_element).addClass("az-ctnr"), n.parse_html(n.dom_content_element), n.html_content = !0, n.loaded_container = n.attrs.container;
                for (var a = 0; a < n.children.length; a++) n.children[a].recursive_render();
                l || (n.dom_content_element.empty(), drupalSettings.glazedBuilder.glazedEditor && (n.show_controls(), n.update_sortable()), n.attach_children()), n.rendered = !0;
                for (a = 0; a < n.children.length; a++) n.children[a].recursive_showed()
            } else {
                var n;
                "" != t.replace(/^\s+|\s+$/g, "") && (r[y(e).attr("data-az-type") + "/" + y(e).attr("data-az-name")] = t.replace(/^\s+|\s+$/g, "")), (n = new te(null, !1)).attrs.container = y(e).attr("data-az-type") + "/" + y(e).attr("data-az-name"), n.attrs.langcode = y(e).attr("data-az-langcode"), n.render(y);
                var i = y(n.dom_element).attr("class") + " " + y(e).attr("class");
                i = y.unique(i.split(" ")).join(" "), y(n.dom_element).attr("class", i), y(n.dom_element).attr("style", y(e).attr("style")), y(n.dom_element).css("display", ""), y(n.dom_element).addClass("glazed"), y(n.dom_element).addClass("az-ctnr");
                var o = y(e).attr("data-az-type"),
                    s = y(e).attr("data-az-name");
                y(e).replaceWith(n.dom_element), y(n.dom_element).attr("data-az-type", o), y(n.dom_element).attr("data-az-name", s), n.showed(y), drupalSettings.glazedBuilder.glazedEditor && n.show_controls()
            }
            return drupalSettings.glazedBuilder.glazedEditor && y(n.dom_element).addClass("glazed-editor"), n
        }
        return null
    }
    "drupalSettings" in window && "glazedBuilder" in window.drupalSettings && "glazedClasses" in window.drupalSettings.glazedBuilder && (L = drupalSettings.glazedBuilder.glazedClasses), U.prototype = {
            el_classes: y.extend({
                "optgroup-bootstrap": Drupal.t("Bootstrap classes"),
                "bg-default": Drupal.t("Background default style"),
                "bg-primary": Drupal.t("Background primary style"),
                "bg-success": Drupal.t("Background success style"),
                "center-block": Drupal.t("Block align center"),
                clearfix: Drupal.t("Clearfix"),
                "hidden-lg": Drupal.t("Hidden on large devices, desktops (≥1200px)"),
                "hidden-md": Drupal.t("Hidden on medium devices, desktops (≥992px)"),
                "hidden-sm": Drupal.t("Hidden on small devices, tablets (≥768px)"),
                "hidden-xs": Drupal.t("Hidden on extra small devices, phones (<768px)"),
                lead: Drupal.t("Text Lead style"),
                "pull-left": Drupal.t("Pull left"),
                "pull-right": Drupal.t("Pull right"),
                "text-center": Drupal.t("Text align center"),
                "text-default": Drupal.t("Text default style"),
                "text-justify": Drupal.t("Text align justify"),
                "text-left": Drupal.t("Text align left"),
                "text-muted": Drupal.t("Text muted style"),
                "text-primary": Drupal.t("Text primary style"),
                "text-right": Drupal.t("Text align right"),
                "text-success": Drupal.t("Text success style"),
                "visible-lg-block": Drupal.t("Visible on large devices, desktops (≥1200px)"),
                "visible-md-block": Drupal.t("Visible on medium devices, desktops (≥992px)"),
                "visible-sm-block": Drupal.t("Visible on small devices, tablets (≥768px)"),
                "visible-xs-block": Drupal.t("Visible on extra small devices, phones (<768px)"),
                well: Drupal.t("Well"),
                small: Drupal.t("Text small style"),
                "optgroup-glazed-shadows": Drupal.t("Drop Shadows"),
                "stpe-dropshadow stpe-dropshadow--curved-hz1 stpe-dropshadow--curved": Drupal.t("Curved Horiztonal Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--curved-hz2 stpe-dropshadow--curved": Drupal.t("Curved Horizontal Double Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--curved-vt2 stpe-dropshadow--curved": Drupal.t("Curved vertical double shadow"),
                "stpe-dropshadow stpe-dropshadow--lifted": Drupal.t("Lifted Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--perspective": Drupal.t("Perspective Drop Shadow"),
                "stpe-dropshadow stpe-dropshadow--raised": Drupal.t("Raised Drop Shadow")
            }, L),
            elements: {},
            tags: {},
            max_nested_depth: 3,
            name: "",
            category: "",
            description: "",
            params: [B({
                type: "textfield",
                heading: Drupal.t("Utility classes"),
                param_name: "el_class",
                description: Drupal.t("Add classes for Bootstrap effects or Glazed theme colors and utilities.")
            }), B({
                type: "style",
                heading: Drupal.t("Style"),
                param_name: "style",
                description: Drupal.t("Style options."),
                tab: Drupal.t("Style")
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Drop shadow"),
                param_name: "shadow",
                max: "5",
                value: "0",
                tab: Drupal.t("Style")
            }), B({
                type: "style",
                heading: Drupal.t("Hover style"),
                param_name: "hover_style",
                important: !0,
                description: Drupal.t("Hover style options."),
                tab: Drupal.t("Hover style")
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Drop shadow"),
                param_name: "hover_shadow",
                max: "5",
                value: "0",
                tab: Drupal.t("Hover style")
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Left"),
                param_name: "pos_left",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Right"),
                param_name: "pos_right",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Top"),
                param_name: "pos_top",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Bottom"),
                param_name: "pos_bottom",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Width"),
                param_name: "pos_width",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Height"),
                param_name: "pos_height",
                tab: Drupal.t("Placement"),
                max: "1",
                step: "0.01",
                hidden: !0
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Z-index"),
                param_name: "pos_zindex",
                tab: Drupal.t("Placement"),
                hidden: !0
            })],
            icon: "",
            thumbnail: "",
            is_container: !1,
            has_content: !1,
            frontend_render: !1,
            show_settings_on_create: !1,
            wrapper_class: "",
            weight: 0,
            hidden: !1,
            disallowed_elements: [],
            controls_base_position: "center",
            show_parent_controls: !1,
            highlighted: !0,
            style_selector: "",
            section: !1,
            controls_position: function(e) {
                if (this.dom_element) {
                    var t = this.dom_element[0].getBoundingClientRect();
                    if (e || 0 < t.bottom && t.top < document.documentElement.clientHeight) {
                        var a = this.controls[0].getBoundingClientRect();
                        if (0 < this.children.length && _.has(this.children[0].controls, "0") && !this.children[0].show_parent_controls) q(this.children[0].controls[0].getBoundingClientRect(), a) && this.children[0].dom_element.addClass("az-element--controls-spacer");
                        else if (_.has(this, "parent") && _.has(this.parent.controls, "0") && !this.show_parent_controls) {
                            if (q(this.parent.controls[0].getBoundingClientRect(), a) && y(this.dom_element).addClass("az-element--controls-spacer"), this.parent.show_parent_controls) q(this.parent.parent.controls[0].getBoundingClientRect(), a) && y(this.dom_element).addClass("az-element--controls-spacer")
                        }
                        if (t.top + y(window).scrollTop() < 300) {
                            var n;
                            if (0 < y("body.body--glazed-header-navbar_pull_down #navbar").length) q(n = y("#navbar .container-col")[0].getBoundingClientRect(), a) && y(this.dom_element).closest(".glazed-editor").css("margin-top", n.height / 2);
                            if (0 < y("body.body--glazed-header-overlay #navbar").length) q(n = y("#navbar")[0].getBoundingClientRect(), a) && this.controls.css("margin-top", n.height + 32);
                            if (0 < y("#navbar.glazed-header--fixed").length) q(n = y("#navbar")[0].getBoundingClientRect(), a) && this.controls.css("margin-top", n.height + 32)
                        }
                        if (!this.is_container || this.has_content) {
                            var i = y(this.dom_element).height(),
                                o = y(window).height();
                            if (o < i) {
                                var s = y(window).scrollTop(),
                                    l = (this.controls.offset().top, s - y(this.dom_element).offset().top + o / 2);
                                40 < l && l < i ? this.controls.css("top", l) : i < l ? this.controls.css("top", i - 40) : this.controls.css("top", 40)
                            }
                        }
                    }
                }
            },
            update_controls_zindex: function() {
                u(this.controls)
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    var e = this;
                    if (this.controls = y('<div class="controls btn-group btn-group-xs"></div>').prependTo(this.dom_element), y(this.dom_element).addClass("az-element--controls-" + this.controls_base_position), this.show_parent_controls && y(this.dom_element).addClass("az-element--controls-show-parent"), setTimeout(function() {
                            e.update_controls_zindex()
                        }, 1e3), y('<span title="' + d("Drag and drop") + '" class="control drag-and-drop btn btn-default glyphicon glyphicon-move"><span class="control-label">' + this.name + "</span></span>").appendTo(this.controls), this.is_container && !this.has_content && (y('<span title="' + d("Add") + '" class="control add btn btn-default glyphicon glyphicon-plus"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_add), y('<span title="' + d("Paste") + '" class="control paste btn btn-default glyphicon glyphicon-hand-down"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_paste)), y('<span title="' + d("Edit") + '" class="control edit btn btn-default glyphicon glyphicon-pencil"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_edit), y('<span title="' + d("Copy") + '" class="control copy btn btn-default glyphicon glyphicon-briefcase"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_copy), y('<span title="' + d("Clone") + '" class="control clone btn btn-default glyphicon glyphicon-duplicate"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_clone), y('<span title="' + d("Remove") + '" class="control remove btn btn-default glyphicon glyphicon-trash"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_remove), window.glazed_online && y('<span title="' + d("Save as template") + '" class="control save-template btn btn-default glyphicon glyphicon-floppy-save"> </span>').appendTo(this.controls).click({
                            object: this
                        }, this.click_save_template), this.update_empty(), 0 < e.children.length && e.children[0].show_parent_controls || y(e.dom_element).hover(function() {
                            e.controls.addClass("controls--show")
                        }, function() {
                            e.controls.removeClass("controls--show")
                        }), setTimeout(function() {
                            e.controls_position(!0)
                        }, 1500), y(window).scroll(_.debounce(function() {
                            30 < window.pageYOffset && e.controls_position(!1)
                        }, 500)), y(window).resize(_.debounce(function() {
                            e.controls_position(!0)
                        }, 500)), e.show_parent_controls) {
                        var a = e.parent;
                        _.isString(e.show_parent_controls) && (a = b.get_element(y(e.dom_element).closest(e.show_parent_controls).attr("data-az-id"))), y(e.dom_element).off("mouseenter").on("mouseenter", function() {
                            y(e.dom_element).data("hover", !0), 0 < y(e.dom_element).parents(".glazed-editor").length && (y(a.controls).addClass("controls--show"), e.controls.addClass("controls--show"), function(e) {
                                e.controls_position(!0), y(a.controls).attr("data-az-cid", y(e.dom_element).attr("data-az-id"));
                                var t = y(e.dom_element).offset();
                                t.top = t.top - parseInt(y(e.dom_element).css("margin-top")) + parseInt(y(a.controls).css("margin-top")), y(a.controls).offset(t), t.left = t.left + y(a.controls).width() - 1, e.controls.offset(t)
                            }(e))
                        }), y(e.dom_element).off("mouseleave").on("mouseleave", function() {
                            y(e.dom_element).data("hover", !1), 0 < y(e.dom_element).parents(".glazed-editor").length && (y(a.controls).removeClass("controls--show"), e.controls.removeClass("controls--show"))
                        }), setTimeout(function() {
                            y(a.controls).off("mouseenter").on("mouseenter", function() {
                                y(a.controls).data("hover", !0);
                                var e = b.get_element(y(this).closest("[data-az-cid]").attr("data-az-cid"));
                                _.isUndefined(e) || y(e.controls).addClass("controls--show")
                            }), y(a.controls).off("mouseleave").on("mouseleave", function() {
                                y(a.controls).data("hover", !1);
                                var e = b.get_element(y(this).closest("[data-az-cid]").attr("data-az-cid"));
                                _.isUndefined(e) || y(e.controls).removeClass("controls--show")
                            })
                        }, 100)
                    }
                }
            },
            get_empty: function() {
                return '<div class="az-empty"></div>'
            },
            update_empty: function() {
                if (drupalSettings.glazedBuilder.glazedEditor)
                    if (0 == this.children.length && this.is_container && !this.has_content || this.has_content && "" == this.attrs.content) {
                        y(this.dom_content_element).find("> .az-empty").remove();
                        var e = y(this.get_empty()).appendTo(this.dom_content_element);
                        0 == y(e).find(".bottom").length && "bottom", 0 == y(e).find(".top").length && "top", y(e).click(function(e) {
                            if (1 == e.which) {
                                var t = y(this).closest("[data-az-id]").attr("data-az-id");
                                b.show(b.get_element(t), function(e) {})
                            }
                        })
                    } else y(this.dom_content_element).find("> .az-empty").remove()
            },
            get_button: function() {
                return "" == this.thumbnail ? '<div class="well text-center pull-left text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>" : '<div class="well pull-left" data-az-element="' + this.base + '" style="background-image: url(' + encodeURI(this.thumbnail) + '); background-position: center center; background-size: cover;"></div>'
            },
            spawn_modal_skeleton: function() {
                y("#az-elements-modal").remove();
                y('<div id="az-elements-modal" class="modal glazed"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><span class="close" data-dismiss="modal">&times;</span><h4 class="modal-title"><img src="' + drupalSettings.glazedBuilder.glazedBaseUrl + 'images/glazed-logo-white.svg"></h4></div><div class="modal-body"></div></div></div></div>').prependTo("body")
            },
            click_add: function(e) {
                return e.data.object.add(), !1
            },
            add: function() {
                b.show(this, function(e) {})
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && (this.is_container && !this.has_content || this instanceof ae) && y(this.dom_content_element).sortable({
                    items: "> .az-element",
                    connectWith: ".az-ctnr",
                    handle: "> .controls > .drag-and-drop",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    scrollSpeed: 100,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            replace_render: function() {
                var e = this.dom_element,
                    t = this.dom_content_element;
                null != e && (this.render(y), y(e).replaceWith(this.dom_element), null != t && y(this.dom_content_element).replaceWith(t)), drupalSettings.glazedBuilder.glazedEditor && this.show_controls()
            },
            update_dom: function() {
                this.detach_children(), y(this.dom_element).remove(), this.parent.detach_children(), this.render(y), this.attach_children(), drupalSettings.glazedBuilder.glazedEditor && this.show_controls(), this.parent.attach_children(), drupalSettings.glazedBuilder.glazedEditor && (this.update_sortable(), this.update_empty()), this.showed(y)
            },
            get_el_classes: function() {
                return this.attrs.el_class
            },
            get_content_classes: function() {
                var e = "";
                return 0 < this.attrs.shadow && (e = "glazed-shadow-" + this.attrs.shadow), 0 < this.attrs.hover_shadow && (e = e + " glazed-shadow-hover-" + this.attrs.hover_shadow), e
            },
            get_hover_style: function() {
                return "hover_style" in this.attrs ? "<style>\x3c!-- .hover-style-" + this.id + ":hover " + this.style_selector + " { " + this.attrs.hover_style + "} --\x3e</style>" : ""
            },
            restore: function(e) {},
            recursive_restore: function(e) {
                for (var t = 0; t < this.children.length; t++) this.children[t].recursive_restore(e);
                this.restore(e)
            },
            showed: function(e) {
                "pos_left" in this.attrs && "" != this.attrs.pos_left && e(this.dom_element).css("left", this.attrs.pos_left), "pos_right" in this.attrs && "" != this.attrs.pos_right && e(this.dom_element).css("right", this.attrs.pos_right), "pos_top" in this.attrs && "" != this.attrs.pos_top && e(this.dom_element).css("top", this.attrs.pos_top), "pos_bottom" in this.attrs && "" != this.attrs.pos_bottom && e(this.dom_element).css("bottom", this.attrs.pos_bottom), "pos_width" in this.attrs && "" != this.attrs.pos_width && e(this.dom_element).css("width", this.attrs.pos_width), "pos_height" in this.attrs && "" != this.attrs.pos_height && e(this.dom_element).css("height", this.attrs.pos_height), "pos_zindex" in this.attrs && "" != this.attrs.pos_zindex && e(this.dom_element).css("z-index", this.attrs.pos_zindex), "hover_style" in this.attrs && "" != this.attrs.hover_style && (e("head").find("#hover-style-" + this.id).remove(), e("head").append(this.get_hover_style()), e(this.dom_element).addClass("hover-style-" + this.id))
            },
            render: function(e) {
                e(this.dom_element).attr("data-az-id", this.id)
            },
            trigger_start_in_animation: function() {
                for (var e = 0; e < this.children.length; e++) "trigger_start_in_animation" in this.children[e] && this.children[e].trigger_start_in_animation()
            },
            trigger_start_out_animation: function() {
                for (var e = 0; e < this.children.length; e++) "trigger_start_out_animation" in this.children[e] && this.children[e].trigger_start_out_animation()
            },
            update_data: function() {
                y(this.dom_element).attr("data-azb", this.base);
                for (var e = 0; e < this.params.length; e++) {
                    var t = this.params[e];
                    if (t.param_name in this.attrs) {
                        var a = this.attrs[t.param_name];
                        ("" == a && t.can_be_empty || "" != a) && "content" !== t.param_name && a !== t.value && (t.safe || (a = encodeURIComponent(a)), y(this.dom_element).attr("data-azat-" + t.param_name, a))
                    }
                }
                null != this.dom_content_element && y(this.dom_content_element).attr("data-azcnt", "true")
            },
            update_data_custom: function() {},
            recursive_update_data: function() {
                this.update_data(), this.update_data_custom();
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_update_data()
            },
            recursive_clear_animation: function() {
                "clear_animation" in this && this.clear_animation();
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_clear_animation()
            },
            recursive_showed: function() {
                this.showed(y);
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_showed()
            },
            update_sorting_children: function() {
                var e = y(this.dom_content_element).sortable("option"),
                    t = [];
                y(this.dom_content_element).find(e.items).each(function() {
                    t.push(b.get_element(y(this).attr("data-az-id")))
                }), this.children = t.filter(Boolean);
                for (var a = 0; a < this.children.length; a++) this.children[a].parent = this;
                this.update_empty()
            },
            update_sorting: function(e, t) {
                var a = b.get_element(y(t.item).closest("[data-az-id]").attr("data-az-id"));
                a && (t.source = b.get_element(y(this).closest("[data-az-id]").attr("data-az-id")), t.from_pos = a.get_child_position(), t.source.update_sorting_children(), t.target = b.get_element(y(t.item).parent().closest("[data-az-id]").attr("data-az-id")), t.source.id != t.target.id && t.target.update_sorting_children(), t.to_pos = a.get_child_position(), y(document).trigger("glazed_update_sorting", t))
            },
            click_edit: function(e) {
                return ie(e), e.data.object.edit(), !1
            },
            edit: function() {
                T.prototype.show_editor(this.params, this, this.edited)
            },
            edited: function(e) {
                for (var t in e) this.attrs[t] = h(e[t]);
                this.update_dom(), y(document).trigger("glazed_edited_element", this.id)
            },
            attrs2string: function() {
                for (var e = "", t = 0; t < this.params.length; t++) {
                    var a = this.params[t];
                    if (a.param_name in this.attrs) {
                        var n = this.attrs[a.param_name];
                        ("" == n && a.can_be_empty || "" != n) && "content" !== a.param_name && n !== a.value && (a.safe || (n = encodeURIComponent(n)), e += a.param_name + '="' + n + '" ')
                    }
                }
                return e
            },
            get_content: function() {
                for (var e = 0; e < this.params.length; e++) {
                    var t = this.params[e];
                    if ("content" === t.param_name && "html" == t.type) return encodeURIComponent(this.attrs.content)
                }
                return this.attrs.content
            },
            set_content: function(e) {
                for (var t = h(e), a = 0; a < this.params.length; a++) {
                    var n = this.params[a];
                    if ("content" === n.param_name && "html" == n.type) {
                        try {
                            t = decodeURIComponent(atob(t.replace(/^#E\-8_/, "")))
                        } catch (e) {
                            t = decodeURIComponent(t.replace(/^#E\-8_/, ""))
                        }
                        return void(this.attrs.content = t)
                    }
                }
                this.attrs.content = t
            },
            parse_attrs: function(e) {
                for (var t = 0; t < this.params.length; t++) {
                    var a = this.params[t];
                    if (a.param_name in e)
                        if (a.safe) this.attrs[a.param_name] = h(e[a.param_name]);
                        else {
                            var n = h(e[a.param_name]);
                            try {
                                this.attrs[a.param_name] = decodeURIComponent(atob(n.replace(/^#E\-8_/, "")))
                            } catch (e) {
                                this.attrs[a.param_name] = decodeURIComponent(n.replace(/^#E\-8_/, ""))
                            }
                        } else "value" in a && _.isString(a.value) && (this.attrs[a.param_name] = a.value)
                }
                for (var i in e) i in this.attrs || (this.attrs[i] = e[i]);
                y(document).trigger("glazed_edited_element", this.id)
            },
            get_nested_depth: function(e) {
                var t = 0;
                return null != this.parent && (t += this.parent.get_nested_depth(e)), this.base == e && t++, t
            },
            get_my_shortcode: function() {
                var e = _.keys(U.prototype.elements),
                    t = _.object(e, Array.apply(null, new Array(e.length)).map(Number.prototype.valueOf, 0));
                return this.get_shortcode(t)
            },
            get_children_shortcode: function() {
                for (var e = _.keys(U.prototype.elements), t = _.object(e, Array.apply(null, new Array(e.length)).map(Number.prototype.valueOf, 0)), a = "", n = 0; n < this.children.length; n++) a += this.children[n].get_shortcode(t);
                return a
            },
            get_shortcode: function(e) {
                e[this.base]++;
                for (var t = "", a = 0; a < this.children.length; a++) t += this.children[a].get_shortcode(e);
                if ("az_unknown" == this.base) o = t;
                else {
                    var n = "";
                    if (1 == e[this.base]) n = this.base;
                    else {
                        var i = e[this.base] - 1;
                        n = this.base + "_" + i
                    }
                    var o = "[" + n + " " + this.attrs2string() + "]";
                    this.is_container && (this.has_content ? o += this.get_content() + "[/" + n + "]" : o += t + "[/" + n + "]")
                }
                return e[this.base]--, o
            },
            parse_shortcode: function(d) {
                var c = _.keys(U.prototype.tags).join("|"),
                    e = C.shortcode.regexp(c),
                    t = y.trim(d).match(e);
                if (_.isNull(t)) {
                    if (0 == d.length) return;
                    "[" == d.substring(0, 1) && "]" == d.slice(-1) ? this.parse_shortcode("[az_unknown]" + d + "[/az_unknown]") : this.parse_shortcode('[az_row][az_column width="1/1"][az_text]' + d + "[/az_text][/az_column][/az_row]")
                }
                _.each(t, function(e) {
                    var t = e.match(p(c)),
                        a = t[5],
                        n = new RegExp("^[\\s]*\\[\\[?(" + _.keys(U.prototype.tags).join("|") + ")(?![\\w-])"),
                        i = C.shortcode.attrs(t[3]),
                        o = t[2];
                    if (!(this.get_nested_depth(o) > U.prototype.max_nested_depth)) {
                        var s = ae;
                        if (o in U.prototype.tags && (s = U.prototype.tags[o]), this instanceof te && null == this.parent && !s.prototype.section) this.parse_shortcode("[az_section]" + d + "[/az_section]");
                        else {
                            var l = new s(this, !0);
                            l.parse_attrs(i.named);
                            var r = U.prototype.tags[o].prototype;
                            _.isString(a) && a.match(n) && !0 === r.is_container ? l.parse_shortcode(a) : _.isString(a) && a.length && "az_row" === o ? l.parse_shortcode('[az_column width="1/1"][az_text]' + a + "[/az_text][/az_column]") : _.isString(a) && a.length && "az_column" === o && ("[" != a.substring(0, 1) || "]" != a.slice(-1)) ? l.parse_shortcode("[az_text]" + a + "[/az_text]") : _.isString(a) && (!0 === r.has_content ? l.set_content(a) : "" != a && l.parse_shortcode("[az_unknown]" + a + "[/az_unknown]"))
                        }
                    }
                }, this)
            },
            parse_html: function(e) {
                var o = this;
                if (0 == y(e).children().closest_descendents("[data-azb]").length && 0 < y.trim(y(e).html()).length) {
                    var t = new H(o, !1);
                    t.children = [];
                    var a = new G(t, !1),
                        n = new U.prototype.elements.az_text(a, !1);
                    n.attrs.content = y(e).html(), n.update_dom(), "update_empty" in o && o.update_empty(), "update_empty" in a && a.update_empty(), "update_empty" in t && t.update_empty()
                } else y(e).children().closest_descendents("[data-azb]").each(function() {
                    var e = y(this).attr("data-azb"),
                        t = ae;
                    e in U.prototype.tags && (t = U.prototype.tags[e]);
                    var a = new t(o, !0);
                    l && (b.elements_instances[a.id] = null, delete b.elements_instances[a.id], a.id = y(this).attr("data-az-id"), b.elements_instances[a.id] = a), a.dom_element = y(this);
                    var n = {};
                    if (y(y(this)[0].attributes).each(function() {
                            0 <= this.nodeName.indexOf("data-azat") && (n[this.nodeName.replace("data-azat-", "")] = this.value)
                        }), a.parse_attrs(n), a.is_container) {
                        var i = y(this).closest_descendents("[data-azcnt]");
                        0 < i.length && (a.dom_content_element = y(i), a.has_content ? a instanceof ae ? (a.attrs.content = y(i).wrap("<div></div>").parent().html(), y(i).unwrap()) : a.attrs.content = y(i).html() : a.parse_html(i))
                    }
                })
            },
            recursive_render: function() {
                for (var e = 0; e < this.children.length; e++) this.children[e].recursive_render();
                l ? this.frontend_render && (this.detach_children(), this.parent.detach_children(), this.render(y), this.attach_children(), this.parent.attach_children()) : (this.render(y), this.attach_children()), drupalSettings.glazedBuilder.glazedEditor && (this.show_controls(), this.update_sortable())
            },
            detach_children: function() {
                for (var e = 0; e < this.children.length; e++) y(this.children[e].dom_element).detach()
            },
            attach_children: function() {
                for (var e = 0; e < this.children.length; e++) y(this.dom_content_element).append(this.children[e].dom_element)
            },
            click_copy: function(e) {
                return e.data.object.copy(), !1
            },
            copy: function() {
                var e = this.get_my_shortcode();
                y("#glazed-clipboard").html(encodeURIComponent(e))
            },
            click_paste: function(e) {
                return e.data.object.paste(0), !1
            },
            paste: function(e) {
                var t = decodeURIComponent(y("#glazed-clipboard").html());
                if ("" != t) {
                    var a = this.children.length;
                    U.prototype.parse_shortcode.call(this, t);
                    for (var n = [], i = a; i < this.children.length; i++) this.children[i].recursive_render(), n.push(this.children[i]);
                    this.children = this.children.slice(0, a), this.children.splice.apply(this.children, [e, 0].concat(n)), this.detach_children(), this.attach_children(), this.update_empty(), this.update_sortable(), this.recursive_showed()
                }
            },
            click_save_template: function(e) {
                return e.data.object.save_template(), !1
            },
            save_template: function() {
                var e = this.get_my_shortcode(),
                    t = window.prompt(Drupal.t("Enter template name"), "");
                "" != t && null != t && function(t, a) {
                    y.ajax({
                        type: "get",
                        url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        y.ajax({
                            type: "POST",
                            url: e,
                            data: {
                                action: "glazed_save_template",
                                url: window.location.href,
                                name: t,
                                template: a
                            },
                            cache: !1,
                            context: this
                        }).done(function(e) {})
                    })
                }(t, e)
            },
            click_clone: function(e) {
                return ie(e), e.data.object.clone(), !1
            },
            clone: function() {
                this.copy();
                for (var e = 0; e < this.parent.children.length; e++)
                    if (this.parent.children[e].id == this.id) {
                        this.parent.paste(e);
                        break
                    }
                y(window).trigger("CKinlineAttach")
            },
            click_remove: function(e) {
                return e.data.object.remove(), !1
            },
            remove: function() {
                b.delete_element(this.id);
                for (var e = 0; e < this.children.length; e++) this.children[e].remove();
                y(this.dom_element).remove();
                for (e = 0; e < this.parent.children.length; e++)
                    if (this.parent.children[e].id == this.id) {
                        this.parent.children.splice(e, 1);
                        break
                    }
                this.parent.update_empty()
            },
            get_child_position: function() {
                for (var e = 0; e < this.parent.children.length; e++)
                    if (this.parent.children[e].id == this.id) return e;
                return -1
            },
            add_css: function(e, t, a) {
                this.get_my_container().css[drupalSettings.glazedBuilder.glazedBaseUrl + e] = !0, t || window.glazed_add_css(e, a)
            },
            add_js_list: function(e) {
                for (var t = this.get_my_container(), a = 0; a < e.paths.length; a++) t.js[drupalSettings.glazedBuilder.glazedBaseUrl + e.paths[a]] = !0;
                window.glazed_add_js_list(e)
            },
            add_js: function(e) {
                this.get_my_container().js[drupalSettings.glazedBuilder.glazedBaseUrl + e.path] = !0, window.glazed_add_js(e)
            },
            add_external_js: function(e, t) {
                this.get_my_container().js[e] = !0, window.glazed_add_external_js(e, t)
            },
            get_my_container: function() {
                return this instanceof te ? this : this.parent.get_my_container()
            },
            get_all_disallowed_elements: function() {
                return "parent" in this ? _.uniq(this.parent.get_all_disallowed_elements().concat(this.disallowed_elements)) : this.disallowed_elements
            }
        }, s(K, U), w(K.prototype, {
            params: [B({
                type: "dropdown",
                heading: Drupal.t("Animation start"),
                param_name: "an_start",
                tab: Drupal.t("Animation"),
                value: {
                    "": Drupal.t("No animation"),
                    appear: Drupal.t("On appear"),
                    hover: Drupal.t("On hover"),
                    click: Drupal.t("On click"),
                    trigger: Drupal.t("On trigger")
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Animation in"),
                param_name: "an_in",
                tab: Drupal.t("Animation"),
                value: t,
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Animation out"),
                param_name: "an_out",
                tab: Drupal.t("Animation"),
                value: t,
                dependency: {
                    element: "an_start",
                    value: ["hover", "trigger"]
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Hidden"),
                param_name: "an_hidden",
                tab: Drupal.t("Animation"),
                value: {
                    before_in: Drupal.t("Before in-animation"),
                    after_in: Drupal.t("After in-animation")
                },
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Infinite"),
                param_name: "an_infinite",
                tab: Drupal.t("Animation"),
                value: {
                    yes: Drupal.t("Yes")
                },
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Appear Boundary"),
                param_name: "an_offset",
                tab: Drupal.t("Animation"),
                max: "100",
                description: Drupal.t("In percent. (50% is center, 100% is bottom of screen)"),
                value: "100",
                dependency: {
                    element: "an_start",
                    value: ["appear"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Duration"),
                param_name: "an_duration",
                tab: Drupal.t("Animation"),
                max: "3000",
                description: Drupal.t("In milliseconds."),
                value: "1000",
                step: "50",
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("In-delay"),
                param_name: "an_in_delay",
                tab: Drupal.t("Animation"),
                max: "10000",
                description: Drupal.t("In milliseconds."),
                value: "0",
                dependency: {
                    element: "an_start",
                    value: ["appear", "hover", "click", "trigger"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Out-delay"),
                param_name: "an_out_delay",
                tab: Drupal.t("Animation"),
                max: "10000",
                description: Drupal.t("In milliseconds."),
                value: "0",
                dependency: {
                    element: "an_start",
                    value: ["hover", "trigger"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Parent number"),
                param_name: "an_parent",
                tab: Drupal.t("Animation"),
                max: "10",
                min: "0",
                description: Drupal.t("Define the number of Parent Containers the animation should attempt to break away from."),
                value: "1",
                dependency: {
                    element: "an_start",
                    value: ["hover", "click"]
                }
            }), B({
                type: "textfield",
                heading: Drupal.t("Name for animations"),
                param_name: "an_name",
                hidden: !0
            })].concat(K.prototype.params),
            set_in_timeout: function() {
                var e = this;
                e.in_timeout = setTimeout(function() {
                    e.clear_animation(), y(e.dom_element).css("opacity", ""), y(e.dom_element).removeClass("animated"), y(e.dom_element).removeClass(e.attrs.an_in), y(e.dom_element).removeClass(e.attrs.an_out), e.animation_in = !1, e.animation_out = !1, y(e.dom_element).css("animation-duration", e.attrs.an_duration + "ms"), y(e.dom_element).css("-webkit-animation-duration", e.attrs.an_duration + "ms"), y(e.dom_element).addClass("animated"), e.animated = !0, "yes" == e.attrs.an_infinite && y(e.dom_element).addClass("infinite"), y(e.dom_element).addClass(e.attrs.an_in), e.animation_in = !0
                }, Math.round(e.attrs.an_in_delay))
            },
            start_in_animation: function() {
                var e = this;
                0 == y(e.dom_element).parents(".glazed-animations-disabled").length && "" != e.attrs.an_in && (e.animated ? e.animation_out ? e.set_in_timeout() : 0 < e.out_timeout && (clearTimeout(e.out_timeout), e.hidden_after_in || e.set_in_timeout()) : e.set_in_timeout())
            },
            set_out_timeout: function() {
                var e = this;
                e.out_timeout = setTimeout(function() {
                    e.clear_animation(), y(e.dom_element).css("opacity", ""), y(e.dom_element).removeClass("animated"), y(e.dom_element).removeClass(e.attrs.an_in), y(e.dom_element).removeClass(e.attrs.an_out), e.animation_in = !1, e.animation_out = !1, y(e.dom_element).css("animation-duration", e.attrs.an_duration + "ms"), y(e.dom_element).css("-webkit-animation-duration", e.attrs.an_duration + "ms"), y(e.dom_element).addClass("animated"), e.animated = !0, "yes" == e.attrs.an_infinite && y(e.dom_element).addClass("infinite"), y(e.dom_element).addClass(e.attrs.an_out), e.animation_out = !0
                }, Math.round(e.attrs.an_out_delay))
            },
            start_out_animation: function() {
                var e = this;
                0 == y(e.dom_element).parents(".glazed-animations-disabled").length && "" != e.attrs.an_out && (e.animated ? e.animation_in ? e.set_out_timeout() : 0 < e.in_timeout && (clearTimeout(e.in_timeout), e.hidden_before_in || e.set_out_timeout()) : e.set_out_timeout())
            },
            clear_animation: function() {
                this.animation_in && (this.hidden_before_in && y(this.dom_element).css("opacity", "1"), this.hidden_after_in && y(this.dom_element).css("opacity", "0")), this.animation_out && (this.hidden_before_in && y(this.dom_element).css("opacity", "0"), this.hidden_after_in && y(this.dom_element).css("opacity", "1")), y(this.dom_element).hasClass("animated") && (y(this.dom_element).css("animation-duration", ""), y(this.dom_element).css("-webkit-animation-duration", ""), y(this.dom_element).removeClass("animated"), this.animated = !1, y(this.dom_element).removeClass("infinite"), y(this.dom_element).removeClass(this.attrs.an_in), y(this.dom_element).removeClass(this.attrs.an_out), this.animation_in = !1, this.animation_out = !1)
            },
            end_animation: function() {
                this.in_timeout = 0, this.out_timeout = 0, this.animation_in && (this.clear_animation(), "hover" != this.attrs.an_start || this.hover || this.attrs.an_in != this.attrs.an_out && this.start_out_animation()), this.animation_out && (this.clear_animation(), "hover" == this.attrs.an_start && this.hover && this.attrs.an_in != this.attrs.an_out && this.start_in_animation())
            },
            trigger_start_in_animation: function() {
                "trigger" == this.attrs.an_start ? this.start_in_animation() : K.baseclass.prototype.trigger_start_in_animation.apply(this, arguments)
            },
            trigger_start_out_animation: function() {
                "trigger" == this.attrs.an_start ? this.start_out_animation() : K.baseclass.prototype.trigger_start_out_animation.apply(this, arguments)
            },
            animation: function() {
                var t = this;
                t.hidden_before_in = 0 <= _.indexOf(t.attrs.an_hidden.split(","), "before_in"), t.hidden_after_in = 0 <= _.indexOf(t.attrs.an_hidden.split(","), "after_in"), t.hidden_before_in && y(t.dom_element).css("opacity", "0"), t.hidden_after_in && y(t.dom_element).css("opacity", "1");
                var e = t.attrs.an_parent;
                "" == e && (e = 1), e = Math.round(e);
                for (var a = 0, n = y(t.dom_element); a < e;) n = y(n).parent().closest("[data-az-id]"), a++;
                if ("" != t.attrs.an_start) {
                    t.in_timeout = 0, t.out_timeout = 0, t.animated = !1, t.animation_in = !1, t.animation_out = !1;
                    t.add_css("vendor/animate.css/animate.min.css", !1, function() {
                        ! function() {
                            switch (y(n).off("click.az_animation" + t.id), y(n).off("mouseenter.az_animation" + t.id), y(n).off("mouseleave.az_animation" + t.id), t.attrs.an_start) {
                                case "click":
                                    y(n).on("click.az_animation" + t.id, function() {
                                        t.animated || t.start_in_animation()
                                    });
                                    break;
                                case "appear":
                                    t.add_js({
                                        path: "vendor/jquery.waypoints/lib/jquery.waypoints.min.js",
                                        loaded: "waypoint" in y.fn,
                                        callback: function() {
                                            y(t.dom_element).waypoint(function(e) {
                                                t.animated || t.start_in_animation()
                                            }, {
                                                offset: t.attrs.an_offset + "%",
                                                handler: function(e) {
                                                    this.destroy()
                                                }
                                            }), y(document).trigger("scroll")
                                        }
                                    });
                                    break;
                                case "hover":
                                    y(n).on("mouseenter.az_animation" + t.id, function() {
                                        t.hover = !0, t.start_in_animation()
                                    }), y(n).on("mouseleave.az_animation" + t.id, function() {
                                        t.hover = !1, t.start_out_animation()
                                    })
                            }
                        }()
                    })
                }
            },
            update_scroll_animation: function() {
                return !1
            },
            showed: function(e) {
                K.baseclass.prototype.showed.apply(this, arguments), this.an_name = "", "an_name" in this.attrs && "" != this.attrs.an_name && (this.an_name = this.attrs.an_name, b.elements_instances_by_an_name[this.an_name] = this), "an_start" in this.attrs && "" != this.attrs.an_start && "no" != this.attrs.an_start && this.animation()
            },
            render: function(e) {
                "an_name" in this.attrs && "" != this.attrs.an_name && e(this.dom_element).attr("data-an-name", this.attrs.an_name), K.baseclass.prototype.render.apply(this, arguments)
            }
        }), b = new P, drupalSettings.glazedBuilder.glazedAjaxUrl || drupalSettings.glazedBuilder.glazedEditor && !window.glazed_online || delete drupalSettings.glazedBuilder.glazedEditor,
        function(t) {
            drupalSettings.glazedBuilder.glazedEditor ? t(drupalSettings.glazedBuilder.glazedEditor) : y.ajax({
                type: "get",
                url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                y.ajax({
                    type: "POST",
                    url: e,
                    data: {
                        action: "glazed_login",
                        url: window.location.href
                    },
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    t(e)
                })
            })
        }(function(e) {
            drupalSettings.glazedBuilder.glazedEditor = e, y(function() {
                ! function() {
                    if (drupalSettings.glazedBuilder.glazedEditor) {
                        for (var e in glazed_add_css("vendor/font-awesome/css/font-awesome.min.css", function() {}), 0 == y("#glazed-clipboard").length && y("body").prepend('<div id="glazed-clipboard" style="display:none"></div>'), glazed_add_js({
                                path: "vendor/chosen/chosen.jquery.min.js"
                            }), glazed_add_css("vendor/chosen/chosen.min.css", function() {}), b.elements_instances)(t = b.elements_instances[e]) instanceof te && y(t.dom_element).addClass("glazed-editor"), null == t.controls && t.show_controls(), t.update_sortable();
                        y("#az-exporter").show()
                    } else {
                        for (var e in b.elements_instances) {
                            var t;
                            (t = b.elements_instances[e]) instanceof te && y(t.dom_element).removeClass("glazed-editor"), null != t.controls && y(t.controls).remove(), t.update_empty()
                        }
                        y("#az-exporter").hide()
                    }
                }()
            })
        }), A = function() {
            ! function() {
                if (Y) return;
                Y = !0, drupalSettings.glazedBuilder.disallowContainers;
                var a = drupalSettings.glazedBuilder.disallowContainers;
                y(".az-container").each(function() {
                    var e = y(this).attr("data-az-name");
                    if (-1 == y.inArray(e, a)) {
                        var t = V(this);
                        t && z.push(t)
                    }
                }), drupalSettings.glazedBuilder.glazedEditor && 0 == y("#glazed-clipboard").length && y("body").prepend('<div id="glazed-clipboard" class="glazed-backend" style="display:none"></div>')
            }(), y.holdReady(!1)
        }, y.holdReady(!0), "complete" === document.readyState ? setTimeout(A) : document.addEventListener ? (document.addEventListener("DOMContentLoaded", A, !1), window.addEventListener("load", A, !1)) : (document.attachEvent("onreadystatechange", A), window.attachEvent("onload", A));
    var Y = !1;

    function F(e, t) {
        F.baseclass.apply(this, arguments)
    }

    function H(e, t) {
        H.baseclass.apply(this, arguments), this.columns = "", t && "boolean" == typeof t || this.set_columns("1/2 + 1/2"), this.attrs.device = "sm"
    }

    function G(e, t) {
        G.baseclass.call(this, e, t)
    }

    function W(e, t) {
        W.baseclass.apply(this, arguments), t && "boolean" == typeof t || this.add_tab()
    }

    function $(e, t) {
        $.baseclass.apply(this, arguments)
    }

    function J(e, t) {
        J.baseclass.apply(this, arguments), t && "boolean" == typeof t || this.add_toggle()
    }

    function X(e, t) {
        X.baseclass.apply(this, arguments)
    }

    function Q(e, t) {
        Q.baseclass.apply(this, arguments), t && "boolean" == typeof t || this.add_slide()
    }

    function Z(e, t) {
        Z.baseclass.apply(this, arguments)
    }

    function ee(e, t) {
        ee.baseclass.apply(this, arguments)
    }

    function te(e, t) {
        te.baseclass.apply(this, arguments), this.rendered = !1, this.loaded_container = null, this.js = {}, this.css = {}
    }

    function ae(e, t) {
        ae.baseclass.apply(this, arguments)
    }

    function ne() {
        if ("glazed_extend" in window)
            for (var e in window.glazed_extend) {
                var t = window.glazed_extend[e],
                    a = [];
                "params" in t && (a = t.params), delete t.params;
                var n = U.prototype.elements[e];
                if (!("extended" in n)) {
                    n.extended = !0, w(n.prototype, t);
                    for (var i = 0; i < a.length; i++) {
                        var o = B(a[i]);
                        n.prototype.params.push(o)
                    }
                }
            }
    }

    function ie(e) {
        var a = [];

        function n(e) {
            for (var t = 0; t < e.children.length; t++) n(e.children[t]);
            e.id in a && (e.attrs.content = a[e.id])
        }
        y(document).find(".az-element.az-text, .az-element.az-blockquote").each(function() {
            var e = y(this);
            0 < e.children(".ckeditor-inline").length && (a[e.attr("data-az-id")] = e.children(".ckeditor-inline").html())
        }), e.data.object.id in a && (e.data.object.attrs.content = a[e.data.object.id]);
        for (var t = 0; t < e.data.object.children.length; t++) n(e.data.object.children[t])
    }
    y.fn.glazed_builder = function(e) {
            var t = {
                init: function(e) {
                    y.extend({
                        test: "test"
                    }, e);
                    return this.each(function() {
                        var n = this;
                        if (!(i = y(this).data("glazed_builder"))) {
                            var e = y("<div>" + y(n).val() + "</div>")[0];
                            y(e).find("> script").remove(), y(e).find("> link[href]").remove(), y(e).find("> .az-container > script").remove(), y(e).find("> .az-container > link[href]").remove(), y(n).css("display", "none");
                            var i, t = null;
                            if (0 < y(e).find("> .az-container[data-az-type][data-az-name]").length) t = y(e).children().insertAfter(n);
                            else {
                                var a = "textarea",
                                    o = "gb" + Math.random().toString(36).substr(2);
                                window.glazed_online && (a = window.glazed_type, o = window.glazed_name), (t = y('<div class="az-element az-container" data-az-type="' + a + '" data-az-name="' + o + '"></div>').insertAfter(n)).append(y(e).html())
                            }
                            window.glazed_title["Save container"] = Drupal.t("Generate HTML and JS for all elements which placed in current container element."), (i = V(t)) && (z.push(i), y(n).data("glazed_builder", i), i.save_container = function() {
                                glazed_add_js({
                                    path: "jsON-js/json2.min.js",
                                    loaded: "JSON" in window,
                                    callback: function() {
                                        _.defer(function() {
                                            if (i.id in b.elements_instances) {
                                                var e = i.get_container_html();
                                                if (window.glazed_online) y(n).val(e);
                                                else {
                                                    var t = i.attrs.container.split("/")[0],
                                                        a = i.attrs.container.split("/")[1];
                                                    y(n).val('<div class="az-element az-container" data-az-type="' + t + '" data-az-name="' + a + '">' + e + "</div>")
                                                }
                                            }
                                        })
                                    }
                                })
                            }, y(document).on("glazed_add_element", i.save_container), y(document).on("glazed_edited_element", i.save_container), y(document).on("glazed_update_element", i.save_container), y(document).on("glazed_delete_element", i.save_container), y(document).on("glazed_update_sorting", i.save_container))
                        }
                    })
                },
                show: function() {
                    this.each(function() {})
                },
                hide: function() {
                    this.each(function() {
                        var e = y(this).data("glazed_builder");
                        if (e) {
                            b.delete_element(e.id);
                            for (var t = 0; t < e.children.length; t++) e.children[t].remove();
                            y(e.dom_element).remove(), y(this).removeData("glazed_builder"), y(this).css("display", "")
                        }
                    })
                }
            };
            return t[e] ? t[e].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof e && e ? void y.error(e) : t.init.apply(this, arguments)
        }, M("az_section", !0, F), w(F.prototype, {
            name: Drupal.t("Section"),
            icon: "et et-icon-focus",
            category: Drupal.t("Layout"),
            params: [B({
                type: "checkbox",
                heading: Drupal.t("Full Width"),
                param_name: "fluid",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("100% Height"),
                param_name: "fullheight",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Vertical Centering"),
                param_name: "vertical_centering",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Background Effect"),
                param_name: "effect",
                tab: Drupal.t("Background Effects"),
                value: {
                    "": Drupal.t("Simple Image"),
                    fixed: Drupal.t("Fixed Image"),
                    parallax: Drupal.t("Parallax Image"),
                    gradient: Drupal.t("Gradient"),
                    youtube: Drupal.t("YouTube Video")
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Parallax speed"),
                param_name: "parallax_speed",
                tab: Drupal.t("Background Effects"),
                value: 20,
                dependency: {
                    element: "effect",
                    value: ["parallax"]
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Parallax Mode"),
                param_name: "parallax_mode",
                tab: Drupal.t("Background Effects"),
                value: {
                    fixed: Drupal.t("Fixed"),
                    scroll: Drupal.t("Local")
                },
                dependency: {
                    element: "effect",
                    value: ["parallax"]
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Disable on Mobile"),
                param_name: "parallax_mobile_disable",
                tab: Drupal.t("Background Effects"),
                value: {
                    yes: Drupal.t("On")
                },
                dependency: {
                    element: "effect",
                    value: ["parallax"]
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Start Color"),
                param_name: "gradient_start_color",
                tab: Drupal.t("Background Effects"),
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("End Color"),
                param_name: "gradient_end_color",
                tab: Drupal.t("Background Effects"),
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Gradient Direction"),
                param_name: "gradient_direction",
                tab: Drupal.t("Background Effects"),
                value: "180",
                min: "1",
                max: "360",
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Start Position"),
                param_name: "gradient_start",
                tab: Drupal.t("Background Effects"),
                value: "0",
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("End Position"),
                param_name: "gradient_end",
                tab: Drupal.t("Background Effects"),
                value: "100",
                dependency: {
                    element: "effect",
                    value: ["gradient"]
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Video Play Options"),
                param_name: "video_options",
                tab: Drupal.t("Background Effects"),
                value: {
                    disMobile: Drupal.t("Disable on mobile devices"),
                    loop: Drupal.t("Loop"),
                    mute: Drupal.t("Muted"),
                    noCrop: Drupal.t("Disable cropping out player"),
                    resizeSection: Drupal.t("Resize section to match video"),
                    showControls: Drupal.t("Show controls")
                },
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            }), B({
                type: "textfield",
                heading: Drupal.t("YouTube Video URL"),
                param_name: "video_youtube",
                tab: Drupal.t("Background Effects"),
                description: Drupal.t("Enter the YouTube video URL."),
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            }), B({
                type: "textfield",
                heading: Drupal.t("Start Time in seconds"),
                param_name: "video_start",
                tab: Drupal.t("Background Effects"),
                description: Drupal.t("Enter time in seconds from where video start to play."),
                value: "0",
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            }), B({
                type: "textfield",
                heading: Drupal.t("Stop Time in seconds"),
                param_name: "video_stop",
                tab: Drupal.t("Background Effects"),
                description: Drupal.t("Enter time in seconds where video ends."),
                value: "0",
                dependency: {
                    element: "effect",
                    value: ["youtube"]
                }
            })].concat(F.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            section: !0,
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor && (F.baseclass.prototype.show_controls.apply(this, arguments), this.parent instanceof te)) {
                    var e = this;
                    y('<div title="' + d("Add Section Below") + '" class="control add-section btn btn-default glyphicon glyphicon-plus-sign" > </div>').appendTo(this.dom_element).click(function() {
                        new U.prototype.elements.az_section(e.parent, e.dom_element.index()).update_dom(), e.parent.update_empty()
                    })
                }
            },
            showed: function(n) {
                F.baseclass.prototype.showed.apply(this, arguments);
                var i = this;
                switch (this.attrs.effect) {
                    case "parallax":
                        if (window.innerWidth < 481 && this.attrs.parallax_mobile_disable) {
                            n(i.dom_element).css("background-attachment", "scroll"), n(i.dom_element).css("background-position", "");
                            break
                        }
                        this.add_js_list({
                            paths: ["vendor/jquery.parallax/jquery.parallax.js", "vendor/jquery.waypoints/lib/jquery.waypoints.min.js"],
                            loaded: "waypoint" in n.fn && "parallax" in n.fn,
                            callback: function() {
                                n(i.dom_element).waypoint(function(e) {
                                    var t = n(i.dom_element).css("background-position").match(/([\w%]*) [\w%]/);
                                    if (null == t) var a = "50%";
                                    else a = t[1];
                                    n(i.dom_element).css("background-attachment", i.attrs.parallax_mode), n(i.dom_element).css("background-position", a + " 0"), n(i.dom_element).parallax(a, i.attrs.parallax_speed / 100)
                                }, {
                                    offset: "100%",
                                    handler: function(e) {
                                        this.destroy()
                                    }
                                }), n(document).trigger("scroll")
                            }
                        });
                        break;
                    case "fixed":
                        n(i.dom_element).css("background-attachment", "fixed");
                        break;
                    case "youtube":
                        ;
                        var o = 0 <= _.indexOf(i.attrs.video_options.split(","), "loop"),
                            s = 0 <= _.indexOf(i.attrs.video_options.split(","), "mute"),
                            l = -1 == _.indexOf(i.attrs.video_options.split(","), "noCrop"),
                            t = 0 <= _.indexOf(i.attrs.video_options.split(","), "resizeSection"),
                            r = -1 == _.indexOf(i.attrs.video_options.split(","), "disMobile"),
                            d = 0 <= _.indexOf(i.attrs.video_options.split(","), "showControls");
                        this.add_css("vendor/jquery.mb.YTPlayer/dist/css/jquery.mb.YTPlayer.min.css", "mb_YTPlayer" in n.fn, function() {}), this.add_js_list({
                            paths: ["vendor/jquery.mb.YTPlayer/dist/jquery.mb.YTPlayer.min.js", "vendor/jquery.waypoints/lib/jquery.waypoints.min.js"],
                            loaded: "waypoint" in n.fn && "mb_YTPlayer" in n.fn,
                            callback: function() {
                                if (t) {
                                    var e = 9 * n(i.dom_element)[0].offsetWidth / 16;
                                    n(i.dom_element).css("height", e), n(i.dom_element).css("overflow", "hidden")
                                }
                                n(i.dom_element).waypoint(function(e) {
                                    var t, a;
                                    n(i.dom_element).attr("data-property", "{videoURL:'" + (t = i.attrs.video_youtube, !(!(a = t.match(/^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/)) || 11 != a[7].length) && a[7]) + "',containment:'[data-az-id=" + i.id + "]', showControls:" + d.toString() + ", autoPlay:true, stopMovieOnBlur:false, loop:" + o.toString() + ", mute:" + s.toString() + ", optimizeDisplay:" + l.toString() + ", startAt:" + i.attrs.video_start + ", stopAt:" + i.attrs.video_stop + ", useOnMobile:" + r.toString() + ", showYTLogo:false, isStandAlonePlayer: false}"), n(i.dom_element).mb_YTPlayer()
                                }, {
                                    offset: "300%",
                                    handler: function(e) {
                                        this.destroy()
                                    }
                                }), n(document).trigger("scroll")
                            }
                        })
                }
            },
            render: function(e) {
                switch (this.dom_element = e('<div id="' + this.id + '" class="az-element az-section ' + this.get_el_classes() + " " + this.get_content_classes() + ' " style="' + this.attrs.style + '"></div>'), "yes" == this.attrs.fullheight && e(this.dom_element).css("height", "100vh"), "yes" == this.attrs.fluid ? this.dom_content_element = e('<div class="az-ctnr container-fluid"></div>').appendTo(this.dom_element) : this.dom_content_element = e('<div class="az-ctnr container"></div>').appendTo(this.dom_element), this.attrs.effect) {
                    case "parallax":
                        e(this.dom_element).attr("data-glazed-builder-libraries", "waypoints");
                        break;
                    case "youtube":
                        e(this.dom_element).attr("data-glazed-builder-libraries", "ytplayer")
                }
                if ("gradient" == this.attrs.effect) {
                    var t = "linear-gradient(" + this.attrs.gradient_direction + "deg, " + this.attrs.gradient_start_color + " " + this.attrs.gradient_start + "%, " + this.attrs.gradient_end_color + " " + this.attrs.gradient_end + "%)";
                    e(this.dom_element).css("background-image") ? e(this.dom_element).css("background-image", t + "," + e(this.dom_element).css("background-image")) : e(this.dom_element).css("background-image", t)
                }
                this.attrs.vertical_centering && "yes" === this.attrs.vertical_centering && e(this.dom_element).addClass("az-util-vertical-centering"), F.baseclass.prototype.render.apply(this, arguments)
            }
        }), M("az_row", !0, H), w(H.prototype, {
            name: Drupal.t("Row"),
            icon: "et et-icon-grid",
            category: Drupal.t("Layout"),
            params: [B({
                type: "dropdown",
                heading: Drupal.t("Device breakpoint"),
                param_name: "device",
                value: {
                    xs: Drupal.t("Extra small devices Phones (<768px)"),
                    sm: Drupal.t("Small devices Tablets (≥768px)"),
                    md: Drupal.t("Medium devices Desktops (≥992px)"),
                    lg: Drupal.t("Large devices Desktops (≥1200px)")
                },
                description: Drupal.t("Bootstrap responsive grid breakpoints")
            }), B({
                type: "checkbox",
                heading: Drupal.t("Equal Height Columns"),
                param_name: "equal",
                value: {
                    yes: Drupal.t("Yes")
                }
            })].concat(H.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    H.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove();
                    for (var e = this, t = this.controls, a = '<div class="row-layouts clearfix">', n = ["1/1", "1/2 + 1/2", "1/3 + 1/3 + 1/3", "1/4 + 1/4 + 1/4 + 1/4", "1/6 + 1/6 + 1/6 + 1/6 + 1/6 + 1/6", "1/4 + 1/2 + 1/4", "1/6 + 4/6 + 1/6", "1/4 + 3/4", "3/4 + 1/4", "2/3 + 1/3", "1/3 + 2/3", "5/12 + 7/12"], i = "", o = 0; o < n.length; o++) {
                        a += '<div title="' + d("Set " + n[o] + " colums") + '" " role="' + n[o] + '" class="az-mini-container control set-columns-layout"><div class="row">', i = n[o].replace(" ", "").split("+");
                        for (var s = 0; s < i.length; s++) a += '<div class="' + c(i[s], "xs") + '"><div class="az-col-solid"></div></div>';
                        a += "</div>", a += "</div>"
                    }
                    a += '<small class="az-row-custom set-columns-layout"><a href="#" class="custom-row-control glazed-util-text-muted text-small">Custom layout</a></small>', a += "</div>";
                    var l = y('<span title="' + d("Set row layout") + '" class="control set-columns btn btn-default glyphicon glyphicon-th"> </span>').insertAfter(this.controls.find(".drag-and-drop")).popover({
                        container: !1,
                        selector: !1,
                        placement: "right",
                        html: "true",
                        trigger: "manual",
                        content: a
                    }).click(function() {
                        y(l).popover("toggle"), u(y(t)), u(y(t).find(".popover")), y(t).find(".popover .set-columns-layout").each(function() {
                            y(this).click({
                                object: e
                            }, e.click_set_columns)
                        }), e.controls.find(".popover").mouseleave(function() {
                            y(l).popover("hide"), y(l).css("display", "")
                        })
                    })
                }
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && y(this.dom_element).sortable({
                    axis: "x",
                    items: "> .az-column",
                    handle: "> .controls > .drag-and-drop",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    tolerance: "pointer",
                    distance: 1,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            update_sorting: function(e, t) {
                H.baseclass.prototype.update_sorting.apply(this, arguments);
                var a = b.get_element(y(this).closest("[data-az-id]").attr("data-az-id"));
                if (a)
                    for (var n = 0; n < a.children.length; n++) a.children[n].update_empty()
            },
            update_dom: function() {
                H.baseclass.prototype.update_dom.apply(this, arguments);
                for (var e = 0; e < this.children.length; e++) this.children[e].update_dom()
            },
            click_set_columns: function(e) {
                var t = y(this).attr("role");
                if ("" == t || null == t) {
                    if ("" == e.data.object.columns) {
                        t = [];
                        for (var a = 0; a < e.data.object.children.length; a++) t.push(e.data.object.children[a].attrs.width);
                        e.data.object.columns = t.join(" + ")
                    }
                    t = window.prompt(Drupal.t("Enter bootstrap grid layout. For example 1/2 + 1/2."), e.data.object.columns)
                }
                return "" != t && null != t && e.data.object.set_columns(t), !1
            },
            set_columns: function(e) {
                var t = (this.columns = e).replace(" ", "").split("+");
                if (0 == this.children.length)
                    for (var a = 0; a < t.length; a++) {
                        (l = new G(this, !0)).update_dom(), l.update_width(t[a])
                    } else if (this.children.length == t.length)
                        for (a = 0; a < t.length; a++) this.children[a].update_width(t[a]);
                    else if (this.children.length > t.length) {
                    var n = this.children[t.length - 1];
                    for (a = 0; a < this.children.length; a++)
                        if (a < t.length) this.children[a].update_width(t[a]);
                        else {
                            for (var i = this.children[a], o = 0; o < i.children.length; o++)(i.children[o].parent = n).children.push(i.children[o]);
                            i.children = []
                        }
                    n.update_dom();
                    var s = this.children.slice(t.length, this.children.length);
                    for (a = 0; a < s.length; a++) s[a].remove()
                } else
                    for (a = 0; a < t.length; a++) {
                        var l;
                        if (a < this.children.length) this.children[a].update_width(t[a]);
                        else(l = new G(this, !0)).update_dom(), l.update_width(t[a])
                    }
                this.update_sortable()
            },
            showed: function(e) {
                H.baseclass.prototype.showed.apply(this, arguments)
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-row row ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, e(this.dom_element).addClass("az-row--" + this.attrs.device), this.attrs.equal && "yes" === this.attrs.equal && e(this.dom_element).addClass("az-row--equal-height"), H.baseclass.prototype.render.apply(this, arguments)
            }
        }), N("az_column", !0, G), w(G.prototype, {
            name: Drupal.t("Column"),
            params: [B({
                type: "textfield",
                heading: Drupal.t("Column with"),
                param_name: "width",
                hidden: !0
            }), B({
                type: "checkbox",
                heading: Drupal.t("Vertical Centering"),
                param_name: "vertical_centering",
                value: {
                    yes: Drupal.t("Yes")
                }
            })].concat(G.prototype.params),
            hidden: !0,
            is_container: !0,
            controls_base_position: "top-left",
            show_parent_controls: !0,
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (G.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".clone").remove(), this.controls.find(".copy").remove(), this.controls.find(".remove").remove())
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            update_width: function(e) {
                y(this.dom_element).removeClass(c(this.attrs.width, this.parent.attrs.device)), this.attrs.width = e, y(this.dom_element).addClass(c(this.attrs.width, this.parent.attrs.device)), y(document).trigger("glazed_update_element", this.id)
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-ctnr az-column ' + this.get_el_classes() + " " + this.get_content_classes() + " " + c(this.attrs.width, this.parent.attrs.device) + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, this.attrs.vertical_centering && "yes" === this.attrs.vertical_centering && e(this.dom_element).addClass("az-util-vertical-centering"), G.baseclass.prototype.render.apply(this, arguments)
            }
        }), M("az_tabs", !0, W), w(W.prototype, {
            name: Drupal.t("Tabs"),
            icon: "pe pe-7s-folder",
            category: Drupal.t("Layout"),
            params: [B({
                type: "dropdown",
                heading: Drupal.t("Tab direction"),
                param_name: "az_dirrection",
                value: {
                    "": Drupal.t("Default"),
                    "tabs-left": Drupal.t("Left"),
                    "tabs-right": Drupal.t("Right")
                }
            })].concat(W.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (W.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove(), y('<span title="' + d("Add tab") + '" class="control add-tab btn btn-default glyphicon glyphicon-plus-sign" > </span>').appendTo(this.controls).click({
                    object: this
                }, this.click_add_tab))
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && y(this.dom_element).sortable({
                    items: "> ul > li",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            update_sorting: function(e, t) {
                var a = b.get_element(y(this).attr("data-az-id"));
                if (a) {
                    var n = y(this).sortable("option"),
                        i = [];
                    y(this).find(n.items).each(function() {
                        var e = y(this).find('a[data-toggle="tab"]').attr("element-id");
                        i.push(b.get_element(e))
                    }), a.children = i;
                    for (var o = 0; o < a.children.length; o++) a.children[o].parent = a;
                    a.update_dom(), y(document).trigger("glazed_update_sorting", t)
                }
            },
            click_add_tab: function(e) {
                return e.data.object.add_tab(), !1
            },
            add_tab: function() {
                var e = new $(this, !1);
                e.update_dom(), this.update_dom(), y(this.dom_element).find('a[href="#' + e.id + '"]').tab("show")
            },
            showed: function(e) {
                W.baseclass.prototype.showed.apply(this, arguments), e(this.dom_element).find("ul.nav-tabs li:first a").tab("show");
                var t = window.location.hash;
                t && e('ul.nav a[href="' + t + '"]').tab("show");
                var a = 0 < e("#toolbar-bar").length ? e("#toolbar-bar").height() : 0,
                    n = 0 < e(".toolbar-tray.toolbar-tray-horizontal.is-active").length ? e("#toolbar-item-administration-tray").height() : 0,
                    i = 0 < e(".glazed-header--sticky, .glazed-header--fixed").length ? e("#navbar").height() : 0;
                if (t && e('ul.nav a[href="' + t + '"]').attr("element-id")) var o = e('ul.nav a[href="' + t + '"]').closest(".az-tabs").parent();
                o && window.innerWidth < 481 ? e("html, body").animate({
                    scrollTop: o.offset().top - 100
                }) : o && e("html, body").animate({
                    scrollTop: o.offset().top - (a + i + n + 30)
                }, 300)
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-tabs tabbable ' + this.get_el_classes() + " " + this.get_content_classes() + this.attrs.az_dirrection + '" style="' + this.attrs.style + '"></div>');
                for (var t = '<ul class="nav nav-tabs" role="tablist">', a = 0; a < this.children.length; a++) {
                    t += '<li><a href="#' + (this.children[a].attrs.hash ? this.children[a].attrs.hash : this.children[a].id) + '" role="tab" data-toggle="tab" element-id="' + this.children[a].id + '">' + this.children[a].attrs.title + "</a></li>"
                }
                t += "</ul>", e(this.dom_element).append(t);
                var n = '<div id="' + this.id + '" class="tab-content"></div>';
                this.dom_content_element = e(n).appendTo(this.dom_element), W.baseclass.prototype.render.apply(this, arguments)
            }
        }), N("az_tab", !0, $), w($.prototype, {
            name: Drupal.t("Tab"),
            params: [B({
                type: "textfield",
                heading: Drupal.t("Tab title"),
                param_name: "title",
                value: Drupal.t("Title")
            }), B({
                type: "textfield",
                heading: Drupal.t("Tab URL hash"),
                param_name: "hash",
                value: ""
            })].concat($.prototype.params),
            hidden: !0,
            is_container: !0,
            controls_base_position: "top-left",
            get_empty: function() {
                return '<div class="az-empty"><div class="top-left well"><h1>↖</h1><span class="glyphicon glyphicon-plus-sign"></span>' + Drupal.t(" add a new tab.") + " " + Drupal.t("Drag tab headers to change order.") + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && ($.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".drag-and-drop").remove(), y('<span class="control btn btn-default glyphicon">' + this.name + "</span>").prependTo(this.controls))
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            edited: function(e) {
                $.baseclass.prototype.edited.apply(this, arguments), this.parent.update_dom(), this.attrs.hash ? elementId = this.attrs.hash : elementId = this.id, y('a[href="#' + elementId + '"]').tab("show")
            },
            clone: function() {
                var e = $.baseclass.prototype.get_my_shortcode.apply(this, arguments);
                y("#glazed-clipboard").html(encodeURIComponent(e)), this.parent.paste(this.parent.children.length), this.parent.update_dom()
            },
            remove: function() {
                $.baseclass.prototype.remove.apply(this, arguments), this.parent.update_dom()
            },
            render: function(e) {
                var t = "";
                t = this.attrs.hash ? this.attrs.hash : this.id, this.dom_element = e('<div id="' + t + '" class="az-element az-ctnr az-tab tab-pane ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, $.baseclass.prototype.render.apply(this, arguments)
            }
        }), M("az_accordion", !0, J), w(J.prototype, {
            name: Drupal.t("Collapsibles"),
            icon: "pe pe-7s-menu",
            category: Drupal.t("Layout"),
            params: [B({
                type: "checkbox",
                heading: Drupal.t("Collapsed?"),
                param_name: "collapsed",
                value: {
                    yes: Drupal.t("Yes")
                }
            })].concat(J.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (J.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove(), y('<span title="' + d("Add toggle") + '" class="control add-toggle btn btn-default glyphicon glyphicon-plus-sign" > </span>').appendTo(this.controls).click({
                    object: this
                }, this.click_add_toggle))
            },
            update_sortable: function() {
                drupalSettings.glazedBuilder.glazedEditor && y(this.dom_element).sortable({
                    axis: "y",
                    items: "> .az-toggle",
                    handle: "> .controls > .drag-and-drop",
                    update: this.update_sorting,
                    placeholder: "az-sortable-placeholder",
                    forcePlaceholderSize: !0,
                    over: function(e, t) {
                        t.placeholder.attr("class", t.helper.attr("class")), t.placeholder.removeClass("ui-sortable-helper"), t.placeholder.addClass("az-sortable-placeholder")
                    }
                })
            },
            click_add_toggle: function(e) {
                return e.data.object.add_toggle(), !1
            },
            add_toggle: function() {
                new X(this, !1).update_dom(), this.update_dom()
            },
            update_dom: function() {
                for (var e = 0; e < this.children.length; e++) this.children[e].update_dom();
                J.baseclass.prototype.update_dom.apply(this, arguments)
            },
            showed: function(e) {
                J.baseclass.prototype.showed.apply(this, arguments), e(this.dom_element).find("> .az-toggle > .in").removeClass("in"), e(this.dom_element).find("> .az-toggle > .collapse:not(:first)").collapse({
                    toggle: !1,
                    parent: "#" + this.id
                }), e(this.dom_element).find("> .az-toggle > .collapse:first").collapse({
                    toggle: "yes" != this.attrs.collapsed,
                    parent: "#" + this.id
                })
            },
            render: function(e) {
                this.dom_element = e('<div id="' + this.id + '" class="az-element az-accordion panel-group ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, J.baseclass.prototype.render.apply(this, arguments)
            }
        }), N("az_toggle", !0, X), w(X.prototype, {
            name: Drupal.t("Toggle"),
            params: [B({
                type: "textfield",
                heading: Drupal.t("Toggle title"),
                param_name: "title",
                value: Drupal.t("Title")
            })].concat(X.prototype.params),
            hidden: !0,
            is_container: !0,
            controls_base_position: "top-left",
            get_empty: function() {
                return '<div class="az-empty"><div class="top-left well"><h1>↖</h1><span class="glyphicon glyphicon-plus-sign"></span>' + Drupal.t(" add a new toggle.") + "</div></div>"
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            clone: function() {
                var e = X.baseclass.prototype.get_my_shortcode.apply(this, arguments);
                y("#glazed-clipboard").html(encodeURIComponent(e)), this.parent.paste(this.parent.children.length)
            },
            render: function(e) {
                var t = "panel-default";
                "" != this.parent.attrs.type && (t = this.parent.attrs.type), this.dom_element = e('<div class="az-element az-toggle panel ' + t + " " + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"><div class="panel-heading"><h4 class="panel-title"><a data-toggle="collapse" data-parent="#' + this.parent.id + '" href="#' + this.id + '">' + this.attrs.title + '</a></h4></div><div id="' + this.id + '" class="panel-collapse collapse"><div class="panel-body az-ctnr"></div></div></div>'), this.dom_content_element = e(this.dom_element).find(".panel-body"), X.baseclass.prototype.render.apply(this, arguments)
            }
        }), M("az_carousel", !0, Q), w(Q.prototype, {
            name: Drupal.t("Carousel"),
            icon: "pe pe-7s-more",
            category: Drupal.t("Layout"),
            params: [B({
                type: "bootstrap_slider",
                heading: Drupal.t("Frames per slide"),
                param_name: "items",
                min: "1",
                max: "10",
                value: "1"
            }), B({
                type: "checkbox",
                heading: Drupal.t("Disable Auto Play"),
                param_name: "autoplay",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Dots Navigation"),
                param_name: "pagination",
                tab: "Dots",
                value: {
                    on: Drupal.t("On")
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Dots Orientation"),
                param_name: "pagination_orientation",
                tab: "Dots",
                value: {
                    outside: Drupal.t("Outside Slider"),
                    inside: Drupal.t("Inside Slider")
                },
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Shape"),
                param_name: "pagination_shape",
                tab: "Dots",
                value: {
                    circle: Drupal.t("Circle"),
                    square: Drupal.t("Square"),
                    triangle: Drupal.t("Triangle"),
                    bar: Drupal.t("Bar")
                },
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Transform Active"),
                param_name: "pagination_transform",
                tab: "Dots",
                value: {
                    "": Drupal.t("None"),
                    growTaller: Drupal.t("Grow Taller"),
                    growWider: Drupal.t("Grow Wider"),
                    scaleUp: Drupal.t("Scale up")
                },
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Dots Color"),
                param_name: "pagination_color",
                tab: "Dots",
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Active Dot Color"),
                param_name: "pagination_active_color",
                tab: "Dots",
                dependency: {
                    element: "pagination",
                    not_empty: {}
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Next/Previous"),
                param_name: "navigation",
                tab: "Next/Previous",
                value: {
                    on: Drupal.t("On")
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Orientation"),
                param_name: "navigation_orientation",
                tab: "Next/Previous",
                value: {
                    outside: Drupal.t("Outside Slider"),
                    inside: Drupal.t("Inside Slider")
                },
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Shape"),
                param_name: "navigation_shape",
                tab: "Next/Previous",
                value: {
                    "": Drupal.t("No Background"),
                    circle: Drupal.t("Circle"),
                    square: Drupal.t("Square"),
                    bar: Drupal.t("Bar")
                },
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Icon Color"),
                param_name: "navigation_icon_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Icon Hover Color"),
                param_name: "navigation_icon_hover_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Background Color"),
                param_name: "navigation_background_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "colorpicker",
                heading: Drupal.t("Background Hover"),
                param_name: "navigation_background_hover_color",
                tab: "Next/Previous",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Icon Thickness"),
                param_name: "navigation_thickness",
                tab: "Next/Previous",
                min: "1",
                max: "10",
                value: "2",
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "dropdown",
                heading: Drupal.t("Outside Navigation Position"),
                param_name: "navigation_position",
                tab: "Next/Previous",
                value: {
                    adjacent: Drupal.t("Adjacent"),
                    bottomCenter: Drupal.t("Bottom Center"),
                    topLeft: Drupal.t("Top Left"),
                    topRight: Drupal.t("Top Right"),
                    bottomCenter: Drupal.t("Bottom Center"),
                    bottomLeft: Drupal.t("Bottom Left"),
                    bottomRight: Drupal.t("Bottom Right")
                },
                dependency: {
                    element: "navigation",
                    not_empty: {}
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Auto Play Interval"),
                param_name: "interval",
                min: "1000",
                max: "20000",
                value: "5000",
                step: "100"
            }), B({
                type: "dropdown",
                heading: Drupal.t("Transition style"),
                param_name: "transition",
                value: {
                    "": Drupal.t("Default"),
                    fade: Drupal.t("fade"),
                    backSlide: Drupal.t("backSlide"),
                    goDown: Drupal.t("goDown"),
                    fadeUp: Drupal.t("fadeUp")
                }
            }), B({
                type: "checkbox",
                heading: Drupal.t("Stop on Hover"),
                param_name: "stoponhover",
                value: {
                    on: Drupal.t("On")
                }
            }), B({
                type: "checkbox",
                hidden: !0,
                heading: Drupal.t("Options"),
                param_name: "options",
                value: {
                    navigation: Drupal.t("Navigation"),
                    auto_play: Drupal.t("Auto play"),
                    mouse: Drupal.t("Mouse drag"),
                    touch: Drupal.t("Touch drag")
                }
            })].concat(Q.prototype.params),
            is_container: !0,
            controls_base_position: "top-left",
            show_settings_on_create: !0,
            frontend_render: !0,
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    Q.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".add").remove(), this.controls.find(".paste").remove();
                    y('<span title="' + d("Add slide") + '" class="control add-toggle btn btn-default glyphicon glyphicon-plus-sign" > </span>').appendTo(this.controls).click({
                        object: this
                    }, this.click_add_slide)
                }
            },
            update_sortable: function() {},
            click_add_slide: function(e) {
                return e.data.object.add_slide(), !1
            },
            add_slide: function() {
                new Z(this, !0).update_dom(), this.update_dom()
            },
            showed: function(l) {
                Q.baseclass.prototype.showed.apply(this, arguments);
                this.add_css("vendor/owl.carousel/owl-carousel/owl.carousel.css", "owlCarousel" in l.fn, function() {}), this.add_css("css/st-owl-carousel.css", "owlCarousel" in l.fn, function() {}), this.add_css("vendor/owl.carousel/owl-carousel/owl.transitions.css", "owlCarousel" in l.fn, function() {});
                var o = this;
                this.add_js({
                    path: "vendor/owl.carousel/owl-carousel/owl.carousel.js",
                    loaded: "owlCarousel" in l.fn,
                    callback: function() {
                        var e = function(e) {
                                var t = null;
                                t = "userItems" in e ? e.userItems : e.$userItems;
                                var a = null;
                                a = "visibleItems" in e ? e.visibleItems : e.$visibleItems;
                                for (var n = 0; n < t.length; n++)
                                    if (_.indexOf(a, n) < 0) {
                                        var i = t[n],
                                            o = l(i).attr("data-az-id"),
                                            s = b.get_element(o);
                                        _.isUndefined(s) || "trigger_start_out_animation" in s && s.trigger_start_out_animation()
                                    }
                                for (n = 0; n < a.length; n++)
                                    if (a[n] < t.length) {
                                        i = t[a[n]], o = l(i).attr("data-az-id"), s = b.get_element(o);
                                        _.isUndefined(s) || "trigger_start_in_animation" in s && s.trigger_start_in_animation()
                                    }
                            },
                            t = "st-owl-theme";
                        o.attrs.pagination && (o.attrs.pagination_orientation && (t += " st-owl-pager-" + o.attrs.pagination_orientation), o.attrs.pagination_shape && (t += " st-owl-pager-" + o.attrs.pagination_shape), o.attrs.pagination_transform && (t += " st-owl-pager-" + o.attrs.pagination_transform)), o.attrs.navigation && (o.attrs.navigation_orientation && (t += " st-owl-navigation-" + o.attrs.navigation_orientation), o.attrs.navigation_shape && (t += " st-owl-navigation-" + o.attrs.navigation_shape), o.attrs.navigation_position && (t += " st-owl-navigation-" + o.attrs.navigation_position));
                        var a, n, i = !1;
                        !Boolean(o.attrs.autoplay) && 0 < o.attrs.interval && (i = o.attrs.interval), l(o.dom_content_element).owlCarousel({
                            addClassActive: !0,
                            afterAction: function() {
                                e(this.owl)
                            },
                            afterMove: function() {},
                            autoPlay: i,
                            beforeMove: function() {},
                            items: o.attrs.items,
                            mouseDrag: !0,
                            navigation: Boolean(o.attrs.navigation),
                            navigationText: !1,
                            pagination: Boolean(o.attrs.pagination),
                            singleItem: "1" == o.attrs.items,
                            startDragging: function() {},
                            stopOnHover: Boolean(o.attrs.stoponhover),
                            theme: t,
                            touchDrag: !0,
                            transitionStyle: "" != o.attrs.transition && o.attrs.transition
                        }), e(o.dom_content_element.data("owlCarousel")), "outside" != o.attrs.navigation_orientation || "topLeft" != o.attrs.navigation_position && "topRight" != o.attrs.navigation_position && "topCenter" != o.attrs.navigation_position || l(o.dom_content_element).find(".owl-buttons").prependTo(l(o.dom_content_element)), l("head").find("#carousel-style-" + o.id).remove(), l("head").append((n = "[data-az-id=" + (a = o).id + "] .st-owl-theme", output = '<style id="carousel-style-' + a.id + '">\x3c!-- ', "triangle" == a.attrs.pagination_shape ? (output += n + " .owl-page {background:transparent !important; border-bottom-color: " + a.attrs.pagination_color + " !important}", output += n + " .owl-page.active {background:transparent !important; border-bottom-color: " + a.attrs.pagination_active_color + " !important}") : (output += n + " .owl-page {background: " + a.attrs.pagination_color + " !important}", output += n + " .owl-page.active {background: " + a.attrs.pagination_active_color + " !important}"), output += n + " .owl-buttons .owl-prev::after, " + n + " .owl-buttons .owl-next::after," + n + " .owl-buttons .owl-prev::before, " + n + " .owl-buttons .owl-next::before {background: " + a.attrs.navigation_icon_color + ";width: " + a.attrs.navigation_thickness + "px;}", output += n + " .owl-buttons .owl-prev:hover::after, " + n + " .owl-buttons .owl-next:hover::after," + n + " .owl-buttons .owl-prev:hover::before, " + n + " .owl-buttons .owl-next:hover::before {background: " + a.attrs.navigation_icon_hover_color + "}", output += n + " .owl-buttons .owl-prev, " + n + " .owl-buttons .owl-next {background: " + a.attrs.navigation_background_color + ";border-color: " + a.attrs.navigation_background_color + " }", output += n + " .owl-buttons .owl-prev:hover, " + n + " .owl-buttons .owl-next:hover {background: " + a.attrs.navigation_background_hover_color + ";border-color: " + a.attrs.navigation_background_hover_color + " }", output += " --\x3e</style>", output))
                    }
                })
            },
            render: function(e) {
                this.dom_element = e('<div id="' + this.id + '" class="az-element az-carousel ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '" data-glazed-builder-libraries="owlcarousel"></div>'), this.dom_content_element = e("<div></div>").appendTo(this.dom_element), Q.baseclass.prototype.render.apply(this, arguments)
            }
        }), N("az_slide", !0, Z), w(Z.prototype, {
            name: Drupal.t("Slide"),
            params: [].concat(Z.prototype.params),
            hidden: !0,
            is_container: !0,
            show_parent_controls: !0,
            controls_base_position: "top-left",
            get_empty: function() {
                return '<div class="az-empty"><div class="top-left well"><h1>↖</h1><span class="glyphicon glyphicon-plus-sign"></span>' + Drupal.t(" add a new slide.") + "</div></div>"
            },
            show_controls: function() {
                drupalSettings.glazedBuilder.glazedEditor && (Z.baseclass.prototype.show_controls.apply(this, arguments), this.controls.find(".clone").remove(), this.controls.find(".drag-and-drop").remove(), y('<span class="control btn btn-default glyphicon">' + this.name + "</span>").prependTo(this.controls))
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            edited: function() {
                Z.baseclass.prototype.edited.apply(this, arguments), this.parent.update_dom()
            },
            render: function(e) {
                "" != this.parent.attrs.type && this.parent.attrs.type, this.dom_element = e('<div class="az-element az-slide az-ctnr ' + this.get_el_classes() + " " + this.get_content_classes() + ' clearfix" style="' + this.attrs.style + '"></div>'), this.dom_content_element = this.dom_element, Z.baseclass.prototype.render.apply(this, arguments)
            }
        }), M("az_layers", !0, ee), w(ee.prototype, {
            name: Drupal.t("Positioned Layers"),
            icon: "et et-icon-layers",
            description: Drupal.t("Free Positioning"),
            category: Drupal.t("Layout"),
            params: [B({
                type: "textfield",
                heading: Drupal.t("Width"),
                param_name: "width",
                description: Drupal.t("For example 100px, or 50%."),
                value: "100%"
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Height"),
                param_name: "height",
                max: "10000",
                value: "500"
            }), B({
                type: "checkbox",
                heading: Drupal.t("Responsive?"),
                param_name: "responsive",
                value: {
                    yes: Drupal.t("Yes")
                }
            }), B({
                type: "bootstrap_slider",
                heading: Drupal.t("Original width"),
                param_name: "o_width",
                hidden: !0
            })].concat(ee.prototype.params),
            show_settings_on_create: !0,
            is_container: !0,
            controls_base_position: "top-left",
            disallowed_elements: ["az_layers"],
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            zindex_normalize: function() {
                for (var e = [], t = 0; t < this.children.length; t++) isNaN(parseInt(this.children[t].attrs.pos_zindex)) && (this.children[t].attrs.pos_zindex = 0), e.push(parseInt(this.children[t].attrs.pos_zindex));
                e = _.sortBy(e, function(e) {
                    return e
                }), e = _.uniq(e);
                for (t = 0; t < this.children.length; t++) {
                    var a = _.sortedIndex(e, parseInt(this.children[t].attrs.pos_zindex));
                    y(this.children[t].dom_element).css("z-index", a), this.children[t].attrs.pos_zindex = a
                }
            },
            update_sortable: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    var n = this;

                    function a(e) {
                        var t = y(e).closest("[data-az-id]").attr("data-az-id"),
                            a = b.get_element(t);
                        a.attrs.pos_left = parseInt(y(e).css("left")) / (y(n.dom_content_element).width() / 100) + "%", a.attrs.pos_top = parseInt(y(e).css("top")) / (y(n.dom_content_element).height() / 100) + "%", a.attrs.pos_width = parseInt(y(e).css("width")) / (y(n.dom_content_element).width() / 100) + "%", a.attrs.pos_height = parseInt(y(e).css("height")) / (y(n.dom_content_element).height() / 100) + "%", i(e), n.attrs.o_width = y(n.dom_element).width(), y(document).trigger("glazed_update_element", t)
                    }

                    function i(e) {
                        y(e).css("left", parseInt(y(e).css("left")) / (y(n.dom_content_element).width() / 100) + "%"), y(e).css("top", parseInt(y(e).css("top")) / (y(n.dom_content_element).height() / 100) + "%"), y(e).css("width", parseInt(y(e).css("width")) / (y(n.dom_content_element).width() / 100) + "%"), y(e).css("height", parseInt(y(e).css("height")) / (y(n.dom_content_element).height() / 100) + "%")
                    }
                    n.zindex_normalize(), y(this.dom_content_element).resizable({
                        start: function(e, t) {
                            for (var a = 0; a < n.children.length; a++) {
                                i(n.children[a].dom_element)
                            }
                        },
                        stop: function(e, t) {
                            n.attrs.width = parseInt(y(n.dom_content_element).css("width")) / (y(n.dom_element).width() / 100) + "%", y(n.dom_content_element).width(n.attrs.width), n.attrs.height = y(n.dom_content_element).height(), y(document).trigger("glazed_update_element", n.id)
                        }
                    });
                    for (var e = 0; e < this.children.length; e++) y.isNumeric(y(this.children[e].dom_element).css("z-index")) || y(this.children[e].dom_element).css("z-index", 0), null == this.children[e].controls && this.children[e].show_controls(), null == this.children[e].attrs.pos_top && (this.children[e].attrs.pos_top = "50%"), null == this.children[e].attrs.pos_left && (this.children[e].attrs.pos_left = "50%"), null == this.children[e].attrs.pos_width && (this.children[e].attrs.pos_width = "50%"), null == this.children[e].attrs.pos_height && (this.children[e].attrs.pos_height = "50%"), 0 == this.children[e].controls.find(".width100").length && y('<span title="' + d("100% width") + '" class="control width100 btn btn-default glyphicon glyphicon-resize-horizontal" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return e.data.object.attrs.pos_left = "0%", y(e.data.object.dom_element).css("left", "0%"), e.data.object.attrs.pos_width = "100%", y(e.data.object.dom_element).css("width", "100%"), !1
                    }), 0 == this.children[e].controls.find(".heigth100").length && y('<span title="' + d("100% heigth") + '" class="control heigth100 btn btn-default glyphicon glyphicon-resize-vertical" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return e.data.object.attrs.pos_top = "0%", y(e.data.object.dom_element).css("top", "0%"), e.data.object.attrs.pos_height = "100%", y(e.data.object.dom_element).css("height", "100%"), !1
                    }), 0 == this.children[e].controls.find(".forward").length && y('<span title="' + d("Bring forward") + '" class="control forward btn btn-default glyphicon glyphicon-arrow-up" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return y.isNumeric(y(e.data.object.dom_element).css("z-index")) ? (y(e.data.object.dom_element).css("z-index", Math.round(y(e.data.object.dom_element).css("z-index")) + 1), e.data.object.attrs.pos_zindex = y(e.data.object.dom_element).css("z-index")) : (y(e.data.object.dom_element).css("z-index", 0), e.data.object.attrs.pos_zindex = 0), n.zindex_normalize(), !1
                    }), 0 == this.children[e].controls.find(".backward").length && y('<span title="' + d("Send backward") + '" class="control backward btn btn-default glyphicon glyphicon-arrow-down" > </span>').appendTo(this.children[e].controls).click({
                        object: this.children[e]
                    }, function(e) {
                        return y.isNumeric(y(e.data.object.dom_element).css("z-index")) ? 0 < Math.round(y(e.data.object.dom_element).css("z-index")) && (y(e.data.object.dom_element).css("z-index", Math.round(y(e.data.object.dom_element).css("z-index")) - 1), e.data.object.attrs.pos_zindex = y(e.data.object.dom_element).css("z-index")) : (y(e.data.object.dom_element).css("z-index", 0), e.data.object.attrs.pos_zindex = 0), n.zindex_normalize(), !1
                    }), y(this.children[e].dom_element).draggable({
                        handle: "> .controls > .drag-and-drop",
                        containment: "#" + this.id,
                        scroll: !1,
                        snap: "#" + this.id + ", .az-element",
                        stop: function(e, t) {
                            a(this)
                        }
                    }), y(this.children[e].dom_element).resizable({
                        containment: "#" + this.id,
                        stop: function(e, t) {
                            a(this)
                        }
                    })
                }
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    ee.baseclass.prototype.show_controls.apply(this, arguments), this.update_sortable();
                    var e = this;
                    y(this.dom_content_element).dblclick(function(t) {
                        1 == t.which && b.show(e, function(e) {
                            e.attrs.pos_top = t.offsetY.toString() + "px", e.attrs.pos_left = t.offsetX.toString() + "px"
                        })
                    })
                }
            },
            attach_children: function() {
                ee.baseclass.prototype.attach_children.apply(this, arguments), drupalSettings.glazedBuilder.glazedEditor && this.update_sortable()
            },
            showed: function(r) {
                ee.baseclass.prototype.showed.apply(this, arguments);
                var d = this;
                if (r(window).off("resize.az_layers" + d.id), "yes" == this.attrs.responsive) {
                    r(window).on("resize.az_layers" + d.id, function() {
                        var e = r(d.dom_element).width();
                        "o_width" in d.attrs && "" != d.attrs.o_width || (d.attrs.o_width = e);
                        var t = e / d.attrs.o_width;
                        r(d.dom_element).css("font-size", 100 * t + "%"), r(d.dom_content_element).css("height", d.attrs.height * t + "px"),
                            function e(t, a) {
                                var n, i, o, s = (n = "style", i = "", null != (o = t.attrs[n].match(/font-size[: ]*([\-\d\.]*)(px|%|em) *;/)) && (i = o[1]), i);
                                "" != s && (s *= a, r(t.dom_element).css("font-size", s + "px"));
                                for (var l = 0; l < t.children.length; l++) e(d.children[l], a)
                            }(d, t)
                    }), r(window).trigger("resize")
                }
            },
            render: function(e) {
                this.dom_element = e('<div class="az-element az-layers ' + this.get_el_classes() + " " + this.get_content_classes() + '" style="' + this.attrs.style + '"><div id="' + this.id + '" class="az-ctnr"></div></div>'), this.dom_content_element = e(this.dom_element).find(".az-ctnr"), e(this.dom_content_element).css("width", this.attrs.width), e(this.dom_content_element).css("height", this.attrs.height), ee.baseclass.prototype.render.apply(this, arguments)
            }
        }), M("az_container", !0, te), w(te.prototype, {
            name: Drupal.t("Glazed Container"),
            icon: "et et-icon-download",
            description: Drupal.t("AJAX Load Fields"),
            category: Drupal.t("Layout"),
            params: [B({
                type: "container",
                heading: Drupal.t("Glazed Container"),
                param_name: "container",
                description: Drupal.t("Type and name used as identificator to save container on server."),
                value: "/"
            })].concat(te.prototype.params),
            show_settings_on_create: !0,
            is_container: !0,
            hidden: !window.glazed_online,
            controls_base_position: "top-left",
            saveable: !0,
            get_button: function() {
                return '<div class="well text-center text-overflow" data-az-element="' + this.base + '"><i class="' + this.icon + '"></i><div>' + this.name + '</div><div class="text-muted small">' + this.description + "</div></div>"
            },
            get_empty: function() {
                var e = '<div class="az-empty"><div class="top well"><strong>  <h3 class="glazed-choose-layout">Choose a template</h3></strong><p class="lead">' + Drupal.t("Or add elements from the + button or sidebar.</p></div>") + '<div class="top-right well"><h1>↗</h1> <span class="glyphicon glyphicon-save"></span>' + Drupal.t(" Don't forget to save your work") + "</div></div>",
                    t = y(e);
                return t.find(".glazed-choose-layout").bind("click", function(e) {
                    if (e.stopPropagation(), 1 == e.which) {
                        var t = y(this).closest("[data-az-id]").attr("data-az-id");
                        b.showTemplates(b.get_element(t), function(e) {})
                    }
                }), t
            },
            show_controls: function() {
                if (drupalSettings.glazedBuilder.glazedEditor) {
                    var a = this;
                    '<li><a href="https://www.youtube.com/watch?v=lODF-8byKRA" target="_blank">Basic Controls Video</a></li>', '<li><a href="http://www.sooperthemes.com/documentation" target="_blank">Documentation</a></li>', '<li><a href="http://www.sooperthemes.com/dashboard/tickets" target="_blank">Support Forum</a></li>', "</ul>", te.baseclass.prototype.show_controls.apply(this, arguments), null == this.parent && (y('<span title="' + d("Toggle editor") + '" class="control toggle-editor btn btn-default glyphicon glyphicon-eye-open" > </span>').appendTo(this.controls).click(function() {
                        return y(a.dom_element).toggleClass("glazed-editor"), !1
                    }), y('<span role="button" tabindex="0" title="' + d("Documentation and Support") + '" class="control glazed-help btn btn-default glyphicon glyphicon-question-sign glazed-builder-popover"> </span>').appendTo(this.controls).popover({
                        html: !0,
                        placement: "left",
                        content: function() {
                            return '<ul class="nav"><li><a href="https://www.youtube.com/watch?v=lODF-8byKRA" target="_blank">Basic Controls Video</a></li><li><a href="http://www.sooperthemes.com/documentation" target="_blank">Documentation</a></li><li><a href="http://www.sooperthemes.com/dashboard/tickets" target="_blank">Support Forum</a></li></ul>'
                        },
                        title: function() {
                            return "Support and Documentation"
                        }
                    }), this.controls.removeClass("btn-group-xs"), this.controls.find(".edit").remove(), this.controls.find(".copy").remove(), this.controls.find(".clone").remove(), this.controls.find(".remove").remove(), this.controls.find(".js-animation").remove(), this.controls.find(".drag-and-drop").attr("title", ""), this.controls.find(".drag-and-drop").removeClass("glyphicon-move"), this.controls.find(".drag-and-drop").removeClass("drag-and-drop")), this.saveable && this.loaded_container && (y(window).bind("beforeunload", function() {
                        if (void 0 !== window.glazed_edited && a.loaded_container in window.glazed_edited) {
                            var e = a.loaded_container;
                            "human_readable" in a.attrs && (e = a.attrs.human_readable);
                            var t = Drupal.t("Field") + " " + e + " " + Drupal.t("might have unsaved changes");
                            return y.notify(t, {
                                type: "warning",
                                z_index: "8000",
                                offset: {
                                    x: 25,
                                    y: 70
                                }
                            }), " "
                        }
                    }), null !== this.dom_element && this.dom_element.on("mousedown", null, a, function(e, t) {
                        var a = (t = e.data).attrs.container;
                        "human_readable" in t.attrs && (a = t.attrs.human_readable), window.glazed_edited[t.loaded_container] = a
                    }), y('<span title="' + d("Save container") + '" class="control save-container btn btn-success glyphicon glyphicon-save" > </span>').appendTo(this.controls).click({
                        object: this
                    }, this.click_save_container))
                }
            },
            get_my_shortcode: function() {
                return this.get_children_shortcode()
            },
            get_hover_styles: function(e) {
                var t = "";
                "" != e.attrs.hover_style && (t = e.get_hover_style());
                for (var a = 0; a < e.children.length; a++) t += this.get_hover_styles(e.children[a]);
                return t
            },
            get_js: function(e) {
                var t = "";
                for (var a in e.js) t += '<script src="' + a + '"><\/script>\n';
                return t
            },
            get_css: function(e) {
                var t = "";
                for (var a in e.css) t += '<link rel="stylesheet" type="text/css" href="' + a + '">\n';
                return t
            },
            get_loader: function() {
                (function e(t) {
                    var a = {};
                    a[t.base] = !0;
                    for (var n = 0; n < t.children.length; n++) {
                        var i = e(t.children[n]);
                        y.extend(a, i)
                    }
                    return a
                })(this),
                function e(t) {
                    var a = {};
                    "an_start" in t.attrs && "" != t.attrs.an_start && (a.an_start = !0);
                    for (var n = 0; n < t.children.length; n++) y.extend(a, e(t.children[n]));
                    return a
                }(this);
                var e = "",
                    t = "";
                return t = drupalSettings.glazedBuilder.glazedDevelopment ? drupalSettings.glazedBuilder.glazedBaseUrl + "glazed_frontend.js" : drupalSettings.glazedBuilder.glazedBaseUrl + "glazed_frontend.min.js",
                    function e(t) {
                        if ("an_start" in t.attrs && "" != t.attrs.an_start) return animation = !0;
                        if (t.constructor.prototype.hasOwnProperty("showed")) {
                            var a = !0;
                            switch (("is_cms_element" in t || "is_template_element" in t) && (a = !1), t.base) {
                                case "az_container":
                                    null == t.parent && (a = !1);
                                    break;
                                case "az_section":
                                    "" == t.attrs.effect && (a = !1);
                                    break;
                                case "az_row":
                                    "yes" != t.attrs.equal && (a = !1)
                            }
                            if (a) return !0
                        }
                        for (var n = 0; n < t.children.length; n++)
                            if (e(t.children[n])) return !0;
                        return !1
                    }(this) && (e += '<script src="' + t + '"><\/script>\n'), e
            },
            get_html: function() {
                this.recursive_update_data(), this.recursive_clear_animation();
                var e = y("<div>" + y(this.dom_content_element).html() + "</div>");
                return this.recursive_restore(e), y(e).find(".az-element > .controls").remove(), y(e).find(".az-section > .add-section").remove(), y(e).find("> .controls").remove(), y(e).find(".az-sortable-controls").remove(), y(e).find(".az-empty").remove(), y(e).find(".ui-resizable-e").remove(), y(e).find(".ui-resizable-s").remove(), y(e).find(".ui-resizable-se").remove(), y(e).find(".ckeditor-inline").each(function() {
                    var e = y(this),
                        t = e.contents();
                    e.replaceWith(t)
                }), y(e).find(".az-text").removeClass("cke_editable cke_editable_inline cke_contents_ltr cke_show_borders"), y(e).find(".init-colorbox-processed").removeClass("init-colorbox-processed"), y(e).find(".az-element--controls-center").removeClass("az-element--controls-center"), y(e).find(".az-element--controls-top-left").removeClass("az-element--controls-top-left"), y(e).find(".az-element--controls-show-parent").removeClass("az-element--controls-show-parent"), y(e).find(".az-element--controls-spacer").removeClass("az-element--controls-spacer"), y(e).find(".az-element--hide-controls").removeClass("az-element--hide-controls"), y(e).find(".editable-highlight").removeClass("editable-highlight"), y(e).find(".styleable-highlight").removeClass("styleable-highlight"), y(e).find(".sortable-highlight").removeClass("sortable-highlight"), y(e).find(".ui-draggable").removeClass("ui-draggable"), y(e).find(".ui-resizable").removeClass("ui-resizable"), y(e).find(".ui-sortable").removeClass("ui-sortable"), y(e).find(".az-element.az-container > .az-ctnr").empty(), y(e).find(".az-element.az-cms-element").empty(), y(e).find(".mbYTP_wrapper").remove(), y(e).find("grammarly-extension").remove(), y(e).html()
            },
            get_container_html: function() {
                return this.get_html() + this.get_css(this) + this.get_hover_styles(this) + this.get_js(this) + this.get_loader()
            },
            click_save_container: function(e) {
                return e.data.object.save_container(), !1
            },
            save_container: function() {
                var t = this;
                glazed_add_js({
                    path: "jsON-js/json2.min.js",
                    loaded: "JSON" in window,
                    callback: function() {
                        var e = t.get_container_html();
                        a(t.attrs.container, t.attrs.human_readable, e, t.attrs.langcode)
                    }
                })
            },
            load_container: function() {
                var a = this;
                window.loadedContainers = window.loadedContainers || {}, window.loadedContainers[a.id] || (window.loadedContainers[a.id] = !0, "" != this.attrs.container && function(t, a, n) {
                    r.hasOwnProperty(t + "/" + a) ? n(r[t + "/" + a]) : window.glazed_online && y.ajax({
                        type: "get",
                        url: drupalSettings.glazedBuilder.glazedCsrfUrl,
                        dataType: "json",
                        cache: !1,
                        context: this
                    }).done(function(e) {
                        y.ajax({
                            type: "POST",
                            url: e,
                            data: {
                                action: "glazed_load_container",
                                type: t,
                                name: a
                            },
                            cache: !drupalSettings.glazedBuilder.glazedEditor
                        }).done(function(e) {
                            r[t + "/" + a] = e, n(e)
                        }).fail(function() {
                            n("")
                        })
                    })
                }(this.attrs.container.split("/")[0], this.attrs.container.split("/")[1], function(e) {
                    if (/^\s*\<[\s\S]*\>\s*$/.exec(e)) {
                        a.loaded_container = a.attrs.container, y(e).appendTo(a.dom_content_element), y(a.dom_content_element).find("> script").detach().appendTo("head"), y(a.dom_content_element).find("> link[href]").detach().appendTo("head"), y(a.dom_element).css("display", ""), y(a.dom_element).addClass("glazed"), a.parse_html(a.dom_content_element), y(a.dom_element).attr("data-az-id", a.id), a.html_content = !0;
                        for (var t = 0; t < a.children.length; t++) a.children[t].recursive_render();
                        a.dom_content_element.empty(), drupalSettings.glazedBuilder.glazedEditor && (a.show_controls(), a.update_sortable()), a.parent.attach_children(), a.attach_children();
                        for (t = 0; t < a.children.length; t++) a.children[t].recursive_showed();
                        y(document).trigger("scroll")
                    } else if (!l) {
                        a.loaded_container = a.attrs.container, a.parse_shortcode(e), y(a.dom_element).attr("data-az-id", a.id), drupalSettings.glazedBuilder.glazedEditor && (a.show_controls(), a.update_sortable());
                        for (t = 0; t < a.children.length; t++) a.children[t].recursive_render();
                        a.attach_children(), null != a.parent && a.parent.update_dom();
                        for (t = 0; t < a.children.length; t++) a.children[t].recursive_showed();
                        y(document).trigger("scroll")
                    }
                    b.try_render_unknown_elements()
                }))
            },
            clone: function() {
                te.baseclass.prototype.clone.apply(this, arguments), this.rendered = !0
            },
            recursive_render: function() {
                l ? (this.render(y), this.children = []) : te.baseclass.prototype.recursive_render.apply(this, arguments), drupalSettings.glazedBuilder.glazedEditor && (this.show_controls(), this.update_sortable())
            },
            update_dom: function() {
                this.loaded_container != this.attrs.container && (this.children = [], y(this.dom_content_element).empty(), this.rendered = !1, null != this.parent && te.baseclass.prototype.update_dom.apply(this, arguments))
            },
            showed: function(e) {
                te.baseclass.prototype.showed.apply(this, arguments);
                var t = this;
                null == this.parent ? t.rendered || (t.rendered = !0, t.load_container()) : this.add_js({
                    path: "vendor/jquery.waypoints/lib/jquery.waypoints.min.js",
                    loaded: "waypoint" in e.fn,
                    callback: function() {
                        e(t.dom_element).waypoint(function(e) {
                            t.rendered || (t.rendered = !0, t.load_container())
                        }, {
                            offset: "100%",
                            handler: function(e) {
                                this.destroy()
                            }
                        }), e(document).trigger("scroll")
                    }
                })
            },
            render: function(e) {
                "/" != this.attrs.container && (this.dom_element = e('<div class="az-element az-container"><div class="az-ctnr"></div></div>'), this.dom_content_element = e(this.dom_element).find(".az-ctnr"), te.baseclass.prototype.render.apply(this, arguments))
            }
        }), N("az_unknown", !0, ae), w(ae.prototype, {
            has_content: !0,
            hidden: !0,
            render: function(e) {
                if (this.dom_element = e('<div class = "az-element az-unknown">' + Drupal.t("Element Not Found.") + "</div>"), this.dom_content_element = this.dom_element, "content" in this.attrs) {
                    var t = /\[[^\]]*\]([^\[]*)\[\/[^\]]*\]/.exec(this.attrs.content);
                    t && e(this.dom_element).append(t[1])
                }
                ae.baseclass.prototype.render.apply(this, arguments)
            }
        }),
        function() {
            if ("glazed_elements" in window)
                for (var e = 0; e < window.glazed_elements.length; e++) {
                    var t = window.glazed_elements[e],
                        a = function(e, t) {
                            a.baseclass.apply(this, arguments)
                        };
                    M(t.base, t.is_container, a), t.baseclass = a.baseclass, t.params = t.params.concat(a.prototype.params), w(a.prototype, t);
                    for (var n = 0; n < a.prototype.params.length; n++) {
                        var i = B(a.prototype.params[n]);
                        a.prototype.params[n] = i
                    }
                }
        }(), drupalSettings.glazedBuilder.glazedEditor && !window.glazed_online || function(e) {
            if (drupalSettings.glazedBuilder && drupalSettings.glazedBuilder.glazedTemplateElements) {
                for (var t in drupalSettings.glazedBuilder.glazedTemplateElements) drupalSettings.glazedBuilder.glazedTemplateElements[t].html = decodeURIComponent(drupalSettings.glazedBuilder.glazedTemplateElements[t].html);
                e(drupalSettings.glazedBuilder.glazedTemplateElements)
            }
        }(function(e) {
            _.isObject(e) && b.create_template_elements(e)
        }),
        function(t) {
            drupalSettings.glazedBuilder.cmsElementNames ? t(drupalSettings.glazedBuilder.cmsElementNames) : y.ajax({
                type: "get",
                url: drupaLsettings.glazeDbuilder.glazeDcsrFurl,
                dataType: "json",
                cache: !1,
                context: this
            }).done(function(e) {
                y.ajax({
                    type: "POST",
                    url: e,
                    data: {
                        action: "glazed_builder_get_cms_element_names",
                        url: window.location.href
                    },
                    dataType: "json",
                    cache: !1,
                    context: this
                }).done(function(e) {
                    t(e)
                }).fail(function() {
                    t(!1)
                })
            })
        }(function(e) {
            _.isObject(e) ? b.create_cms_elements(e) : b.cms_elements_loaded = !0
        }), ne(), Drupal.behaviors.CKinlineAttach = {
            attach: function() {
                y(window).bind("CKinlineAttach", function() {
                    function e() {
                        CKEDITOR.on("instanceCreated", function(e) {
                            var t = e.editor;
                            t.on("focus", function() {
                                y(t.element.$).parents(".az-element").addClass("az-element--hide-controls")
                            }), t.on("blur", function() {
                                y(t.element.$).parents(".az-element").removeClass("az-element--hide-controls")
                            })
                        }), window.glazed_builder_set_ckeditor_config("inline"), y("body").find(".glazed").each(function() {
                            y(this).hasClass("glazed-editor") && y(this).find(".az-element.az-text, .az-element.az-blockquote").each(function() {
                                var e = y(this);
                                if (!e.find(".ckeditor-inline").length) {
                                    $controls = e.find(".controls").appendTo("body"), e.wrapInner("<div class='ckeditor-inline' contenteditable='true' />"), e.prepend($controls);
                                    var t = e.find(".ckeditor-inline")[0];
                                    void 0 !== t && 0 == y(t).hasClass("cke_focus") && CKEDITOR.inline(t)
                                }
                            })
                        })
                    }
                    "CKEDITOR" in window ? e() : glazed_add_js({
                        path: "vendor/ckeditor/ckeditor.js",
                        callback: function() {
                            _.isObject(CKEDITOR) && e()
                        }
                    })
                }).trigger("CKinlineAttach"), y("button.control.save-container").on("click", function() {
                    y(window).trigger("CKinlineAttach")
                }), y(".controls .control.toggle-editor").bind("click", function() {
                    y(".wrap-containers .glazed").each(function() {
                        var e = y(this);
                        if (e.hasClass("glazed-editor")) e.find(".az-element.az-text .ckeditor-inline, .az-element.az-blockquote .ckeditor-inline").each(function() {
                            var e = y(this);
                            e.attr("contenteditable", !0);
                            var t = e[0];
                            void 0 !== t && CKEDITOR.inline(t)
                        });
                        else
                            for (var t in y(this).find(".az-element.az-text .ckeditor-inline, .az-element.az-blockquote .ckeditor-inline").each(function() {
                                    var e = y(this);
                                    e.attr("contenteditable", !1), e.off("click")
                                }), CKEDITOR.instances) CKEDITOR.instances[t].destroy()
                    })
                }), y(document).on("click", function(e) {
                    y(e.target).is(".glazed-builder-popover") || y(".glazed-builder-popover").popover("hide")
                })
            }
        }
}(window.jQuery);