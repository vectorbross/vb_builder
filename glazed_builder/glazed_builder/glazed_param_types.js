(function($) {
  var p = '';
  var fp = '';
  if ('glazed_prefix' in window) {
    p = window.glazed_prefix;
    fp = window.glazed_prefix.replace('-', '_');
  }

  function toAbsoluteURL(url) {
    if (url.search(/^\/\//) != -1) {
      return window.location.protocol + url
    }
    if (url.search(/:\/\//) != -1) {
      return url
    }
    if (url.search(/^\//) != -1) {
      return window.location.origin + url
    }
    var base = window.location.href.match(/(.*\/)/)[0]
    return base + url
  }

  function t(text) {
    if ('glazed_t' in window) {
      return window.glazed_t(text);
    }
    else {
      return text;
    }
  }

  window.glazed_open_popup = function(url) {
    window.open(url, '', 'location,width=800,height=600,top=0');
  }

  function chosen_select(options, input) {
    var single_select = '<select>';
    for (var key in options) {
      single_select = single_select + '<option value="' + key + '">"' + options[key] + '"</option>';
    }
    single_select = single_select + '</select>';
    $(input).css('display', 'none');
    var select = $(single_select).insertAfter(input);
    if ($(input).val().length) {
      $(select).append('<option value=""></option>');
      var value = $(input).val();
      if (!$(select).find('option[value="' + value + '"]').length) {
        $(select).append('<option value="' + value + '">"' + value + '"</option>');
      }
      $(select).find('option[value="' + value + '"]').attr("selected", "selected");
    }
    else {
      $(select).append('<option value="" selected></option>');
    }
    $(select).chosen({
      search_contains: true,
      allow_single_deselect: true,
    });
    $(select).change(function() {
      $(this).find('option:selected').each(function() {
        $(input).val($(this).val());
      });
    });
    $(select).parent().find('.chosen-container').width('100%');
    $('<div><a class="direct-input" href="#">' + Drupal.t("Edit as text") + '</a></div>').insertBefore(select).click(
      function() {
        $(input).css('display', 'block');
        $(select).parent().find('.chosen-container').remove();
        $(select).remove();
        $(this).remove();
      });
    return select;
  }

  function multiple_chosen_select(options, input, delimiter) {
    var multiple_select = '<select multiple="multiple">';
    var optgroup = '';
    for (var key in options) {
      if (key.indexOf("optgroup") >= 0) {
        if (optgroup == '') {
          multiple_select = multiple_select + '</optgroup>';
        }
        multiple_select = multiple_select + '<optgroup label="' + options[key] + '">';
        optgroup = options[key];
        continue;
      }
      multiple_select = multiple_select + '<option value="' + key + '">"' + options[key] + '"</option>';
    }
    if (optgroup != '') {
      multiple_select = multiple_select + '</optgroup>';
    }
    multiple_select = multiple_select + '</select>';
    $(input).css('display', 'none');
    var select = $(multiple_select).insertAfter(input);
    if ($(input).val().length) {
      var values = $(input).val().split(delimiter);
      for (var i = 0; i < values.length; i++) {
        if (!$(select).find('option[value="' + values[i] + '"]').length) {
          $(select).append('<option value="' + values[i] + '">"' + values[i] + '"</option>');
        }
        $(select).find('option[value="' + values[i] + '"]').attr("selected", "selected");
      }
    }
    $(select).chosen({
      search_contains: true,
    });
    $(select).change(function() {
      var selected = [];
      $(this).find('option:selected').each(function() {
        selected.push($(this).val());
      });
      $(input).val(selected.join(delimiter));
    });
    $(select).parent().find('.chosen-container').width('100%');
    $('<div><a class="direct-input" href="#">' + Drupal.t("Edit as text") + '</a></div>').insertBefore(select).click(
      function() {
        $(input).css('display', 'block');
        $(select).parent().find('.chosen-container').remove();
        $(select).remove();
        $(this).remove();
      });
    return select;
  }

  function image_select(input) {
    images_select(input, '');
  }

  function images_select(input, delimiter) {
    if ('images_select' in window) {
      window.images_select(input, delimiter);
    }
    else {
      // 23/12/2016 removed code for proprietary file manager ~Jur
    }
  }

  function file_select(input) {
    files_select(input, '');
  }

  function files_select(input, delimiter) {
    if ('files_select' in window) {
      window.files_select(input, delimiter);
    }
  }

  function colorpicker(input) {
    if ('wpColorPicker' in $.fn) {
      $(input).wpColorPicker();
      _.defer(function() {
        $(input).wpColorPicker({
          change: _.throttle(function() {
            $(input).trigger('change');
          }, 1000)
        });
      });
    }
    else {
      window.wpColorPickerL10n = {
        "clear": Drupal.t("Clear"),
        "defaultString": Drupal.t("Default"),
        "pick": Drupal.t("Select Color"),
        "current": Drupal.t("Current Color")
      }
      glazed_add_js({
        path: 'vendor/jquery.iris/dist/iris.min.js?v1',
        callback: function() {
          glazed_add_js({
            path: 'js/glazed.iris.min.js',
            callback: function() {
              glazed_add_css('css/color-picker.min.css', function() {
                $(input).wpColorPicker();
              });
            }
          });
        }
      });
    }
  }

  function nouislider(slider, min, max, value, step, target) {
    glazed_add_css('vendor/noUiSlider/jquery.nouislider.min.css', function() {});
    glazed_add_js({
      path: 'vendor/noUiSlider/jquery.nouislider.min.js',
      callback: function() {
        $(slider).noUiSlider({
          start: [(value == '' || isNaN(parseFloat(value)) || value == 'NaN') ? min : parseFloat(value)],
          step: parseFloat(step),
          range: {
            min: [parseFloat(min)],
            max: [parseFloat(max)]
          },
        }).on('change', function() {
          $(target).val($(slider).val());
        });
      }
    });
  }

  function initBootstrapSlider(slider, min, max, value, step, formatter) {
    glazed_add_css('vendor/bootstrap-slider/bootstrap-slider.min.css', function() {});
    glazed_add_js({
      path: 'vendor/bootstrap-slider/bootstrap-slider.min.js',
      callback: function () {
        if (formatter) {
          $(slider).bootstrapSlider({
            step: parseFloat(step),
            min: parseFloat(min),
            max: parseFloat(max),
            tooltip: 'hide',
            value: (value == '' || isNaN(parseFloat(value)) || value == 'NaN') ? min : parseFloat(value),
            formatter: function (value) {
              return value + ' px';
            },
          });
        } else {
          $(slider).bootstrapSlider({
            step: parseFloat(step),
            min: parseFloat(min),
            max: parseFloat(max),
            tooltip: 'hide',
            value: (value == '' || isNaN(parseFloat(value)) || value == 'NaN') ? min : parseFloat(value),
          });
        }
      }
    });
  }

  function initBootstrapSwitch(element) {
    glazed_add_css('vendor/bootstrap-switch/bootstrap-switch.min.css', function () {
    });
    glazed_add_js({
      path: 'vendor/bootstrap-switch/bootstrap-switch.min.js',
      callback: function () {
        $(element).find('[type="checkbox"]').bootstrapSwitch({
          onColor: "success",
          onText: "On",
          offText: "Off",
          size: "small",
        }).on('switchChange.bootstrapSwitch', function(event, state) {
          $(this).trigger('change');
        });
      }
    });
  }

  function render_image(value, width, height) {
    if ($.isNumeric(width))
      width = width + 'px';
    if ($.isNumeric(height))
      height = height + 'px';
    var img = $('<div style="background-image: url(' + encodeURI(value) + ');" data-src="' + value + '" ></div>');
    if (width.length > 0)
      $(img).css('width', width);
    if (height.length > 0)
      $(img).css('height', height);
    return img;
  }


  /**
   * Load Drupal node links
   */
  function glazed_builder_get_node_links(request, callback) {
    if (window.glazed_online) {
      if (drupalSettings.glazedBuilder.glazedAjaxUrl) {
        $.ajax({
          type: 'POST',
          url: drupalSettings.glazedBuilder.glazedAjaxUrl,
          data: {
            action: 'glazed_get_node_links',
            request: request
          },
          dataType: "json",
          cache: false,
          context: this
        }).done(function(data) {
          callback(data);
        }).fail(function() {
          callback(false);
        });
      }
      else {
        callback(false);
      }
    }
    else {
      callback(false);
    }
  }

  var icons = [];
  if ('glazed_icons' in window)
    icons = window.glazed_icons;

  var glazed_param_types = [

  {
    type: 'bootstrap_slider',
    create: function() {
      this.min = 0;
      this.max = 100;
      this.step = 1;
      this.formatter = false;
    },
    get_value: function() {
      var v = $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      return (v == '') ? NaN : parseFloat(v).toString();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' +
        this.description + '</p></div>');
    },
    opened: function() {
      initBootstrapSlider(
        $(this.dom_element).find('input[name="' + this.param_name + '"]'),
        this.min,
        this.max,
        this.get_value(),
        this.step,
        this.formatter);
    },
  },

  {
    type: 'checkbox',
    get_value: function() {
      var values = [];
      _.each($(this.dom_element).find('input[name="' + this.param_name + '"]:checked'), function(obj) {
        values.push($(obj).val());
      });
      return values.join(',');
    },
    render: function(value) {
      if (value == null)
        value = '';
      var values = value.split(',');
      var inputs = '';
      var count = Object.keys(this.value).length;
      if (count == 1) {
        for (var name in this.value) {
          if (_.indexOf(values, name) >= 0) {
            inputs += '<div class="checkbox"><input name="' + this.param_name +
              '" type="checkbox" checked value="' + name + '"></div>';
          }
          else {
            inputs += '<div class="checkbox"><input name="' + this.param_name +
              '" type="checkbox" value="' + name + '"></div>';
          }
        }
        this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading + '</label><div class="wrap-checkbox">' + inputs +
          '</div><p class="help-block">' + this.description + '</p>');
        initBootstrapSwitch(this.dom_element);
      } else {
        for (var name in this.value) {
          if (_.indexOf(values, name) >= 0) {
            inputs += '<div class="checkbox"><label><input name="' + this.param_name +
              '" type="checkbox" checked value="' + name + '">' + this.value[name] + '</label></div>';
          }
          else {
            inputs += '<div class="checkbox"><label><input name="' + this.param_name +
              '" type="checkbox" value="' + name + '">' + this.value[name] + '</label></div>';
          }
        }
        this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading + '</label><div class="wrap-checkbox">' + inputs +
          '</div><p class="help-block">' + this.description + '</p>');
      }
    }
  },

  {
    type: 'checkboxes',
      get_value: function() {
      var values = [];
      _.each($(this.dom_element).find('input[name="' + this.param_name + '"]:checked'), function(obj) {
        values.push($(obj).val());
      });
      return values.join(',');
    },
    render: function(value) {
      if (value == null)
        value = '';
      var values = value.split(',');
      var inputs = '';
      if (value == '') {
        for (var name in this.value) {
          inputs += '<label>' + this.value[name] + '</label><div class="wrap-checkbox"><div class="checkbox"><input name="' + this.param_name +
            '" type="checkbox" checked value="' + name + '"></div></div>';
        }
      } else {
        for (var name in this.value) {
          if (_.indexOf(values, name) >= 0) {
            inputs += '<label>' + this.value[name] + '</label><div class="wrap-checkbox"><div class="checkbox"><input name="' + this.param_name +
              '" type="checkbox" checked value="' + name + '"></div></div>';
          }
          else {
            inputs += '<label>' + this.value[name] + '</label><div class="wrap-checkbox"><div class="checkbox"><input name="' + this.param_name +
              '" type="checkbox" value="' + name + '"></div></div>';
          }
        }
      }
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '">' + inputs +
        '<p class="help-block">' + this.description + '</p>');
      initBootstrapSwitch(this.dom_element);
    }
  },

  {
    type: 'colorpicker',
    get_value: function() {
      return $(this.dom_element).find('#' + this.id).val();
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input id="' + this.id + '" name="' + this.param_name + '" type="text" value="' + value +
        '"></div><p class="help-block">' + this.description + '</p></div>');
    },
    opened: function() {
      colorpicker('#' + this.id);
    },
  },

  {
    type: 'css',
    safe: false,
    get_value: function() {
      return $(this.dom_element).find('#' + this.id).val();
    },
    opened: function() {
      var param = this;
      glazed_add_js({
        path: 'vendor/ace/ace.js',
        callback: function() {
          var aceeditor = ace.edit(param.id);
          aceeditor.setTheme("ace/theme/chrome");
          aceeditor.getSession().setMode("ace/mode/css");
          aceeditor.setOptions({
            minLines: 10,
            maxLines: 30,
          });
          $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
          aceeditor.on(
            'change',
            function(e) {
              $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
              aceeditor.resize();
            }
          );
        }
      });
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading + '</label><div id="' +
        this.id + '"><textarea class="form-control" rows="10" cols="45" name="' + this.param_name +
        '" ">' + value + '</textarea></div><p class="help-block">' + this.description + '</p></div>'
      );
    },
  },

  {
    type: 'datetime',
    create: function() {
      this.formatDate = '';
      this.formatTime = '';
      this.timepicker = false;
      this.datepicker = false;
    },
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' + this.description +
        '</p></div>');
    },
    opened: function() {
      var param = this;
      glazed_add_css('vendor/datetimepicker/jquery.datetimepicker.css', function() {});
      glazed_add_js({
        path: 'vendor/datetimepicker/jquery.datetimepicker.js',
        callback: function() {
          if (param.datepicker && param.timepicker)
            param.format = param.formatDate + ' ' + param.formatTime;
          if (param.datepicker && !param.timepicker)
            param.format = param.formatDate;
          if (!param.datepicker && param.timepicker)
            param.format = param.formatTime;
          $(param.dom_element).find('input[name="' + param.param_name + '"]').datetimepicker({
            format: param.format,
            timepicker: param.timepicker,
            datepicker: param.datepicker,
            inline: true,
          });
        }
      });
    },
  },

  {
    type: 'dropdown',
    get_value: function () {
      if (Object.keys(this.value).length < 10) {
        var val = $(this.dom_element).find('input[name="' + this.param_name + '"]:checked').val();
        if (typeof val != 'undefined')
          return val;
      } else {
        return $(this.dom_element).find('select[name="' + this.param_name + '"] > option:selected').val();
      }
    },
    render: function (value) {
      var content = '<div class="form-radios">';
      if (Object.keys(this.value).length < 10) {
        /* Render radios */
        var inValue = value in this.value;
        for (var name in this.value) {
          var radio = '';
          var inputName = (name == '') ? 'default' : name;
          var id = 'dropdown-' + this.param_name + '-' + inputName;
          if (!inValue) {
            radio += '<div class="form-item form-type-radio">'
              + '<input type="radio" id="' + id + '" name="' + this.param_name + '" value="' + name + '" checked="checked" class="form-radio">'
              + '<label class="option" for="' + id + '">' + this.value[name] + ' </label>'
              + '</div>';
            inValue = true;
          } else {
            if (name == value) {
              radio += '<div class="form-item form-type-radio">'
                + '<input type="radio" id="' + id + '" name="' + this.param_name + '" value="' + name + '" checked="checked" class="form-radio">'
                + '<label class="option" for="' + id + '">' + this.value[name] + ' </label>'
                + '</div>';
            }
            else {
              radio += '<div class="form-item form-type-radio">'
                + '<input type="radio" id="' + id + '" name="' + this.param_name + '" value="' + name + '" class="form-radio">'
                + '<label class="option" for="' + id + '">' + this.value[name] + ' </label>'
                + '</div>';
            }
          }
          content += radio;
        }
        content += '</div>';
      } else {
        /* Render select */
        content = '<select name="' + this.param_name + '" class="form-control">';
        for (var name in this.value) {
          var option = '';
          if (name == value) {
            option = '<option selected value="' + name + '">' + this.value[name] + '</option>';
          }
          else {
            option = '<option value="' + name + '">' + this.value[name] + '</option>';
          }
          content += option;
        }
        content += '/<select>';
      }
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading + '</label><div class="clearfix">' + content +
        '</div><p class="help-block">' + this.description + '</p></div>');
    }
  },

  {
    type: 'google_font',
    hidden: !'glazed_google_fonts' in window,
    get_value: function() {
      var font = $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      var subset = $(this.dom_element).find('input[name="' + this.param_name + '_subset"]').val();
      var variant = $(this.dom_element).find('input[name="' + this.param_name + '_variant"]').val();
      return font + '|' + subset + '|' + variant;
    },
    render: function(value) {
      var font = '';
      var subset = '';
      var variant = '';
      if (_.isString(value) && value != '' && value.split('|').length == 3) {
        font = value.split('|')[0];
        subset = value.split('|')[1];
        variant = value.split('|')[2];
      }
      var font_input = '<div class="col-sm-4"><label>' + this.heading + '</label><input class="form-control" name="' + this.param_name + '" type="text" value="' + font + '"></div>';
      var subset_input = '<div class="col-sm-4"><label>' + Drupal.t('Subset') + '</label><input class="form-control" name="' + this.param_name + '_subset" type="text" value="' + subset + '"></div>';
      var variant_input = '<div class="col-sm-4"><label>' + Drupal.t('Variant') + '</label><input class="' +
        'form-control" name="' + this.param_name + '_variant" type="text" value="' + variant + '"></div>';
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><div class="row">' + font_input +
        subset_input + variant_input + '</div><p class="help-block">' + this.description +
        '</p></div>');
    },
    opened: function() {
      var element = this;
      var fonts = Object.keys(window.glazed_google_fonts);
      fonts = _.object(fonts, fonts);
      var font_select = null;
      var subset_select = null;
      var variant_select = null;
      font_select = chosen_select(fonts, $(this.dom_element).find('input[name="' + this.param_name + '"]'));
      $(font_select).chosen().change(function() {
        var f = Object.keys(window.glazed_google_fonts)[0];
        if ($(this).val() in window.glazed_google_fonts)
          f = window.glazed_google_fonts[$(this).val()];
        var subsets = {};
        for (var i = 0; i < f.subsets.length; i++) {
          subsets[f.subsets[i].id] = f.subsets[i].name;
        }
        var variants = {};
        for (var i = 0; i < f.variants.length; i++) {
          variants[f.variants[i].id] = f.variants[i].name;
        }

        $(subset_select).parent().find('.direct-input').click();
        subset_select = chosen_select(subsets, $(element.dom_element).find('input[name="' + element.param_name +
          '_subset"]'));

        $(variant_select).parent().find('.direct-input').click();
        variant_select = chosen_select(variants, $(element.dom_element).find('input[name="' + element.param_name +
          '_variant"]'));
      });
      $(font_select).chosen().trigger('change');
    },
  },

  {
    type: 'html',
    safe: false,
    get_value: function() {
      return $(this.dom_element).find('#' + this.id).val();
    },
    opened: function() {
      var param = this;
      glazed_add_js({
        path: 'vendor/ace/ace.js',
        callback: function() {
          var aceeditor = ace.edit(param.id);
          aceeditor.setTheme("ace/theme/chrome");
          aceeditor.getSession().setMode("ace/mode/html");
          aceeditor.setOptions({
            minLines: 10,
            maxLines: 30,
          });
          $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
          aceeditor.on(
            'change',
            function(e) {
              $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
              aceeditor.resize();
            }
          );
        }
      });
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading + '</label><div id="' +
        this.id + '"><textarea class="form-control" rows="10" cols="45" name="' + this.param_name +
        '" ">' + value + '</textarea></div><p class="help-block">' + this.description + '</p></div>'
      );
    },
  },

  {
    type: 'icon',
    icons: [
'fas fa-ad',
'fas fa-address-book',
'fas fa-address-card',
'fas fa-adjust',
'fas fa-air-freshener',
'fas fa-align-center',
'fas fa-align-justify',
'fas fa-align-left',
'fas fa-align-right',
'fas fa-allergies',
'fas fa-ambulance',
'fas fa-american-sign-language-interpreting',
'fas fa-anchor',
'fas fa-angle-double-down',
'fas fa-angle-double-left',
'fas fa-angle-double-right',
'fas fa-angle-double-up',
'fas fa-angle-down',
'fas fa-angle-left',
'fas fa-angle-right',
'fas fa-angle-up',
'fas fa-angry',
'fas fa-ankh',
'fas fa-apple-alt',
'fas fa-archive',
'fas fa-archway',
'fas fa-arrow-alt-circle-down',
'fas fa-arrow-alt-circle-left',
'fas fa-arrow-alt-circle-right',
'fas fa-arrow-alt-circle-up',
'fas fa-arrow-circle-down',
'fas fa-arrow-circle-left',
'fas fa-arrow-circle-right',
'fas fa-arrow-circle-up',
'fas fa-arrow-down',
'fas fa-arrow-left',
'fas fa-arrow-right',
'fas fa-arrow-up',
'fas fa-arrows-alt',
'fas fa-arrows-alt-h',
'fas fa-arrows-alt-v',
'fas fa-assistive-listening-systems',
'fas fa-asterisk',
'fas fa-at',
'fas fa-atlas',
'fas fa-atom',
'fas fa-audio-description',
'fas fa-award',
'fas fa-baby',
'fas fa-baby-carriage',
'fas fa-backspace',
'fas fa-backward',
'fas fa-bacon',
'fas fa-balance-scale',
'fas fa-balance-scale-left',
'fas fa-balance-scale-right',
'fas fa-ban',
'fas fa-band-aid',
'fas fa-barcode',
'fas fa-bars',
'fas fa-baseball-ball',
'fas fa-basketball-ball',
'fas fa-bath',
'fas fa-battery-empty',
'fas fa-battery-full',
'fas fa-battery-half',
'fas fa-battery-quarter',
'fas fa-battery-three-quarters',
'fas fa-bed',
'fas fa-beer',
'fas fa-bell',
'fas fa-bell-slash',
'fas fa-bezier-curve',
'fas fa-bible',
'fas fa-bicycle',
'fas fa-biking',
'fas fa-binoculars',
'fas fa-biohazard',
'fas fa-birthday-cake',
'fas fa-blender',
'fas fa-blender-phone',
'fas fa-blind',
'fas fa-blog',
'fas fa-bold',
'fas fa-bolt',
'fas fa-bomb',
'fas fa-bone',
'fas fa-bong',
'fas fa-book',
'fas fa-book-dead',
'fas fa-book-medical',
'fas fa-book-open',
'fas fa-book-reader',
'fas fa-bookmark',
'fas fa-border-all',
'fas fa-border-none',
'fas fa-border-style',
'fas fa-bowling-ball',
'fas fa-box',
'fas fa-box-open',
'fas fa-boxes',
'fas fa-braille',
'fas fa-brain',
'fas fa-bread-slice',
'fas fa-briefcase',
'fas fa-briefcase-medical',
'fas fa-broadcast-tower',
'fas fa-broom',
'fas fa-brush',
'fas fa-bug',
'fas fa-building',
'fas fa-bullhorn',
'fas fa-bullseye',
'fas fa-burn',
'fas fa-bus',
'fas fa-bus-alt',
'fas fa-business-time',
'fas fa-calculator',
'fas fa-calendar',
'fas fa-calendar-alt',
'fas fa-calendar-check',
'fas fa-calendar-day',
'fas fa-calendar-minus',
'fas fa-calendar-plus',
'fas fa-calendar-times',
'fas fa-calendar-week',
'fas fa-camera',
'fas fa-camera-retro',
'fas fa-campground',
'fas fa-candy-cane',
'fas fa-cannabis',
'fas fa-capsules',
'fas fa-car',
'fas fa-car-alt',
'fas fa-car-battery',
'fas fa-car-crash',
'fas fa-car-side',
'fas fa-caret-down',
'fas fa-caret-left',
'fas fa-caret-right',
'fas fa-caret-square-down',
'fas fa-caret-square-left',
'fas fa-caret-square-right',
'fas fa-caret-square-up',
'fas fa-caret-up',
'fas fa-carrot',
'fas fa-cart-arrow-down',
'fas fa-cart-plus',
'fas fa-cash-register',
'fas fa-cat',
'fas fa-certificate',
'fas fa-chair',
'fas fa-chalkboard',
'fas fa-chalkboard-teacher',
'fas fa-charging-station',
'fas fa-chart-area',
'fas fa-chart-bar',
'fas fa-chart-line',
'fas fa-chart-pie',
'fas fa-check',
'fas fa-check-circle',
'fas fa-check-double',
'fas fa-check-square',
'fas fa-cheese',
'fas fa-chess',
'fas fa-chess-bishop',
'fas fa-chess-board',
'fas fa-chess-king',
'fas fa-chess-knight',
'fas fa-chess-pawn',
'fas fa-chess-queen',
'fas fa-chess-rook',
'fas fa-chevron-circle-down',
'fas fa-chevron-circle-left',
'fas fa-chevron-circle-right',
'fas fa-chevron-circle-up',
'fas fa-chevron-down',
'fas fa-chevron-left',
'fas fa-chevron-right',
'fas fa-chevron-up',
'fas fa-child',
'fas fa-church',
'fas fa-circle',
'fas fa-circle-notch',
'fas fa-city',
'fas fa-clinic-medical',
'fas fa-clipboard',
'fas fa-clipboard-check',
'fas fa-clipboard-list',
'fas fa-clock',
'fas fa-clone',
'fas fa-closed-captioning',
'fas fa-cloud',
'fas fa-cloud-download-alt',
'fas fa-cloud-meatball',
'fas fa-cloud-moon',
'fas fa-cloud-moon-rain',
'fas fa-cloud-rain',
'fas fa-cloud-showers-heavy',
'fas fa-cloud-sun',
'fas fa-cloud-sun-rain',
'fas fa-cloud-upload-alt',
'fas fa-cocktail',
'fas fa-code',
'fas fa-code-branch',
'fas fa-coffee',
'fas fa-cog',
'fas fa-cogs',
'fas fa-coins',
'fas fa-columns',
'fas fa-comment',
'fas fa-comment-alt',
'fas fa-comment-dollar',
'fas fa-comment-dots',
'fas fa-comment-medical',
'fas fa-comment-slash',
'fas fa-comments',
'fas fa-comments-dollar',
'fas fa-compact-disc',
'fas fa-compass',
'fas fa-compress',
'fas fa-compress-arrows-alt',
'fas fa-concierge-bell',
'fas fa-cookie',
'fas fa-cookie-bite',
'fas fa-copy',
'fas fa-copyright',
'fas fa-couch',
'fas fa-credit-card',
'fas fa-crop',
'fas fa-crop-alt',
'fas fa-cross',
'fas fa-crosshairs',
'fas fa-crow',
'fas fa-crown',
'fas fa-crutch',
'fas fa-cube',
'fas fa-cubes',
'fas fa-cut',
'fas fa-database',
'fas fa-deaf',
'fas fa-democrat',
'fas fa-desktop',
'fas fa-dharmachakra',
'fas fa-diagnoses',
'fas fa-dice',
'fas fa-dice-d20',
'fas fa-dice-d6',
'fas fa-dice-five',
'fas fa-dice-four',
'fas fa-dice-one',
'fas fa-dice-six',
'fas fa-dice-three',
'fas fa-dice-two',
'fas fa-digital-tachograph',
'fas fa-directions',
'fas fa-divide',
'fas fa-dizzy',
'fas fa-dna',
'fas fa-dog',
'fas fa-dollar-sign',
'fas fa-dolly',
'fas fa-dolly-flatbed',
'fas fa-donate',
'fas fa-door-closed',
'fas fa-door-open',
'fas fa-dot-circle',
'fas fa-dove',
'fas fa-download',
'fas fa-drafting-compass',
'fas fa-dragon',
'fas fa-draw-polygon',
'fas fa-drum',
'fas fa-drum-steelpan',
'fas fa-drumstick-bite',
'fas fa-dumbbell',
'fas fa-dumpster',
'fas fa-dumpster-fire',
'fas fa-dungeon',
'fas fa-edit',
'fas fa-egg',
'fas fa-eject',
'fas fa-ellipsis-h',
'fas fa-ellipsis-v',
'fas fa-envelope',
'fas fa-envelope-open',
'fas fa-envelope-open-text',
'fas fa-envelope-square',
'fas fa-equals',
'fas fa-eraser',
'fas fa-ethernet',
'fas fa-euro-sign',
'fas fa-exchange-alt',
'fas fa-exclamation',
'fas fa-exclamation-circle',
'fas fa-exclamation-triangle',
'fas fa-expand',
'fas fa-expand-arrows-alt',
'fas fa-external-link-alt',
'fas fa-external-link-square-alt',
'fas fa-eye',
'fas fa-eye-dropper',
'fas fa-eye-slash',
'fas fa-fan',
'fas fa-fast-backward',
'fas fa-fast-forward',
'fas fa-fax',
'fas fa-feather',
'fas fa-feather-alt',
'fas fa-female',
'fas fa-fighter-jet',
'fas fa-file',
'fas fa-file-alt',
'fas fa-file-archive',
'fas fa-file-audio',
'fas fa-file-code',
'fas fa-file-contract',
'fas fa-file-csv',
'fas fa-file-download',
'fas fa-file-excel',
'fas fa-file-export',
'fas fa-file-image',
'fas fa-file-import',
'fas fa-file-invoice',
'fas fa-file-invoice-dollar',
'fas fa-file-medical',
'fas fa-file-medical-alt',
'fas fa-file-pdf',
'fas fa-file-powerpoint',
'fas fa-file-prescription',
'fas fa-file-signature',
'fas fa-file-upload',
'fas fa-file-video',
'fas fa-file-word',
'fas fa-fill',
'fas fa-fill-drip',
'fas fa-film',
'fas fa-filter',
'fas fa-fingerprint',
'fas fa-fire',
'fas fa-fire-alt',
'fas fa-fire-extinguisher',
'fas fa-first-aid',
'fas fa-fish',
'fas fa-fist-raised',
'fas fa-flag',
'fas fa-flag-checkered',
'fas fa-flag-usa',
'fas fa-flask',
'fas fa-flushed',
'fas fa-folder',
'fas fa-folder-minus',
'fas fa-folder-open',
'fas fa-folder-plus',
'fas fa-font',
'fas fa-football-ball',
'fas fa-forward',
'fas fa-frog',
'fas fa-frown',
'fas fa-frown-open',
'fas fa-funnel-dollar',
'fas fa-futbol',
'fas fa-gamepad',
'fas fa-gas-pump',
'fas fa-gavel',
'fas fa-gem',
'fas fa-genderless',
'fas fa-ghost',
'fas fa-gift',
'fas fa-gifts',
'fas fa-glass-cheers',
'fas fa-glass-martini',
'fas fa-glass-martini-alt',
'fas fa-glass-whiskey',
'fas fa-glasses',
'fas fa-globe',
'fas fa-globe-africa',
'fas fa-globe-americas',
'fas fa-globe-asia',
'fas fa-globe-europe',
'fas fa-golf-ball',
'fas fa-gopuram',
'fas fa-graduation-cap',
'fas fa-greater-than',
'fas fa-greater-than-equal',
'fas fa-grimace',
'fas fa-grin',
'fas fa-grin-alt',
'fas fa-grin-beam',
'fas fa-grin-beam-sweat',
'fas fa-grin-hearts',
'fas fa-grin-squint',
'fas fa-grin-squint-tears',
'fas fa-grin-stars',
'fas fa-grin-tears',
'fas fa-grin-tongue',
'fas fa-grin-tongue-squint',
'fas fa-grin-tongue-wink',
'fas fa-grin-wink',
'fas fa-grip-horizontal',
'fas fa-grip-lines',
'fas fa-grip-lines-vertical',
'fas fa-grip-vertical',
'fas fa-guitar',
'fas fa-h-square',
'fas fa-hamburger',
'fas fa-hammer',
'fas fa-hamsa',
'fas fa-hand-holding',
'fas fa-hand-holding-heart',
'fas fa-hand-holding-usd',
'fas fa-hand-lizard',
'fas fa-hand-middle-finger',
'fas fa-hand-paper',
'fas fa-hand-peace',
'fas fa-hand-point-down',
'fas fa-hand-point-left',
'fas fa-hand-point-right',
'fas fa-hand-point-up',
'fas fa-hand-pointer',
'fas fa-hand-rock',
'fas fa-hand-scissors',
'fas fa-hand-spock',
'fas fa-hands',
'fas fa-hands-helping',
'fas fa-handshake',
'fas fa-hanukiah',
'fas fa-hard-hat',
'fas fa-hashtag',
'fas fa-hat-cowboy',
'fas fa-hat-cowboy-side',
'fas fa-hat-wizard',
'fas fa-haykal',
'fas fa-hdd',
'fas fa-heading',
'fas fa-headphones',
'fas fa-headphones-alt',
'fas fa-headset',
'fas fa-heart',
'fas fa-heart-broken',
'fas fa-heartbeat',
'fas fa-helicopter',
'fas fa-highlighter',
'fas fa-hiking',
'fas fa-hippo',
'fas fa-history',
'fas fa-hockey-puck',
'fas fa-holly-berry',
'fas fa-home',
'fas fa-horse',
'fas fa-horse-head',
'fas fa-hospital',
'fas fa-hospital-alt',
'fas fa-hospital-symbol',
'fas fa-hot-tub',
'fas fa-hotdog',
'fas fa-hotel',
'fas fa-hourglass',
'fas fa-hourglass-end',
'fas fa-hourglass-half',
'fas fa-hourglass-start',
'fas fa-house-damage',
'fas fa-hryvnia',
'fas fa-i-cursor',
'fas fa-ice-cream',
'fas fa-icicles',
'fas fa-icons',
'fas fa-id-badge',
'fas fa-id-card',
'fas fa-id-card-alt',
'fas fa-igloo',
'fas fa-image',
'fas fa-images',
'fas fa-inbox',
'fas fa-indent',
'fas fa-industry',
'fas fa-infinity',
'fas fa-info',
'fas fa-info-circle',
'fas fa-italic',
'fas fa-jedi',
'fas fa-joint',
'fas fa-journal-whills',
'fas fa-kaaba',
'fas fa-key',
'fas fa-keyboard',
'fas fa-khanda',
'fas fa-kiss',
'fas fa-kiss-beam',
'fas fa-kiss-wink-heart',
'fas fa-kiwi-bird',
'fas fa-landmark',
'fas fa-language',
'fas fa-laptop',
'fas fa-laptop-code',
'fas fa-laptop-medical',
'fas fa-laugh',
'fas fa-laugh-beam',
'fas fa-laugh-squint',
'fas fa-laugh-wink',
'fas fa-layer-group',
'fas fa-leaf',
'fas fa-lemon',
'fas fa-less-than',
'fas fa-less-than-equal',
'fas fa-level-down-alt',
'fas fa-level-up-alt',
'fas fa-life-ring',
'fas fa-lightbulb',
'fas fa-link',
'fas fa-lira-sign',
'fas fa-list',
'fas fa-list-alt',
'fas fa-list-ol',
'fas fa-list-ul',
'fas fa-location-arrow',
'fas fa-lock',
'fas fa-lock-open',
'fas fa-long-arrow-alt-down',
'fas fa-long-arrow-alt-left',
'fas fa-long-arrow-alt-right',
'fas fa-long-arrow-alt-up',
'fas fa-low-vision',
'fas fa-luggage-cart',
'fas fa-magic',
'fas fa-magnet',
'fas fa-mail-bulk',
'fas fa-male',
'fas fa-map',
'fas fa-map-marked',
'fas fa-map-marked-alt',
'fas fa-map-marker',
'fas fa-map-marker-alt',
'fas fa-map-pin',
'fas fa-map-signs',
'fas fa-marker',
'fas fa-mars',
'fas fa-mars-double',
'fas fa-mars-stroke',
'fas fa-mars-stroke-h',
'fas fa-mars-stroke-v',
'fas fa-mask',
'fas fa-medal',
'fas fa-medkit',
'fas fa-meh',
'fas fa-meh-blank',
'fas fa-meh-rolling-eyes',
'fas fa-memory',
'fas fa-menorah',
'fas fa-mercury',
'fas fa-meteor',
'fas fa-microchip',
'fas fa-microphone',
'fas fa-microphone-alt',
'fas fa-microphone-alt-slash',
'fas fa-microphone-slash',
'fas fa-microscope',
'fas fa-minus',
'fas fa-minus-circle',
'fas fa-minus-square',
'fas fa-mitten',
'fas fa-mobile',
'fas fa-mobile-alt',
'fas fa-money-bill',
'fas fa-money-bill-alt',
'fas fa-money-bill-wave',
'fas fa-money-bill-wave-alt',
'fas fa-money-check',
'fas fa-money-check-alt',
'fas fa-monument',
'fas fa-moon',
'fas fa-mortar-pestle',
'fas fa-mosque',
'fas fa-motorcycle',
'fas fa-mountain',
'fas fa-mouse',
'fas fa-mouse-pointer',
'fas fa-mug-hot',
'fas fa-music',
'fas fa-network-wired',
'fas fa-neuter',
'fas fa-newspaper',
'fas fa-not-equal',
'fas fa-notes-medical',
'fas fa-object-group',
'fas fa-object-ungroup',
'fas fa-oil-can',
'fas fa-om',
'fas fa-otter',
'fas fa-outdent',
'fas fa-pager',
'fas fa-paint-brush',
'fas fa-paint-roller',
'fas fa-palette',
'fas fa-pallet',
'fas fa-paper-plane',
'fas fa-paperclip',
'fas fa-parachute-box',
'fas fa-paragraph',
'fas fa-parking',
'fas fa-passport',
'fas fa-pastafarianism',
'fas fa-paste',
'fas fa-pause',
'fas fa-pause-circle',
'fas fa-paw',
'fas fa-peace',
'fas fa-pen',
'fas fa-pen-alt',
'fas fa-pen-fancy',
'fas fa-pen-nib',
'fas fa-pen-square',
'fas fa-pencil-alt',
'fas fa-pencil-ruler',
'fas fa-people-carry',
'fas fa-pepper-hot',
'fas fa-percent',
'fas fa-percentage',
'fas fa-person-booth',
'fas fa-phone',
'fas fa-phone-alt',
'fas fa-phone-slash',
'fas fa-phone-square',
'fas fa-phone-square-alt',
'fas fa-phone-volume',
'fas fa-photo-video',
'fas fa-piggy-bank',
'fas fa-pills',
'fas fa-pizza-slice',
'fas fa-place-of-worship',
'fas fa-plane',
'fas fa-plane-arrival',
'fas fa-plane-departure',
'fas fa-play',
'fas fa-play-circle',
'fas fa-plug',
'fas fa-plus',
'fas fa-plus-circle',
'fas fa-plus-square',
'fas fa-podcast',
'fas fa-poll',
'fas fa-poll-h',
'fas fa-poo',
'fas fa-poo-storm',
'fas fa-poop',
'fas fa-portrait',
'fas fa-pound-sign',
'fas fa-power-off',
'fas fa-pray',
'fas fa-praying-hands',
'fas fa-prescription',
'fas fa-prescription-bottle',
'fas fa-prescription-bottle-alt',
'fas fa-print',
'fas fa-procedures',
'fas fa-project-diagram',
'fas fa-puzzle-piece',
'fas fa-qrcode',
'fas fa-question',
'fas fa-question-circle',
'fas fa-quidditch',
'fas fa-quote-left',
'fas fa-quote-right',
'fas fa-quran',
'fas fa-radiation',
'fas fa-radiation-alt',
'fas fa-rainbow',
'fas fa-random',
'fas fa-receipt',
'fas fa-record-vinyl',
'fas fa-recycle',
'fas fa-redo',
'fas fa-redo-alt',
'fas fa-registered',
'fas fa-remove-format',
'fas fa-reply',
'fas fa-reply-all',
'fas fa-republican',
'fas fa-restroom',
'fas fa-retweet',
'fas fa-ribbon',
'fas fa-ring',
'fas fa-road',
'fas fa-robot',
'fas fa-rocket',
'fas fa-route',
'fas fa-rss',
'fas fa-rss-square',
'fas fa-ruble-sign',
'fas fa-ruler',
'fas fa-ruler-combined',
'fas fa-ruler-horizontal',
'fas fa-ruler-vertical',
'fas fa-running',
'fas fa-rupee-sign',
'fas fa-sad-cry',
'fas fa-sad-tear',
'fas fa-satellite',
'fas fa-satellite-dish',
'fas fa-save',
'fas fa-school',
'fas fa-screwdriver',
'fas fa-scroll',
'fas fa-sd-card',
'fas fa-search',
'fas fa-search-dollar',
'fas fa-search-location',
'fas fa-search-minus',
'fas fa-search-plus',
'fas fa-seedling',
'fas fa-server',
'fas fa-shapes',
'fas fa-share',
'fas fa-share-alt',
'fas fa-share-alt-square',
'fas fa-share-square',
'fas fa-shekel-sign',
'fas fa-shield-alt',
'fas fa-ship',
'fas fa-shipping-fast',
'fas fa-shoe-prints',
'fas fa-shopping-bag',
'fas fa-shopping-basket',
'fas fa-shopping-cart',
'fas fa-shower',
'fas fa-shuttle-van',
'fas fa-sign',
'fas fa-sign-in-alt',
'fas fa-sign-language',
'fas fa-sign-out-alt',
'fas fa-signal',
'fas fa-signature',
'fas fa-sim-card',
'fas fa-sitemap',
'fas fa-skating',
'fas fa-skiing',
'fas fa-skiing-nordic',
'fas fa-skull',
'fas fa-skull-crossbones',
'fas fa-slash',
'fas fa-sleigh',
'fas fa-sliders-h',
'fas fa-smile',
'fas fa-smile-beam',
'fas fa-smile-wink',
'fas fa-smog',
'fas fa-smoking',
'fas fa-smoking-ban',
'fas fa-sms',
'fas fa-snowboarding',
'fas fa-snowflake',
'fas fa-snowman',
'fas fa-snowplow',
'fas fa-socks',
'fas fa-solar-panel',
'fas fa-sort',
'fas fa-sort-alpha-down',
'fas fa-sort-alpha-down-alt',
'fas fa-sort-alpha-up',
'fas fa-sort-alpha-up-alt',
'fas fa-sort-amount-down',
'fas fa-sort-amount-down-alt',
'fas fa-sort-amount-up',
'fas fa-sort-amount-up-alt',
'fas fa-sort-down',
'fas fa-sort-numeric-down',
'fas fa-sort-numeric-down-alt',
'fas fa-sort-numeric-up',
'fas fa-sort-numeric-up-alt',
'fas fa-sort-up',
'fas fa-spa',
'fas fa-space-shuttle',
'fas fa-spell-check',
'fas fa-spider',
'fas fa-spinner',
'fas fa-splotch',
'fas fa-spray-can',
'fas fa-square',
'fas fa-square-full',
'fas fa-square-root-alt',
'fas fa-stamp',
'fas fa-star',
'fas fa-star-and-crescent',
'fas fa-star-half',
'fas fa-star-half-alt',
'fas fa-star-of-david',
'fas fa-star-of-life',
'fas fa-step-backward',
'fas fa-step-forward',
'fas fa-stethoscope',
'fas fa-sticky-note',
'fas fa-stop',
'fas fa-stop-circle',
'fas fa-stopwatch',
'fas fa-store',
'fas fa-store-alt',
'fas fa-stream',
'fas fa-street-view',
'fas fa-strikethrough',
'fas fa-stroopwafel',
'fas fa-subscript',
'fas fa-subway',
'fas fa-suitcase',
'fas fa-suitcase-rolling',
'fas fa-sun',
'fas fa-superscript',
'fas fa-surprise',
'fas fa-swatchbook',
'fas fa-swimmer',
'fas fa-swimming-pool',
'fas fa-synagogue',
'fas fa-sync',
'fas fa-sync-alt',
'fas fa-syringe',
'fas fa-table',
'fas fa-table-tennis',
'fas fa-tablet',
'fas fa-tablet-alt',
'fas fa-tablets',
'fas fa-tachometer-alt',
'fas fa-tag',
'fas fa-tags',
'fas fa-tape',
'fas fa-tasks',
'fas fa-taxi',
'fas fa-teeth',
'fas fa-teeth-open',
'fas fa-temperature-high',
'fas fa-temperature-low',
'fas fa-tenge',
'fas fa-terminal',
'fas fa-text-height',
'fas fa-text-width',
'fas fa-th',
'fas fa-th-large',
'fas fa-th-list',
'fas fa-theater-masks',
'fas fa-thermometer',
'fas fa-thermometer-empty',
'fas fa-thermometer-full',
'fas fa-thermometer-half',
'fas fa-thermometer-quarter',
'fas fa-thermometer-three-quarters',
'fas fa-thumbs-down',
'fas fa-thumbs-up',
'fas fa-thumbtack',
'fas fa-ticket-alt',
'fas fa-times',
'fas fa-times-circle',
'fas fa-tint',
'fas fa-tint-slash',
'fas fa-tired',
'fas fa-toggle-off',
'fas fa-toggle-on',
'fas fa-toilet',
'fas fa-toilet-paper',
'fas fa-toolbox',
'fas fa-tools',
'fas fa-tooth',
'fas fa-torah',
'fas fa-torii-gate',
'fas fa-tractor',
'fas fa-trademark',
'fas fa-traffic-light',
'fas fa-train',
'fas fa-tram',
'fas fa-transgender',
'fas fa-transgender-alt',
'fas fa-trash',
'fas fa-trash-alt',
'fas fa-trash-restore',
'fas fa-trash-restore-alt',
'fas fa-tree',
'fas fa-trophy',
'fas fa-truck',
'fas fa-truck-loading',
'fas fa-truck-monster',
'fas fa-truck-moving',
'fas fa-truck-pickup',
'fas fa-tshirt',
'fas fa-tty',
'fas fa-tv',
'fas fa-umbrella',
'fas fa-umbrella-beach',
'fas fa-underline',
'fas fa-undo',
'fas fa-undo-alt',
'fas fa-universal-access',
'fas fa-university',
'fas fa-unlink',
'fas fa-unlock',
'fas fa-unlock-alt',
'fas fa-upload',
'fas fa-user',
'fas fa-user-alt',
'fas fa-user-alt-slash',
'fas fa-user-astronaut',
'fas fa-user-check',
'fas fa-user-circle',
'fas fa-user-clock',
'fas fa-user-cog',
'fas fa-user-edit',
'fas fa-user-friends',
'fas fa-user-graduate',
'fas fa-user-injured',
'fas fa-user-lock',
'fas fa-user-md',
'fas fa-user-minus',
'fas fa-user-ninja',
'fas fa-user-nurse',
'fas fa-user-plus',
'fas fa-user-secret',
'fas fa-user-shield',
'fas fa-user-slash',
'fas fa-user-tag',
'fas fa-user-tie',
'fas fa-user-times',
'fas fa-users',
'fas fa-users-cog',
'fas fa-utensil-spoon',
'fas fa-utensils',
'fas fa-vector-square',
'fas fa-venus',
'fas fa-venus-double',
'fas fa-venus-mars',
'fas fa-vial',
'fas fa-vials',
'fas fa-video',
'fas fa-video-slash',
'fas fa-vihara',
'fas fa-voicemail',
'fas fa-volleyball-ball',
'fas fa-volume-down',
'fas fa-volume-mute',
'fas fa-volume-off',
'fas fa-volume-up',
'fas fa-vote-yea',
'fas fa-vr-cardboard',
'fas fa-walking',
'fas fa-wallet',
'fas fa-warehouse',
'fas fa-water',
'fas fa-wave-square',
'fas fa-weight',
'fas fa-weight-hanging',
'fas fa-wheelchair',
'fas fa-wifi',
'fas fa-wind',
'fas fa-window-close',
'fas fa-window-maximize',
'fas fa-window-minimize',
'fas fa-window-restore',
'fas fa-wine-bottle',
'fas fa-wine-glass',
'fas fa-wine-glass-alt',
'fas fa-won-sign',
'fas fa-wrench',
'fas fa-x-ray',
'fas fa-yen-sign',
'fas fa-yin-yang',
'fa fa-address-book',
'fa fa-address-card',
'far fa-address-book',
'far fa-address-card',
'far fa-angry',
'far fa-arrow-alt-circle-down',
'far fa-arrow-alt-circle-left',
'far fa-arrow-alt-circle-right',
'far fa-arrow-alt-circle-up',
'far fa-bell',
'far fa-bell-slash',
'far fa-bookmark',
'far fa-building',
'far fa-calendar',
'far fa-calendar-alt',
'far fa-calendar-check',
'far fa-calendar-minus',
'far fa-calendar-plus',
'far fa-calendar-times',
'far fa-caret-square-down',
'far fa-caret-square-left',
'far fa-caret-square-right',
'far fa-caret-square-up',
'far fa-chart-bar',
'far fa-check-circle',
'far fa-check-square',
'far fa-circle',
'far fa-clipboard',
'far fa-clock',
'far fa-clone',
'far fa-closed-captioning',
'far fa-comment',
'far fa-comment-alt',
'far fa-comment-dots',
'far fa-comments',
'far fa-compass',
'far fa-copy',
'far fa-copyright',
'far fa-credit-card',
'far fa-dizzy',
'far fa-dot-circle',
'far fa-edit',
'far fa-envelope',
'far fa-envelope-open',
'far fa-eye',
'far fa-eye-slash',
'far fa-file',
'far fa-file-alt',
'far fa-file-archive',
'far fa-file-audio',
'far fa-file-code',
'far fa-file-excel',
'far fa-file-image',
'far fa-file-pdf',
'far fa-file-powerpoint',
'far fa-file-video',
'far fa-file-word',
'far fa-flag',
'far fa-flushed',
'far fa-folder',
'far fa-folder-open',
'far fa-frown',
'far fa-frown-open',
'far fa-futbol',
'far fa-gem',
'far fa-grimace',
'far fa-grin',
'far fa-grin-alt',
'far fa-grin-beam',
'far fa-grin-beam-sweat',
'far fa-grin-hearts',
'far fa-grin-squint',
'far fa-grin-squint-tears',
'far fa-grin-stars',
'far fa-grin-tears',
'far fa-grin-tongue',
'far fa-grin-tongue-squint',
'far fa-grin-tongue-wink',
'far fa-grin-wink',
'far fa-hand-lizard',
'far fa-hand-paper',
'far fa-hand-peace',
'far fa-hand-point-down',
'far fa-hand-point-left',
'far fa-hand-point-right',
'far fa-hand-point-up',
'far fa-hand-pointer',
'far fa-hand-rock',
'far fa-hand-scissors',
'far fa-hand-spock',
'far fa-handshake',
'far fa-hdd',
'far fa-heart',
'far fa-hospital',
'far fa-hourglass',
'far fa-id-badge',
'far fa-id-card',
'far fa-image',
'far fa-images',
'far fa-keyboard',
'far fa-kiss',
'far fa-kiss-beam',
'far fa-kiss-wink-heart',
'far fa-laugh',
'far fa-laugh-beam',
'far fa-laugh-squint',
'far fa-laugh-wink',
'far fa-lemon',
'far fa-life-ring',
'far fa-lightbulb',
'far fa-list-alt',
'far fa-map',
'far fa-meh',
'far fa-meh-blank',
'far fa-meh-rolling-eyes',
'far fa-minus-square',
'far fa-money-bill-alt',
'far fa-moon',
'far fa-newspaper',
'far fa-object-group',
'far fa-object-ungroup',
'far fa-paper-plane',
'far fa-pause-circle',
'far fa-play-circle',
'far fa-plus-square',
'far fa-question-circle',
'far fa-registered',
'far fa-sad-cry',
'far fa-sad-tear',
'far fa-save',
'far fa-share-square',
'far fa-smile',
'far fa-smile-beam',
'far fa-smile-wink',
'far fa-snowflake',
'far fa-square',
'far fa-star',
'far fa-star-half',
'far fa-sticky-note',
'far fa-stop-circle',
'far fa-sun',
'far fa-surprise',
'far fa-thumbs-down',
'far fa-thumbs-up',
'far fa-times-circle',
'far fa-tired',
'far fa-trash-alt',
'far fa-user',
'far fa-user-circle',
'far fa-window-close',
'far fa-window-maximize',
'far fa-window-minimize',
'far fa-window-restore',
'fab fa-500px',
'fab fa-accessible-icon',
'fab fa-accusoft',
'fab fa-acquisitions-incorporated',
'fab fa-adn',
'fab fa-adobe',
'fab fa-adversal',
'fab fa-affiliatetheme',
'fab fa-airbnb',
'fab fa-algolia',
'fab fa-alipay',
'fab fa-amazon',
'fab fa-amazon-pay',
'fab fa-amilia',
'fab fa-android',
'fab fa-angellist',
'fab fa-angrycreative',
'fab fa-angular',
'fab fa-app-store',
'fab fa-app-store-ios',
'fab fa-apper',
'fab fa-apple',
'fab fa-apple-pay',
'fab fa-artstation',
'fab fa-asymmetrik',
'fab fa-atlassian',
'fab fa-audible',
'fab fa-autoprefixer',
'fab fa-avianex',
'fab fa-aviato',
'fab fa-aws',
'fab fa-bandcamp',
'fab fa-battle-net',
'fab fa-behance',
'fab fa-behance-square',
'fab fa-bimobject',
'fab fa-bitbucket',
'fab fa-bitcoin',
'fab fa-bity',
'fab fa-black-tie',
'fab fa-blackberry',
'fab fa-blogger',
'fab fa-blogger-b',
'fab fa-bluetooth',
'fab fa-bluetooth-b',
'fab fa-bootstrap',
'fab fa-btc',
'fab fa-buffer',
'fab fa-buromobelexperte',
'fab fa-buy-n-large',
'fab fa-buysellads',
'fab fa-canadian-maple-leaf',
'fab fa-cc-amazon-pay',
'fab fa-cc-amex',
'fab fa-cc-apple-pay',
'fab fa-cc-diners-club',
'fab fa-cc-discover',
'fab fa-cc-jcb',
'fab fa-cc-mastercard',
'fab fa-cc-paypal',
'fab fa-cc-stripe',
'fab fa-cc-visa',
'fab fa-centercode',
'fab fa-centos',
'fab fa-chrome',
'fab fa-chromecast',
'fab fa-cloudscale',
'fab fa-cloudsmith',
'fab fa-cloudversify',
'fab fa-codepen',
'fab fa-codiepie',
'fab fa-confluence',
'fab fa-connectdevelop',
'fab fa-contao',
'fab fa-cotton-bureau',
'fab fa-cpanel',
'fab fa-creative-commons',
'fab fa-creative-commons-by',
'fab fa-creative-commons-nc',
'fab fa-creative-commons-nc-eu',
'fab fa-creative-commons-nc-jp',
'fab fa-creative-commons-nd',
'fab fa-creative-commons-pd',
'fab fa-creative-commons-pd-alt',
'fab fa-creative-commons-remix',
'fab fa-creative-commons-sa',
'fab fa-creative-commons-sampling',
'fab fa-creative-commons-sampling-plus',
'fab fa-creative-commons-share',
'fab fa-creative-commons-zero',
'fab fa-critical-role',
'fab fa-css3',
'fab fa-css3-alt',
'fab fa-cuttlefish',
'fab fa-d-and-d',
'fab fa-d-and-d-beyond',
'fab fa-dashcube',
'fab fa-delicious',
'fab fa-deploydog',
'fab fa-deskpro',
'fab fa-dev',
'fab fa-deviantart',
'fab fa-dhl',
'fab fa-diaspora',
'fab fa-digg',
'fab fa-digital-ocean',
'fab fa-discord',
'fab fa-discourse',
'fab fa-dochub',
'fab fa-docker',
'fab fa-draft2digital',
'fab fa-dribbble',
'fab fa-dribbble-square',
'fab fa-dropbox',
'fab fa-drupal',
'fab fa-dyalog',
'fab fa-earlybirds',
'fab fa-ebay',
'fab fa-edge',
'fab fa-elementor',
'fab fa-ello',
'fab fa-ember',
'fab fa-empire',
'fab fa-envira',
'fab fa-erlang',
'fab fa-ethereum',
'fab fa-etsy',
'fab fa-evernote',
'fab fa-expeditedssl',
'fab fa-facebook',
'fab fa-facebook-f',
'fab fa-facebook-messenger',
'fab fa-facebook-square',
'fab fa-fantasy-flight-games',
'fab fa-fedex',
'fab fa-fedora',
'fab fa-figma',
'fab fa-firefox',
'fab fa-first-order',
'fab fa-first-order-alt',
'fab fa-firstdraft',
'fab fa-flickr',
'fab fa-flipboard',
'fab fa-fly',
'fab fa-font-awesome',
'fab fa-font-awesome-alt',
'fab fa-font-awesome-flag',
'fab fa-fonticons',
'fab fa-fonticons-fi',
'fab fa-fort-awesome',
'fab fa-fort-awesome-alt',
'fab fa-forumbee',
'fab fa-foursquare',
'fab fa-free-code-camp',
'fab fa-freebsd',
'fab fa-fulcrum',
'fab fa-galactic-republic',
'fab fa-galactic-senate',
'fab fa-get-pocket',
'fab fa-gg',
'fab fa-gg-circle',
'fab fa-git',
'fab fa-git-alt',
'fab fa-git-square',
'fab fa-github',
'fab fa-github-alt',
'fab fa-github-square',
'fab fa-gitkraken',
'fab fa-gitlab',
'fab fa-gitter',
'fab fa-glide',
'fab fa-glide-g',
'fab fa-gofore',
'fab fa-goodreads',
'fab fa-goodreads-g',
'fab fa-google',
'fab fa-google-drive',
'fab fa-google-play',
'fab fa-google-plus',
'fab fa-google-plus-g',
'fab fa-google-plus-square',
'fab fa-google-wallet',
'fab fa-gratipay',
'fab fa-grav',
'fab fa-gripfire',
'fab fa-grunt',
'fab fa-gulp',
'fab fa-hacker-news',
'fab fa-hacker-news-square',
'fab fa-hackerrank',
'fab fa-hips',
'fab fa-hire-a-helper',
'fab fa-hooli',
'fab fa-hornbill',
'fab fa-hotjar',
'fab fa-houzz',
'fab fa-html5',
'fab fa-hubspot',
'fab fa-imdb',
'fab fa-instagram',
'fab fa-intercom',
'fab fa-internet-explorer',
'fab fa-invision',
'fab fa-ioxhost',
'fab fa-itch-io',
'fab fa-itunes',
'fab fa-itunes-note',
'fab fa-java',
'fab fa-jedi-order',
'fab fa-jenkins',
'fab fa-jira',
'fab fa-joget',
'fab fa-joomla',
'fab fa-js',
'fab fa-js-square',
'fab fa-jsfiddle',
'fab fa-kaggle',
'fab fa-keybase',
'fab fa-keycdn',
'fab fa-kickstarter',
'fab fa-kickstarter-k',
'fab fa-korvue',
'fab fa-laravel',
'fab fa-lastfm',
'fab fa-lastfm-square',
'fab fa-leanpub',
'fab fa-less',
'fab fa-line',
'fab fa-linkedin',
'fab fa-linkedin-in',
'fab fa-linode',
'fab fa-linux',
'fab fa-lyft',
'fab fa-magento',
'fab fa-mailchimp',
'fab fa-mandalorian',
'fab fa-markdown',
'fab fa-mastodon',
'fab fa-maxcdn',
'fab fa-mdb',
'fab fa-medapps',
'fab fa-medium',
'fab fa-medium-m',
'fab fa-medrt',
'fab fa-meetup',
'fab fa-megaport',
'fab fa-mendeley',
'fab fa-microsoft',
'fab fa-mix',
'fab fa-mixcloud',
'fab fa-mizuni',
'fab fa-modx',
'fab fa-monero',
'fab fa-napster',
'fab fa-neos',
'fab fa-nimblr',
'fab fa-node',
'fab fa-node-js',
'fab fa-npm',
'fab fa-ns8',
'fab fa-nutritionix',
'fab fa-odnoklassniki',
'fab fa-odnoklassniki-square',
'fab fa-old-republic',
'fab fa-opencart',
'fab fa-openid',
'fab fa-opera',
'fab fa-optin-monster',
'fab fa-orcid',
'fab fa-osi',
'fab fa-page4',
'fab fa-pagelines',
'fab fa-palfed',
'fab fa-patreon',
'fab fa-paypal',
'fab fa-penny-arcade',
'fab fa-periscope',
'fab fa-phabricator',
'fab fa-phoenix-framework',
'fab fa-phoenix-squadron',
'fab fa-php',
'fab fa-pied-piper',
'fab fa-pied-piper-alt',
'fab fa-pied-piper-hat',
'fab fa-pied-piper-pp',
'fab fa-pinterest',
'fab fa-pinterest-p',
'fab fa-pinterest-square',
'fab fa-playstation',
'fab fa-product-hunt',
'fab fa-pushed',
'fab fa-python',
'fab fa-qq',
'fab fa-quinscape',
'fab fa-quora',
'fab fa-r-project',
'fab fa-raspberry-pi',
'fab fa-ravelry',
'fab fa-react',
'fab fa-reacteurope',
'fab fa-readme',
'fab fa-rebel',
'fab fa-red-river',
'fab fa-reddit',
'fab fa-reddit-alien',
'fab fa-reddit-square',
'fab fa-redhat',
'fab fa-renren',
'fab fa-replyd',
'fab fa-researchgate',
'fab fa-resolving',
'fab fa-rev',
'fab fa-rocketchat',
'fab fa-rockrms',
'fab fa-safari',
'fab fa-salesforce',
'fab fa-sass',
'fab fa-schlix',
'fab fa-scribd',
'fab fa-searchengin',
'fab fa-sellcast',
'fab fa-sellsy',
'fab fa-servicestack',
'fab fa-shirtsinbulk',
'fab fa-shopware',
'fab fa-simplybuilt',
'fab fa-sistrix',
'fab fa-sith',
'fab fa-sketch',
'fab fa-skyatlas',
'fab fa-skype',
'fab fa-slack',
'fab fa-slack-hash',
'fab fa-slideshare',
'fab fa-snapchat',
'fab fa-snapchat-ghost',
'fab fa-snapchat-square',
'fab fa-soundcloud',
'fab fa-sourcetree',
'fab fa-speakap',
'fab fa-speaker-deck',
'fab fa-spotify',
'fab fa-squarespace',
'fab fa-stack-exchange',
'fab fa-stack-overflow',
'fab fa-stackpath',
'fab fa-staylinked',
'fab fa-steam',
'fab fa-steam-square',
'fab fa-steam-symbol',
'fab fa-sticker-mule',
'fab fa-strava',
'fab fa-stripe',
'fab fa-stripe-s',
'fab fa-studiovinari',
'fab fa-stumbleupon',
'fab fa-stumbleupon-circle',
'fab fa-superpowers',
'fab fa-supple',
'fab fa-suse',
'fab fa-swift',
'fab fa-symfony',
'fab fa-teamspeak',
'fab fa-telegram',
'fab fa-telegram-plane',
'fab fa-tencent-weibo',
'fab fa-the-red-yeti',
'fab fa-themeco',
'fab fa-themeisle',
'fab fa-think-peaks',
'fab fa-trade-federation',
'fab fa-trello',
'fab fa-tripadvisor',
'fab fa-tumblr',
'fab fa-tumblr-square',
'fab fa-twitch',
'fab fa-twitter',
'fab fa-twitter-square',
'fab fa-typo3',
'fab fa-uber',
'fab fa-ubuntu',
'fab fa-uikit',
'fab fa-umbraco',
'fab fa-uniregistry',
'fab fa-untappd',
'fab fa-ups',
'fab fa-usb',
'fab fa-usps',
'fab fa-ussunnah',
'fab fa-vaadin',
'fab fa-viacoin',
'fab fa-viadeo',
'fab fa-viadeo-square',
'fab fa-viber',
'fab fa-vimeo',
'fab fa-vimeo-square',
'fab fa-vimeo-v',
'fab fa-vine',
'fab fa-vk',
'fab fa-vnv',
'fab fa-vuejs',
'fab fa-waze',
'fab fa-weebly',
'fab fa-weibo',
'fab fa-weixin',
'fab fa-whatsapp',
'fab fa-whatsapp-square',
'fab fa-whmcs',
'fab fa-wikipedia-w',
'fab fa-windows',
'fab fa-wix',
'fab fa-wizards-of-the-coast',
'fab fa-wolf-pack-battalion',
'fab fa-wordpress',
'fab fa-wordpress-simple',
'fab fa-wpbeginner',
'fab fa-wpexplorer',
'fab fa-wpforms',
'fab fa-wpressr',
'fab fa-xbox',
'fab fa-xing',
'fab fa-xing-square',
'fab fa-y-combinator',
'fab fa-yahoo',
'fab fa-yammer',
'fab fa-yandex',
'fab fa-yandex-international',
'fab fa-yarn',
'fab fa-yelp',
'fab fa-yoast',
'fab fa-youtube',
'fab fa-youtube-square',
'fab fa-zhihu'
    ].concat(icons),

    icon_sets: { 
      'glyphicon' : 'Glyphicon Halflings', 
      'et' : 'ET Line Icons', 
      'mat' : 'Google Material Design', 
      'fa ' : 'Font Awesome 4', 
      'fas' : 'Font Awesome 5 Pro Solid', 
      'fal' : 'Font Awesome 5 Pro Light', 
      'far' : 'Font Awesome 5 Pro Regular', 
      'fab' : 'Font Awesome 5 Pro Brands', 
      'pe' : 'Pixeden Line Icons' 
     }, 

    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' + this.description +
        '</p></div>');
    },
    opened: function() {
      var icons = $('<div class="icons loading"></div>').appendTo(this.dom_element);
      glazed_add_css('css/fontawesome.css', function() {
        icons.removeClass('loading');
      }); 
      var $iconFilters = $('<div class="az-icon-filters clearfix"><input type="search" class="cb-search-icon pull-left" placeholder="' + Drupal.t('Search icon') + '"/></div>');
      $iconFilters.insertBefore(icons);

      var $iconsWrapper = $('<div class="cb-icons-wrapper"/>').appendTo(icons);
      var icon = '';
      for (var i = 0; i < this.icons.length; i++) {
        if (this.icons[i][0] == 'm') {
          icon = '<span class="' + this.icons[i] + ' ui-selectee">' + this.icons[i].replace('material-icons mat-', '') + '</span>';
        }
        else {
          icon = '<span class="' + this.icons[i] + ' ui-selectee"></span>';
        }
        $iconsWrapper.append(icon);
      }

      var $icons = $iconsWrapper.find('span');
      $iconFilters.find('input').on('input', _.debounce(function() {
        var searchKey = $(this).val();
        if (searchKey == '') {
          $icons.removeClass('az-search-hide');
        }
        else {
          $icons
            .removeClass('az-search-hide')
            .each(function() {
              var $icon = $(this);
              var classes = $icon.attr('class').replace('ui-selectee', '');
              $icon.toggleClass('az-search-hide', classes.indexOf(searchKey) == -1);
          })
        }
      }, 200));

      $iconsWrapper.find('.ui-selectee').click(function() {
        $iconsWrapper.find('.ui-selected').removeClass('ui-selected');
        $(this).toggleClass('ui-selected');
        var icon = $(this).attr('class').replace(/ ui-selected| ui-selectee| az-filter-hide| az-search-hide/gi, '');
        $(param.dom_element).find('input[name="' + param.param_name + '"]').val($.trim(icon));
      });

      var param = this;
      if (this.get_value() != '')
        $(icons).find('.' + $.trim(this.get_value()).replace(/ /g, '.')).addClass("ui-selected");
    },
  },

  {
    type: 'image',
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control glazed-builder-image-input" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' + this.description +
        '</p></div>');
    },
    opened: function() {
      image_select($(this.dom_element).find('input[name="' + this.param_name + '"]'));
    },
  },

  {
    type: 'images',
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control glazed-builder-image-input glazed-builder-multi-image-input" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' + this.description +
        '</p></div>');
    },
    opened: function() {
      images_select($(this.dom_element).find('input[name="' + this.param_name + '"]'), ',');
    },
  },

  {
    type: 'file',
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control glazed-builder-file-input" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' + this.description +
        '</p></div>');
    },
    opened: function() {
      file_select($(this.dom_element).find('input[name="' + this.param_name + '"]'));
    },
  },

  {
    type: 'integer_slider',
    create: function() {
      this.min = 0;
      this.max = 100;
      this.step = 1;
    },
    get_value: function() {
      var v = $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
      return (v == '') ? NaN : parseFloat(v).toString();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><div class="slider"></div><p class="help-block">' +
        this.description + '</p></div>');
    },
    opened: function() {
      nouislider($(this.dom_element).find('.slider'), this.min, this.max, this.get_value(), this.step, $(this.dom_element)
        .find('input[name="' + this.param_name + '"]'));
    },
  },

  {
    type: 'javascript',
    safe: false,
    get_value: function() {
      return $(this.dom_element).find('#' + this.id).val();
    },
    opened: function() {
      var param = this;
      glazed_add_js({
        path: 'vendor/ace/ace.js',
        callback: function() {
          var aceeditor = ace.edit(param.id);
          aceeditor.setTheme("ace/theme/chrome");
          aceeditor.getSession().setMode("ace/mode/javascript");
          aceeditor.setOptions({
            minLines: 10,
            maxLines: 30,
          });
          $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
          aceeditor.on(
            'change',
            function(e) {
              $(param.dom_element).find('#' + param.id).val(aceeditor.getSession().getValue());
              aceeditor.resize();
            }
          );
        }
      });
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading + '</label><div id="' +
        this.id + '"><textarea class="form-control" rows="10" cols="45" name="' + this.param_name +
        '" ">' + value + '</textarea></div><p class="help-block">' + this.description + '</p></div>'
      );
    },
  },

  {
    type: 'link',
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '"></div><p class="help-block">' + this.description +
        '</p></div>');
    },
  },

  {
    type: 'links',
    get_value: function() {
      return $(this.dom_element).find('#' + this.id).val();
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><textarea id="' + this.id + '" class="form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value +
        '</textarea></div><p class="help-block">' + this.description + '</p></div>');
    },
  },

  {
    type: 'rawtext',
    safe: false,
    get_value: function() {
      return $(this.dom_element).find('#' + this.id).val();
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><textarea id="' + this.id + '" class="form-control" rows="10" cols="45" name="' + this.param_name + '" ">' + value +
        '</textarea></div><p class="help-block">' + this.description + '</p></div>');
    },
  },

  {
    type: 'saved_datetime',
    get_value: function() {
      return (new Date).toUTCString();
    },
  },

  {
    type: 'style',
    create: function() {
      this.important = false;
    },
    get_value: function() {
      var imp = '';
      if (this.important) {
        imp = ' !important';
      }
      var style = '';
      var margin_top = $(this.dom_element).find('[name="margin_top"]').val();
      if (margin_top != '') {
        if ($.isNumeric(margin_top))
          margin_top = margin_top + 'px';
        style += 'margin-top:' + margin_top + imp + ';';
      }
      var margin_bottom = $(this.dom_element).find('[name="margin_bottom"]').val();
      if (margin_bottom != '') {
        if ($.isNumeric(margin_bottom))
          margin_bottom = margin_bottom + 'px';
        style += 'margin-bottom:' + margin_bottom + imp + ';';
      }
      var margin_left = $(this.dom_element).find('[name="margin_left"]').val();
      if (margin_left != '') {
        if ($.isNumeric(margin_left))
          margin_left = margin_left + 'px';
        style += 'margin-left:' + margin_left + imp + ';';
      }
      var margin_right = $(this.dom_element).find('[name="margin_right"]').val();
      if (margin_right != '') {
        if ($.isNumeric(margin_right))
          margin_right = margin_right + 'px';
        style += 'margin-right:' + margin_right + imp + ';';
      }
      var border_top_width = $(this.dom_element).find('[name="border_top_width"]').val();
      if (border_top_width != '') {
        if ($.isNumeric(border_top_width))
          border_top_width = border_top_width + 'px';
        style += 'border-top-width:' + border_top_width + imp + ';';
      }
      var border_bottom_width = $(this.dom_element).find('[name="border_bottom_width"]').val();
      if (border_bottom_width != '') {
        if ($.isNumeric(border_bottom_width))
          border_bottom_width = border_bottom_width + 'px';
        style += 'border-bottom-width:' + border_bottom_width + imp + ';';
      }
      var border_left_width = $(this.dom_element).find('[name="border_left_width"]').val();
      if (border_left_width != '') {
        if ($.isNumeric(border_left_width))
          border_left_width = border_left_width + 'px';
        style += 'border-left-width:' + border_left_width + imp + ';';
      }
      var border_right_width = $(this.dom_element).find('[name="border_right_width"]').val();
      if (border_right_width != '') {
        if ($.isNumeric(border_right_width))
          border_right_width = border_right_width + 'px';
        style += 'border-right-width:' + border_right_width + imp + ';';
      }
      var padding_top = $(this.dom_element).find('[name="padding_top"]').val();
      if (padding_top != '') {
        if ($.isNumeric(padding_top))
          padding_top = padding_top + 'px';
        style += 'padding-top:' + padding_top + imp + ';';
      }
      var padding_bottom = $(this.dom_element).find('[name="padding_bottom"]').val();
      if (padding_bottom != '') {
        if ($.isNumeric(padding_bottom))
          padding_bottom = padding_bottom + 'px';
        style += 'padding-bottom:' + padding_bottom + imp + ';';
      }
      var padding_left = $(this.dom_element).find('[name="padding_left"]').val();
      if (padding_left != '') {
        if ($.isNumeric(padding_left))
          padding_left = padding_left + 'px';
        style += 'padding-left:' + padding_left + imp + ';';
      }
      var padding_right = $(this.dom_element).find('[name="padding_right"]').val();
      if (padding_right != '') {
        if ($.isNumeric(padding_right))
          padding_right = padding_right + 'px';
        style += 'padding-right:' + padding_right + imp + ';';
      }
      var color = $(this.dom_element).find('#' + this.color_id).val();
      if (color != '') {
        style += 'color:' + color + imp + ';';
      }
      var fontsize = $(this.dom_element).find('[name="fontsize"]').val();
      if (fontsize != 0) {
        if ($.isNumeric(fontsize))
          fontsize = Math.round(fontsize) + 'px';
        style += 'font-size:' + fontsize + imp + ';';
      }
      var border_color = $(this.dom_element).find('#' + this.border_color_id).val();
      if (border_color != '') {
        style += 'border-color:' + border_color + imp + ';';
      }
      var border_radius = $(this.dom_element).find('[name="border_radius"]').val();
      if (border_radius != 0) {
        if ($.isNumeric(border_radius))
          border_radius = Math.round(border_radius) + 'px';
        style += 'border-radius:' + border_radius + imp + ';';
      }
      var border_style = $(this.dom_element).find('select[name="border_style"] > option:selected').val();
      if (border_style != '') {
        style += 'border-style:' + border_style + imp + ';';
      }
      var bg_color = $(this.dom_element).find('#' + this.bg_color_id).val();
      if (bg_color) {
        style += 'background-color:' + bg_color + imp + ';';
      }
      var bg_image = $(this.dom_element).find('[name="bg_image"]').val();
      if (bg_image) {
        style += 'background-image: url(' + encodeURI(bg_image) + ');';
      }
      var background_style = $(this.dom_element).find('select[name="background_style"] > option:selected').val();
      if (background_style.match(/repeat/)) {
        style += 'background-repeat: ' + background_style + imp + ';';
      }
      else if (background_style.match(/cover|contain/)) {
        style += 'background-repeat: no-repeat;';
        style += 'background-size: ' + background_style + imp + ';';
      }
      var background_position = $(this.dom_element).find('select[name="background_position"] > option:selected').val();
      if (background_position != '') {
        style += 'background-position:' + background_position + imp + ';';
      }
      var opacity = $(this.dom_element).find('[name="opacity"]').val();
      if (opacity != 0) {
        style += 'opacity:' + opacity + imp + ';';
      }
      return style;
    },
    render: function(value) {
      value = value.replace(/!important/g, '');
      var output = '<div class="style">';
      var match = null;
      var v = '';
      output += '<div class="layout pane-1">';
      output += '<div class="margin"><label>' + Drupal.t('Margin') + '</label>';
      match = value.match(/margin-top[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="margin_top" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/margin-bottom[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="margin_bottom" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/margin-left[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="margin_left" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/margin-right[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="margin_right" type="text" placeholder="-" value="' + v + '">';
      output += '<div class="border"><label>' + Drupal.t('Border') + '</label>';
      match = value.match(/border-top-width[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="border_top_width" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/border-bottom-width[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="border_bottom_width" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/border-left-width[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="border_left_width" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/border-right-width[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="border_right_width" type="text" placeholder="-" value="' + v + '">';
      output += '<div class="padding"><label>' + Drupal.t('Padding') + '</label>';
      match = value.match(/padding-top[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="padding_top" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/padding-bottom[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="padding_bottom" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/padding-left[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="padding_left" type="text" placeholder="-" value="' + v + '">';
      match = value.match(/padding-right[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<input name="padding_right" type="text" placeholder="-" value="' + v + '">';
      output += '<div class="content">';
      output += '</div></div></div></div>';
      output += '</div>';
      output += '<div class="settings pane-2">';
      output += '<div class="font form-group"><label>' + Drupal.t('Font color') + '</label>';
      this.color_id = 'gb' + _.uniqueId();
      match = value.match(/(^| |;)color[: ]*\s?([^;]{4,}) *;/);
      if (match == null)
        v = '';
      else
        v = match[2];
      output += '<div><input id="' + this.color_id + '" name="color" type="text" value="' + v + '"></div></div>';
      output += '<div class="border form-group"><label>' + Drupal.t('Border color') + '</label>';
      this.border_color_id = 'gb' + _.uniqueId();
      match = value.match(/border-color[: ]*\s?([^;]{4,}) *;/);
      if (match == null)
        v = '';
      else
        v = match[1];
      output += '<div><input id="' + this.border_color_id + '" name="border_color" type="text" value="' + v +
        '"></div></div>';
      output += '<div class="background form-group"><label>' + Drupal.t('Background') + '</label>';
      this.bg_color_id = 'gb' + _.uniqueId();
      match = value.match(/background-color[: ]*\s?([^;]{4,}) *;/);
      if (match == null)
        v = '';
      else
        v = match[1];
      output += '<div><input id="' + this.bg_color_id + '" name="bg_color" type="text" value="' + v +
        '"></div>';
      this.bg_image_id = 'gb' + _.uniqueId();
      match = value.match(/background-image[: ]*url\(([^\)]+)\) *;/);
      if (match == null)
        v = '';
      else
        v = decodeURI(match[1]);
      output += '<input id="' + this.bg_image_id + '" name="bg_image" class="form-control glazed-builder-image-input" type="text" value="' + v + '"></div>';
      match = value.match(/background-repeat[: ]*([-\w]*) *;/);
      if (match == null) {
        v = '';
      }
      else {
        if (match[1] == 'repeat') {
          v = match[1];
        }
        else {
          if (match[1] == 'repeat-x') {
            v = 'repeat-x';
          }
          else {
            match = value.match(/background-size[: ]*([-\w]*) *;/);
            if (match == null) {
              v = 'no-repeat';
            }
            else {
              v = match[1];
            }
          }
        }
      }
      output += '<div class="form-group"><label>' + Drupal.t('Background style') + '</label><div class="form-controls"><select name="background_style" class="form-control">';
      var background_styles = {
        '': Drupal.t("Theme defaults"),
        'cover': Drupal.t("Cover"),
        'contain': Drupal.t("Contain"),
        'no-repeat': Drupal.t("No Repeat"),
        'repeat': Drupal.t("Repeat"),
      };
      for (var key in background_styles) {
        if (key == v)
          output += '<option selected value="' + key + '">' + background_styles[key] + '</option>';
        else
          output += '<option value="' + key + '">' + background_styles[key] + '</option>';
      }
      output += '</select>';
      output += '</div>';
      output += '</div>';
      match = value.match(/background-position[: ]*([\s\w]*) *;/);
      if (match == null)
        v = '';
      else
        v = match[1];
      output += '<div class="form-group"><label>' + Drupal.t('Background position') + '</label><div class="form-controls"><select name="background_position" class="form-control">';
      var background_position = {
        '': Drupal.t("Theme defaults"),
        'center center': Drupal.t("Center  Center"),
        'left top': Drupal.t("Left Top"),
        'left center': Drupal.t("Left Center"),
        'left bottom': Drupal.t("Left Bottom"),
        'right top': Drupal.t("Right Top"),
        'right center': Drupal.t("Right Center"),
        'right bottom': Drupal.t("Right Bottom"),
        'center bottom': Drupal.t("Center Bottom"),
        'center top': Drupal.t("Center Top"),
      };
      for (var key in background_position) {
        if (key == v)
          output += '<option selected value="' + key + '">' + background_position[key] + '</option>';
        else
          output += '<option value="' + key + '">' + background_position[key] + '</option>';
      }
      output += '</select>';
      output += '</div></div>';


      match = value.match(/border-style[: ]*(\w*) *;/);
      if (match == null)
        v = '';
      else
        v = match[1];
      output += '<div class="form-group"><label>' + Drupal.t('Border style') + '</label><div class="form-controls"><select name="border_style" class="form-control">';
      var border_styles = {
        '': Drupal.t("Theme defaults"),
        'solid': Drupal.t("Solid"),
        'dotted': Drupal.t("Dotted"),
        'dashed': Drupal.t("Dashed"),
        'none': Drupal.t("None"),
      };
      for (var key in border_styles) {
        if (key == v)
          output += '<option selected value="' + key + '">' + border_styles[key] + '</option>';
        else
          output += '<option value="' + key + '">' + border_styles[key] + '</option>';
      }
      output += '</select>';
      output += '</div></div>';
      match = value.match(/font-size[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<div class="form-group"><label>' + Drupal.t('Font size') + '</label><div class="form-controls"><input name="fontsize" class="form-control bootstrap-slider" type="text" value="' + v + '"></div></div>';
      match = value.match(/border-radius[: ]*([\-\d\.]*)(px|%|em) *;/);
      if (match == null)
        v = '';
      else
        v = match[1] + match[2];
      output += '<div class="form-group"><label>' + Drupal.t('Border radius') + '</label><div class="form-controls"><input name="border_radius" class="form-control bootstrap-slider" type="text" value="' + v + '"></div></div>';
      match = value.match(/opacity[: ]*([\d\.]*) *;/);
      if (match == null)
        v = '';
      else
        v = match[1];
      output += '<div class="form-group"><label>' + Drupal.t('Opacity') +'</label><div class="form-controls"><input name="opacity" class="form-control bootstrap-slider" type="text" value="' + v + '"></div>';
      output += '</div>';
      output += '</div>';
      output += '</div>';
      this.dom_element = $(output);
    },
    opened: function() {
      image_select($(this.dom_element).find('input[name="bg_image"]'));
      colorpicker('#' + this.color_id);
      colorpicker('#' + this.border_color_id);
      colorpicker('#' + this.bg_color_id);
      initBootstrapSlider($(this.dom_element).find('input[name="opacity"]'), 0, 1, $(this.dom_element).find(
        'input[name="opacity"]').val(), 0.01);

      initBootstrapSlider($(this.dom_element).find('input[name="fontsize"]'), 0, 100, $(this.dom_element).find(
        'input[name="fontsize"]').val(), 1, true);
      initBootstrapSlider($(this.dom_element).find('input[name="border_radius"]'), 0, 100, $(this.dom_element).find(
        'input[name="border_radius"]').val(), 1);
    },
  },

  {
    type: 'autocomplete_link',
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    opened: function() {
      var linkInput = $('input[name="' + this.param_name + '"]');

      linkInput.autocomplete({
        source: function( request, response ) {
          glazed_builder_get_node_links(request, function(data) {
            response(data);
          });
        },
        select: function(event, ui) {
          event.preventDefault();
          linkInput.val(ui.item.id);
        }
      });
    },
    render: function(value) {
      var required = this.required ? 'required' : '';
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '" ' + required + '></div><p class="help-block">' + this.description +
        '</p></div>');
    }
  },

  {
    type: 'textarea',
    safe: false,
    get_value: function() {
      // Return data.
      return CKEDITOR.instances[this.id].getData();
    },
    render: function(value) {
      this.id = 'gb' + _.uniqueId();
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><textarea id="' + this.id + '" rows="10" cols="45" name="' + this.param_name + '" ">' +
        value + '</textarea></div><p class="help-block">' + this.description + '</p></div>');
    },
    opened: function() {
      var param = this;
      if ('glazed_ckeditor' in window) {
        window.glazed_ckeditor($(this.dom_element).find('#' + param.id));
      }
      else {
        function ckeditor_add_editor() {

          // Don't add spaces to empty blocks
          CKEDITOR.config.fillEmptyBlocks = false;
          // Disabling content filtering.
          CKEDITOR.config.allowedContent = true;
          // Prevent wrapping inline content in paragraphs
          CKEDITOR.config.autoParagraph = false;

          // Theme integration
          CKEDITOR.config.contentsCss = ['//cdn.jsdelivr.net/bootstrap/3.3.7/css/bootstrap.min.css'];
          if (typeof window.drupalSettings != "undefined"
            && typeof window.drupalSettings.basePath != "undefined"
            && typeof window.drupalSettings.glazed != "undefined"
            && typeof window.drupalSettings.glazed.glazedPath.length != "undefined") {
            CKEDITOR.config.contentsCss.push(drupalSettings.basePath + window.drupalSettings.glazed.glazedPath +
              'css/glazed.css');
          }

          // Styles dropdown
          CKEDITOR.config.stylesSet = [{
            name: 'Lead',
            element: 'p',
            attributes: {
              'class': 'lead'
            }
          }, {
            name: 'Muted',
            element: 'p',
            attributes: {
              'class': 'text-muted'
            }
          }, {
            name: 'Highlighted',
            element: 'mark'
          }, {
            name: 'Small',
            element: 'small'
          }, ];

          var palette = [];
          for (var name in drupalSettings.glazedBuilder.palette) {
            // VB => filter out duplicate colors
            if(!palette.includes(drupalSettings.glazedBuilder.palette[name].substring(1))) {
              palette.push(drupalSettings.glazedBuilder.palette[name].substring(1));
            }
          }

          // Only once apply this settings
          var palletsString = palette.join(',') + ',';
          if ((CKEDITOR.config.hasOwnProperty('colorButton_colors')) && (CKEDITOR.config.colorButton_colors.indexOf(palletsString)) < 0) {
            CKEDITOR.config.colorButton_colors = palletsString + CKEDITOR.config.colorButton_colors;
          }

          // Added config toolbar
          CKEDITOR.config.toolbar = [{
            name: 'basicstyles',
            items: ['Bold', 'Italic', 'Underline', 'Strike', 'Superscript', 'Subscript', 'RemoveFormat']
          }, {
            name: 'paragraph',
            items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'BulletedList',
              'NumberedList', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv'
            ]
          }, {
            name: 'clipboard',
            items: ['Undo', 'Redo', 'PasteText', 'PasteFromWord']
          }, {
            name: 'links',
            items: ['Link', 'Unlink']
          }, {
            name: 'insert',
            items: ['Image', 'HorizontalRule', 'SpecialChar', 'Table', 'Templates']
          }, {
            name: 'colors',
            items: ['TextColor']
          }, {
            name: 'document',
            items: ['Source']
          }, {
            name: 'tools',
            items: ['ShowBlocks', 'Maximize']
          }, {
            name: 'styles',
            items: ['Format', 'Styles', 'FontSize']
          }, {
            name: 'editing',
            items: ['Scayt']
          }, ];

          CKEDITOR.config.fontSize_sizes = '8/8px;9/9px;10/10px;11/11px;12/12px;14/14px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;36/36px;48/48px;60/60px;72/72px;90/90px;117/117px;144/144px';

          // Don't move about our Glazed Builder stylesheet link tags
          CKEDITOR.config.protectedSource.push(/<link.*?>/gi);

          CKEDITOR.replace(param.id);
        }
        if ('CKEDITOR' in window) {
          ckeditor_add_editor();
        }
        else {
          glazed_add_js({
            path: 'vendor/ckeditor/ckeditor.js',
            callback: function() {
              if (_.isObject(CKEDITOR)) {
                ckeditor_add_editor();
              }
            }
          });
        }
      }
    },
    closed: function() {
      // Destroy ckeditor.
      CKEDITOR.instances[this.id].destroy();
    }
  },

  {
    type: 'textfield',
    get_value: function() {
      return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
    },
    render: function(value) {
      var required = this.required ? 'required' : '';
      this.dom_element = $('<div class="form-group form-group--' + this.param_name + '"><label>' + this.heading +
        '</label><div><input class="form-control" name="' + this.param_name +
        '" type="text" value="' + value + '" ' + required + '></div><p class="help-block">' + this.description +
        '</p></div>');
    }
  },
  // {
  //   type: 'hidden',
  //   get_value: function() {
  //     return $(this.dom_element).find('input[name="' + this.param_name + '"]').val();
  //   },
  //   render: function(value) {
  //     this.dom_element = $('<input class="form-control" name="' + this.param_name + '" type="hidden" value="' + value + '">');
  //   }
  // },

];

if ('glazed_param_types' in window) {
  window.glazed_param_types = window.glazed_param_types.concat(glazed_param_types);
}
else {
  window.glazed_param_types = glazed_param_types;
}

})(window.jQuery);
